import React, {useMemo, useState} from "react";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import {useTheme} from "@mui/material/styles";
import {useAppSelector} from "src/state/hooks";
import TablePagination from "src/components/TablePagination";
import {CarDealerSeller} from "src/types";
import HomeBottomSalesTable, {TableSellerEntry} from "./HomeBottomSalesTable";
import HomeBottomTablePicker from "./HomeBottomTablePicker";
import {usePrecalculatedSales} from "../../../SalesFunnel/utils/usePrecalculatedSales";
import {
  useGetCarDealerSellersQuery,
  useGetTopSellersQuery,
  useGetCarDealerSalesNewQuery,
  useGetCarDealerSalesUsedQuery,
  useGetCarDealerQuery,
} from 'src/state/reducers/carDealerApi'

const HomeBottomTable = () => {
  const [date, setDate] = useState(new Date());

  const {data:topSellers} = useGetTopSellersQuery();
  const {data:sellersAll} = useGetCarDealerSellersQuery();

  const {data:salesNew} = useGetCarDealerSalesNewQuery();
  const {data:salesUsed} = useGetCarDealerSalesUsedQuery();

  const sellers = useMemo(() => {
    return (sellersAll || []).filter((el: CarDealerSeller) =>
      el.authorities?.includes("ROLE_SELLER_NEW"),
    );
  }, [sellersAll]);

  const sales = useMemo(() => {
    return [...(salesNew?.content || []), ...(salesUsed?.content || [])];
  }, [salesNew, salesUsed]);

  const precalculatedSales = usePrecalculatedSales(
    sellers,
    sales,
    topSellers,
    date.getMonth() + 1,
  );

  const theme = useTheme();

  const tableSellers: TableSellerEntry[] = useMemo(() => {
    return (
      precalculatedSales.sellersList?.map((sellerListEntry) => ({
        name: sellerListEntry.seller.fullName,
        sold: sellerListEntry.cars?.length || 0,
        goal: +(
          sellerListEntry.seller.goals
            .filter(
              (goal) =>
                parseInt(goal.month, 10) === date.getMonth() + 1 &&
                parseInt(goal.year, 10) === date.getFullYear(),
            )
            .pop()?.goal || "0"
        ),
      })) || []
    );
  }, [precalculatedSales, date]);

  const [page, setPage] = useState(0);
  const count = tableSellers?.length || 0;
  const [rowsPerPage] = useState(4);

  const visibleTopSellers = useMemo(() => {
    return tableSellers?.slice(
      page * rowsPerPage,
      page * rowsPerPage + rowsPerPage,
    );
  }, [page, rowsPerPage, tableSellers]);

  return (
    <Box
      sx={{
        background: "#FFFFFF",
        boxShadow: "0px 4px 20px rgba(20, 32, 100, 0.08)",
        borderRadius: "14px",
        padding: "32px 30px",
      }}
      style={{
        paddingTop: "20px !important",
        height: "100%",
      }}
    >
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          mb: "30px",
          [theme.breakpoints.down("sm")]: {
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          },
        }}
      >
        <Typography
          component="div"
          sx={{
            fontStyle: "normal",
            fontWeight: 600,
            fontSize: "18px",
            lineHeight: "22.32px",
            color: "#222238",
            [theme.breakpoints.down("sm")]: {
              marginTop: 20,
            },
          }}
        >
          Best Sales Performers
        </Typography>
        <Box
          sx={{
            [theme.breakpoints.down("sm")]: {
              marginTop: 20,
            },
          }}
        >
          <HomeBottomTablePicker
            forceRangeType="month"
            date={date}
            onDateChanged={setDate}
          />
        </Box>
      </Box>
      <HomeBottomSalesTable sellers={visibleTopSellers} />
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          mt: "30px",
        }}
      >
        <TablePagination
          nopadding
          noselect
          noshadow
          count={Math.ceil(count / rowsPerPage)}
          page={page}
          rowsPerPage={rowsPerPage}
          rowsPerPageOptions={[4, 8, 12, 18, 40]}
          onChangePage={(_, page) => setPage(page)}
        />
      </Box>
    </Box>
  );
};

export default HomeBottomTable;
