import {useCallback} from "react";
import {useNavigate} from "react-router-dom";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import {styled} from "@mui/material/styles";

export type TableSellerEntry = {
  name: string;
  sold: number;
  goal: number;
};

const HomeBottomSalesTable = ({sellers}: {sellers: TableSellerEntry[]}) => {
  const navigate = useNavigate();
  const onNavigateToSalesPerformance = useCallback(() => {
    navigate("/sales-performance");
  }, [navigate]);

  return (
    <Box
      sx={{
        background: "#F6F8FA",
      }}
    >
      <TableRow onClick={onNavigateToSalesPerformance}>
        <Grid container>
          <Grid item xs={8}>
            <TableTitle>Sellers</TableTitle>
          </Grid>
          <Grid item xs={2}>
            <TableTitle>Sold</TableTitle>
          </Grid>
          <Grid item xs={2}>
            <TableTitle>Goal</TableTitle>
          </Grid>
        </Grid>
      </TableRow>
      {sellers.map((salesPerson, idx) => (
        <TableRow key={idx} onClick={onNavigateToSalesPerformance}>
          <Grid container>
            <Grid item xs={8}>
              <TableEntry>{salesPerson.name}</TableEntry>
            </Grid>
            <Grid item xs={2}>
              <TableEntry>{salesPerson.sold}</TableEntry>
            </Grid>
            <Grid item xs={2}>
              <TableEntry>{salesPerson.goal}</TableEntry>
            </Grid>
          </Grid>
        </TableRow>
      ))}
    </Box>
  );
};

const TableRow = styled(Box)({
  cursor: "pointer",
  padding: "21px 24px",
  borderBottom: "2px solid white",
});

const TableTitle = styled(Box)({
  fontStyle: "normal",
  fontWeight: 600,
  fontSize: "13px",
  lineHeight: "16.9px",
  letterSpacing: "0.5px",
  textTransform: "uppercase",
  color: "#98A2B7",
});

const TableEntry = styled(Box)({
  fontStyle: "normal",
  fontWeight: 600,
  fontSize: "16px",
  lineHeight: "160%",
  color: "#222238",
});

export default HomeBottomSalesTable;
