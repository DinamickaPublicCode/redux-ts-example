export enum CreateDealTabs {
  GENERAL = "GENERAL",
  SALES = "SALES",
  PREPARATION_REQUEST = "PREPARATION REQUEST",
  SERVICE = "SERVICE",
  DELIVERY = "DELIVERY",
  FINANCE = "FINANCE",
}

export type CreateDealTab =
  | CreateDealTabs.GENERAL
  | CreateDealTabs.SALES
  | CreateDealTabs.PREPARATION_REQUEST
  | CreateDealTabs.SERVICE
  | CreateDealTabs.DELIVERY
  | CreateDealTabs.FINANCE;

export const createDealTabs: CreateDealTab[] = [
  CreateDealTabs.GENERAL,
  CreateDealTabs.SALES,
  CreateDealTabs.PREPARATION_REQUEST,
  CreateDealTabs.SERVICE,
  CreateDealTabs.DELIVERY,
  CreateDealTabs.FINANCE,
];
