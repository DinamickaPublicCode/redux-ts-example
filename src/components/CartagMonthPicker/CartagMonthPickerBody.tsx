import {useState, useMemo, useCallback, useEffect, useRef} from "react";
import dayjs from "dayjs";
import CartagMonthPickerView from "./CartagMonthPickerView";
import range from "lodash/range";

type CustomMonthPickerRangeType = "year" | "month";

const CartagMonthPickerBody = ({
  date: initialDate,
  onChange,
  noswitch,
  immediate,
  rangeType: initialRangeType,
  setRangeType: onRangeTypeChange,
  onApplyButtonPressed,
}: {
  onApplyButtonPressed?: () => void;
  date: Date;
  onChange: (date: Date) => void;
  noswitch?: boolean;
  immediate?: boolean;
  rangeType: CustomMonthPickerRangeType;
  setRangeType: (rangeType: CustomMonthPickerRangeType) => void;
}) => {
  const [date, setDate] = useState(initialDate);
  const [rangeType, setRangeType] = useState(initialRangeType);

  const onMountRef = useRef(0);
  useEffect(() => {
    /**
     * This side effect is used for the immediate date change
     */
    if (!immediate) {
      return;
    }
    /**
     * ref for bypassing the initial types update
     */
    if (onMountRef.current === 0) {
      onMountRef.current += 1;
      return;
    }
    onChange(date);
  }, [date, immediate, onChange]);

  /**
   * Moment is used, since it's quite simple to handle month operations on date.
   */
  const currentMomentDate = useMemo(() => dayjs(date), [date]);

  /**
   * Calcualte the range start/end like it was shown in the design template.
   * Subtract 10 years since we're interested in selecting past years
   */
  const getSliceStart = useCallback(() => {
    const year = currentMomentDate.year();
    let sliceStart = Math.floor(year / 10) * 10;
    if (+(year.toString().split("").pop() || "0") < 2) {
      console.log("sub");
      sliceStart -= 10;
    }
    return sliceStart;
  }, [currentMomentDate]);

  const sliceTitle = useMemo(() => {
    if (rangeType === "year") {
      const sliceStart = getSliceStart();
      return `${sliceStart} - ${sliceStart + 11}`;
    } else {
      return "" + currentMomentDate.year();
    }
  }, [currentMomentDate, rangeType, getSliceStart]);

  const onChangeRange = useCallback(
    (type: "positive" | "negative") => {
      /**
       * If we have the year range type, add/subtract 10 years
       * otherwise 1 year.
       */
      const newMomentDate = dayjs(currentMomentDate.toDate());
      if (type === "negative") {
        setDate(
          newMomentDate
            .subtract(rangeType === "year" ? 10 : 1, "year")
            .toDate(),
        );
      } else {
        setDate(
          newMomentDate.add(rangeType === "year" ? 10 : 1, "year").toDate(),
        );
      }
    },
    [rangeType, currentMomentDate, setDate],
  );

  /**
   * That's quite suboptimal for now since it will run on each date change
   */
  const itemTitles = useMemo(() => {
    if (rangeType === "month") {
      return dayjs.monthsShort();
    } else {
      const sliceStart = getSliceStart();
      return range(sliceStart, sliceStart + 12).map((x) => x.toString());
    }
  }, [rangeType, getSliceStart]);

  const onIndexChange = useCallback(
    (index: number) => {
      const newMomentDate = dayjs(currentMomentDate.toDate());
      if (rangeType === "month") {
        setDate(newMomentDate.set("month", index).toDate());
      } else {
        const sliceStart = getSliceStart();
        setDate(newMomentDate.set("year", sliceStart + index).toDate());
      }
    },
    [currentMomentDate, getSliceStart, rangeType, setDate],
  );

  const currentIndex = useMemo(() => {
    if (rangeType === "month") {
      return currentMomentDate.month();
    } else {
      const sliceStart = getSliceStart();
      return currentMomentDate.year() - sliceStart;
    }
  }, [currentMomentDate, getSliceStart, rangeType]);

  return (
    <CartagMonthPickerView
      onChangeRange={onChangeRange}
      onRangeTypeChange={onRangeTypeChange}
      immediate={immediate}
      rangeType={rangeType}
      setRangeType={setRangeType}
      noswitch={noswitch}
      sliceTitle={sliceTitle}
      currentIndex={currentIndex}
      itemTitles={itemTitles}
      onIndexChange={onIndexChange}
      onChange={onChange}
      date={date}
      onApplyButtonPressed={onApplyButtonPressed}
    />
  );
};

export default CartagMonthPickerBody;
