import React from "react";
import {useSnackbar} from "src/thirdparty/_notistack";
import makeStyles from "@mui/styles/makeStyles";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import CloseIcon from "@mui/icons-material/Close";
import {Theme} from "@mui/material/styles";

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    flexGrow: 1,
    minWidth: "500px !important",
  },
  snackWrapper: {
    display: "flex",
    alignItems: "center",
    borderRadius: 10,
    padding: "9px 25px",
    backgroundColor: theme.palette.secondary.main,
    boxShadow: "0px 3px 25px rgba(116, 173, 109, 0.3)",
    width: "100%",
  },
  typography: {
    flex: 1,
    fontWeight: 600,
    color: "#FFFFFF",
  },
}));

const useCustomErrorSnackbar = () => {
  const classes = useStyles();
  const {enqueueSnackbar, closeSnackbar} = useSnackbar();

  return (msg: string) =>
    enqueueSnackbar(msg, {
      preventDuplicate: true,
      variant: "error",
      anchorOrigin: {
        vertical: "bottom",
        horizontal: "center",
      },
      content: (key, message) => {
        return (
          <div className={classes.root}>
            <div className={classes.snackWrapper}>
              <CloseIcon
                htmlColor={"#fff"}
                style={{
                  marginRight: 20,
                }}
              />
              <Typography className={classes.typography}>{message}</Typography>
              <Box>
                <Button
                  style={{
                    fontWeight: 600,
                    fontSize: 14,
                    textTransform: "uppercase",
                    color: "#222238",
                    backgroundColor: "#FFFFFF",
                    borderRadius: 10,
                    padding: "7px 22px",
                  }}
                  onClick={() => closeSnackbar(key)}
                >
                  Dismiss
                </Button>
              </Box>
            </div>
          </div>
        );
      },
    });
};

export default useCustomErrorSnackbar;
