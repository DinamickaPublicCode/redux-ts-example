import {StatisticsData} from "src/types";

const getDetail = (
  inventory: StatisticsData | undefined,
  typeAction?: string,
) => {
  if (typeAction) {
    return inventory?.details
      .filter((detail) => detail.typeAction === typeAction)
      .pop();
  } else {
    if (!inventory?.details?.length) {
      return undefined;
    }
    return inventory?.details[0];
  }
};

export default getDetail;
