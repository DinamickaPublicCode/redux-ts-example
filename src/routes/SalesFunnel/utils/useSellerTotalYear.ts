import {CarDealerSeller, TopSeller} from "../../../types";
import {useMemo} from "react";

/**
 * Total year entry comes from another endpoint. So it's necessary to map sellers onto top sellers
 * by id
 *
 * @param topSellers
 * @param seller
 */
export const useSellerTotalYear = (
  topSellers: TopSeller[] | undefined,
  seller?: CarDealerSeller,
) => {
  return useMemo(() => {
    if (!seller) {
      return 0;
    }
    const totalYear = topSellers
      ?.filter((topSeller) => topSeller.id === seller.id)
      .pop()?.totalYear;
    if (!totalYear) {
      return 0;
    }
    return +totalYear;
  }, [topSellers, seller]);
};
