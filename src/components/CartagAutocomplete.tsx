import React from "react";
import Autocomplete from "@mui/material/Autocomplete";
import Box from "@mui/material/Box";
import CircularProgress from "@mui/material/CircularProgress";
import Typography from "@mui/material/Typography";
import ListItem from "@mui/material/ListItem";
import CartagInputField from "./CartagInputField";
import SearchIcon from "@mui/icons-material/Search";
import {useCommonStyles} from "../utils/commonStyles";
import {FinanceTabType} from "src/types";

interface CartagAutocompleteProps {
  options: FinanceTabType[];
  inputValue: string;
  stockValue: FinanceTabType | null;
  disableLabel?: boolean;
  labelText?: string;
  name?: string;
  onValueChange: (value: string) => void;
  onStockChange: (value: FinanceTabType | null) => void;
  loading: boolean;
}

const CartagAutocomplete: React.FC<CartagAutocompleteProps> = ({
  options,
  inputValue,
  stockValue,
  disableLabel,
  labelText,
  name,
  onValueChange,
  onStockChange,
  loading,
}) => {
  const classes = useCommonStyles();

  return (
    <Box sx={{width: "100%"}}>
      {!disableLabel && (
        <Typography className={classes.inputLabel}>{labelText}</Typography>
      )}
      <Autocomplete
        getOptionLabel={(option) =>
          typeof option === "string" ? option : option.referenceStock
        }
        isOptionEqualToValue={(option, value) => option.id === value.id}
        filterOptions={(x) => x}
        options={options}
        loading={loading}
        inputValue={inputValue}
        value={stockValue}
        onChange={(event, value) =>
          onStockChange(value as FinanceTabType | null)
        }
        freeSolo
        autoComplete
        openOnFocus={false}
        renderInput={(params) => {
          return (
            <CartagInputField
              disableLabel
              {...params}
              InputProps={{
                ...params.InputProps,
                endAdornment: (
                  <>
                    {loading ? (
                      <CircularProgress color="primary" size={20} />
                    ) : (
                      <SearchIcon htmlColor="#1A06F9" />
                    )}
                  </>
                ),
              }}
              name={name}
              onChange={(event) => onValueChange(event.target.value)}
            />
          );
        }}
        renderOption={(props, option, state) => {
          return (
            <ListItem {...props} selected={state.selected}>
              {option.referenceStock}
            </ListItem>
          );
        }}
      />
    </Box>
  );
};

export default CartagAutocomplete;
