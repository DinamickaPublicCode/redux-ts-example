import {useContext, createContext} from "react";

type StockServiceTabContextData = {
  onChecked: (
    departmentType: string,
    stepType: string,
    idAction: string,
    checked: boolean,
  ) => void;
  onWorkOrderInfoChanged: (
    departmentType: string,
    stepType: string,
    workOrderValue: string,
  ) => void;
  getActionValue: (
    departmentType: string,
    stepType: string,
    idAction: string,
  ) => boolean;
  getWorkOrderValue: (departmentType: string, stepType: string) => string;
};

export const StockServiceTabContext =
  createContext<null | StockServiceTabContextData>(null);

export const useStockServiceTabContext = () =>
  useContext(StockServiceTabContext) as StockServiceTabContextData;
