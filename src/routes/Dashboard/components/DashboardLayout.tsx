import {useState} from "react";
import ArrowDropDown from "@mui/icons-material/ArrowDropDown";
import {useTranslation} from "react-i18next";
import CreateDealDialog from "src/components/CreateDealDialog";
import CreateInventoryDialog from "src/components/CreateInventoryDialog";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import {styled, useTheme} from "@mui/material/styles";
import Typography from "@mui/material/Typography";
import DashboardTabs from "src/routes/Dashboard/components/DashboardTabs";
import Header from "src/components/Header";
import suspended from "src/utils/suspended";
import CircularProgress from "@mui/material/CircularProgress";

const DashboardTable = suspended(() => import("./DashboardTable"), {
  fallback: (
    <Box
      sx={{width: "100%"}}
      display="flex"
      justifyContent="center"
      alignItems="center"
      height="100px">
      <CircularProgress />
    </Box>
  ),
});

const DashboardLayout = () => {
  const {t} = useTranslation();

  const [openCreateDeal, setOpenCreateDeal] = useState(false);
  const [openCreateInventory, setOpenCreateInventory] = useState(false);
  const theme = useTheme();
  return (
    <>
      <Header />
      <Container maxWidth="xl">
        <Grid container spacing={2}>
          <Grid
            item
            xs={12}
            sx={{
              margin: theme.spacing(3, 0),
              display: "flex",
              justifyContent: "space-between",
              [theme.breakpoints.down("sm")]: {
                flexDirection: "column",
              },
            }}>
            <Typography
              sx={{
                [theme.breakpoints.down("sm")]: {
                  textAlign: "center",
                },
              }}
              variant="h4"
              style={{
                fontWeight: 700,
                fontSize: "28px",
              }}>
              {t("Navigation.Dashboard")}
            </Typography>
            <Box
              sx={{
                [theme.breakpoints.down("sm")]: {
                  marginTop: "30px",
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  marginRight: "4%",
                },
              }}>
              <TitleButton
                sx={{
                  backgroundColor: "#fff",
                  boxShadow: "0px 4px 20px rgba(203, 205, 217, 0.3)",
                  [theme.breakpoints.down("sm")]: {
                    width: "100%",
                  },
                }}
                endIcon={<ArrowDropDown />}
                onClick={() => setOpenCreateInventory(true)}>
                {t("buttonNames.AddInventory")}
              </TitleButton>
              <TitleButton
                variant="contained"
                sx={{
                  boxShadow: "0px 3px 14px rgba(2, 53, 236, 0.3)",
                  [theme.breakpoints.down("sm")]: {
                    width: "100%",
                    marginTop: 10,
                  },
                }}
                onClick={() => setOpenCreateDeal(true)}>
                {t("buttonNames.CreateDeal")}
              </TitleButton>
            </Box>
          </Grid>
          <Grid item xs={12}>
            <DashboardTabs>
              <DashboardTable />
            </DashboardTabs>
          </Grid>
        </Grid>
        <CreateDealDialog
          open={openCreateDeal}
          handleClose={() => setOpenCreateDeal(false)}
        />
        <CreateInventoryDialog
          open={openCreateInventory}
          handleClose={() => setOpenCreateInventory(false)}
        />
      </Container>
    </>
  );
};

const TitleButton = styled(Button)(({theme}) => ({
  marginLeft: theme.spacing(3),
  padding: "8px 20px",
  textTransform: "uppercase",
  minWidth: 164,
}));

export { TitleButton }

export default DashboardLayout;
