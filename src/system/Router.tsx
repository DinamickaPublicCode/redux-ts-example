import {BrowserRouter, Route, Routes} from "react-router-dom";
import RequireAuth from "src/components/RequireAuth";
import LogIn from "src/routes/LogIn";
import suspended, {SuspendedOptions} from "src/utils/suspended";
import DashboardBackdrop from "../components/DashboardBackdrop";
import {Suspense} from "react";

const suspendedOptions: SuspendedOptions = {
  nosuspense: false,
  fallback: <DashboardBackdrop />,
};

/**
 * Code-split pages and use supsense with loader to show the appropriate page
 */
const Home = suspended(() => import("src/routes/Home"), suspendedOptions);
const Dashboard = suspended(
  () => import("src/routes/Dashboard"),
  suspendedOptions,
);
const SalesFunnel = suspended(
  () => import("src/routes/SalesFunnel"),
  suspendedOptions,
);
const SalesCalendar = suspended(
  () => import("src/routes/SalesCalendar"),
  suspendedOptions,
);
const Settings = suspended(
  () => import("src/routes/Settings"),
  suspendedOptions,
);
const NotificationsList = suspended(
  () => import("src/routes/NotificationsList"),
  suspendedOptions,
);

const Router = () => (
  <BrowserRouter>
    <Suspense fallback={<DashboardBackdrop />}>
      {/** This will show a dashboard when the page is not ready */}
      <Routes>
        <Route path="/login" element={<LogIn />} />
        <Route
          path="/"
          element={
            <RequireAuth>
              <Home />
            </RequireAuth>
          }
        />
        <Route
          path="/dashboard"
          element={
            <RequireAuth>
              <Dashboard />
            </RequireAuth>
          }
        />
        <Route
          path="/sales-performance"
          element={
            <RequireAuth>
              <SalesFunnel />
            </RequireAuth>
          }
        />
        <Route
          path="/sales-calendar"
          element={
            <RequireAuth>
              <SalesCalendar />
            </RequireAuth>
          }
        />
        <Route
          path="/settings/*"
          element={
            <RequireAuth>
              <Settings />
            </RequireAuth>
          }
        />
        <Route
          path="/notifications-list"
          element={
            <RequireAuth>
              <NotificationsList />
            </RequireAuth>
          }
        />
      </Routes>
    </Suspense>
  </BrowserRouter>
);

export default Router;
