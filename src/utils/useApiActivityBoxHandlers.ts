import {useState, useRef, useEffect, useCallback} from "react";
import {getActivityMessages} from "src/state/api";
import {FinanceTabType} from "src/types";
import {useAppSelector} from "src/state/hooks";
import {requestGeneratedAvatar} from "src/utils/avatarGenerator";
import {
  ActivityBoxApiMessage,
  ActivityBoxBaseContextProps,
  ActivityBoxMessageType,
  TabDefinition,
} from "src/types/activityBoxInterfaces";
import {ACTIVITY_BOX_TRANSLATION_KEY} from "src/system/constants";
import {useGetUserQuery} from 'src/state/reducers/userApi';

export const useApiActivityBoxHandlers = (
  currentDeal?: FinanceTabType | null,
) => {
  const [activityMessages, setActivityMessages] = useState(
    [] as ActivityBoxApiMessage[],
  );

  const idInventory = currentDeal?.id;

  const {data: user} = useGetUserQuery();;

  const offset = useRef(0);
  useEffect(() => {
    if (!currentDeal) {
      offset.current = 0;
    }
  }, [currentDeal]);

  const OFFSET_INCREMENT = 50;

  const loadMore = useCallback(async () => {
    const o = offset.current;
    if (idInventory === undefined) {
      return;
    }

    const response = await getActivityMessages(
      idInventory,
      1 + o / OFFSET_INCREMENT,
      OFFSET_INCREMENT,
    );

    if (!response.data || !Array.isArray(response.data)) {
      console.error("data is not valid", {response});
      return;
    }

    const messages = response.data as ActivityBoxApiMessage[];
    setActivityMessages((activityMessages) => [
      ...activityMessages,
      ...messages,
    ]);
    offset.current += OFFSET_INCREMENT;
  }, [idInventory]);

  useEffect(() => {
    // On each change of id, the chat needs to be cleaned up
    setActivityMessages([]);
    (async () => {
      // await initCommentsTestStore();
      await loadMore();
    })();
  }, [idInventory, loadMore]);

  const onRequestUserAvatar = useCallback(
    (
      userId: string,
      userName: string,
      onAvatarUrlSet: (arg0: string) => void,
    ) => {
      onAvatarUrlSet(requestGeneratedAvatar(userId, userName));
    },
    [],
  );

  const onRequestMoreMessages = useCallback(
    (onLoadingStateChange: (x: boolean) => void) => {
      onLoadingStateChange(true);
      setTimeout(() => {
        loadMore();
        onLoadingStateChange(false);
      }, 1500);
    },
    [loadMore],
  );

  const baseContextProps = {
    tabs: baseActivityBoxTabs,
    onRequestUserAvatar,
    onMessageSend: async (type, message, onLoadingStateChange) => {
      onLoadingStateChange(true);
      const idUser = user?.id || "idUser1";
      const userName = user?.fullName || "First Name LastName";

      // TODO: fetch the correct id
      try {
        // await sendActivityEvent(outRequest);
      } catch (e) {
        console.error(e);
      }

      setActivityMessages([
        ...activityMessages,
        {
          date: new Date().toISOString(),
          labelDate: "now",
          idUser,
          userName,
          id: "" + Math.floor(Math.random() * 1000),
          message,
          title: "",
          type,
          idInventory: "",
        },
      ]);
      onLoadingStateChange(false);
    },
    onRequestMoreMessages,
  } as ActivityBoxBaseContextProps;

  return {
    apiMessages: activityMessages,
    ...baseContextProps,
  };
};

export const baseActivityBoxTabs = [
  {
    localeId: `${ACTIVITY_BOX_TRANSLATION_KEY}.TAB_ITEMS.ALL`,
    filter: (_) => true,
    sendMessageType: ActivityBoxMessageType.CHAT,
  },
  {
    localeId: `${ACTIVITY_BOX_TRANSLATION_KEY}.TAB_ITEMS.CHAT`,
    filter: (a) => a.type === ActivityBoxMessageType.CHAT,
    sendMessageType: ActivityBoxMessageType.CHAT,
  },
  {
    localeId: `${ACTIVITY_BOX_TRANSLATION_KEY}.TAB_ITEMS.NOTE`,
    filter: (a) => a.type === ActivityBoxMessageType.NOTE,
    sendMessageType: ActivityBoxMessageType.CHAT,
  },
  {
    localeId: `${ACTIVITY_BOX_TRANSLATION_KEY}.TAB_ITEMS.ACTION`,
    filter: (a) => a.type === ActivityBoxMessageType.ACTION,
    sendMessageType: null,
  },
] as TabDefinition[];
