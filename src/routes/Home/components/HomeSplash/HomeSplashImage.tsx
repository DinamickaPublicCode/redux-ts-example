import usePublicUrl from "../../../../utils/usePublicUrl";
import Box from "@mui/material/Box";
import HomeSplashImageSvg from "./HomeSplashImageSvg";
import React from "react";

const HomeSplashImage = () => {
  const publicUrl = usePublicUrl();

  return (
    <Box
      sx={{
        overflow: "hidden",
        width: 710,
        position: "relative",
      }}
    >
      <Box
        style={{
          top: 80,
          left: 222,
          background: `url(${publicUrl("images/home/image_0.png")})`,
          backgroundSize: 344,
          backgroundPosition: "-130px -160px",
        }}
        sx={splashImageStyle}
      />
      <Box
        style={{
          top: 270,
          left: 20,
          background: `url(${publicUrl("images/home/image_1.png")})`,
          backgroundSize: "567px",
          backgroundPosition: "-320px -110px",
        }}
        sx={splashImageStyle}
      />
      <Box
        sx={{
          position: "absolute",
          top: 0,
        }}
      />

      <Box sx={{ml: "-730px"}}>
        <HomeSplashImageSvg />
      </Box>
    </Box>
  );
};

const splashImageStyle = {
  position: "absolute",
  width: 234,
  height: 234,
  transform: "scale(-1, 1)",
  borderRadius: "50%",
  border: "10px solid white",
};

export default HomeSplashImage;
