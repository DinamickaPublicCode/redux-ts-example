import React from "react";

const CameraIcon = (color: string) => (
  <svg width={86} height={86} viewBox="0 0 86 86" fill="none">
    <g filter="url(#prefix__filter0_d)">
      <rect x={20} y={18} width={46} height={46} rx={23} fill="#fff" />
      <rect x={20.5} y={18.5} width={45} height={45} rx={22.5} stroke={color} />
    </g>
    <path
      d="M43.498 44.992a3.006 3.006 0 100-6.012 3.006 3.006 0 000 6.012zM47.89 34.377l-.193-1.095c-.104-.519-.563-1.52-1.092-1.52h-6.212c-.529 0-.988 1.001-1.092 1.52l-.192 1.095h8.78z"
      fill="#fff"
    />
    <path
      d="M51.33 35.488H35.67c-.92 0-1.67.75-1.67 1.67v9.649c0 .92.75 1.67 1.67 1.67h15.66c.92 0 1.67-.75 1.67-1.67v-9.649c0-.92-.75-1.67-1.67-1.67zm-12.58 3.489h-1.781a.557.557 0 010-1.114h1.781a.557.557 0 010 1.114zm4.75 7.125a4.124 4.124 0 01-4.12-4.12 4.124 4.124 0 014.12-4.119 4.124 4.124 0 014.12 4.12 4.124 4.124 0 01-4.12 4.119z"
      fill={color}
    />
    <defs>
      <filter
        id="prefix__filter0_d"
        x={0}
        y={0}
        width={86}
        height={86}
        filterUnits="userSpaceOnUse"
        colorInterpolationFilters="sRGB"
      >
        <feFlood floodOpacity={0} result="BackgroundImageFix" />
        <feColorMatrix
          in="SourceAlpha"
          values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
        />
        <feOffset dy={2} />
        <feGaussianBlur stdDeviation={10} />
        <feColorMatrix values="0 0 0 0 0.796875 0 0 0 0 0.804464 0 0 0 0 0.85 0 0 0 0.5 0" />
        <feBlend in2="BackgroundImageFix" result="effect1_dropShadow" />
        <feBlend in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
      </filter>
    </defs>
  </svg>
);

export default CameraIcon;
