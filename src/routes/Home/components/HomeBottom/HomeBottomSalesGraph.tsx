import React, {useEffect, useState} from "react";
import {CityTemperature} from "@visx/mock-data/lib/mocks/cityTemperature";
import {
  AnimatedAreaSeries,
  AnimatedAxis,
  AnimatedGlyphSeries,
  AnimatedLineSeries,
  Tooltip,
  XYChart,
} from "@visx/xychart";
import HomeBottomSalesGraphControls from "./HomeBottomSalesGraphControls";
import {curveCardinal} from "@visx/curve";
import {LinearGradient} from "@visx/gradient";
import Typography from "@mui/material/Typography";
import dayjs from "dayjs";

export type XYChartProps = {
  width: number;
  height: number;
};

type City = "San Francisco" | "New York" | "Austin";

const cache: {
  [index: string]: {
    date: string;
    Austin: string;
    "New York": string;
    "San Francisco": string;
  }[];
} = {};

const HomeBottomSalesGraph = ({
  height,
  width,
  dataKey,
}: XYChartProps & {
  dataKey: string;
}) => {
  const [revealed, setRevealed] = useState(false);
  useEffect(() => {
    const timeout = setTimeout(() => setRevealed(true), 230);
    return () => {
      clearTimeout(timeout);
    };
  }, []);
  return (
    <HomeBottomSalesGraphControls>
      {({
        accessors,
        animationTrajectory,
        config,
        editAnnotationLabelPosition,
        renderBarGroup,
        renderGlyph,
        renderHorizontally,
        setAnnotationDataIndex,
        setAnnotationDataKey,
        sharedTooltip,
        showHorizontalCrosshair,
        showTooltip,
        showVerticalCrosshair,
        snapTooltipToDatumX,
        snapTooltipToDatumY,
        theme,
        xAxisOrientation,
        yAxisOrientation,
      }) => {
        let data = cache[dataKey];
        if (!data) {
          const IS_DENSE = true;
          const daysInMonth = dayjs(new Date(dataKey)).daysInMonth();
          const gen = () => "" + (55 + Math.random() * 25);
          data = Array(IS_DENSE ? daysInMonth : 12)
            .fill(0)
            .map((_, i) => ({
              date: ("" + (i + 1)).length === 2 ? "" + (i + 1) : "0" + (i + 1),
              Austin: gen(),
              "New York": gen(),
              "San Francisco": gen(),
            }));
          cache[dataKey] = data;
        }

        if (!revealed) {
          data = data.map((x) => ({
            ...x,
            Austin: "0",
          }));
        }
        return (
          <XYChart
            theme={theme}
            margin={{
              top: 30,
              bottom: 20,
              left: 0,
              right: 0,
            }}
            xScale={config.x}
            yScale={{
              ...config.y,
              domain: [0, 100],
            }}
            width={width}
            height={Math.min(514, height)}
            captureEvents={!editAnnotationLabelPosition}
            onPointerUp={(d) => {
              setAnnotationDataKey(
                d.key as "New York" | "San Francisco" | "Austin",
              );
              setAnnotationDataIndex(d.index);
            }}
          >
            <LinearGradient
              id="area-gradient"
              from={"#1519ef"}
              to={"#ffffff"}
              fromOpacity={0.7}
              toOpacity={0}
            />
            <AnimatedAreaSeries
              dataKey="Austin"
              data={data}
              xAccessor={accessors.x.Austin}
              yAccessor={accessors.y.Austin}
              fillOpacity={0.4}
              curve={curveCardinal}
              fill="url(#area-gradient)"
              renderLine={false}
            />
            <AnimatedLineSeries
              dataKey="Austin"
              strokeWidth={3}
              data={data}
              xAccessor={accessors.x.Austin}
              yAccessor={accessors.y.Austin}
              curve={curveCardinal}
            />
            {revealed && (
              <AnimatedGlyphSeries
                dataKey="Austin"
                data={data}
                xAccessor={accessors.x["Austin"]}
                yAccessor={accessors.y["Austin"]}
                renderGlyph={renderGlyph}
              />
            )}
            <AnimatedAxis
              key={`time-axis-${animationTrajectory}-${renderHorizontally}`}
              orientation={
                renderHorizontally ? yAxisOrientation : xAxisOrientation
              }
              numTicks={data.length}
              stroke={"white"}
              animationTrajectory={
                animationTrajectory as
                  | "max"
                  | "min"
                  | "center"
                  | "outside"
                  | undefined
              }
            />
            {showTooltip && (
              <Tooltip<CityTemperature>
                showHorizontalCrosshair={showHorizontalCrosshair}
                showVerticalCrosshair={showVerticalCrosshair}
                snapTooltipToDatumX={snapTooltipToDatumX}
                snapTooltipToDatumY={snapTooltipToDatumY}
                showDatumGlyph={
                  (snapTooltipToDatumX || snapTooltipToDatumY) &&
                  !renderBarGroup
                }
                showSeriesGlyphs={sharedTooltip && !renderBarGroup}
                renderTooltip={({tooltipData}) => (
                  <>
                    {(
                      (sharedTooltip
                        ? Object.keys(tooltipData?.datumByKey ?? {})
                        : [tooltipData?.nearestDatum?.key]
                      ).filter((city) => city) as City[]
                    ).map((city) => (
                      <div key={city}>
                        <Typography>
                          Total{" "}
                          <span
                            style={{
                              fontWeight: "bold",
                            }}
                          >
                            {tooltipData?.nearestDatum?.datum
                              ? Math.floor(
                                  +accessors[renderHorizontally ? "x" : "y"][
                                    city
                                  ](tooltipData?.nearestDatum?.datum),
                                )
                              : "–"}
                          </span>
                        </Typography>
                      </div>
                    ))}
                  </>
                )}
              />
            )}
          </XYChart>
        );
      }}
    </HomeBottomSalesGraphControls>
  );
};

export default HomeBottomSalesGraph;
