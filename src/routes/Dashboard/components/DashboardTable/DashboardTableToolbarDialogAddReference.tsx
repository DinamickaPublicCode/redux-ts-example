import React from "react";
import Transition from "src/components/Transition";
import CloseIcon from "@mui/icons-material/Close";
import IconButton from "@mui/material/IconButton";
import DialogTitle from "@mui/material/DialogTitle";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import {styled, useTheme} from "@mui/material/styles";

interface DashboardTableToolbarDialogAddReferenceProps {
  open: boolean;
  handleClose: () => void;
  selected: string[];
}

const DashboardTableToolbarDialogAddReference: React.FC<
  DashboardTableToolbarDialogAddReferenceProps
> = ({open, selected, handleClose}) => {
  const theme = useTheme();
  return (
    <Dialog
      open={open}
      TransitionComponent={Transition}
      keepMounted
      maxWidth={"xs"}
      onClose={handleClose}
      /* classes={{paper: classes.dialogPaper}} */
      sx={{
        padding: "35px",
        width: "100%",
      }}
      aria-labelledby="alert-dialog-archive-title"
      aria-describedby="alert-dialog-archive-description"
    >
      <DialogTitle
        sx={{
          padding: 0,
          marginBottom: "30px",
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
        /* classes={{ root: classes.dialogTitle }}  */ id="alert-dialog-archive-title"
      >
        <Typography variant="h6">Add work order reference</Typography>
        <IconButton size="small" onClick={handleClose}>
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent
        style={{
          padding: 0,
          marginBottom: 25,
        }}
      >
        <SelectedItemsBox>
          {selected.length === 1
            ? `${selected.length} Item Selected`
            : `${selected.length} Items Selected`}
        </SelectedItemsBox>
        <Typography variant="body1" color="textSecondary">
          Référence w/o mécanique
        </Typography>
        <SelectedItemsBox
          sx={{
            backgroundColor: "#F6F8FA",
            color: theme.palette.common.black,
            fontWeight: 400,
          }}
        >
          Loren sochauvski
        </SelectedItemsBox>
        <Typography variant="body1" color="textSecondary">
          Référence w/o livraison mécanique
        </Typography>
        <SelectedItemsBox
          sx={{
            backgroundColor: "#F6F8FA",
            color: theme.palette.common.black,
            fontWeight: 400,
          }}
        >
          Clean
        </SelectedItemsBox>
      </DialogContent>
      <DialogActions style={{padding: 0}}>
        <Button
          style={{
            textTransform: "uppercase",
            padding: 15,
          }}
          fullWidth
          variant="contained"
          onClick={handleClose}
        >
          Submit
        </Button>
      </DialogActions>
    </Dialog>
  );
};

const SelectedItemsBox = styled(Box)(({theme}) => ({
  marginBottom: "25px",
  padding: "15px",
  color: theme.palette.primary.main,
  fontWeight: 500,
  fontSize: "16px",
  backgroundColor: "rgba(26, 6, 249, 0.06)",
  borderRadius: theme.shape.borderRadius,
}));

export default DashboardTableToolbarDialogAddReference;
