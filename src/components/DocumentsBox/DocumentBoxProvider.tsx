import React, {createContext, useContext, useEffect, useState} from "react";
import {
  DocumentBoxActionHandlers,
  DocumentBoxContextData,
  DocumentBoxEntries,
  DocumentBoxEntryCategory,
  DocumentBoxStyling,
  DocumentFileTypes,
  DocumentsApiData,
} from "src/types/documentBoxInterfaces";
import {DocumentsBoxLayout} from "./DocumentsBoxLayout";

/**
 * Use this component and pass all the necessary props.
 * @param props
 */
const DocumentBoxProvider = ({
  styling,
  documents,
  actions,
  shouldRender,
}: {
  documents: DocumentsApiData;
  styling?: DocumentBoxStyling;
  shouldRender: boolean;
  actions: DocumentBoxActionHandlers;
}) => {
  if (!styling) {
    styling = {
      hasTopPadding: true,
      edgeInsets: 35,
    };
  }

  const [parsedDocuments, setParsedDocuments] = useState(
    null as null | DocumentBoxEntries,
  );

  useEffect(() => {
    setParsedDocuments(makeDocumentsSectionsFromJSON(documents));
  }, [documents]);

  return (
    <DocumentBoxContext.Provider
      value={{
        documents: parsedDocuments || {
          categories: [],
        },
        actions,
      }}
    >
      <DocumentsBoxLayout shouldRender={shouldRender} {...styling} />
    </DocumentBoxContext.Provider>
  );
};

export function useDocumentBoxContext(): DocumentBoxContextData {
  return useContext(DocumentBoxContext) as unknown as DocumentBoxContextData;
}

export function useDocumentBoxActions(): DocumentBoxActionHandlers {
  const {actions} = useDocumentBoxContext();
  return actions;
}

export function makeDocumentsSectionsFromJSON(
  apiDocuments: DocumentsApiData,
): DocumentBoxEntries {
  //
  const categories: DocumentBoxEntryCategory[] = [];
  for (const type in DocumentFileTypes) {
    // Find document of type
    const category: DocumentBoxEntryCategory = {
      type: type as DocumentFileTypes,
    };
    const filtered = apiDocuments.files.filter((file) => file.type === type);
    if (filtered.length) {
      let {id, name} = filtered[0];
      const url = name;

      // I suppose the name could be a URL
      // so trim it to the filename look
      {
        let nameFormatted: string[] = name.split("\\");
        nameFormatted = nameFormatted[nameFormatted.length - 1].split("/");
        name = nameFormatted[nameFormatted.length - 1].split("?")[0];
      }

      category.file = {
        id,
        url,
        name,
      };
      categories.push(category);
    }
  }
  return {categories};
}

const DocumentBoxContext = createContext(null as null | DocumentBoxContextData);

export default DocumentBoxProvider;
