import React, {useState, useEffect} from "react";
import Box from "@mui/material/Box";
import Checkbox from "@mui/material/Checkbox";
import FormControlLabel from "@mui/material/FormControlLabel";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";
import {LoadingButton} from "@mui/lab";
import {useTranslation} from "react-i18next";
import { ReactComponent as CheckboxIcon } from "src/assets/icons/CheckboxIcon.svg";
import { ReactComponent as CheckboxCheckedIcon } from "src/assets/icons/CheckboxCheckedIcon.svg";
import {FinanceTabType} from "src/types";
import {Formik} from "formik";
import * as yup from "yup";
import CartagInputField from "../CartagInputField";
// import { updateInventory } from "src/types/api";
import useCustomInfoSnackbar from "src/utils/useCustomInfoSnackbar";
import {useTabsStyles} from "./StockDeliveryTab";

const INITIAL_DATA = {
  referenceStock: "",
  niv: "",
  cle: "",
  make: "",
  model: "",
  color: "",
  fuel: "",
  year: 0,
  km: 0,
  price: 0,
  transmission: "",
  doors: 0,
  newCar: false,
};

const validationSchema = yup.object().shape({
  referenceStock: yup
    .string()
    .matches(/^[0-9a-zA-Z]+$/, "Stock must be alphanumeric")
    .required("Stock is Required"),
  niv: yup
    .string()
    .matches(/^[0-9a-zA-Z]+$/, "NIV must be alphanumeric")
    .required("NIV is Required"),
  cle: yup.string(),
  make: yup.string().required("Make is Required"),
  model: yup.string().required("Model is Required"),
  color: yup.string().required("Color is Required"),
  fuel: yup.string().required("Fuel is Required"),
  year: yup.number().required("Year is Required"),
  km: yup.number().required("km is Required"),
  price: yup.number().required("Price is Required"),
  transmission: yup.string(),
  doors: yup.number(),
  newCar: yup.boolean(),
});

interface GeneralTabProps {
  currentDeal: FinanceTabType | null;
  revalidateInventory: () => void;
}

const GeneralTab: React.FC<GeneralTabProps> = ({
  currentDeal,
  revalidateInventory,
}) => {
  const classes = useTabsStyles();
  const {t} = useTranslation();

  const enqueueInfoSnackbar = useCustomInfoSnackbar();

  const [inputsData, setInputsData] = useState(INITIAL_DATA);

  useEffect(() => {
    if (currentDeal) {
      setInputsData((prevProps) => {
        return {
          ...prevProps,
          referenceStock: currentDeal?.referenceStock ?? "",
          niv: currentDeal?.generalInfo?.serialNumberNiv ?? "",
          cle: currentDeal?.generalInfo?.numCle ?? "",
          make: currentDeal?.generalInfo?.make ?? "",
          model: currentDeal?.generalInfo?.model ?? "",
          color: currentDeal?.generalInfo?.color ?? "",
          fuel: currentDeal?.generalInfo?.fuel ?? "",
          year: !!currentDeal?.generalInfo?.year
            ? currentDeal.generalInfo.year
            : 0,
          km: !!currentDeal?.generalInfo?.km ? currentDeal.generalInfo.km : 0,
          price: currentDeal?.generalInfo?.price
            ? currentDeal.generalInfo.price
            : 0,
          transmission: currentDeal?.generalInfo?.transmission ?? "",
          doors: currentDeal?.generalInfo?.door
            ? currentDeal.generalInfo.door
            : 0,
          newCar: currentDeal?.generalInfo?.newCar ?? false,
        };
      });
    }
  }, [currentDeal]);

  return (
    <Paper elevation={0} className={classes.tabPaper}>
      <Box
        display="flex"
        alignItems="center"
        justifyContent="space-between"
        style={{
          padding: "20px 40px",
          backgroundColor: "#F6F8FA",
          marginBottom: 40,
        }}
      >
        <Typography
          variant="h6"
          style={{
            fontSize: 18,
            fontWeight: 600,
          }}
        >
          General Information
        </Typography>
      </Box>

      <Formik
        initialValues={inputsData}
        enableReinitialize
        validationSchema={validationSchema}
        onSubmit={async (values, {setSubmitting}) => {
          if (currentDeal) {
            const data = {
              action: "UPDATE_INV",
              referenceStock: currentDeal.referenceStock,
              generalInfo: {
                color: values.color,
                door: values.doors,
                fuel: values.fuel,
                km: values.km,
                make: values.make,
                model: values.model,
                newCar: values.newCar,
                numCle: values.cle,
                price: values.price,
                serialNumberNiv: values.niv,
                transmission: values.transmission,
                year: values.year,
              },
            };
            console.log("submit", data);
            try {
              enqueueInfoSnackbar("Successfully updated");
            } catch (e) {
              console.log(e);
            } finally {
              revalidateInventory();
              setSubmitting(false);
            }
          }
        }}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          /* and other goodies */
        }) => (
          <form onSubmit={handleSubmit}>
            <Grid
              container
              style={{
                padding: "0 20px",
              }}
            >
              <Grid item xs={12} sm={6} className={classes.gridSpacing}>
                <CartagInputField
                  labelText={t("DashboardTableHead.referenceStock")}
                  name="referenceStock"
                  error={!!touched.referenceStock && !!errors.referenceStock}
                  helperText={errors.referenceStock}
                  value={values.referenceStock}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
              </Grid>
              <Grid item xs={12} sm={6} className={classes.gridSpacing}>
                <CartagInputField
                  labelText={t("DashboardTableHead.niv")}
                  name="niv"
                  error={!!touched.niv && !!errors.niv}
                  helperText={errors.niv}
                  value={values.niv}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
              </Grid>
              <Grid item xs={12} className={classes.gridSpacing}>
                <FormControlLabel
                  label={t("newCar")}
                  onChange={handleChange}
                  checked={values.newCar}
                  value={values.newCar}
                  control={
                    <Checkbox
                      icon={<CheckboxIcon />}
                      checkedIcon={<CheckboxCheckedIcon />}
                      name="newCar"
                      color="primary"
                    />
                  }
                />
              </Grid>

              <Grid item xs={12} sm={6} className={classes.gridSpacing}>
                <CartagInputField
                  labelText={t("DashboardTableHead.cle")}
                  name="cle"
                  error={!!touched.cle && !!errors.cle}
                  helperText={errors.cle}
                  value={values.cle}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
              </Grid>
              <Grid item xs={12} sm={6} className={classes.gridSpacing}>
                <CartagInputField
                  labelText={t("DashboardTableHead.make")}
                  name="make"
                  error={!!touched.make && !!errors.make}
                  helperText={errors.make}
                  value={values.make}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
              </Grid>

              <Grid item xs={12} sm={6} className={classes.gridSpacing}>
                <CartagInputField
                  labelText={t("DashboardTableHead.model")}
                  name="model"
                  error={!!touched.model && !!errors.model}
                  helperText={errors.model}
                  value={values.model}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
              </Grid>
              <Grid item xs={12} sm={6} className={classes.gridSpacing}>
                <CartagInputField
                  labelText={t("DashboardTableHead.color")}
                  name="color"
                  error={!!touched.color && !!errors.color}
                  helperText={errors.color}
                  value={values.color}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
              </Grid>

              <Grid item xs={12} sm={6} className={classes.gridSpacing}>
                <CartagInputField
                  labelText={t("DashboardTableHead.fuel")}
                  name="fuel"
                  error={!!touched.fuel && !!errors.fuel}
                  helperText={errors.fuel}
                  value={values.fuel}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
              </Grid>
              <Grid item xs={12} sm={6} className={classes.gridSpacing}>
                <CartagInputField
                  labelText={t("DashboardTableHead.year")}
                  name="year"
                  type="number"
                  error={!!touched.year && !!errors.year}
                  helperText={errors.year}
                  value={values.year}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
              </Grid>

              <Grid item xs={12} sm={6} className={classes.gridSpacing}>
                <CartagInputField
                  labelText={t("DashboardTableHead.km")}
                  name="km"
                  type="number"
                  error={!!touched.km && !!errors.km}
                  helperText={errors.km}
                  value={values.km}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
              </Grid>
              <Grid item xs={12} sm={6} className={classes.gridSpacing}>
                <CartagInputField
                  labelText={t("DashboardTableHead.price")}
                  name="price"
                  type="number"
                  inputProps={{
                    min: "0",
                    step: "0.01",
                  }}
                  error={!!touched.price && !!errors.price}
                  helperText={errors.price}
                  value={values.price}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
              </Grid>

              <Grid item xs={12} sm={6} className={classes.gridSpacing}>
                <CartagInputField
                  labelText={t("DashboardTableHead.transmission")}
                  name="transmission"
                  error={!!touched.transmission && !!errors.transmission}
                  helperText={errors.transmission}
                  value={values.transmission}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
              </Grid>
              <Grid item xs={12} sm={6} className={classes.gridSpacing}>
                <CartagInputField
                  labelText={t("door")}
                  name="doors"
                  type="number"
                  error={!!touched.doors && !!errors.doors}
                  helperText={errors.doors}
                  value={values.doors}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
              </Grid>
            </Grid>
            <Box width="100%" className={classes.submitButtonWrapper}>
              <LoadingButton
                type="submit"
                variant="contained"
                color="primary"
                disabled={isSubmitting}
                className={classes.submitButton}
              >
                {t("actions.UPDATE_INV")}
              </LoadingButton>
            </Box>
          </form>
        )}
      </Formik>
    </Paper>
  );
};

export default GeneralTab;
