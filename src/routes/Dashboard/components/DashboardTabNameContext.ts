import React from "react";
import {TabViewsType} from "src/types";

const DashboardTabNameContext = React.createContext<TabViewsType>("INVENTORY");

export default DashboardTabNameContext;
