import {Theme} from "@mui/material/styles";

import createStyles from "@mui/styles/createStyles";
import makeStyles from "@mui/styles/makeStyles";

export const useSalesFunnelRowStyles = makeStyles((theme: Theme) =>
  createStyles({
    userProfileGridItem: {
      borderRight: "2px solid #F6F8FA",
      borderLeft: "2px solid #F6F8FA",
      borderBottom: "2px solid white",
      // [theme.breakpoints.down(400)]:{
      //   display:"none",
      // }
    },
    userProfileGridItem1: {
      width: "142px",
      [theme.breakpoints.down("sm")]: {
        display: "none",
      },
    },
    soldGoal: {
      borderBottom: "2px solid white",
      paddingTop: 14,
      paddingBottom: 12,
    },
    totalValue: {
      paddingTop: 17,
      paddingBottom: 15,
    },
    avatar: {
      width: 54,
      height: 54,
      border: "2px solid white",
      boxShadow: "0px 4px 12px rgba(203, 205, 217, 0.25)",
      borderRadius: "50%",
      marginBottom: 10,
      background: "white",
      flexShrink: 0,
    },
    userName: {
      fontStyle: "normal",
      fontWeight: "normal",
      fontSize: "14px",
      lineHeight: "18.2px",
      textAlign: "center",
      color: "#222238",
    },
    statsFontBold: {
      fontStyle: "normal",
      fontWeight: 600,
      fontSize: "18px",
      lineHeight: "124%",
      color: "black",
    },
    statsFont: {
      fontStyle: "normal",
      fontWeight: "normal",
      fontSize: "14px",
      lineHeight: "18.2px",
      color: "#98A2B7",
    },
    scrollableItemsList: {
      // Hide a tabs indicator
      "& span:last-child": {
        display: "none",
      },

      display: "flex",
      flex: 1,
      position: "relative",
      flexDirection: "row",
      height: "138px",
      paddingRight: "10px",

      backgroundColor: "white",
      borderTop: "2px solid #F6F8FA",
      overflowX: "auto",

      width: "20px",
    },
    userStats: {
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      height: "138px",
      [theme.breakpoints.down("sm")]: {
        display: "none",
      },
    },
  }),
);
