import React, {useEffect} from "react";
import Box from "@mui/material/Box";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import {useSalesFunnelStyles} from "../../utils/salesFunnelStyles";

import {useApiActivityBoxHandlers} from "src/utils/useApiActivityBoxHandlers";
import ActivityBox from "src/components/ActivityBox";
import {useAppSelector} from "src/state/hooks";
import {ActivityBoxType} from "src/types/activityBoxInterfaces";
import {useDispatch} from "react-redux";
import CloseIcon from "@mui/icons-material/Close";
import {useGetInventoryByIdQuery} from 'src/state/reducers/carDealerApi';

const SalesFunnelChatBox = ({
  chatInventoryId,
  onChatInventoryIdChanged,
}: {
  chatInventoryId: string | null;
  onChatInventoryIdChanged: (value: string | null) => void;
}) => {
  const styles = useSalesFunnelStyles();
  const dispatch = useDispatch();
  const {data:inventory, refetch} = useGetInventoryByIdQuery('');
  const activityBoxHandlers = useApiActivityBoxHandlers(inventory);

  useEffect(() => {
    const handler = (e: KeyboardEvent) => {
      if (e.key === "Escape" && chatInventoryId !== null) {
        onChatInventoryIdChanged(null);
      }
    };
    document.addEventListener("keyup", handler);
    return () => {
      document.removeEventListener("keyup", handler);
    };
  });

  useEffect(() => {
    refetch();
  }, [chatInventoryId, dispatch]);

  return (
    <div className={styles.chatBox}>
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          p: 3,
          pl: 0,
        }}
      >
        <Typography
          component="div"
          sx={{
            fontWeight: 600,
            fontSize: 13,
            marginLeft: "24px",
            letterSpacing: "0.5px",
            textTransform: "uppercase",
            color: "#98A2B7",
          }}
        >
          Activity
        </Typography>
        <Box>
          <IconButton
            size="small"
            onClick={() => onChatInventoryIdChanged(null)}
          >
            <CloseIcon />
          </IconButton>
        </Box>
      </Box>
      <ActivityBox
        type={ActivityBoxType.POPUP}
        shouldRender={true}
        {...activityBoxHandlers}
      />
    </div>
  );
};

export default SalesFunnelChatBox;
