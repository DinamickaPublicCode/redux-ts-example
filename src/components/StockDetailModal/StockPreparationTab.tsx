import React, {useRef, useState} from "react";
import {useTranslation} from "react-i18next";
import {
  DepartmentSpecification,
  FinanceTabType,
  UpdateInventoryDataAction,
  UpdateInventoryDataType,
} from "src/types";
import CartagBackdrop from "../CartagBackdrop";
import StockPreparationTabForm from "./StockPreparationTabForm";
import {STOCK_DETAIL_TRANSLATION_KEY} from "../../system/constants";
import {GenericFormSubmitButton} from "../GenericFormSubmitButton";
import {StockPreparationTabContext} from "./StockPreparationTabContext";
import useCustomErrorSnackbar from "../../utils/useCustomErrorSnackbar";
import useCustomInfoSnackbar from "../../utils/useCustomInfoSnackbar";
import {useAppSelector} from "src/state/hooks";
import {useGetCarDealerQuery} from 'src/state/reducers/carDealerApi';

const subKey = `${STOCK_DETAIL_TRANSLATION_KEY}.PREPARATION_REQUEST.SUBMIT_BUTTONS`;

const StockPreparationTab = ({
  currentDeal,
  revalidateInventory,
}: {
  currentDeal: FinanceTabType | null;
  revalidateInventory: () => void;
}) => {
  const {t} = useTranslation();
  const {isLoading, onSubmitHandler, onDepartmentUpdated, carDealer} =
    usePreparationRequestsTabHandlers(currentDeal);

  return (
    <StockPreparationTabContext.Provider
      value={{
        deal: currentDeal,
      }}
    >
      <CartagBackdrop open={isLoading} />
      <form
        action="/src/routes"
        onSubmit={(e) => {
          e.preventDefault();
          onSubmitHandler(
            UpdateInventoryDataType.ADD_REQUEST,
            revalidateInventory,
          );
        }}
      >
        {carDealer?.departments.map((department, idx) => (
          <StockPreparationTabForm
            onDepartmentUpdated={onDepartmentUpdated}
            key={idx}
            department={department as unknown as DepartmentSpecification}
          />
        ))}
        <GenericFormSubmitButton
          burgerButtons={[
            {
              title: t(`${subKey}.SELL_AS_IS`),
              onClick: () =>
                onSubmitHandler(
                  UpdateInventoryDataType.SELL_AS_IS,
                  revalidateInventory,
                ),
            },
            {
              title: t(`${subKey}.PREPARE_ON_DELIVERY`),
              onClick: () =>
                onSubmitHandler(
                  UpdateInventoryDataType.PREPARE_ON_DELIV,
                  revalidateInventory,
                ),
            },
          ]}
        >
          {t(`${subKey}.ADD_DEMAND`)}
        </GenericFormSubmitButton>
      </form>
    </StockPreparationTabContext.Provider>
  );
};

type DepartmentState = {
  departmentType: string;
  steps: {
    stepType: string;
    requests: {
      idRequest: string;
    }[];
  }[];
};

interface PreparationRequestData extends UpdateInventoryDataAction {
  departments: DepartmentState[];
}

export const usePreparationRequestsTabHandlers = (
  currentDeal: FinanceTabType | null,
) => {
  const {data: user} = useGetUserQuery();;
  const {data:carDealer} = useGetCarDealerQuery();
  const [isLoading, setIsLoading] = useState(false);

  const errorSnackbar = useCustomErrorSnackbar();
  const infoSnackbar = useCustomInfoSnackbar();

  const requestState = useRef<PreparationRequestData>({
    action: UpdateInventoryDataType.ADD_REQUEST,
    idUser: "",
    userName: "",
    idCarDealer: "",
    departments: [],
  });

  const onDepartmentUpdated = (departmentState: DepartmentState) => {
    if (
      requestState.current.departments.filter(
        (department) =>
          department.departmentType === departmentState.departmentType,
      ).length === 0
    ) {
      requestState.current.departments.push(departmentState);
    } else {
      requestState.current.departments = requestState.current.departments.map(
        (department) =>
          department.departmentType === departmentState.departmentType
            ? departmentState
            : department,
      );
    }
  };

  const onSubmitHandler = async (
    actionType: UpdateInventoryDataType,
    revalidateInventory: () => void,
  ) => {
    if (!user?.id || !user?.fullName) {
      // TODO: Use localized string in here
      errorSnackbar("Couldn't fetch the user data");
      return;
    }

    if (!currentDeal?.idCarDealer) {
      // TODO: Use localized string in here
      errorSnackbar("Car dealer id isn't valid");
      return;
    }

    requestState.current.action = actionType;
    requestState.current.idUser = user?.id || "user";
    requestState.current.userName = user?.fullName || "name";
    requestState.current.idCarDealer = currentDeal?.idCarDealer || "id";

    // Don't send the departments data.
    if (actionType !== UpdateInventoryDataType.ADD_REQUEST) {
      setIsLoading(true);
      try {
        infoSnackbar("success");
      } catch (e) {
        // The error should appear in the main snackbar
      }
      revalidateInventory();
      setIsLoading(false);
      return;
    }

    // Only leave departments with steps filled with requests.
    const filteredDepartments = requestState.current.departments.filter(
      (dep) => dep.steps.filter((s) => s.requests.length).length,
    );

    if (filteredDepartments.length === 0) {
      // TODO: Use localized string in here
      errorSnackbar("No requests were selected");
      return;
    }

    setIsLoading(true);
    try {
      // await updateInventoryData(requestState.current, currentDeal?.id || "");
      infoSnackbar("success");
    } catch (e) {
      // The error should appear in the main snackbar
    }
    revalidateInventory();
    setIsLoading(false);
  };

  return {
    isLoading: isLoading || !carDealer,
    carDealer,
    onSubmitHandler,
    onDepartmentUpdated,
  };
};

export default StockPreparationTab;
