import Box from "@mui/material/Box/Box";
import Header from "./Header";
import CircularProgress from "@mui/material/CircularProgress";

const DashboardBackdrop = () => (
  <Box>
    <Header />
    <Box
      display="flex"
      justifyContent="center"
      alignItems="center"
      width="100%"
      height="500px"
    >
      <CircularProgress />
    </Box>
  </Box>
);

export default DashboardBackdrop;
