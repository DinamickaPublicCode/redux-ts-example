export const personalPreference = [
  {
    name: "Basic Info",
    slag: "basic-info",
  },
  {
    name: "Notification",
    slag: "notification",
  },
];

export const adminArea = [
  {
    name: "User lists",
    slag: "user-lists",
  },
  {
    name: "Add new users",
    slag: "add-new-users",
  },
  {
    name: "Liste Des Autorisations",
    slag: "liste-les-lutorisations",
  },
  {
    name: "Liste Des Rôles",
    slag: "liste-des-rôles",
  },
  {
    name: "Personnalisation Des Autorisations",
    slag: "personnalisation-des-autorisations",
  },
  {
    name: "Liste Des Marques",
    slag: "liste-des-marques",
  },
  {
    name: "Liste Des Modèles",
    slag: "liste-des-modèles",
  },
  {
    name: "Groupe De Commodités",
    slag: "groupe-de-commodités",
  },
  {
    name: "Ajouter Des Commodités à un groupe",
    slag: "ajouter-des-commodités-à-un-groupe",
  },
  {
    name: "Groupe D'accessoires",
    slag: "groupe-d'accessoires",
  },
  {
    name: "Ajouter Des Accessoires à un groupe",
    slag: "ajouter-des-accessoires-à-un-groupe",
  },
  {
    name: "Modifiler Les Couleurs",
    slag: "modifiler-les-couleurs",
  },
];
