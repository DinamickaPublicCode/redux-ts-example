import React, {Dispatch, SetStateAction, useContext, useState} from "react";
import {Link as RouterLink} from "react-router-dom";
import Box from "@mui/material/Box";
import Drawer from "@mui/material/Drawer";
import FormControlLabel from "@mui/material/FormControlLabel";
import Link from "@mui/material/Link";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import {CartagSwitch} from "../CartagSwitch";
import NotificationsFilterMenu from "./NotificationsFilterMenu";
import NotificationCard from "../NotificationCard";
import {useTranslation} from "react-i18next";
import {useDispatch} from "react-redux";
import StockDetailModal from "../StockDetailModal/StockDetailModal";
import {setNotificationSwitchCheck} from "src/state/reducers/notifications";
import {useTheme} from "@mui/material/styles";
import NotificationsSidebarContext from "./NotificationsSidebarContext";
import {useAppSelector} from "src/state/hooks";

export interface NotificationsSidebarProps {
  open: boolean;
  onClose: Dispatch<SetStateAction<boolean>>;
}

const NotificationsSidebar: React.FC = () => {
  const {open, onClose} = useContext(NotificationsSidebarContext);
  const {t} = useTranslation();
  const dispatch = useDispatch();

  const {notifications, switchCheck} = useAppSelector(
    (state) => state.notifications,
  );

  const [inventoryIdFofStockDetailModal, setInventoryIdFofStockDetailModal] =
    useState<string | undefined>(undefined);

  const toggleDrawer =
    (toggle: boolean) => (event: React.KeyboardEvent | React.MouseEvent) => {
      if (
        event.type === "keydown" &&
        ((event as React.KeyboardEvent).key === "Tab" ||
          (event as React.KeyboardEvent).key === "Shift")
      ) {
        return;
      }
      onClose(toggle);
    };
  const theme = useTheme();
  return (
    <Drawer
      anchor="right"
      open={open}
      onClose={toggleDrawer(false)}
      sx={{
        width: "497px",
        flexShrink: 0,
        [theme.breakpoints.down("sm")]: {
          width: "300px",
        },
        "& .MuiBackdrop-root": {
          top: "74px",
        },
        "& .MuiPaper-root": {
          width: "497px",
          [theme.breakpoints.down("sm")]: {
            width: 300,
          },
          top: "74px",
          // height: "calc(100vh - 75px)",
        },
      }}
    >
      <Toolbar
        style={{
          backgroundColor: "#F6F8FA",
          justifyContent: "space-between",
        }}
      >
        <Typography
          variant="h5"
          style={{
            fontWeight: 600,
            fontSize: 22,
          }}
        >
          {t("Navigation.Notifications")}
        </Typography>
        <div>
          <FormControlLabel
            sx={{
              marginRight: "15px",
              "& .MuiFormControlLabel-label": {
                margin: "0 10px",
                fontWeight: 600,
                fontSize: "14px",
                color: "#222238",
              },
              [theme.breakpoints.down("sm")]: {
                marginTop: "20px",
              },
            }}
            control={
              <CartagSwitch
                checked={switchCheck === "DELIVERED"}
                onChange={(event: {
                  target: {
                    checked: boolean;
                  };
                }) => {
                  dispatch(
                    setNotificationSwitchCheck(
                      event.target.checked ? "DELIVERED" : "ALL",
                    ),
                  );
                }}
              />
            }
            labelPlacement="start"
            label={switchCheck === "DELIVERED" ? t("Unread") : t("ALL")}
          />
          <NotificationsFilterMenu />
          {/* <NotificationsDotMenu /> */}
        </div>
      </Toolbar>
      <Box
        sx={{
          overflow: "auto",
          flex: 1,
        }}
        role="presentation"
      >
        {notifications?.length
          ? notifications.map((el) => {
              return (
                <NotificationCard
                  key={el.id}
                  small
                  data={el}
                  onClose={onClose}
                  openModal={setInventoryIdFofStockDetailModal}
                />
              );
            })
          : null}
      </Box>
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          p: 3.5,
          boxShadow: "0px -8px 25px rgba(20, 32, 100, 0.06)",
        }}
      >
        <Link
          to={"/notifications-list"}
          component={RouterLink}
          style={{
            fontWeight: 600,
            fontSize: 16,
          }}
        >
          {t("See all notifications")}
        </Link>
      </Box>

      <StockDetailModal
        open={inventoryIdFofStockDetailModal !== undefined}
        handleClose={() => setInventoryIdFofStockDetailModal(undefined)}
        currentDealId={inventoryIdFofStockDetailModal}
      />
    </Drawer>
  );
};

export default NotificationsSidebar;
