export const USE_FULL_PERCENTAGE_SALES = false;
export const USE_FULL_PERCENTAGE_SALES_FOR_SELLER = false;

// goal cache for mocking
export const goalCache: {
  [index: string]: number;
} = {};
