import React, {useEffect, useMemo, useState} from "react";
import Box from "@mui/material/Box";
import CircularProgress from "@mui/material/CircularProgress";
import CountUp from "src/components/CountUp";
import makeStyles from "@mui/styles/makeStyles";

const HomeStatsSpinnerImpl = ({percent}: {percent: number}) => {
  const classes = useStyles({});

  const offset = useMemo(() => {
    const percentInternal = percent;
    const radius = BASE_CIRCLE_SIZE / 2;
    const angle = percentInternal * 0.01 * 2 * Math.PI - Math.PI * 0.5;
    const baseOffset = radius;
    return [
      baseOffset + Math.sin(angle) * radius - 10,
      baseOffset + Math.cos(angle) * radius - 10,
    ];
  }, [percent]);

  const [circleOpacity, setCircleOpacity] = useState(0);
  const [theOffset, setTheOffset] = useState([0, 0]);
  const [countUpLoaded, setCountUpLoaded] = useState(false);

  useEffect(() => {
    setCircleOpacity(0);
    const timeout = setTimeout(() => {
      setTheOffset(offset);
      setCircleOpacity(1);
    }, 444);
    return () => {
      clearTimeout(timeout);
    };
    // eslint-disable-next-line
  }, [percent]);

  useEffect(() => {
    setCountUpLoaded(true);
  }, []);

  return (
    <>
      <Box
        sx={{
          position: "absolute",
          left: 0,
          width: `${BASE_CIRCLE_SIZE}px`,
          height: `${BASE_CIRCLE_SIZE}px`,
        }}>
        <Box
          sx={{
            position: "absolute",
            top: `${theOffset[0]}px`,
            left: `${theOffset[1]}px`,
            width: "19px",
            height: "19px",
            transition: "opacity 0.2s ease-out",
            backgroundColor: "#1A06F9",
            borderRadius: "50%",
            opacity: circleOpacity,
          }}
        />
      </Box>
      <Box
        sx={{
          position: "absolute",
          left: 0,
          width: `${BASE_CIRCLE_SIZE}px`,
          height: `${BASE_CIRCLE_SIZE}px`,
        }}>
        <svg width={`${BASE_CIRCLE_SIZE}`} height={`${BASE_CIRCLE_SIZE}`}>
          <linearGradient id="linearColors" x1="0" y1="0" x2="1" y2="1">
            <stop offset="5%" stopColor="rgba(26,6,249, 1)" />
            <stop offset="90%" stopColor="rgba(26,6,249, 0)" />
          </linearGradient>
        </svg>
      </Box>
      <Box
        sx={{
          position: "absolute",
          left: 0,
        }}>
        <CircularProgress
          classes={{
            circle: classes.circle,
          }}
          variant="determinate"
          value={percent}
          thickness={0.5}
          size={`${BASE_CIRCLE_SIZE}px`}
        />
      </Box>
      <Box
        style={{left: 0}}
        sx={{
          transition: "all 0.3s ease-out",
          fontStyle: "normal",
          fontWeight: "bold",
          fontSize: "28px",
          lineHeight: "36.4px",
          textAlign: "center",
          color: "#222238",
          position: "absolute",
          width: "225px",
          height: "216px",
          top: "-2px",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}>
        <CountUp loaded={countUpLoaded} end={percent} duration={1} />%
      </Box>
    </>
  );
};

const useStyles = makeStyles(() => ({
  circle: {
    stroke: "url(#linearColors)",
  },
}));

const BASE_CIRCLE_SIZE = 215;

export default HomeStatsSpinnerImpl;
