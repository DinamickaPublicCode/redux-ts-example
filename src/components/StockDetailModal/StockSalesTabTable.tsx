import React, {useState, useMemo, Dispatch, SetStateAction} from "react";
import {Theme} from "@mui/material/styles";
import createStyles from "@mui/styles/createStyles";
import makeStyles from "@mui/styles/makeStyles";
import Checkbox from "@mui/material/Checkbox";
import Paper from "@mui/material/Paper";
import TableRow from "@mui/material/TableRow";
import TableHead from "@mui/material/TableHead";
import TableContainer from "@mui/material/TableContainer";
import TableCell from "@mui/material/TableCell";
import TableBody from "@mui/material/TableBody";
import Table from "@mui/material/Table";
import IconButton from "@mui/material/IconButton";
import Collapse from "@mui/material/Collapse";
import {
  CheckboxWhiteBackIcon,
  CheckboxIndeterminateIcon,
  CheckboxCheckedIcon,
  CheckboxIcon,
} from "src/assets/icons";
import {AccessoriesType, CarDealerGroupAccessories} from "src/types";
import KeyboardArrowDown from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowUp from "@mui/icons-material/KeyboardArrowUp";

interface Data {
  name: string;
  price: number;
}

interface HeadCell {
  disablePadding: boolean;
  id: keyof Data;
  label: string;
}

const headCells: HeadCell[] = [
  {
    id: "name",
    disablePadding: true,
    label: "Name",
  },
  {
    id: "price",
    disablePadding: false,
    label: "Price",
  },
];

const Row = ({
  row,
  selected,
  setSelected,
}: {
  row: CarDealerGroupAccessories;
  selected: AccessoriesType[];
  setSelected: Dispatch<SetStateAction<AccessoriesType[]>>;
}) => {
  const [open, setOpen] = useState(false);

  const selectedIds = selected.map((doc) => doc.idAcc);
  const labelId = `enhanced-table-checkbox-${row.idGroupAcc}`;

  const summPrice = row.accessories.reduce((acc, item) => acc + item.price, 0);

  const handleInnerSelectAllClick = (
    event: React.ChangeEvent<HTMLInputElement>,
  ) => {
    if (event.target.checked) {
      setSelected((prevState) => [
        ...Array.from(
          new Set([...prevState, ...row.accessories.map((n) => n)]),
        ),
      ]);
      return;
    }
    setSelected((prevState) => [
      ...prevState.filter(
        (str) => !row.accessories.map((n) => n.idAcc).includes(str.idAcc),
      ),
    ]);
  };

  const handleInnerClick = (
    event: React.ChangeEvent<HTMLInputElement>,
    doc: AccessoriesType,
  ) => {
    if (event.target.checked) {
      setSelected((prevState) => [...prevState, doc]);
    } else {
      setSelected((prevState) => [
        ...prevState.filter((el) => el.idAcc !== doc.idAcc),
      ]);
    }
  };

  return (
    <>
      <TableRow hover role="checkbox" tabIndex={-1}>
        <TableCell padding="checkbox">
          <Checkbox
            icon={<CheckboxWhiteBackIcon />}
            checkedIcon={<CheckboxCheckedIcon />}
            indeterminateIcon={<CheckboxIndeterminateIcon />}
            color="default"
            indeterminate={
              selectedIds.length > 0 &&
              row.accessories.some((el) => selectedIds.includes(el.idAcc)) &&
              !row.accessories.every((el) => selectedIds.includes(el.idAcc))
            }
            checked={
              row.accessories.length > 0 &&
              row.accessories.every((el) => selectedIds.includes(el.idAcc))
            }
            onChange={handleInnerSelectAllClick}
          />
        </TableCell>
        <TableCell padding="checkbox">
          <IconButton
            aria-label="expand row"
            size="small"
            onClick={() => setOpen(!open)}
          >
            {open ? <KeyboardArrowUp /> : <KeyboardArrowDown />}
          </IconButton>
        </TableCell>
        <TableCell scope="row" id={labelId} padding="none">
          {row.name}
        </TableCell>
        <TableCell>{summPrice}</TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{padding: 0}} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Table>
              <TableBody>
                {row.accessories.map((innerRow) => (
                  <TableRow key={innerRow.idAcc}>
                    <TableCell
                      style={{
                        border: "none",
                      }}
                      padding="checkbox"
                    >
                      <Checkbox
                        color="default"
                        icon={<CheckboxIcon />}
                        checkedIcon={<CheckboxCheckedIcon />}
                        checked={
                          selectedIds.length > 0 &&
                          selectedIds.includes(innerRow.idAcc)
                        }
                        onChange={(event) => handleInnerClick(event, innerRow)}
                      />
                    </TableCell>
                    <TableCell
                      style={{
                        border: "none",
                      }}
                      padding="checkbox"
                    >
                      <IconButton
                        size="small"
                        style={{
                          visibility: "hidden",
                        }}
                      >
                        <KeyboardArrowUp />
                      </IconButton>
                    </TableCell>
                    <TableCell
                      style={{
                        border: "none",
                        width: 320,
                      }}
                      scope="row"
                      padding="none"
                    >
                      {innerRow.name}
                    </TableCell>
                    <TableCell
                      style={{
                        border: "none",
                      }}
                    >
                      {innerRow.price}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </Collapse>
        </TableCell>
      </TableRow>
    </>
  );
};

interface SalesTabTableProps {
  groupAccessories: CarDealerGroupAccessories[] | undefined;
  selected: AccessoriesType[];
  setSelected: Dispatch<SetStateAction<AccessoriesType[]>>;
}

const StockSalesTabTable: React.FC<SalesTabTableProps> = ({
  groupAccessories,
  selected,
  setSelected,
}) => {
  const classes = useStyles();

  const handleSelectAllClick = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (groupAccessories) {
      if (event.target.checked) {
        const newSelecteds = groupAccessories.reduce(
          (acc: AccessoriesType[], n) => {
            return [...acc, ...n.accessories.map((el) => el)];
          },
          [],
        );
        setSelected(newSelecteds);
        return;
      }
      setSelected([]);
    }
  };

  const selectedIds = useMemo(
    () => selected.map((doc) => doc.idAcc),
    [selected],
  );
  const allIds = useMemo(
    () =>
      groupAccessories &&
      groupAccessories.reduce((acc: string[], n) => {
        return [...acc, ...n.accessories.map((el) => el.idAcc)];
      }, []),
    [groupAccessories],
  );
  const isSelectedSome = useMemo(
    () => allIds && allIds.some((el) => selectedIds.includes(el)),
    [allIds, selectedIds],
  );
  const isSelectedEvery = useMemo(
    () => allIds && allIds.every((el) => selectedIds.includes(el)),
    [allIds, selectedIds],
  );

  return (
    <Paper elevation={0} className={classes.paper}>
      <TableContainer>
        <Table size={"medium"}>
          <TableHead
            style={{
              backgroundColor: "#F6F8FA",
            }}
          >
            <TableRow>
              <TableCell padding="checkbox">
                <Checkbox
                  icon={<CheckboxWhiteBackIcon />}
                  checkedIcon={<CheckboxCheckedIcon />}
                  indeterminateIcon={<CheckboxIndeterminateIcon />}
                  color="default"
                  indeterminate={
                    !!isSelectedSome && isSelectedSome && !isSelectedEvery
                  }
                  checked={!!isSelectedEvery && isSelectedEvery}
                  onChange={handleSelectAllClick}
                />
              </TableCell>
              <TableCell padding="checkbox" />
              {headCells.map((headCell) => (
                <TableCell
                  key={headCell.id}
                  padding={headCell.disablePadding ? "none" : "normal"}
                  className={classes.headColumn}
                >
                  {headCell.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>

          <TableBody>
            {groupAccessories &&
              groupAccessories.map((a, id) => {
                return (
                  <Row
                    key={id}
                    row={a}
                    selected={selected}
                    setSelected={setSelected}
                  />
                );
              })}
          </TableBody>
        </Table>
      </TableContainer>
    </Paper>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      width: "100%",
      border: "2px solid #F6F8FA",
      overflow: "hidden",
      borderBottomLeftRadius: 0,
      borderBottomRightRadius: 0,
    },
    headColumn: {
      fontWeight: 600,
      fontSize: 13,
      textTransform: "uppercase",
      color: "#98A2B7",
      borderRight: "2px solid #fff",
      "&:last-child": {
        borderRight: "none",
      },
    },
  }),
);
export default StockSalesTabTable;
