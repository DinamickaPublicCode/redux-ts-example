import {useState} from "react";
import DocumentBoxProvider from "../DocumentsBox";
import ActivitySidebarTitleSwitch from "./ActivitySidebarTitleSwitch";
import ActivityBox from "../ActivityBox";
import {useApiDocumentBoxHandlers} from "../../utils/useApiDocumentBoxHandlers";
import {useApiActivityBoxHandlers} from "../../utils/useApiActivityBoxHandlers";
import {FinanceTabType} from "src/types";

const ActivitySidebar = ({
  currentDeal,
}: {
  currentDeal: FinanceTabType | null;
}) => {
  const [isOn, setIsOn] = useState(false);
  const activityBoxHandlers = useApiActivityBoxHandlers();
  const documentBoxHandlers = useApiDocumentBoxHandlers(currentDeal);

  return (
    <>
      <ActivitySidebarTitleSwitch checked={isOn} onCheckedSet={setIsOn} />
      <ActivityBox shouldRender={!isOn} {...activityBoxHandlers} />
      <DocumentBoxProvider
        shouldRender={isOn}
        styling={{
          hasTopPadding: false,
          edgeInsets: 24,
        }}
        {...documentBoxHandlers}
      />
    </>
  );
};

export default ActivitySidebar;
