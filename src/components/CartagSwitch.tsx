import {Theme} from "@mui/material/styles";
import withStyles from "@mui/styles/withStyles";
import createStyles from "@mui/styles/createStyles";
import Switch from "@mui/material/Switch";

export const CartagSwitch = withStyles((theme: Theme) =>
  createStyles({
    root: {
      width: 52,
      height: 28,
      padding: 0,
      display: "flex",
      boxShadow: "0px 3px 15px rgba(203, 205, 217, 0.5)",
    },
    switchBase: {
      padding: 3,
      color: theme.palette.grey[500],
      "&$checked": {
        transform: "translateX(22px)",
        color: theme.palette.common.white,
        "& + $track": {
          opacity: 1,
          backgroundColor: theme.palette.common.white,
          borderColor: theme.palette.primary.main,
        },
      },
    },
    thumb: {
      width: 22,
      height: 22,
      backgroundColor: theme.palette.primary.main,
      boxShadow: "none",
    },
    track: {
      boxShadow: "0px 4px 12px rgba(203, 205, 217, 0.25)",
      borderRadius: 28 / 2,
      opacity: 1,
      backgroundColor: theme.palette.common.white,
    },
    checked: {},
  }),
)(Switch);
