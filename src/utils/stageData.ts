import {StageType} from "src/types";

export enum StageEnum {
  NEW_INV = "NEW_INV",
  FIRST_PREPARATION_NOT_DONE = "FIRST_PREPARATION_NOT_DONE",
  READY_FOR_SALE = "READY_FOR_SALE",
  WAITING_APV_CREDIT = "WAITING_APV_CREDIT",
  PREPARATION_DELIVERY = "PREPARATION_DELIVERY",
  READY_FOR_DELIVERY = "READY_FOR_DELIVERY",
  DELIVERY_DONE = "DELIVERY_DONE",
  COUNTED = "COUNTED",
  WHOLESALE = "WHOLESALE",
  OUTSOURCING = "OUTSOURCING",
}

export enum stageDataColor {
  NEW_INV = "#222238",
  FIRST_PREPARATION_NOT_DONE = "#FF134C",
  READY_FOR_SALE = "#8DD384",
  WAITING_APV_CREDIT = "#FFC93F",
  PREPARATION_DELIVERY = "#F49136",
  READY_FOR_DELIVERY = "#BC3DFF",
  DELIVERY_DONE = "#889DFF",
  COUNTED = "#1A06F9",
  WHOLESALE = "#B59CC8",
  OUTSOURCING = "#C8B19C",
}

export enum stagePreparationDataColor {
  REQUEST_NEW = "#FFC93F",
  REQUEST_NEW_WITH_WO = "#8DD384",
  REQUEST_HIGH_PRIORITY = "#BC3DFF",
  REQUEST_HIGH_PRIORITY_WITH_WO = "#FD88FF",
  REQUEST_DONE = "#1A06F9",
  REQUEST_REFUSED = "#FF134C",
  REQUEST_TO_COMPLETE = "#FF903F",
}

const stageDataPercentage = {
  NEW_INV: 0,
  FIRST_PREPARATION_NOT_DONE: 5,
  READY_FOR_SALE: 10,
  WAITING_APV_CREDIT: 25,
  PREPARATION_DELIVERY: 50,
  READY_FOR_DELIVERY: 75,
  DELIVERY_DONE: 99,
  COUNTED: 100,
  WHOLESALE: null,
  OUTSOURCING: null,
};

export interface StagePreparationDataType {
  name: keyof typeof stagePreparationDataColor;
  color: string;
}
export interface StageDataType {
  name: keyof typeof stageDataColor;
  color: string;
}

export const stagePreparationDataSelect: StagePreparationDataType[] = [
  {
    name: "REQUEST_NEW",
    color: stagePreparationDataColor.REQUEST_NEW,
  },
  {
    name: "REQUEST_NEW_WITH_WO",
    color: stagePreparationDataColor.REQUEST_NEW_WITH_WO,
  },
  {
    name: "REQUEST_HIGH_PRIORITY",
    color: stagePreparationDataColor.REQUEST_HIGH_PRIORITY,
  },
  {
    name: "REQUEST_HIGH_PRIORITY_WITH_WO",
    color: stagePreparationDataColor.REQUEST_HIGH_PRIORITY_WITH_WO,
  },
  {
    name: "REQUEST_DONE",
    color: stagePreparationDataColor.REQUEST_DONE,
  },
  {
    name: "REQUEST_REFUSED",
    color: stagePreparationDataColor.REQUEST_REFUSED,
  },
  {
    name: "REQUEST_TO_COMPLETE",
    color: stagePreparationDataColor.REQUEST_TO_COMPLETE,
  },
];

export const stageDataSelect: StageDataType[] = [
  {
    name: StageEnum.NEW_INV,
    color: stageDataColor.NEW_INV,
  },
  {
    name: StageEnum.FIRST_PREPARATION_NOT_DONE,
    color: stageDataColor.FIRST_PREPARATION_NOT_DONE,
  },
  {
    name: StageEnum.READY_FOR_SALE,
    color: stageDataColor.READY_FOR_SALE,
  },
  {
    name: StageEnum.WAITING_APV_CREDIT,
    color: stageDataColor.WAITING_APV_CREDIT,
  },
  {
    name: StageEnum.PREPARATION_DELIVERY,
    color: stageDataColor.PREPARATION_DELIVERY,
  },
  {
    name: StageEnum.READY_FOR_DELIVERY,
    color: stageDataColor.READY_FOR_DELIVERY,
  },
  {
    name: StageEnum.DELIVERY_DONE,
    color: stageDataColor.DELIVERY_DONE,
  },
  {
    name: StageEnum.COUNTED,
    color: stageDataColor.COUNTED,
  },
  {
    name: StageEnum.WHOLESALE,
    color: stageDataColor.WHOLESALE,
  },
  {
    name: StageEnum.OUTSOURCING,
    color: stageDataColor.OUTSOURCING,
  },
];
export const getColor = (stage: StageType) => {
  return stageDataColor[stage];
};
export const getPercentage = (stage: StageType) => {
  return stageDataPercentage[stage];
};
