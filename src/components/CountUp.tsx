import React, {useEffect} from "react";
import {CountUp as CountUpJs} from "countup.js";
import {useRef, useState} from "react";

/**
 * Global variable that increments the unique identifier used to set the HTML id on page
 */
let currentId = 0;

/**
 * Countup.js integration
 */
const CountUp = ({
  loaded,
  end,
  duration,
}: {
  loaded: boolean;
  end: number;
  duration: number;
}) => {
  const [id] = useState(() => currentId++);
  const [theEnd, setTheEnd] = useState(-1);
  const countUp = useRef<null | CountUpJs>(null);

  useEffect(() => {
    if (theEnd !== end) {
      setTheEnd(end);
      countUp.current = null;
      return;
    }
    if (countUp.current === null && end !== 0) {
      countUp.current = new CountUpJs(`countup-${id}`, end, {
        duration,
      });
    }
    if (end === 0) {
      return;
    }
    if (!loaded) {
      countUp.current?.reset();
    } else {
      countUp.current?.start();
    }
  }, [end, id, loaded, duration, theEnd]);

  return <span id={`countup-${id}`} />;
};

export default CountUp;
