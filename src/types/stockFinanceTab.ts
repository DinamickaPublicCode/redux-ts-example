import {DocumentInfo} from ".";

export type ClientInformation = {
  saleReference: string;
  name: string;
  telephone: string;
  email: string;
};

export type SaleInformation = {
  nameSeller: string;
  nameFinanceManager: string;
  saleDate: Date;
  limitPreparationDate: string;
  deliveryBeginDate: string;
};

export type AccessoriesInformation = {
  accessoryFiles: DocumentInfo[];
};
