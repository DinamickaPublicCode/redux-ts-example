import React from "react";
import {withStyles} from "@mui/styles";
import MuiAccordion from "@mui/material/Accordion";
import MuiAccordionSummary from "@mui/material/AccordionSummary";
import MuiAccordionDetails from "@mui/material/AccordionDetails";
import Typography from "@mui/material/Typography";
import ArrowDropDown from "@mui/icons-material/ArrowDropDown";
import Box from "@mui/material/Box";
import {LoadingButton} from "@mui/lab";
import {useTheme} from "@mui/material/styles";

interface StockFinanceAccordionProps {
  children?: React.ReactNode;
  stepIcon?: JSX.Element;
  panelId: string;
  panelName: string;
  isEditButton: boolean;
  open: boolean;
  onButtonClicked?: () => void;
  handleChange?: (
    panel: string,
  ) => (event: React.SyntheticEvent, newExpanded: boolean) => void;
}

const StockFinanceAccordion: React.FC<StockFinanceAccordionProps> = ({
  stepIcon,
  panelId,
  panelName,
  open,
  handleChange,
  children,
  onButtonClicked,
  isEditButton,
}) => {
  const theme = useTheme();
  return (
    <div>
      <Accordion
        expanded={open}
        onChange={handleChange ? handleChange(panelId) : undefined}
      >
        <AccordionSummary expandIcon={<ArrowDropDown htmlColor={"#CBD1DD"} />}>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              width: "100%",
            }}
          >
            <Typography
              style={{
                display: "flex",
                alignItems: "center",
                fontWeight: 600,
                fontSize: 18,
              }}
            >
              {stepIcon}{" "}
              <span
                style={{
                  margin: "0 20px 0 10px",
                  fontSize: 13,
                  color: "#98A2B7",
                }}
              >
                {panelId}
              </span>{" "}
              {panelName}
            </Typography>

            {onButtonClicked ? (
              isEditButton ? (
                <LoadingButton
                  sx={{
                    color: theme.palette.primary.main,
                    fontWeight: 600,
                    fontSize: "14px",
                    lineHeight: "17px",
                    textAlign: "center",
                    letterSpacing: "0.004em",
                    boxShadow: "0px 3px 20px rgba(203, 205, 217, 0.4)",
                    borderRadius: "10px",
                    padding: "10px 35px",
                  }}
                  onClick={onButtonClicked}
                  style={{
                    padding: "7px 40px",
                  }}
                >
                  Edit
                </LoadingButton>
              ) : (
                <LoadingButton
                  onClick={onButtonClicked}
                  variant="contained"
                  style={{
                    padding: "7px 40px",
                  }}
                >
                  Save
                </LoadingButton>
              )
            ) : (
              <></>
            )}
          </Box>
        </AccordionSummary>
        <AccordionDetails>{children}</AccordionDetails>
      </Accordion>
    </div>
  );
};

const Accordion = withStyles({
  root: {
    overflow: "hidden",
    border: "2px solid #F6F8FA",
    boxShadow: "none",
    marginBottom: 30,
    "&$expanded": {
      "&:last-child": {
        marginBottom: 30,
      },
    },
  },
  expanded: {},
})(MuiAccordion);

const AccordionSummary = withStyles({
  root: {
    backgroundColor: "#F6F8FA",
    marginBottom: -1,
    minHeight: 80,
    "&$expanded": {
      minHeight: 80,
    },
  },
  content: {
    "&$expanded": {
      margin: "12px 0",
    },
  },
  expanded: {},
})(MuiAccordionSummary);

const AccordionDetails = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiAccordionDetails);

export default StockFinanceAccordion;
