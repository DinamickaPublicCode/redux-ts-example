import React, {useEffect, useState, useMemo} from "react";
import {useLocation, useNavigate} from "react-router-dom";
import {useAppSelector} from "src/state/hooks";
import {TabViews, TabViewsType} from "src/types";
import {styled, useTheme} from "@mui/material/styles";
import Paper from "@mui/material/Paper";
import Box from "@mui/material/Box";
import {TabContext, TabPanel} from "@mui/lab";
import ArrowDropDown from "@mui/icons-material/ArrowDropDown";
import DashboardTabsAddViewButton from "./DashboardTabsAddViewButton";
import TabsButton from "./DashboardTabsButton";
import CartagBackdrop from "src/components/CartagBackdrop";
import {queryArrayFromString} from "src/utils/queryArrFromString";
import DashboardTabNameContext from "../DashboardTabNameContext";
import {useGetUserQuery} from 'src/state/reducers/userApi';

const DashboardTabs: React.FC<{children: React.ReactNode}> = ({children}) => {
  const {data: user} = useGetUserQuery();;
  const loading = useAppSelector((state) => state.loading.loading);
  const navigate = useNavigate();

  const [tabId, setTabId] = useState<TabViewsType>(TabViews.INVENTORY);
  const [tabArr, setTabArr] = useState<
    {
      name: TabViewsType;
      icon: JSX.Element;
    }[]
  >([]);

  const tabExists = useMemo(
    () => Boolean(tabArr.find((el) => el.name === tabId)),
    [tabArr, tabId],
  );
  useEffect(() => {
    if (!tabExists) {
      setTabId(TabViews.INVENTORY);
    }
  }, [tabExists]);
  const location = useLocation();
  const tabFromQuery = useMemo(
    () => queryArrayFromString(location.search).find((i) => i.includes("dep")),
    [location.search],
  );
  useEffect(() => {
    if (tabFromQuery) {
      setTabId(tabFromQuery.split("=")[1] as TabViewsType);
    }
  }, [tabFromQuery]);

  useEffect(() => {
    if (user) {
      setTabArr(
        user.views?.map((view) => ({
          name: view,
          icon: (
            <ArrowDropDown
              style={{
                fill: "#98A2B7",
              }}
            />
          ),
        })) ?? [],
      );
    }
  }, [user]);

  const handleSaveTabs = (tabs: TabViewsType[]) => {
    setTabArr([
      ...tabs.map((tab) => ({
        name: tab,
        icon: (
          <ArrowDropDown
            style={{
              fill: "#98A2B7",
            }}
          />
        ),
      })),
    ]);
  };

  const handleChangeTab =
    (newValue: TabViewsType) => (_event: React.SyntheticEvent) => {
      if (tabId !== newValue) {
        if (newValue === TabViews.INVENTORY) {
          navigate({
            pathname: "/dashboard",
            search: "",
          });
        } else {
          navigate({
            pathname: "/dashboard",
            search: `?dep=${newValue}`,
          });
        }
        setTabId(newValue);
      }
    };

  const sortTabs = (
    a: {
      name: TabViewsType;
      icon: JSX.Element;
    },
    b: {
      name: TabViewsType;
      icon: JSX.Element;
    },
  ) => {
    if (a.name === TabViews.INVENTORY) return -1;
    if (b.name === TabViews.INVENTORY) return 1;
    return 0;
  };

  const theme = useTheme();

  return (
    <TabContext value={`${tabId}`}>
      <CartagBackdrop open={loading} nobg />
      <Box
        sx={{
          display: "flex",
          flexGrow: 1,
          [theme.breakpoints.down("sm")]: {
            flexDirection: "column",
          },
        }}>
        {tabArr.sort(sortTabs).map((tab) => {
          return (
            <TabsButton
              key={tab.name}
              tabId={tabId}
              tab={tab}
              handleChangeTab={handleChangeTab}
            />
          );
        })}
        <DashboardTabsAddViewButton handleSaveTabs={handleSaveTabs} />
      </Box>
      {tabArr.map(({name}, idx) => {
        return (
          <TabPanel
            key={`name${idx}`}
            value={name}
            style={{
              padding: 0,
            }}>
            <DashboardTabNameContext.Provider value={name}>
              <StyledPaper>{children}</StyledPaper>
            </DashboardTabNameContext.Provider>
          </TabPanel>
        );
      })}
    </TabContext>
  );
};

const StyledPaper = styled(Paper, {
  shouldForwardProp: (prop) => prop !== "tabId",
})<{tabId?: string}>(({tabId}) => ({
  boxShadow: "0px 4px 20px rgba(20, 32, 100, 0.08)",
  borderTopLeftRadius: tabId === TabViews.INVENTORY ? 0 : 10,
}));

export default DashboardTabs;
