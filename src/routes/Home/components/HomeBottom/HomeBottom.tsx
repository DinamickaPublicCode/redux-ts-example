import React from "react";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import HomeBottomTable from "./HomeBottomTable";
import HomeBottomSales from "./HomeBottomSales";

const HomeBottom = () => (
  <Box sx={{mt: 4}}>
    <Grid container spacing={3}>
      <Grid item xs={12} sm={12} md={6}>
        <HomeBottomTable />
      </Grid>
      <Grid item xs={12} sm={12} md={6}>
        <HomeBottomSales />
      </Grid>
    </Grid>
  </Box>
);

export default HomeBottom;
