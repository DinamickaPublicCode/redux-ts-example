import {useCallback, useState} from "react";
import {DocumentInputFieldData} from "src/types/documentBoxInterfaces";
import {DocumentInfo} from "src/types";
import useCustomErrorSnackbar from "./useCustomErrorSnackbar";

export const useFileUpload = () => {
  const [documents, setDocuments] = useState<DocumentInfo[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const enqueueErrorSnackbar = useCustomErrorSnackbar();

  const MAX_SIZE = 5000000;

  const onAddExistingToList = useCallback((item: DocumentInfo) => {
    setDocuments((prevState) => [...prevState, item]);
  }, []);

  const onUploadFile = useCallback(
    async (event: DocumentInputFieldData, type: string) => {
      try {
        setIsLoading(true);
        if (!event.files?.length) {
          console.error(
            "onUploadFile event.files?.length",
            event.files?.length,
          );
        }
        const {files} = event;
        const file = files[0];

        if (file.size > MAX_SIZE) {
          enqueueErrorSnackbar("Maximum file size is 5MB");
        }

        // const res = await uploadDocumentOrImage(file);
        setDocuments((prevState) => [
          ...prevState,
          {
            id: `${Math.floor(Math.random() * 1000000000)}`,
            type,
            name: file.name,
          } as unknown as DocumentInfo,
        ]);
        setIsLoading(false);
      } catch (error) {
        console.error(error);
      }
    },
    [enqueueErrorSnackbar],
  );

  const onDeleteFromList = (itemIdsArr: string[]) => {
    console.log("onDeleteButtonClicked", itemIdsArr);
    const newItems = documents.filter((el) => !itemIdsArr.includes(el.id));
    setDocuments(newItems);
  };

  return {
    documents,
    isLoading,
    onUploadFile,
    onAddExistingToList,
    onDeleteFromList,
  };
};
