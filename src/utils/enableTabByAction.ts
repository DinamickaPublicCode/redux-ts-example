import {CreateDealTabs} from "./createDealTabs";

export const enableTabByAction = (action: string | undefined) => {
  if (action === "CREATE_DEAL") {
    return CreateDealTabs.SALES;
  }
  if (action === "SEND_MSG") {
    return CreateDealTabs.GENERAL;
  }
  if (action === "ADD_REQUEST") {
    return CreateDealTabs.PREPARATION_REQUEST;
  }
  if (action === "PREPARATION") {
    return CreateDealTabs.SERVICE;
  }
  if (action === "APV_CREDIT") {
    return CreateDealTabs.SALES;
  }
  if (action === "DELIVERY") {
    return CreateDealTabs.DELIVERY;
  }
  return CreateDealTabs.GENERAL;
};
