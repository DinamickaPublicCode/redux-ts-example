import React from "react";
import Paper from "@mui/material/Paper";
import InputBase from "@mui/material/InputBase";
import IconButton from "@mui/material/IconButton";
import SearchIcon from "@mui/icons-material/Search";

const DeliveryModalSearch = ({
  placeholder,
  value,
  onChange,
  name,
}: {
  name?: string;
  value?: string;
  placeholder?: string;
  onChange?: (
    event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>,
  ) => void;
}) => (
  <Paper
    component="div"
    elevation={0}
    style={{
      background: "#F6F8FA",
      padding: "2px 4px",
      display: "flex",
      flexDirection: "row",
      alignItems: "center",
      width: "100%",
    }}
  >
    <InputBase
      style={{
        marginLeft: 14,
        flex: 1,
      }}
      placeholder={placeholder}
      value={value}
      name={name}
      onChange={onChange}
    />
    <IconButton
      type="submit"
      style={{
        padding: 7,
        marginRight: 10,
      }}
      size="large"
    >
      <SearchIcon htmlColor="#1A06F9" />
    </IconButton>
  </Paper>
);

export default DeliveryModalSearch;
