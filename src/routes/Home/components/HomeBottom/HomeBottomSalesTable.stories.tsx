import React from 'react';
import Box from "@mui/material/Box";
import { ComponentStory, ComponentMeta } from '@storybook/react';
import HomeBottomSalesTable from './HomeBottomSalesTable';
import { withRouter } from 'storybook-addon-react-router-v6';

export default {
  title: 'Simple table',
  component: HomeBottomSalesTable,
  decorators: [withRouter]
} as ComponentMeta<typeof HomeBottomSalesTable>;


const Template: ComponentStory<typeof HomeBottomSalesTable> = (args) => <HomeBottomSalesTable {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  sellers: [
     {
        "name":"Alain Lambert",
        "sold":1,
        "goal":0
     },
     {
        "name":"Emile Armstrong",
        "sold":1,
        "goal":0
     },
     {
        "name":"Maryse Rheault",
        "sold":2,
        "goal":0
     },
     {
        "name":"Israel Deschamps",
        "sold":1,
        "goal":0
     }
  ]
}
