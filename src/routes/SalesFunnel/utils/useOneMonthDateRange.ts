import {useMemo} from "react";
import dayjs from "dayjs";

/**
 * Transform date into end-point friendly ISO string range.
 *
 * @param date
 */
export const useOneMonthDateRange = (date: Date) =>
  useMemo(() => {
    const formatMoment: (m: dayjs.Dayjs) => string = (m: dayjs.Dayjs) =>
      m.toISOString();
    const alignedDate = new Date(date.getFullYear(), date.getMonth());
    const dateMoment = dayjs(alignedDate).add(1, "day");
    const formatted = formatMoment(dateMoment);
    const pastMonthMoment = dateMoment
      .add(dateMoment.daysInMonth(), "days")
      .subtract(1, "day");
    return [formatted, formatMoment(pastMonthMoment)];
  }, [date]) as [string, string];
