import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import React from "react";

export const GenericFormSection = ({
  children,
  title,
  marginTop,
}: {
  title: string;
  marginTop?: number;
  children: JSX.Element[] | JSX.Element;
}) => (
  <Box
    sx={{
      marginTop: marginTop ? marginTop + "px" : marginTop,
    }}
  >
    <Typography
      sx={{
        fontWeight: 600,
        fontSize: "13px",
        lineHeight: "16.9px",
        letterSpacing: "0.5px",
        textTransform: "uppercase",
        color: "#98A2B7",
        marginBottom: "12px",
      }}
      component="div"
    >
      {title}
    </Typography>
    {children}
  </Box>
);
