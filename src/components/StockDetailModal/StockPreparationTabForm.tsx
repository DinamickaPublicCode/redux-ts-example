import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import React, {useRef, useMemo, useState, useContext} from "react";
import {useTranslation} from "react-i18next";
import chunk from "lodash/chunk";
import {DepartmentSpecification} from "src/types";
import {GenericFormBoxHeader} from "../GenericFormBoxHeader";
import {GenericFormBox} from "../GenericFormBox";
import {GenericFormSection} from "../GenericFormSection";
import {GenericFormCheckbox} from "../GenericFormCheckbox";
import {GenericFormDepartmentHeader} from "../GenericFormDepartmentHeader";
import {StockPreparationTabContext} from "./StockPreparationTabContext";

type DepartmentState = {
  departmentType: string;
  steps: {
    stepType: string;
    requests: {
      idRequest: string;
    }[];
  }[];
};

const StockPreparationTabForm = ({
  department,
  onDepartmentUpdated,
}: {
  department: DepartmentSpecification;
  onDepartmentUpdated: (data: DepartmentState) => void;
}) => {
  const {t} = useTranslation();
  const departmentState = useRef({
    departmentType: department.departmentType,
    steps: [],
  } as DepartmentState);

  const {deal} = useContext(StockPreparationTabContext);
  // Yeah, this is a hack to trigger updates, since the checkboxes states are stored in a Ref
  const theState = useState(Math.random());
  const checkBoxValues: {
    [index: string]: boolean;
  } = useMemo(() => {
    const vals: {
      [index: string]: boolean;
    } = {};
    deal?.departments?.forEach((department) =>
      department.steps?.forEach((step) =>
        step.requests?.map((request) => {
          return (vals[step.stepType + request.idRequest] = true);
        }),
      ),
    );
    return vals;
  }, [deal]);

  return (
    <GenericFormBox
      header={
        <GenericFormBoxHeader>
          <GenericFormDepartmentHeader>
            {t(`${department.departmentType}`)}
          </GenericFormDepartmentHeader>
        </GenericFormBoxHeader>
      }
    >
      {department.steps ? (
        department.steps.map((step, index) => (
          <Box key={index}>
            <GenericFormSection
              key={step.stepName}
              title={t(`${step.stepName}`)}
            >
              {chunk(step.requests, 3).map((row, id) => (
                <Grid container key={id}>
                  {row.map((request) => (
                    <Grid key={request.idRequest} item xs={4}>
                      <GenericFormCheckbox
                        checked={
                          checkBoxValues[step.stepType + request.idRequest] ||
                          false
                        }
                        onChange={(checked) => {
                          checkBoxValues[step.stepType + request.idRequest] =
                            checked;

                          if (departmentState.current.steps.length === 0) {
                            departmentState.current.steps.push({
                              stepType: step.stepType,
                              requests: [
                                {
                                  idRequest: request.idRequest,
                                },
                              ],
                            });
                          } else {
                            departmentState.current.steps =
                              departmentState.current.steps.map((stepState) =>
                                stepState.stepType === step.stepType
                                  ? {
                                      ...stepState,
                                      requests: checked
                                        ? [
                                            ...stepState.requests.filter(
                                              (requestState) =>
                                                requestState.idRequest !==
                                                request.idRequest,
                                            ),
                                            {
                                              idRequest: request.idRequest,
                                            },
                                          ]
                                        : stepState.requests.filter(
                                            (requestState) =>
                                              requestState.idRequest !==
                                              request.idRequest,
                                          ),
                                    }
                                  : stepState,
                              );
                          }

                          onDepartmentUpdated(departmentState.current);
                          theState[1](Math.random());
                        }}
                        label={request.labelRequest}
                      />
                    </Grid>
                  ))}
                </Grid>
              )) || null}
            </GenericFormSection>
          </Box>
        ))
      ) : (
        <></>
      )}
    </GenericFormBox>
  );
};

export default StockPreparationTabForm;
