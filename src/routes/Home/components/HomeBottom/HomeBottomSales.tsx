import {useState} from "react";
import SalesGraphImplementation from "./HomeBottomSalesGraph";
import VisibilitySensor from "react-visibility-sensor";
import Box from "@mui/material/Box";
import Fade from "@mui/material/Fade";
import Typography from "@mui/material/Typography";
import CartagMonthPicker from "src/components/CartagMonthPicker";
import {useTheme} from "@mui/material/styles";
import {ParentSize} from "@visx/responsive";

const HomeBottomSales = () => {
  const [date, setDate] = useState(new Date());

  const [visible, setVisible] = useState(false);
  const theme = useTheme();
  return (
    <>
      <VisibilitySensor
        onChange={(newVisible: boolean | ((prevState: boolean) => boolean)) => {
          if (!visible) {
            setVisible(newVisible);
          }
        }}
      >
        <Box
          style={{
            paddingTop: "20px",
            height: "100%",
          }}
          sx={{
            background: "#FFFFFF",
            boxShadow: "0px 4px 20px rgba(20, 32, 100, 0.08)",
            borderRadius: "14px",
            padding: "32px 30px",
          }}
        >
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between",
              [theme.breakpoints.down("sm")]: {
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              },
            }}
          >
            <Typography
              component="div"
              sx={{
                fontStyle: "normal",
                fontWeight: 600,
                fontSize: "18px",
                lineHeight: "22.32px",
                color: "#222238",
                [theme.breakpoints.down("sm")]: {
                  marginTop: 20,
                },
              }}
            >
              Sales graph
            </Typography>
            <Box
              sx={{
                [theme.breakpoints.down("sm")]: {
                  marginTop: 20,
                },
              }}
            >
              <CartagMonthPicker
                noswitch={true}
                rangeType={"month"}
                setRangeType={() => {}}
                date={date}
                onDateChanged={setDate}
              />
            </Box>
          </Box>
          <ParentSize>
            {({width, height}) => (
              <div
                style={{
                  height: "100%",
                  display: "flex",
                  flexDirection: "column",
                }}
              >
                <Box>
                  <Fade in={height > 100}>
                    <div>
                      {visible && (
                        <SalesGraphImplementation
                          dataKey={date.toISOString()}
                          width={width}
                          height={height}
                        />
                      )}
                    </div>
                  </Fade>
                </Box>
              </div>
            )}
          </ParentSize>
        </Box>
      </VisibilitySensor>
    </>
  );
};

export default HomeBottomSales;
