import React, {useEffect, useMemo, useState} from "react";
import {useTranslation} from "react-i18next";
import Box from "@mui/material/Box";
import Checkbox from "@mui/material/Checkbox";
import Container from "@mui/material/Container";
import FormControl from "@mui/material/FormControl";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormGroup from "@mui/material/FormGroup";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";
import {styled} from "@mui/material/styles";
import { ReactComponent as CheckboxIcon } from "src/assets/icons/CheckboxIcon.svg";
import { ReactComponent as CheckboxCheckedIcon } from "src/assets/icons/CheckboxCheckedIcon.svg";
import { ReactComponent as CheckboxIndeterminateIcon } from "src/assets/icons/CheckboxIndeterminateIcon.svg";
import {CheckboxWhiteBackIcon} from "src/assets/icons";
import Header from "src/components/Header";
import {CartagSwitch} from "src/components/CartagSwitch";
import NotificationCard from "../../../components/NotificationCard";
import TablePagination from "src/components/TablePagination";
import {useDispatch} from "react-redux";
import {useAppSelector} from "src/state/hooks";
import {NotificationType} from "src/types";
import StockDetailModal from "src/components/StockDetailModal/StockDetailModal";
import {
  setNotificationsPage,
  setNotificationsPerPage,
  setActiveNotificationFilters,
  setNotificationSwitchCheck,
  setNotificationArchivedSwitchCheck,
} from "src/state/reducers/notifications";

const NotificationsListLayout = () => {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const {
    notifications,
    pagesCount,
    switchCheck,
    switchCheckArchived,
    page,
    perPage,
  } = useAppSelector((state) => state.notifications);

  const [inventoryIdFofStockDetailModal, setInventoryIdFofStockDetailModal] =
    useState<string | undefined>(undefined);

  const notificationsId = notifications?.map((el) => el.id);
  const [selectedNotifications, setSelectedNotifications] = useState<string[]>(
    [],
  );
  const [checkedFilters, setCheckedFilters] = useState<NotificationType[]>([]);

  useEffect(() => {
    dispatch(setActiveNotificationFilters(checkedFilters));
  }, [checkedFilters, dispatch]);

  const onSelectAllNotifications = (
    event: React.ChangeEvent<HTMLInputElement>,
  ) => {
    if (notificationsId) {
      if (event.target.checked) {
        return setSelectedNotifications((prevState) => [
          ...Array.from(new Set([...prevState, ...notificationsId])),
        ]);
      }
      setSelectedNotifications([]);
    }
  };
  const onSelectNotification = (
    event: React.ChangeEvent<HTMLInputElement>,
    id: string,
  ) => {
    if (notificationsId) {
      if (event.target.checked) {
        setSelectedNotifications((prevState) => [
          ...Array.from(new Set([...prevState, id])),
        ]);
        return;
      }
      setSelectedNotifications((prevState) => [
        ...prevState.filter((str) => str !== id),
      ]);
    }
  };

  const handleFilterItem = (
    event: React.ChangeEvent<HTMLInputElement>,
    type: NotificationType,
  ) => {
    if (event.target.checked) {
      return setCheckedFilters((prevState) => [
        ...Array.from(new Set([...prevState, type])),
      ]);
    }
    return setCheckedFilters((prevState) => [
      ...prevState.filter((el) => el !== type),
    ]);
  };

  const isSelectedSome = useMemo(
    () =>
      notificationsId?.length &&
      notificationsId.some((el) => selectedNotifications.includes(el)),
    [notificationsId, selectedNotifications],
  );
  const isSelectedEvery = useMemo(
    () =>
      notificationsId?.length &&
      notificationsId.every((el) => selectedNotifications.includes(el)),
    [notificationsId, selectedNotifications],
  );

  const handleChangePage = (_event: unknown, newPage: number) => {
    dispatch(setNotificationsPage(newPage));
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<{
      name?: string;
      value: number;
    }>,
  ) => {
    dispatch(setNotificationsPerPage(event.target.value));
    dispatch(setNotificationsPage(0));
  };

  return (
    <>
      <Header />

      <Container maxWidth="xl">
        <TitleWrapper>
          <Typography
            variant="h4"
            style={{
              fontWeight: 700,
              fontSize: 28,
            }}
          >
            {t("Navigation.Notifications")}
          </Typography>
        </TitleWrapper>
        <Grid container spacing={3}>
          <Filters item xs={12} md={3}>
            <Paper
              style={{
                padding: "10px 0",
                boxShadow: "0px 4px 20px rgba(20, 32, 100, 0.08)",
                height: "calc(100vh - 80px - 80px - 20px)",
              }}
            >
              <Box
                sx={{
                  pt: 3.8,
                  display: "flex",
                  flexDirection: "column",
                  minWidth: 280,
                  outline: "none",
                }}
              >
                <FormControl
                  component="div"
                  style={{
                    padding: "0 22px 22px",
                  }}
                >
                  <FilterTitle>Filter notification</FilterTitle>
                  <FormGroup>
                    {(
                      [
                        "SALE",
                        "FINANCE",
                        "MECHANIC",
                        "ESTHETIC",
                        "DELIVERY",
                      ] as NotificationType[]
                    ).map((el) => {
                      return (
                        <CheckboxLabel
                          key={el}
                          checked={
                            !!checkedFilters.length &&
                            checkedFilters.includes(el)
                          }
                          control={
                            <Checkbox
                              color="default"
                              icon={<CheckboxIcon />}
                              checkedIcon={<CheckboxCheckedIcon />}
                              onChange={(event) => handleFilterItem(event, el)}
                              name={el}
                            />
                          }
                          label={t(`${el}`)}
                        />
                      );
                    })}
                  </FormGroup>
                </FormControl>
                {/* <Button
                variant="contained"
                style={{ margin: 20 }}
                onClick={handleFilter}
              >
                {t(`buttonNames.Apply`)}
              </Button> */}
              </Box>
            </Paper>
          </Filters>
          <Grid item xs={12} md={9}>
            <Paper
              style={{
                height: "100%",
                boxShadow: "0px 4px 20px rgba(20, 32, 100, 0.08)",
                overflow: "hidden",
                display: "flex",
                flexDirection: "column",
              }}
            >
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  backgroundColor: "#F6F8FA",
                  p: 2.3,
                }}
              >
                <Box>
                  <Checkbox
                    indeterminate={
                      !!isSelectedSome && isSelectedSome && !isSelectedEvery
                    }
                    checked={!!isSelectedEvery && isSelectedEvery}
                    onChange={onSelectAllNotifications}
                    style={{
                      marginRight: 20,
                    }}
                    color="default"
                    icon={<CheckboxWhiteBackIcon />}
                    checkedIcon={<CheckboxCheckedIcon />}
                    indeterminateIcon={<CheckboxIndeterminateIcon />}
                  />
                </Box>
                <Box
                  sx={{
                    width: "100%",
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: "center",
                  }}
                >
                  <Box></Box>
                  <Box>
                    <CustomSwitchLabel
                      control={
                        <CartagSwitch
                          checked={switchCheckArchived}
                          onChange={(event: {
                            target: {
                              checked: boolean;
                            };
                          }) =>
                            dispatch(
                              setNotificationArchivedSwitchCheck(
                                event.target.checked,
                              ),
                            )
                          }
                        />
                      }
                      labelPlacement="start"
                      label={
                        !switchCheckArchived
                          ? t("Show All Notifications")
                          : t("Show Archive Notifications")
                      }
                    />
                    <CustomSwitchLabel
                      control={
                        <CartagSwitch
                          checked={switchCheck === "DELIVERED"}
                          onChange={(event: {
                            target: {
                              checked: boolean;
                            };
                          }) => {
                            dispatch(
                              setNotificationSwitchCheck(
                                event.target.checked ? "DELIVERED" : "ALL",
                              ),
                            );
                          }}
                        />
                      }
                      labelPlacement="start"
                      label={
                        switchCheck === "DELIVERED" ? t("Unread") : t("ALL")
                      }
                    />
                  </Box>
                </Box>
              </Box>
              <Box
                sx={{
                  p: 2.3,
                  flex: 1,
                  maxHeight: "calc(100vh - 78px - 78px - 78px - 64px - 24px)",
                  overflowY: "auto",
                }}
              >
                {notifications?.length
                  ? notifications.map((el) => {
                      return (
                        <Box
                          key={el.id}
                          sx={{
                            display: "flex",
                            alignItems: "flex-start",
                          }}
                        >
                          <Checkbox
                            checked={
                              !!selectedNotifications.length &&
                              selectedNotifications.includes(el.id)
                            }
                            onChange={(event) =>
                              onSelectNotification(event, el.id)
                            }
                            color="default"
                            style={{
                              marginRight: 20,
                            }}
                            icon={<CheckboxIcon />}
                            checkedIcon={<CheckboxCheckedIcon />}
                          />
                          <NotificationCard
                            onClose={(e) => {}}
                            data={el}
                            openModal={setInventoryIdFofStockDetailModal}
                          />
                        </Box>
                      );
                    })
                  : null}
              </Box>
              <TablePagination
                count={pagesCount}
                page={page}
                rowsPerPage={perPage}
                rowsPerPageOptions={[50]}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={
                  handleChangeRowsPerPage as unknown as React.ComponentProps<
                    typeof TablePagination
                  >["onChangeRowsPerPage"]
                }
              />
            </Paper>
          </Grid>
        </Grid>

        <StockDetailModal
          open={inventoryIdFofStockDetailModal !== undefined}
          handleClose={() => setInventoryIdFofStockDetailModal(undefined)}
          currentDealId={inventoryIdFofStockDetailModal}
        />
      </Container>
    </>
  );
};

const TitleWrapper = styled(Box)(({theme}) => ({
  margin: theme.spacing(3, 0),
  display: "flex",
  justifyContent: "space-between",
}));

const FilterTitle = styled(Typography)({
  fontWeight: 600,
  fontSize: 18,
  color: "#222238",
  marginBottom: 15,
});

const CheckboxLabel = styled(FormControlLabel)({
  fontWeight: 600,
  margin: 10,
});

const CustomSwitchLabel = styled(FormControlLabel)({
  margin: "0 10px",
  fontWeight: 600,
  fontSize: 14,
  color: "#222238",
  marginRight: "15px",
});

const Filters = styled(Grid)(({theme}) => ({
  [theme.breakpoints.down("sm")]: {
    height: 350,
  },
}));

export default NotificationsListLayout;
