import dayjs from "dayjs";
import isBetween from "dayjs/plugin/isBetween";
import relativeTime from "dayjs/plugin/relativeTime";
import localeData from "dayjs/plugin/localeData";
import weekday from "dayjs/plugin/weekday";

dayjs.extend(isBetween);
dayjs.extend(relativeTime);
dayjs.extend(localeData);
dayjs.extend(weekday);
