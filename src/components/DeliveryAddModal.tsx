import Box from "@mui/material/Box";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import React, {useEffect, useMemo, useState} from "react";
import {useTranslation} from "react-i18next";
import CartagInputField from "./CartagInputField";
import StockInputFieldBody from "./StockDetailModal/StockInputFieldBody";
import CartagDatePicker from "./CartagDatePicker";
import {FormSubtitle} from "./StockDetailModal/StockDeliveryTab";
import Transition from "./Transition";
import {DialogBody} from "./DialogBody";
import * as yup from "yup";
import {Formik} from "formik";
import CartagBackdrop from "./CartagBackdrop";
import {FinanceTabType} from "src/types";
import {searchStockList} from "src/state/api";
import CartagAutocomplete from "./CartagAutocomplete";
import {LoadingButton} from "@mui/lab";
import {TelephoneMaskComponent} from "./TelephoneMaskComponent";
import useCustomErrorSnackbar from "src/utils/useCustomErrorSnackbar";
import useCustomInfoSnackbar from "src/utils/useCustomInfoSnackbar";
import GenericFormModalTitle from "./GenericFormModalTitle";
import {useCommonModalStyles} from "../utils/useCommonModalStyles";
import StockCarInfoReadiness from "./StockCarInfoReadiness";
import useDeliveryModalTranslation from "../utils/useDeliveryModalTranslation";
import {InputBaseComponentProps} from "@mui/material/InputBase";
import {useAppSelector} from "src/state/hooks";
import {useGetUserQuery} from 'src/state/reducers/userApi';

const INITIAL_VALUES = {
  name: "",
  telephone: "",
  email: "",
  deliveryStartDate: new Date(),
  deliveryEndDate: new Date(),
};

export interface DeliveryAddModalProps {
  isOpen: boolean;
  onIsOpenChanged: (val: boolean) => void;
  idCarDealer?: string;
}

const DeliveryAddModal: React.FC<DeliveryAddModalProps> = ({
  isOpen,
  onIsOpenChanged,
}) => {
  const [isLoading, setIsLoading] = useState(false);

  const validationSchema = useMemo(() => {
    return yup.object().shape({
      name: yup.string().required("Name is required"),
      telephone: yup
        .string()
        .max(12, "max length 10 symbols")
        .required("Telephone is Required"),
      email: yup.string().email().required("Email is required"),
      deliveryStartDate: yup.date().required("Start date is required"),
      deliveryEndDate: yup.date().required("End date is required"),
    });
  }, []);

  const handleClose = () => onIsOpenChanged(false);
  const {t} = useDeliveryModalTranslation();
  const {t: tb} = useTranslation();
  const commonStyles = useCommonModalStyles();
  const {data: user} = useGetUserQuery();;
  const enqueueErrorSnackbar = useCustomErrorSnackbar();
  const enqueueInfoSnackbar = useCustomInfoSnackbar();
  const autoCompleteHandlers = useStockAutocompleteHandlers(isOpen);

  const [disabledValues, setDisabledValues] = useState({
    delavente: "",
    saleReference: "",
    financier: "",
  });

  const {stockValue: currentStock} = autoCompleteHandlers;

  useEffect(() => {
    if (currentStock?.sales) {
      setDisabledValues({
        delavente: currentStock?.sales[0].saleReference || "",
        saleReference: currentStock?.sales[0].sellerInfo?.nameSeller || "",
        financier: currentStock?.sales[0].creditInfo?.nameFinanceManager || "",
      });
    }
  }, [currentStock, isOpen]);

  return (
    <>
      <CartagBackdrop open={isLoading} />
      <Dialog
        open={isOpen}
        TransitionComponent={Transition}
        keepMounted
        maxWidth={"md"}
        onClose={handleClose}
        classes={{
          paper: commonStyles.dialogPaper,
        }}
        aria-labelledby="alert-dialog-archive-title"
        aria-describedby="alert-dialog-archive-description"
      >
        <DialogBody>
          <Formik
            initialValues={INITIAL_VALUES}
            enableReinitialize
            validationSchema={validationSchema}
            onSubmit={async (values, {setSubmitting, resetForm}) => {
              if (user) {
                setSubmitting(true);
                setIsLoading(true);
                try {
                  enqueueInfoSnackbar("Success");
                  onIsOpenChanged(false);
                } catch (e) {
                  console.log(e);
                } finally {
                  resetForm({
                    values: INITIAL_VALUES,
                  });
                  setIsLoading(false);
                  setSubmitting(false);
                  onIsOpenChanged(false);
                }
              } else {
                enqueueErrorSnackbar(
                  "Invalid incoming data. Check currentDeal or user",
                );
              }
            }}
          >
            {({
              handleSubmit,
              handleChange,
              handleBlur,
              values,
              touched,
              errors,
              setFieldValue,
              isSubmitting,
            }) => (
              <form onSubmit={handleSubmit}>
                <GenericFormModalTitle
                  title={tb("Add new Delivery appointment")}
                  handleClose={handleClose}
                />
                <Box
                  sx={{
                    p: "28px 34px 0 42px",
                  }}
                >
                  <Box
                    sx={{
                      mb: "56px",
                    }}
                  >
                    <Grid container spacing={5}>
                      <Grid item xs={12} md={6}>
                        <CartagAutocomplete
                          {...autoCompleteHandlers}
                          name="referenceStock"
                          labelText={tb("DashboardTableHead.referenceStock")}
                        />
                      </Grid>
                      <Grid item xs={3} />
                      <Grid item xs={12} md={3}>
                        {currentStock && (
                          <Box
                            className={
                              commonStyles.itemReadinessAddNewDeliveryAppointment
                            }
                            sx={{
                              display: "flex",
                              flexDirection: "column",
                            }}
                          >
                            <Typography
                              component="div"
                              className={commonStyles.grayUppercaseText}
                            >
                              {tb("Item readiness")}
                            </Typography>
                            <Box
                              sx={{
                                mt: "13px",
                              }}
                            >
                              <StockCarInfoReadiness
                                stageType={
                                  currentStock?.stageInfo.stage || "NEW_INV"
                                }
                                icons={currentStock?.stageInfo.icons}
                              />
                            </Box>
                          </Box>
                        )}
                      </Grid>
                    </Grid>
                  </Box>
                  <Grid container spacing={5}>
                    <Grid item xs={12} md={6}>
                      <CartagInputField
                        labelText={t("REFERENCE_OF_THE_SALE")}
                        disabled
                        value={disabledValues.delavente}
                        name="delavente"
                      />
                    </Grid>
                    <Grid item xs={12} md={6}>
                      <CartagInputField
                        disabled
                        labelText={t("REPRESENTATIVE")}
                        name="saleReference"
                        value={disabledValues.saleReference}
                      />
                    </Grid>
                  </Grid>
                  <Grid container spacing={5}>
                    <Grid item xs={12} md={6}>
                      <CartagInputField
                        labelText={t("FINANCIAL_DIRECTOR")}
                        disabled
                        value={disabledValues.financier}
                      />
                    </Grid>
                  </Grid>
                  <FormSubtitle>{tb("Client Information")}</FormSubtitle>
                  <Grid container spacing={5}>
                    <Grid item xs={12} md={6}>
                      <CartagInputField
                        name="name"
                        labelText={t("CUSTOMER_NAME")}
                        error={!!touched.name && !!errors.name}
                        helperText={errors.name}
                        value={values.name}
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />
                    </Grid>
                    <Grid item xs={12} md={6}>
                      <CartagInputField
                        name="telephone"
                        labelText={t("PHONE")}
                        type="tel"
                        InputProps={{
                          inputComponent:
                            TelephoneMaskComponent as unknown as React.ElementType<InputBaseComponentProps>,
                        }}
                        error={!!touched.telephone && !!errors.telephone}
                        helperText={errors.telephone}
                        value={values.telephone}
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />
                    </Grid>
                  </Grid>

                  <Grid container spacing={5}>
                    <Grid item xs={12} md={6}>
                      <CartagInputField
                        name="email"
                        labelText={t("EMAIL")}
                        error={!!touched.email && !!errors.email}
                        helperText={errors.email}
                        value={values.email}
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />
                    </Grid>
                  </Grid>

                  <FormSubtitle>{tb("Delivery Information")}</FormSubtitle>

                  <Grid container spacing={5}>
                    <Grid item xs={12} md={6}>
                      <StockInputFieldBody label={t("DELIVERY_START_DATE")}>
                        <CartagDatePicker
                          isDateTimePicker
                          margin="none"
                          error={
                            !!touched.deliveryStartDate &&
                            !!errors.deliveryStartDate
                          }
                          helperText={
                            errors.deliveryStartDate as unknown as React.ReactNode
                          }
                          valueDate={values.deliveryStartDate}
                          onChangeDate={(value) =>
                            setFieldValue("deliveryStartDate", value)
                          }
                        />
                      </StockInputFieldBody>
                    </Grid>
                    <Grid item xs={12} md={6}>
                      <StockInputFieldBody label={t("DELIVERY_END_DATE")}>
                        <CartagDatePicker
                          isDateTimePicker
                          margin="none"
                          error={
                            !!touched.deliveryEndDate &&
                            !!errors.deliveryEndDate
                          }
                          helperText={
                            errors.deliveryEndDate as unknown as React.ReactNode
                          }
                          valueDate={values.deliveryEndDate}
                          onChangeDate={(value) =>
                            setFieldValue("deliveryEndDate", value)
                          }
                        />
                      </StockInputFieldBody>
                    </Grid>
                  </Grid>
                </Box>
                <DialogActions className={commonStyles.dialogActions}>
                  <LoadingButton
                    style={{
                      textTransform: "uppercase",
                      padding: "7px 8px",
                    }}
                    variant="contained"
                    color="primary"
                    type="submit"
                    disabled={!currentStock?.id}
                  >
                    {tb(`Create Appointment`)}
                  </LoadingButton>
                </DialogActions>
              </form>
            )}
          </Formik>
        </DialogBody>
      </Dialog>
    </>
  );
};

const useStockAutocompleteHandlers = (open: boolean) => {
  const {data: user} = useGetUserQuery();;
  const [stockSearchValue, setStockSearchValue] = useState<string>("L0996");
  const [currentStock, setCurrentStock] = useState<FinanceTabType | null>(null);
  const [stockResults, setStockResults] = useState<FinanceTabType[]>([]);
  const [loadingStock, setLoadingStock] = useState<boolean>(false);

  useEffect(() => {
    (async () => {
      if (user && stockSearchValue && stockSearchValue.length > 2) {
        try {
          setLoadingStock(true);
          const res = await searchStockList(
            user.defaultCarDealer.id,
            stockSearchValue,
          );
          setStockResults(res.data);
          setCurrentStock(
            res.data.content
              .filter(
                (x: {referenceStock: string}) => x.referenceStock === "L0996",
              )
              .pop(),
          );
        } catch (e) {
          console.log(e);
        } finally {
          setLoadingStock(false);
        }
      }
    })();
  }, [stockSearchValue, user, open]);

  const onStockValueChange = (value: string) => {
    setStockSearchValue(value);
  };
  const onStockObjectChange = (value: FinanceTabType | null) => {
    setCurrentStock(value);
    setStockSearchValue(value?.referenceStock as string);
  };

  return {
    loading: loadingStock,
    options: stockResults,
    inputValue: stockSearchValue,
    stockValue: currentStock,
    onValueChange: onStockValueChange,
    onStockChange: onStockObjectChange,
  };
};

export default DeliveryAddModal;
