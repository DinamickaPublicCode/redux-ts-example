import {useActivityBoxStyle} from "./ActivityBoxLayout";
import Box from "@mui/material/Box";
import clsx from "clsx";
import React from "react";

const ActivityBoxTabItem = ({
  isSelected,
  title,
  onClick,
}: {
  isSelected: boolean;
  title: string;
  onClick: () => void;
}) => {
  const styles = useActivityBoxStyle();
  return (
    <Box
      display="flex"
      flexDirection="row"
      alignItems="center"
      onClick={onClick}
      className={clsx(styles.tabItem, isSelected ? styles.tabItemSelected : "")}
    >
      <Box>{title}</Box>
    </Box>
  );
};

export default ActivityBoxTabItem;
