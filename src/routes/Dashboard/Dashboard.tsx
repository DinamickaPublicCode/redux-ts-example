import DashboardLayout from "./components/DashboardLayout";

const Dashboard = () => <DashboardLayout />;

export default Dashboard;
