import Header from "../../../../components/Header";
import Box from "@mui/material/Box";
import Grow from "@mui/material/Grow";
import SalesFunnelChatBox from "./SalesFunnelChatBox";
import {SalesFunnelTopRow} from "./SalesFunnelTopRow";
import SalesFunnelContent from "../SalesFunnelContent";
import {SalesFunnelFooter} from "./SalesFunnerFooter";
import React from "react";
import {useSalesFunnelContext} from "../../state/SalesFunnelProvider";

const SalesFunnelLayout = () => {
  const {chatInventoryId, onChatInventoryIdChanged} = useSalesFunnelContext();

  return (
    <div>
      <Header />
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Box
          sx={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "center",
            width: "100%",
          }}
        >
          <Box
            sx={{
              display: "flex",
              flex: 1,
              flexDirection: "column",
              position: "relative",
            }}
          >
            <Grow
              in={chatInventoryId !== null}
              style={{
                transformOrigin: "0 0 0",
              }}
              {...(chatInventoryId !== null
                ? {
                    timeout: 150,
                  }
                : {})}
            >
              <div>
                <SalesFunnelChatBox
                  chatInventoryId={chatInventoryId}
                  onChatInventoryIdChanged={onChatInventoryIdChanged}
                />
              </div>
            </Grow>
            <SalesFunnelTopRow />
            <SalesFunnelContent />
          </Box>
        </Box>
        <SalesFunnelFooter />
      </Box>
    </div>
  );
};

export default SalesFunnelLayout;
