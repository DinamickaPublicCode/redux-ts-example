import React from 'react';

import { ComponentStory, ComponentMeta } from '@storybook/react';
import HomeStatsSpinner from './HomeStatsSpinner';

export default {
  title: 'Progress chart',
  component: HomeStatsSpinner,
} as ComponentMeta<typeof HomeStatsSpinner>;


const Template: ComponentStory<typeof HomeStatsSpinner> = (args) => <HomeStatsSpinner {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  percent: 80
}
