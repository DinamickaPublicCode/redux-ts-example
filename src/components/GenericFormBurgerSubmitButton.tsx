import React, {useState} from "react";
import useCustomDropdownHandlers from "../utils/useCustomDropdownHandlers";
import {useTheme} from "@mui/material/styles";
import ModalDropdownPopper from "./ModalDropdown/ModalDropdownPopper";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import ArrowDropDown from "@mui/icons-material/ArrowDropDown";

export const GenericFormBurgerSubmitButton = ({
  burgerButtons,
}: {
  burgerButtons: {
    title: string;
    onClick: () => void;
  }[];
}) => {
  const [index, setIndex] = useState(0);
  const handlers = useCustomDropdownHandlers(
    burgerButtons.map((button) => button.title),
    setIndex,
  );
  const theme = useTheme();
  return (
    <ModalDropdownPopper {...handlers}>
      <Box
        display="flex"
        flexDirection="row"
        alignItems="center"
        sx={{
          boxShadow: "0px 3px 20px rgba(203, 205, 217, 0.4)",
          color: theme.palette.primary.main,
          fontWeight: 600,
          fontSize: "14px",
          lineHeight: "17px",
          textAlign: "center",
          letterSpacing: "0.004em",
        }}
        marginRight="12px"
      >
        <Button
          type="button"
          onClick={burgerButtons[index].onClick}
          sx={{
            paddingTop: "10px",
            paddingBottom: "10px",
            fontWeight: 600,
            paddingLeft: "35px",
            paddingRight: "5px",
            borderRadius: 0,
            borderTopLeftRadius: "10px",
            borderBottomLeftRadius: "10px",
          }}
        >
          {handlers.currentValue}
        </Button>
        <Button
          type="button"
          onClick={handlers.handleToggle}
          sx={{
            paddingTop: "10px",
            paddingBottom: "10px",
            fontWeight: 600,
            borderRadius: 0,
            borderTopRightRadius: "10px",
            borderBottomRightRadius: "10px",
          }}
        >
          <ArrowDropDown />
        </Button>
      </Box>
    </ModalDropdownPopper>
  );
};
