import {createTheme, responsiveFontSizes} from "@mui/material/styles";
import {withStyles} from "@mui/styles";

let theme = createTheme({
  palette: {
    primary: {
      main: "#1A06F9",
    },
    secondary: {
      main: "#FF134C",
    },
    background: {
      default: "#F2F2F2",
    },
  },
  shape: {
    borderRadius: 10,
  },
  typography: {
    fontFamily: [
      "Nunito Sans",
      "Roboto",
      "Helvetica",
      "Arial",
      "sans-serif",
    ].join(","),
    button: {
      textTransform: "none",
      fontWeight: 400,
    },
  },
  components: {
    MuiButton: {
      styleOverrides: {
        root: {
          height: "fit-content",

          "&:hover": {
            backgroundColor: "#1807DC",
          },
        },
      },
    },
    MuiPaper: {
      styleOverrides: {
        elevation1: {
          boxShadow: "0px 4px 20px rgba(20, 32, 100, 0.08)",
        },
      },
    },
  },
});

theme = responsiveFontSizes(theme);

const GlobalCss = withStyles({
  // @global is handled by jss-plugin-global.
  "@global": {
    "*": {
      scrollbarWidth: "thin" /* "auto" or "thin"  */,
      scrollbarColor: `${theme.palette.primary.main} #fff`,
      "&::-webkit-scrollbar": {
        width: 10 /* width of the entire scrollbar */,
      },
      "&::-webkit-scrollbar-track": {
        background: "#fff" /* color of the tracking area */,
      },
      "&::-webkit-scrollbar-thumb": {
        backgroundColor: theme.palette.primary.main,
        borderRadius: 20,
        border: "3px solid #fff",
      },
    },
  },
})(() => null);

export {theme, GlobalCss};
