import React, {useState, useEffect, useRef, useCallback} from "react";
import Box from "@mui/material/Box";
import Snackbar from "@mui/material/Snackbar";

type CartagSnackbarOptions = {
  variant?: "info" | "error";
  buttonText?: string;
  onButtonClick?: () => void;
  autoHideDuration?: number;
  _internalId?: number;
  onClose?: () => void;
  isClosed?: boolean;
  renderer?: ({options}: {options: CartagSnackbarOptions}) => JSX.Element;
};
type CustomSnackbarContextData = {
  pushSnackbar: (options: CartagSnackbarOptions) => void;
};

const CustomSnackbarContext =
  React.createContext<CustomSnackbarContextData | null>(null);

const SNACKBAR_HEIGHT = 76;
const SNACKBAR_SPACING = 24;

const SnackbarBody = ({
  options,
  index,
  openedStates,
}: {
  options: CartagSnackbarOptions;
  index: number;
  openedStates: React.MutableRefObject<boolean[]>;
}) => {
  const [isOpen, setIsOpen] = useState(true);

  const onClose = useCallback(() => {
    setIsOpen(false);
    openedStates.current[index] = false;
  }, [index, openedStates]);

  useEffect(() => {
    let timeout = null as null | number;
    if (options.autoHideDuration) {
      timeout = setTimeout(() => {
        onClose();
      }, options.autoHideDuration) as unknown as number;
    }
    return () => {
      if (timeout !== null) {
        clearTimeout(timeout);
      }
    };
  }, [options.autoHideDuration, onClose]);

  const renderer =
    options.renderer || ((props: CartagSnackbarOptions) => <Box />);
  return (
    <Snackbar
      style={{
        bottom: 24 + index * (SNACKBAR_HEIGHT + SNACKBAR_SPACING),
      }}
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "center",
      }}
      open={isOpen}
      message="Note archived"
    >
      {renderer({
        options: {
          ...options,
          onClose,
        },
      })}
    </Snackbar>
  );
};

export const CartagSnackbarProvider = ({
  children,
}: {
  children: JSX.Element | JSX.Element[];
}) => {
  const [snackbars, setSnackbars] = useState<CartagSnackbarOptions[]>([
    {},
    {},
    {},
    {},
    {},
  ]);
  const openedStates = useRef([false, false, false, false, false]);

  return (
    <>
      <CustomSnackbarContext.Provider
        value={{
          pushSnackbar: (options) => {
            const idx = openedStates.current.indexOf(false);
            openedStates.current[idx] = true;
            setSnackbars((snackbars) =>
              snackbars.map((x, i) =>
                i === idx
                  ? {
                      isClosed: false,
                      ...options,
                      _internalId: Math.random(),
                    }
                  : x,
              ),
            );
          },
        }}
      >
        {children}
      </CustomSnackbarContext.Provider>
      {snackbars.map((options, index) => (
        <SnackbarBody
          openedStates={openedStates}
          key={options._internalId || index}
          options={options}
          index={index}
        />
      ))}
    </>
  );
};
