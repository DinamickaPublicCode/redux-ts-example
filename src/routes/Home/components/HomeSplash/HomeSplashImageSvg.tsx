import React from "react";
import usePublicUrl from "src/utils/usePublicUrl";

const HomeSplashImageSvg = (props: React.SVGProps<SVGSVGElement>) => {
  const publicUrl = usePublicUrl();
  return (
    <svg
      width={1440}
      height={580}
      viewBox="0 0 1440 580"
      fill="none"
      {...props}
    >
      <mask
        id="prefix__a"
        maskUnits="userSpaceOnUse"
        x={0}
        y={33}
        width={1440}
        height={480}
      >
        <path fill="#fff" d="M0 33h1440v480H0z" />
      </mask>
      <g mask="url(#prefix__a)">
        <g filter="url(#prefix__filter0_i)">
          <path
            d="M1344 93c0-33.137 26.86-60 60-60h36v654.593c0 26.51-21.49 48-48 48s-48-21.49-48-48V93z"
            fill="#1A06F9"
          />
        </g>
        <g filter="url(#prefix__filter1_i)">
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M1440 290h-96v1h-82v427h86v-25.314c7.41 16.946 24.32 28.787 44 28.787 26.51 0 48-21.491 48-48V290z"
            fill="#222238"
          />
        </g>
        <g filter="url(#prefix__filter2_i)">
          <path
            d="M1261.5 377c11.36 0 18.61-2.237 29.1-6.584a86.412 86.412 0 0028.06-18.751 86.456 86.456 0 0018.76-28.063A86.63 86.63 0 001344 290.5h-82.5V377z"
            fill="#fff"
          />
        </g>
        <g filter="url(#prefix__filter3_i)">
          <path
            d="M1257.5 204a86.47 86.47 0 0161.16 25.335 86.456 86.456 0 0118.76 28.063A86.63 86.63 0 011344 290.5h-86.5V204z"
            fill="#FF134C"
          />
        </g>
        <g filter="url(#prefix__filter4_i)">
          <circle cx={1392} cy={174} r={35} fill="#fff" />
        </g>
        <g filter="url(#prefix__filter5_i)">
          <circle cx={1392} cy={376} r={35} fill="#fff" />
        </g>
        <g filter="url(#prefix__filter6_i)">
          <circle cx={1392} cy={376} r={15} fill="#1A06F9" />
        </g>
        <circle cx={937.5} cy={174.5} r={35.5} stroke="#fff" strokeWidth={20} />
        <g filter="url(#prefix__filter7_i)">
          <path
            d="M879 126c0-33.137 26.863-60 60-60h36v223h-96V126z"
            fill="#FFC93F"
          />
        </g>
        <g filter="url(#prefix__filter8_i)">
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M879 222c12.703 0 23-10.297 23-23s-10.297-23-23-23-23 10.297-23 23 10.297 23 23 23zm0 22c24.853 0 45-20.147 45-45s-20.147-45-45-45-45 20.147-45 45 20.147 45 45 45z"
            fill="#fff"
          />
        </g>
        <g filter="url(#prefix__filter9_i)">
          <path
            d="M879 154a45.013 45.013 0 00-17.221 3.425 45.03 45.03 0 00-14.599 9.755 45.03 45.03 0 00-9.755 14.599 45.013 45.013 0 000 34.442 45.03 45.03 0 009.755 14.599 45.03 45.03 0 0014.599 9.755A45.013 45.013 0 00879 244v-22.035a22.964 22.964 0 01-21.217-31.753 22.97 22.97 0 0112.429-12.429 22.964 22.964 0 018.788-1.748V154z"
            fill="#FF134C"
          />
        </g>
        <g filter="url(#prefix__filter10_i)">
          <path
            d="M1166 350c0-33.137 26.86-60 60-60h36v379c0 26.51-21.49 48-48 48s-48-21.49-48-48V350z"
            fill="#1A06F9"
          />
        </g>
        <g filter="url(#prefix__filter11_i)">
          <path fill="#222238" d="M1070 470h96v96h-96z" />
        </g>
        <g filter="url(#prefix__filter12_i)">
          <circle cx={1214} cy={424} r={23} fill="#fff" />
        </g>
        <g filter="url(#prefix__filter13_i)">
          <path
            d="M975 289h36c33.14 0 60 26.863 60 60v164h-96V289z"
            fill="#FF134C"
          />
        </g>
        <g filter="url(#prefix__filter14_i)">
          <circle cx={778} cy={291} r={17} fill="#1A06F9" />
        </g>
        <g filter="url(#prefix__filter15_i)">
          <path
            d="M1070.5 471c12.54 0 24.96-2.341 36.55-6.889 11.58-4.548 22.11-11.214 30.98-19.618 8.87-8.404 15.9-18.38 20.7-29.36 4.8-10.98 7.27-22.748 7.27-34.633 0-11.885-2.47-23.653-7.27-34.633-4.8-10.98-11.83-20.956-20.7-29.36s-19.4-15.07-30.98-19.618c-11.59-4.548-24.01-6.889-36.55-6.889v181z"
            fill="#FFC93F"
          />
        </g>
        <g filter="url(#prefix__filter16_i)">
          <path
            d="M1070 109h132c33.14 0 60 26.863 60 60v121h-192V109z"
            fill="#222238"
          />
        </g>
        <circle cx={1071} cy={199} r={91} fill="#fff" />
        <g filter="url(#prefix__filter17_d)">
          <circle cx={1071} cy={199} r={117} fill="#fff" />
        </g>
        <mask
          id="prefix__b"
          maskUnits="userSpaceOnUse"
          x={963}
          y={91}
          width={216}
          height={216}
        >
          <circle cx={1071} cy={199} r={107.043} fill="#E69BF6" />
        </mask>
        <g mask="url(#prefix__b)">
          <path
            transform="matrix(-1 0 0 1 1297.53 -74.828)"
            fill="url(#prefix__pattern0)"
            d="M0 0h343.532v425.681H0z"
          />
        </g>
      </g>
      <g filter="url(#prefix__filter18_i)">
        <path
          d="M879 289h96v238.945c0 26.51-21.49 48-48 48s-48-21.49-48-48V289z"
          fill="#1A06F9"
        />
      </g>
      <g filter="url(#prefix__filter19_i)">
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M1244 16c-9.39 0-17 7.611-17 17s7.61 17 17 17 17-7.611 17-17-7.61-17-17-17zm-33 17c0-18.225 14.77-33 33-33s33 14.775 33 33-14.77 33-33 33-33-14.775-33-33z"
          fill="#FF134C"
        />
      </g>
      <path
        d="M1440 513c0-11.688-2.3-23.261-6.77-34.059a89.056 89.056 0 00-48.17-48.166 88.999 88.999 0 00-96.99 19.293 89.04 89.04 0 00-19.3 28.873A89.045 89.045 0 001262 513h178z"
        fill="#fff"
      />
      <g filter="url(#prefix__filter20_d)">
        <circle cx={1351} cy={513} r={47} fill="#fff" />
      </g>
      <circle cx={1354} cy={501} r={77} fill="#fff" />
      <g filter="url(#prefix__filter21_d)">
        <circle cx={1354} cy={501} r={41} fill="#fff" />
      </g>
      <circle cx={1354} cy={501} r={37} fill="#FFC93F" />
      <mask
        id="prefix__c"
        maskUnits="userSpaceOnUse"
        x={1317}
        y={464}
        width={74}
        height={74}
      >
        <circle cx={1354} cy={501} r={37} fill="#fff" />
      </mask>
      <g mask="url(#prefix__c)">
        <path fill="url(#prefix__pattern1)" d="M1312 478h80v138h-80z" />
      </g>
      <g filter="url(#prefix__filter22_d)">
        <circle cx={865} cy={388} r={117} fill="#fff" />
      </g>
      <mask
        id="prefix__d"
        maskUnits="userSpaceOnUse"
        x={759}
        y={282}
        width={212}
        height={212}
      >
        <circle cx={865} cy={388} r={106} fill="#FFC100" />
      </mask>
      <g mask="url(#prefix__d)">
        <path
          transform="matrix(-1 0 0 1 1246.23 173.699)"
          fill="url(#prefix__pattern2)"
          d="M0 0h534.628v356.419H0z"
        />
      </g>
      <defs>
        <filter
          id="prefix__filter0_i"
          x={1344}
          y={33}
          width={96}
          height={706.593}
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity={0} result="BackgroundImageFix" />
          <feBlend in="SourceGraphic" in2="BackgroundImageFix" result="shape" />
          <feColorMatrix
            in="SourceAlpha"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
            result="hardAlpha"
          />
          <feOffset dy={4} />
          <feGaussianBlur stdDeviation={10} />
          <feComposite in2="hardAlpha" operator="arithmetic" k2={-1} k3={1} />
          <feColorMatrix values="0 0 0 0 0.061277 0 0 0 0 0.0196875 0 0 0 0 0.525 0 0 0 0.3 0" />
          <feBlend in2="shape" result="effect1_innerShadow" />
        </filter>
        <filter
          id="prefix__filter1_i"
          x={1262}
          y={290}
          width={178}
          height={435.473}
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity={0} result="BackgroundImageFix" />
          <feBlend in="SourceGraphic" in2="BackgroundImageFix" result="shape" />
          <feColorMatrix
            in="SourceAlpha"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
            result="hardAlpha"
          />
          <feOffset dy={4} />
          <feGaussianBlur stdDeviation={10} />
          <feComposite in2="hardAlpha" operator="arithmetic" k2={-1} k3={1} />
          <feColorMatrix values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.93 0" />
          <feBlend in2="shape" result="effect1_innerShadow" />
        </filter>
        <filter
          id="prefix__filter2_i"
          x={1261.5}
          y={290.5}
          width={82.5}
          height={90.5}
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity={0} result="BackgroundImageFix" />
          <feBlend in="SourceGraphic" in2="BackgroundImageFix" result="shape" />
          <feColorMatrix
            in="SourceAlpha"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
            result="hardAlpha"
          />
          <feOffset dy={4} />
          <feGaussianBlur stdDeviation={8} />
          <feComposite in2="hardAlpha" operator="arithmetic" k2={-1} k3={1} />
          <feColorMatrix values="0 0 0 0 0.152622 0 0 0 0 0.199227 0 0 0 0 0.245833 0 0 0 0.1 0" />
          <feBlend in2="shape" result="effect1_innerShadow" />
        </filter>
        <filter
          id="prefix__filter3_i"
          x={1257.5}
          y={204}
          width={86.5}
          height={90.5}
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity={0} result="BackgroundImageFix" />
          <feBlend in="SourceGraphic" in2="BackgroundImageFix" result="shape" />
          <feColorMatrix
            in="SourceAlpha"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
            result="hardAlpha"
          />
          <feOffset dy={4} />
          <feGaussianBlur stdDeviation={10} />
          <feComposite in2="hardAlpha" operator="arithmetic" k2={-1} k3={1} />
          <feColorMatrix values="0 0 0 0 0.658333 0 0 0 0 0.049375 0 0 0 0 0.196454 0 0 0 0.31 0" />
          <feBlend in2="shape" result="effect1_innerShadow" />
        </filter>
        <filter
          id="prefix__filter4_i"
          x={1357}
          y={139}
          width={70}
          height={74}
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity={0} result="BackgroundImageFix" />
          <feBlend in="SourceGraphic" in2="BackgroundImageFix" result="shape" />
          <feColorMatrix
            in="SourceAlpha"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
            result="hardAlpha"
          />
          <feOffset dy={4} />
          <feGaussianBlur stdDeviation={8} />
          <feComposite in2="hardAlpha" operator="arithmetic" k2={-1} k3={1} />
          <feColorMatrix values="0 0 0 0 0.152622 0 0 0 0 0.199227 0 0 0 0 0.245833 0 0 0 0.1 0" />
          <feBlend in2="shape" result="effect1_innerShadow" />
        </filter>
        <filter
          id="prefix__filter5_i"
          x={1357}
          y={341}
          width={70}
          height={74}
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity={0} result="BackgroundImageFix" />
          <feBlend in="SourceGraphic" in2="BackgroundImageFix" result="shape" />
          <feColorMatrix
            in="SourceAlpha"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
            result="hardAlpha"
          />
          <feOffset dy={4} />
          <feGaussianBlur stdDeviation={8} />
          <feComposite in2="hardAlpha" operator="arithmetic" k2={-1} k3={1} />
          <feColorMatrix values="0 0 0 0 0.152622 0 0 0 0 0.199227 0 0 0 0 0.245833 0 0 0 0.1 0" />
          <feBlend in2="shape" result="effect1_innerShadow" />
        </filter>
        <filter
          id="prefix__filter6_i"
          x={1377}
          y={361}
          width={30}
          height={34}
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity={0} result="BackgroundImageFix" />
          <feBlend in="SourceGraphic" in2="BackgroundImageFix" result="shape" />
          <feColorMatrix
            in="SourceAlpha"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
            result="hardAlpha"
          />
          <feOffset dy={4} />
          <feGaussianBlur stdDeviation={10} />
          <feComposite in2="hardAlpha" operator="arithmetic" k2={-1} k3={1} />
          <feColorMatrix values="0 0 0 0 0.061277 0 0 0 0 0.0196875 0 0 0 0 0.525 0 0 0 0.3 0" />
          <feBlend in2="shape" result="effect1_innerShadow" />
        </filter>
        <filter
          id="prefix__filter7_i"
          x={879}
          y={66}
          width={96}
          height={227}
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity={0} result="BackgroundImageFix" />
          <feBlend in="SourceGraphic" in2="BackgroundImageFix" result="shape" />
          <feColorMatrix
            in="SourceAlpha"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
            result="hardAlpha"
          />
          <feOffset dy={4} />
          <feGaussianBlur stdDeviation={10} />
          <feComposite in2="hardAlpha" operator="arithmetic" k2={-1} k3={1} />
          <feColorMatrix values="0 0 0 0 0.645833 0 0 0 0 0.492196 0 0 0 0 0.0995659 0 0 0 0.34 0" />
          <feBlend in2="shape" result="effect1_innerShadow" />
        </filter>
        <filter
          id="prefix__filter8_i"
          x={834}
          y={154}
          width={90}
          height={94}
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity={0} result="BackgroundImageFix" />
          <feBlend in="SourceGraphic" in2="BackgroundImageFix" result="shape" />
          <feColorMatrix
            in="SourceAlpha"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
            result="hardAlpha"
          />
          <feOffset dy={4} />
          <feGaussianBlur stdDeviation={8} />
          <feComposite in2="hardAlpha" operator="arithmetic" k2={-1} k3={1} />
          <feColorMatrix values="0 0 0 0 0.152622 0 0 0 0 0.199227 0 0 0 0 0.245833 0 0 0 0.1 0" />
          <feBlend in2="shape" result="effect1_innerShadow" />
        </filter>
        <filter
          id="prefix__filter9_i"
          x={834}
          y={154}
          width={45}
          height={94}
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity={0} result="BackgroundImageFix" />
          <feBlend in="SourceGraphic" in2="BackgroundImageFix" result="shape" />
          <feColorMatrix
            in="SourceAlpha"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
            result="hardAlpha"
          />
          <feOffset dy={4} />
          <feGaussianBlur stdDeviation={10} />
          <feComposite in2="hardAlpha" operator="arithmetic" k2={-1} k3={1} />
          <feColorMatrix values="0 0 0 0 0.658333 0 0 0 0 0.049375 0 0 0 0 0.196454 0 0 0 0.31 0" />
          <feBlend in2="shape" result="effect1_innerShadow" />
        </filter>
        <filter
          id="prefix__filter10_i"
          x={1166}
          y={290}
          width={96}
          height={431}
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity={0} result="BackgroundImageFix" />
          <feBlend in="SourceGraphic" in2="BackgroundImageFix" result="shape" />
          <feColorMatrix
            in="SourceAlpha"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
            result="hardAlpha"
          />
          <feOffset dy={4} />
          <feGaussianBlur stdDeviation={10} />
          <feComposite in2="hardAlpha" operator="arithmetic" k2={-1} k3={1} />
          <feColorMatrix values="0 0 0 0 0.061277 0 0 0 0 0.0196875 0 0 0 0 0.525 0 0 0 0.3 0" />
          <feBlend in2="shape" result="effect1_innerShadow" />
        </filter>
        <filter
          id="prefix__filter11_i"
          x={1070}
          y={470}
          width={96}
          height={100}
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity={0} result="BackgroundImageFix" />
          <feBlend in="SourceGraphic" in2="BackgroundImageFix" result="shape" />
          <feColorMatrix
            in="SourceAlpha"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
            result="hardAlpha"
          />
          <feOffset dy={4} />
          <feGaussianBlur stdDeviation={10} />
          <feComposite in2="hardAlpha" operator="arithmetic" k2={-1} k3={1} />
          <feColorMatrix values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.93 0" />
          <feBlend in2="shape" result="effect1_innerShadow" />
        </filter>
        <filter
          id="prefix__filter12_i"
          x={1191}
          y={401}
          width={46}
          height={50}
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity={0} result="BackgroundImageFix" />
          <feBlend in="SourceGraphic" in2="BackgroundImageFix" result="shape" />
          <feColorMatrix
            in="SourceAlpha"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
            result="hardAlpha"
          />
          <feOffset dy={4} />
          <feGaussianBlur stdDeviation={8} />
          <feComposite in2="hardAlpha" operator="arithmetic" k2={-1} k3={1} />
          <feColorMatrix values="0 0 0 0 0.152622 0 0 0 0 0.199227 0 0 0 0 0.245833 0 0 0 0.1 0" />
          <feBlend in2="shape" result="effect1_innerShadow" />
        </filter>
        <filter
          id="prefix__filter13_i"
          x={975}
          y={289}
          width={96}
          height={228}
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity={0} result="BackgroundImageFix" />
          <feBlend in="SourceGraphic" in2="BackgroundImageFix" result="shape" />
          <feColorMatrix
            in="SourceAlpha"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
            result="hardAlpha"
          />
          <feOffset dy={4} />
          <feGaussianBlur stdDeviation={10} />
          <feComposite in2="hardAlpha" operator="arithmetic" k2={-1} k3={1} />
          <feColorMatrix values="0 0 0 0 0.658333 0 0 0 0 0.049375 0 0 0 0 0.196454 0 0 0 0.31 0" />
          <feBlend in2="shape" result="effect1_innerShadow" />
        </filter>
        <filter
          id="prefix__filter14_i"
          x={761}
          y={274}
          width={34}
          height={38}
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity={0} result="BackgroundImageFix" />
          <feBlend in="SourceGraphic" in2="BackgroundImageFix" result="shape" />
          <feColorMatrix
            in="SourceAlpha"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
            result="hardAlpha"
          />
          <feOffset dy={4} />
          <feGaussianBlur stdDeviation={10} />
          <feComposite in2="hardAlpha" operator="arithmetic" k2={-1} k3={1} />
          <feColorMatrix values="0 0 0 0 0.061277 0 0 0 0 0.0196875 0 0 0 0 0.525 0 0 0 0.3 0" />
          <feBlend in2="shape" result="effect1_innerShadow" />
        </filter>
        <filter
          id="prefix__filter15_i"
          x={1070.5}
          y={290}
          width={95.5}
          height={185}
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity={0} result="BackgroundImageFix" />
          <feBlend in="SourceGraphic" in2="BackgroundImageFix" result="shape" />
          <feColorMatrix
            in="SourceAlpha"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
            result="hardAlpha"
          />
          <feOffset dy={4} />
          <feGaussianBlur stdDeviation={10} />
          <feComposite in2="hardAlpha" operator="arithmetic" k2={-1} k3={1} />
          <feColorMatrix values="0 0 0 0 0.645833 0 0 0 0 0.492196 0 0 0 0 0.0995659 0 0 0 0.34 0" />
          <feBlend in2="shape" result="effect1_innerShadow" />
        </filter>
        <filter
          id="prefix__filter16_i"
          x={1070}
          y={109}
          width={192}
          height={185}
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity={0} result="BackgroundImageFix" />
          <feBlend in="SourceGraphic" in2="BackgroundImageFix" result="shape" />
          <feColorMatrix
            in="SourceAlpha"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
            result="hardAlpha"
          />
          <feOffset dy={4} />
          <feGaussianBlur stdDeviation={10} />
          <feComposite in2="hardAlpha" operator="arithmetic" k2={-1} k3={1} />
          <feColorMatrix values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.93 0" />
          <feBlend in2="shape" result="effect1_innerShadow" />
        </filter>
        <filter
          id="prefix__filter17_d"
          x={938}
          y={70}
          width={266}
          height={266}
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity={0} result="BackgroundImageFix" />
          <feColorMatrix
            in="SourceAlpha"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
          />
          <feOffset dy={4} />
          <feGaussianBlur stdDeviation={8} />
          <feColorMatrix values="0 0 0 0 0.0406829 0 0 0 0 0.0140625 0 0 0 0 0.3375 0 0 0 0.15 0" />
          <feBlend in2="BackgroundImageFix" result="effect1_dropShadow" />
          <feBlend in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
        </filter>
        <filter
          id="prefix__filter18_i"
          x={879}
          y={289}
          width={96}
          height={290.945}
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity={0} result="BackgroundImageFix" />
          <feBlend in="SourceGraphic" in2="BackgroundImageFix" result="shape" />
          <feColorMatrix
            in="SourceAlpha"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
            result="hardAlpha"
          />
          <feOffset dy={4} />
          <feGaussianBlur stdDeviation={10} />
          <feComposite in2="hardAlpha" operator="arithmetic" k2={-1} k3={1} />
          <feColorMatrix values="0 0 0 0 0.061277 0 0 0 0 0.0196875 0 0 0 0 0.525 0 0 0 0.3 0" />
          <feBlend in2="shape" result="effect1_innerShadow" />
        </filter>
        <filter
          id="prefix__filter19_i"
          x={1211}
          y={0}
          width={66}
          height={70}
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity={0} result="BackgroundImageFix" />
          <feBlend in="SourceGraphic" in2="BackgroundImageFix" result="shape" />
          <feColorMatrix
            in="SourceAlpha"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
            result="hardAlpha"
          />
          <feOffset dy={4} />
          <feGaussianBlur stdDeviation={10} />
          <feComposite in2="hardAlpha" operator="arithmetic" k2={-1} k3={1} />
          <feColorMatrix values="0 0 0 0 0.658333 0 0 0 0 0.049375 0 0 0 0 0.196454 0 0 0 0.31 0" />
          <feBlend in2="shape" result="effect1_innerShadow" />
        </filter>
        <filter
          id="prefix__filter20_d"
          x={1288}
          y={454}
          width={126}
          height={126}
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity={0} result="BackgroundImageFix" />
          <feColorMatrix
            in="SourceAlpha"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
          />
          <feOffset dy={4} />
          <feGaussianBlur stdDeviation={8} />
          <feColorMatrix values="0 0 0 0 0.0406829 0 0 0 0 0.0140625 0 0 0 0 0.3375 0 0 0 0.15 0" />
          <feBlend in2="BackgroundImageFix" result="effect1_dropShadow" />
          <feBlend in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
        </filter>
        <filter
          id="prefix__filter21_d"
          x={1297}
          y={448}
          width={114}
          height={114}
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity={0} result="BackgroundImageFix" />
          <feColorMatrix
            in="SourceAlpha"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
          />
          <feOffset dy={4} />
          <feGaussianBlur stdDeviation={8} />
          <feColorMatrix values="0 0 0 0 0.0406829 0 0 0 0 0.0140625 0 0 0 0 0.3375 0 0 0 0.15 0" />
          <feBlend in2="BackgroundImageFix" result="effect1_dropShadow" />
          <feBlend in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
        </filter>
        <filter
          id="prefix__filter22_d"
          x={732}
          y={259}
          width={266}
          height={266}
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity={0} result="BackgroundImageFix" />
          <feColorMatrix
            in="SourceAlpha"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
          />
          <feOffset dy={4} />
          <feGaussianBlur stdDeviation={8} />
          <feColorMatrix values="0 0 0 0 0.0406829 0 0 0 0 0.0140625 0 0 0 0 0.3375 0 0 0 0.15 0" />
          <feBlend in2="BackgroundImageFix" result="effect1_dropShadow" />
          <feBlend in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
        </filter>
        <pattern
          id="prefix__pattern0"
          patternContentUnits="objectBoundingBox"
          width={1}
          height={1}
        >
          <use
            xlinkHref="#prefix__image0"
            transform="matrix(.00206 0 0 .00166 0 0)"
          />
        </pattern>
        <pattern
          id="prefix__pattern1"
          patternContentUnits="objectBoundingBox"
          width={1}
          height={1}
        >
          <use
            xlinkHref="#prefix__image1"
            transform="matrix(.00325 0 0 .00188 0 -.004)"
          />
        </pattern>
        <pattern
          id="prefix__pattern2"
          patternContentUnits="objectBoundingBox"
          width={1}
          height={1}
        >
          <use
            xlinkHref="#prefix__image2"
            transform="matrix(.00053 0 0 .0008 0 0)"
          />
        </pattern>
        <image
          id="prefix__image0"
          width={485}
          height={602}
          xlinkHref={publicUrl("images/home/image_0.png")}
        />
        <image
          id="prefix__image1"
          width={308}
          height={536}
          xlinkHref={publicUrl("images/home/image_2.png")}
        />
        <image
          id="prefix__image2"
          width={1880}
          height={1253}
          xlinkHref={publicUrl("images/home/image_1.png")}
        />
      </defs>
    </svg>
  );
};
export default HomeSplashImageSvg;
