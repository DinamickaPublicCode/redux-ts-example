import React, {useEffect, useMemo, useState} from "react";
import Box from "@mui/material/Box";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import Grid from "@mui/material/Grid";
import {CarDealerSeller} from "src/types";
import {LoadingButton} from "@mui/lab";
import {SellersListEntry} from "src/routes/SalesFunnel/types/apiDataProvider";
import useCustomErrorSnackbar from "src/utils/useCustomErrorSnackbar";
import useCustomInfoSnackbar from "src/utils/useCustomInfoSnackbar";
import Transition from "../../../../components/Transition";
import MonthPicker from "../../../../components/MonthPicker";
import {DialogBody} from "../../../../components/DialogBody";
import {usePrecalculatedSales} from "../../utils/usePrecalculatedSales";
import {CarFilter} from "../../types/salesFunnelContext";
import {getSellerRoleFromCarFilter} from "../../utils/getSellerRoleFromCarFilter";
import {useCommonModalStyles} from "../../../../utils/useCommonModalStyles";
import {useSetGoalsModalTranslation} from "../../utils/useSetGoalsModalTranslation";
import SetGoalsModalDialogTitle from "./SetGoalsModalDialogTitle";
import SetGoalsSellerEntry, {
  columnSizes,
  columnSpacing,
  EditGoalEntry,
} from "./SetGoalsSellerEntry";
import {SetGoalsModalTableHeader} from "./SetGoalsModalTableHeader";
import {SetGoalsSearchField} from "./SetGoalsSearchField";
import {
  useGetCarDealerSellersQuery,
  useGetTopSellersQuery,
  useGetCarDealerSalesNewQuery,
  useGetCarDealerSalesUsedQuery,
} from 'src/state/reducers/carDealerApi';

const extractSellers = (sellers?: SellersListEntry[]) =>
  sellers?.map((seller) => seller);

interface SetGoalsDialogProps {
  isOpen: boolean;
  onIsOpenChanged: (val: boolean) => void;
  initialDate: Date;
  newCar: boolean;
  idCarDealer: string | undefined;
}

const SetGoalsDialog: React.FC<SetGoalsDialogProps> = ({
  isOpen,
  onIsOpenChanged,
  initialDate,
  newCar,
  idCarDealer,
}) => {
  const styles = useCommonModalStyles();
  const {t} = useSetGoalsModalTranslation();
  const handleClose = () => onIsOpenChanged(false);

  const [isSubmitting, setIsSubmitting] = useState(false);

  /**
   * We keep the local date types, but it gets referenced and updated based on a current context value
   */
  const [date, setDate] = useState(initialDate);

  const {sellersList} = useSellersListFromCarDealer(date, idCarDealer, newCar);

  const [searchQuery, setSearchQuery] = useState("");

  const [filteredSellers, setFilteredSellers] = useState(() =>
    extractSellers(sellersList),
  );

  const sellerIdMap = useMemo(() => {
    const theMap = new Map<string, CarDealerSeller>();
    sellersList?.forEach((entry) => {
      theMap.set(entry.seller.id, entry.seller);
    });
    return theMap;
  }, [sellersList]);

  const [currentlyEditedGoals, setCurrentlyEditedGoals] = useState<
    EditGoalEntry[]
  >([]);

  const onSearchSubmit = (value: string) => {
    if (value.length === 0) {
      setFilteredSellers(extractSellers(sellersList));
      return;
    }
    setFilteredSellers(
      sellersList?.filter((x) =>
        x.seller.fullName.toLowerCase().includes(value.toLowerCase()),
      ),
    );
  };

  const year = date.getFullYear();

  const enqueueErrorSnackbar = useCustomErrorSnackbar();
  const enqueueInfoSnackbar = useCustomInfoSnackbar();

  const onSubmit = async () => {
    if (currentlyEditedGoals.length === 0 || isSubmitting) {
      enqueueErrorSnackbar("Nothing was modified");
      return;
    }
    console.log({
      currentlyEditedGoals,
    });
    setIsSubmitting(true);

    const usersWhichAreUpdated = currentlyEditedGoals
      .map((entry) => ({
        seller: sellerIdMap.get(entry.sellerId) as CarDealerSeller,
        entry,
      }))
      .map(
        ({seller, entry}) =>
          ({
            id: seller.id,
            fullName: seller.fullName,
            email: seller.email,
            orderNew: seller.orderNew,
            orderUsed: seller.orderUsed,
            goals: [
              {
                year: "" + year,
                month: "" + entry.month,
                goal: "" + entry.value,
                newCar,
              },
            ],
          } as CarDealerSeller),
      );
    try {
      const updatedSellers = [...usersWhichAreUpdated];
      console.log({
        updatedSellers,
      });
      enqueueInfoSnackbar("Success");
      handleClose();
      setIsSubmitting(false);
    } catch (e) {}
  };

  /**
   * When there are updated incoming props, reset the search field and filtered results.
   */
  useEffect(() => {
    setFilteredSellers(extractSellers(sellersList));
    setSearchQuery("");
  }, [sellersList]);

  useEffect(() => {
    setDate(initialDate);
  }, [initialDate]);

  return (
    <Dialog
      open={isOpen}
      TransitionComponent={Transition}
      keepMounted
      maxWidth={"md"}
      onClose={handleClose}
      classes={{
        paper: styles.dialogPaper,
      }}
      aria-labelledby="alert-dialog-archive-title"
      aria-describedby="alert-dialog-archive-description"
    >
      <DialogBody>
        <SetGoalsModalDialogTitle
          date={date}
          setDate={setDate}
          handleClose={handleClose}
        />
        <Box marginTop="33px" paddingLeft="30px" paddingRight="30px">
          <Box marginBottom="20px">
            <SetGoalsModalTableHeader />
          </Box>
          <Grid container spacing={columnSpacing}>
            <Grid item xs={columnSizes[0]}>
              <SetGoalsSearchField
                value={searchQuery}
                onChange={(value) => {
                  onSearchSubmit(searchQuery);
                  setSearchQuery(value);
                  if (value.length === 0) {
                    onSearchSubmit("");
                  }
                }}
                onSubmit={(value) => {
                  onSearchSubmit(searchQuery);
                  setSearchQuery(value);
                  if (value.length === 0) {
                    onSearchSubmit("");
                  }
                }}
              />
            </Grid>
          </Grid>
          <Box
            maxHeight="200px"
            marginTop="19px"
            style={{
              overflowX: "hidden",
              overflowY: "auto",
            }}
          >
            {filteredSellers?.map((seller) => (
              <SetGoalsSellerEntry
                key={seller.seller.id}
                onValueChanged={(entry) => {
                  if (
                    currentlyEditedGoals.filter(
                      (g) => g.sellerId === entry.sellerId,
                    ).length === 0
                  ) {
                    currentlyEditedGoals.push(entry);
                  }
                  const newGoals = currentlyEditedGoals
                    .map((g) =>
                      g.sellerId === entry.sellerId
                        ? entry.value.length === 0
                          ? null
                          : entry
                        : g,
                    )
                    .filter((g) => g !== null) as EditGoalEntry[];
                  setCurrentlyEditedGoals(newGoals);
                }}
                newCar={newCar}
                date={date}
                sellersListEntry={seller}
              />
            ))}
          </Box>
        </Box>
        <DialogActions className={styles.dialogActions}>
          <div className={styles.monthPickerDown}>
            <MonthPicker date={date} onDateChanged={setDate} />
          </div>
          <LoadingButton
            style={{
              textTransform: "uppercase",
              padding: "7px 60px",
            }}
            variant="contained"
            onClick={onSubmit}
            disabled={isSubmitting}
          >
            {t(`Save Goal`)}
          </LoadingButton>
        </DialogActions>
      </DialogBody>
    </Dialog>
  );
};

const useSellersListFromCarDealer = (
  date: Date,
  idCarDealer: string | undefined,
  newCar: boolean,
) => {
  const carType: CarFilter = newCar ? "NEW" : "USED";

  const {data:topSellers} = useGetTopSellersQuery();
  const {data:sellersAll} = useGetCarDealerSellersQuery();

  const {data:salesNew} = useGetCarDealerSalesNewQuery();
  const {data:salesUsed} = useGetCarDealerSalesUsedQuery();

  const role = getSellerRoleFromCarFilter(carType);

  const sellers = useMemo(() => {
    return (sellersAll || []).filter((el: CarDealerSeller) =>
      el.authorities?.includes(role),
    );
  }, [sellersAll, role]);

  const sales = useMemo(
    () => (carType === "NEW" ? salesNew : salesUsed),
    [carType, salesNew, salesUsed],
  );

  const {sellersList} = usePrecalculatedSales(
    sellers,
    sales?.content,
    topSellers,
    date.getMonth() + 1,
  );

  return {sellersList};
};

export default SetGoalsDialog;
