import React from "react";
import ActivityBoxTabsBlock, {useTabItems} from "./ActivityBoxTabsBlock";
import MessageList from "./ActivityBoxMessageList";
import {ActivityBoxType} from "src/types/activityBoxInterfaces";
import {Theme} from "@mui/material/styles";
import {makeStyles} from "@mui/styles";
import Box from "@mui/material/Box";

const ActivityBoxLayout = ({
  shouldRender,
  type,
}: {
  type: ActivityBoxType;
  shouldRender: boolean;
}) => {
  const tabsBlockHandlers = useTabItems(0);
  const {currentIndex} = tabsBlockHandlers;

  return (
    <Box>
      {shouldRender && <ActivityBoxTabsBlock {...tabsBlockHandlers} />}
      <MessageList
        type={type}
        // This one supposendly should ease up on sluggishness
        // instead of blatantly returning null. But it appears that
        // the slowdown comes from re-rendering content itself
        style={{
          display: shouldRender ? "block" : "none",
        }}
        tabIndex={currentIndex}
      />
    </Box>
  );
};

// TODO: Reference colors from the theme
export const useActivityBoxStyle = makeStyles((theme: Theme) => ({
  activityBoxTitleBlock: {
    paddingLeft: 0,
    paddingRight: 30,
    paddingTop: 28,
    paddingBottom: 20,
    backgroundColor: "rgb(246, 248, 250)",
  },
  title: {
    fontWeight: 600,
    fontSize: 13,
    marginLeft: 24,
    letterSpacing: "0.5px",
    textTransform: "uppercase",
    color: "#98A2B7",
  },
  tabsBlock: {
    width: "100%",
    height: 46,
    backgroundColor: theme.palette.common.white,
    padding: "9px 0",
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    [theme.breakpoints.down("sm")]: {
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      height: 200,
    },
  },
  tabItem: {
    cursor: "pointer",
    borderRadius: "30px",
    textAlign: "center",
    fontStyle: "normal",
    fontWeight: 600,
    fontSize: "14px",
    lineHeight: "120%",
    padding: "9px 18px",
    marginLeft: 10,
  },
  tabItemSelected: {
    background: "#F1F0FF",
    color: theme.palette.primary.main,
  },
}));

export default ActivityBoxLayout;
