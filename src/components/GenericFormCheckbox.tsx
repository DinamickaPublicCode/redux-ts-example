import Box from "@mui/material/Box";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import {CheckboxCheckedIcon, CheckboxIcon} from "../assets/icons";
import React from "react";

export const GenericFormCheckbox = ({
  disabled,
  label,
  marginBottom,
  onChange,
  checked,
}: {
  checked?: boolean;
  disabled?: boolean;
  label: string;
  marginBottom?: string;
  onChange?: (checked: boolean) => void;
}) => {
  marginBottom = marginBottom || "8px";
  return (
    <Box marginBottom={marginBottom}>
      <FormControlLabel
        label={label}
        control={
          <Checkbox
            checked={checked}
            disabled={disabled}
            onChange={(_, checked) => {
              if (onChange) {
                onChange(checked);
              }
            }}
            icon={<CheckboxIcon />}
            checkedIcon={<CheckboxCheckedIcon />}
            name={label.toLowerCase()}
            color="primary"
          />
        }
      />
    </Box>
  );
};
