import React, {SetStateAction, useState, Dispatch} from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import IconButton from "@mui/material/IconButton";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";
import {
  BoxIcon,
  LikeIcon,
  MessageIcon,
  PhoneCallIcon,
  TimeIcon,
  TruckIcon,
  UserIcon,
  VehicleIcon,
} from "src/assets/icons";
import SalesCalendarCarItem from "./SalesCalendarCarItem";
import dayjs from "dayjs";
import {useTranslation} from "react-i18next";
import {CarDealerSale} from "src/types";
import ConfirmationDialog from "src/components/ConfirmationDialog";
import useCustomInfoSnackbar from "src/utils/useCustomInfoSnackbar";

interface SalesCalendarCarInfoProps {
  currentCar: CarDealerSale;
  openModal: (open: boolean) => void;
  setOpenStockWithChat: Dispatch<SetStateAction<boolean>>;
}

const SalesCalendarCarInfo: React.FC<SalesCalendarCarInfoProps> = ({
  currentCar,
  openModal,
  setOpenStockWithChat,
}) => {
  const {t} = useTranslation();
  const enqueueInfoSnackbar = useCustomInfoSnackbar();

  const [confirmationOpen, setConfirmationOpen] = useState<boolean>(false);

  const handleDeliveryDone = async () => {
    try {
      enqueueInfoSnackbar("Success");
    } catch (e) {
      console.log(e);
    } finally {
      setConfirmationOpen(false);
    }
  };

  return (
    <Paper>
      <div>
        <Typography
          variant="h4"
          style={{
            fontWeight: 700,
            fontSize: 28,
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          {currentCar.generalInfo.make}
          <IconButton onClick={() => setOpenStockWithChat(true)} size="large">
            <MessageIcon />
          </IconButton>
        </Typography>
        <Typography
          style={{
            color: "#98A2B7",
            marginBottom: 20,
          }}
        >
          {currentCar.generalInfo.model}
        </Typography>
        {currentCar.limitPreparationDate && (
          <SalesCalendarCarItem
            icon={<TimeIcon />}
            title={t("CarInfo.Preparation Date")}
            descr={
              dayjs(currentCar.limitPreparationDate).format("DD-MM-YYYY") ?? ""
            }
          />
        )}
        {(currentCar.sales[0].deliveryInfo?.deliveryBeginDate ||
          currentCar.sales[0].deliveryInfo?.deliveryEndDate) && (
          <SalesCalendarCarItem
            icon={<TruckIcon />}
            title={t("CarInfo.Delivery Date")}
            descr={`${
              currentCar.sales[0].deliveryInfo?.deliveryBeginDate
                ? dayjs(
                    currentCar.sales[0].deliveryInfo?.deliveryBeginDate ?? "",
                  ).format("DD-MM-YYYY hh:mm")
                : "date don't exist"
            } : ${
              currentCar.sales[0].deliveryInfo?.deliveryEndDate
                ? dayjs(
                    currentCar.sales[0].deliveryInfo?.deliveryEndDate,
                  ).format("DD-MM-YYYY hh:mm")
                : "date don't exist"
            }`}
          />
        )}
        <SalesCalendarCarItem
          icon={<VehicleIcon />}
          title={t("CarInfo.Stock")}
          descr={currentCar.referenceStock}
        />
        <SalesCalendarCarItem
          icon={<BoxIcon />}
          title={t("CarInfo.Sales Refrence")}
          descr={currentCar.sales[0].saleReference}
        />
        <SalesCalendarCarItem
          icon={<LikeIcon />}
          title={t("CarInfo.Sellers Name")}
          descr={currentCar.sales[0].sellerInfo.nameSeller}
        />
        {currentCar.sales[0].clientInfo.fullName && (
          <SalesCalendarCarItem
            icon={<UserIcon />}
            title={t("CarInfo.Client Name")}
            descr={currentCar.sales[0].clientInfo.fullName}
          />
        )}
        {currentCar.sales[0].clientInfo.telNumber1 && (
          <SalesCalendarCarItem
            icon={<PhoneCallIcon />}
            title={t("CarInfo.Client Phone")}
            descr={currentCar.sales[0].clientInfo.telNumber1}
          />
        )}
      </div>
      <Box display="flex" justifyContent="space-between">
        <Button
          style={{
            padding: "7px 20px",
            width: "100%",
            marginRight: 15,
            boxShadow: "0px 3px 14px rgba(2, 53, 236, 0.3)",
            fontWeight: 600,
            fontSize: 14,
            textTransform: "uppercase",
          }}
          variant={"contained"}
          onClick={() => setConfirmationOpen(true)}
        >
          {t("buttonNames.DeliveryDone")}
        </Button>
        <Button
          style={{
            padding: "7px 20px",
            width: "100%",
            marginLeft: 15,
            backgroundColor: "#fff",
            color: "#1A06F9",
            boxShadow: "0px 4px 20px rgba(203, 205, 217, 0.3)",
            fontWeight: 600,
            fontSize: 14,
            textTransform: "uppercase",
          }}
          variant={"contained"}
          onClick={() => openModal(true)}
        >
          {t("buttonNames.Reschedule")}
        </Button>
      </Box>

      <ConfirmationDialog
        open={confirmationOpen}
        handleClose={() => setConfirmationOpen(false)}
        handleRequest={handleDeliveryDone}
      />
    </Paper>
  );
};

export default SalesCalendarCarInfo;
