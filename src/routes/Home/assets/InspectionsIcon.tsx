import React from "react";

const InspectionsIcon = (color: string) => (
  <svg width={86} height={86} viewBox="0 0 86 86" fill="none">
    <g filter="url(#prefix__filter0_d)">
      <rect x={20} y={18} width={46} height={46} rx={23} fill="#fff" />
      <rect x={20.5} y={18.5} width={45} height={45} rx={22.5} stroke={color} />
    </g>
    <path fill="#fff" d="M33 31h20v20H33z" />
    <path
      d="M39.172 35.96h7.656a1.76 1.76 0 001.758-1.757v-1.445A1.76 1.76 0 0046.828 31h-7.656a1.76 1.76 0 00-1.758 1.758v1.445c0 .97.789 1.758 1.758 1.758z"
      fill={color}
    />
    <path
      d="M49.52 33.5v.579c0 1.627-1.268 2.951-2.827 2.951h-7.386c-1.56 0-2.827-1.324-2.827-2.951V33.5c-.601.544-.98 1.345-.98 2.231V48.05c0 1.627 1.268 2.951 2.827 2.951h9.346c1.559 0 2.827-1.324 2.827-2.951V35.73c0-.886-.379-1.688-.98-2.231zm-9.075 12.43a.607.607 0 01-.132.824.547.547 0 01-.788-.137l-.294-.43-.294.43a.547.547 0 01-.789.137.607.607 0 01-.13-.823l.518-.759-.519-.759a.607.607 0 01.132-.823.55.55 0 01.788.137l.294.43.294-.43a.55.55 0 01.788-.137c.255.19.313.558.132.823l-.52.759.52.759zm.577-5.885l-1.508 1.885a.556.556 0 01-.499.208.567.567 0 01-.44-.322l-.603-1.257a.603.603 0 01.252-.792.554.554 0 01.759.263l.216.452.955-1.194a.55.55 0 01.796-.075c.24.21.272.582.072.832zm6.5 5.767h-3.316a.578.578 0 01-.565-.59c0-.326.253-.59.565-.59h3.317c.312 0 .565.264.565.59 0 .326-.253.59-.565.59zm0-4.613h-3.316a.578.578 0 01-.565-.59c0-.326.253-.59.565-.59h3.317c.312 0 .565.264.565.59 0 .326-.253.59-.565.59z"
      fill={color}
    />
    <defs>
      <filter
        id="prefix__filter0_d"
        x={0}
        y={0}
        width={86}
        height={86}
        filterUnits="userSpaceOnUse"
        colorInterpolationFilters="sRGB"
      >
        <feFlood floodOpacity={0} result="BackgroundImageFix" />
        <feColorMatrix
          in="SourceAlpha"
          values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
        />
        <feOffset dy={2} />
        <feGaussianBlur stdDeviation={10} />
        <feColorMatrix values="0 0 0 0 0.796875 0 0 0 0 0.804464 0 0 0 0 0.85 0 0 0 0.5 0" />
        <feBlend in2="BackgroundImageFix" result="effect1_dropShadow" />
        <feBlend in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
      </filter>
    </defs>
  </svg>
);
export default InspectionsIcon;
