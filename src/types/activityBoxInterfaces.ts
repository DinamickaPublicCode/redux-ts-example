export enum ActivityBoxMessageType {
  CHAT = "CHAT",
  NOTE = "NOTE",
  ACTION = "ACTION",
}

export type ActivityBoxApiMessage = {
  date: string;
  labelDate: string;
  idUser: string;
  userName: string;
  id: string;
  message: string;
  title: string;
  type: string;
  idInventory: string;
};

export type ActivityBoxMessage = {
  id: string;
  type: ActivityBoxMessageType;
  date: Date;
  dateDisplay: string;
  intentoryId: string;
  user: {
    id: string;
    name: string;
  };
  message: {
    title: string;
    text: string;
  };
};

export type MessageFilter = (message: ActivityBoxMessage) => boolean;

export type LoadingStateChangeCallback = (isLoading: boolean) => void;

export type TabDefinition = {
  localeId: string;
  filter: MessageFilter;
  sendMessageType: null | ActivityBoxMessageType;
};

export type ActivityBoxBaseContextProps = {
  tabs: TabDefinition[];
  onRequestUserAvatar: (
    userId: string,
    userName: string,
    onAvatarUpdateUrl: (url: string) => void,
  ) => void;
  onMessageSend: (
    type: ActivityBoxMessageType,
    message: string,
    onLoadingStateChange: LoadingStateChangeCallback,
  ) => void;
  onRequestMoreMessages: (
    onLoadingStateChange: LoadingStateChangeCallback,
  ) => void;
};

export interface ActivityBoxProviderProps extends ActivityBoxBaseContextProps {
  apiMessages: ActivityBoxApiMessage[];
  shouldRender: boolean;
  type?: ActivityBoxType;
}

export interface ActivityBoxContextData extends ActivityBoxBaseContextProps {
  messages: ActivityBoxMessage[];
}

export enum ActivityBoxType {
  POPUP = 1,
  MODAL = 2,
}
