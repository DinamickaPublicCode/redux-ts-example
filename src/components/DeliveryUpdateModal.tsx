import React, {useEffect, useState} from "react";
import {useTranslation} from "react-i18next";
import * as yup from "yup";
import {Formik} from "formik";
import Box from "@mui/material/Box";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import CartagInputField from "./CartagInputField";
import StockInputFieldBody from "./StockDetailModal/StockInputFieldBody";
import {FormSubtitle} from "./StockDetailModal/StockDeliveryTab";
import Transition from "./Transition";
import {DialogBody} from "./DialogBody";
import {CarDealerSale} from "src/types";
import CartagBackdrop from "./CartagBackdrop";
import {LoadingButton} from "@mui/lab";
import useCustomErrorSnackbar from "src/utils/useCustomErrorSnackbar";
import useCustomInfoSnackbar from "src/utils/useCustomInfoSnackbar";
import CartagDatePicker from "./CartagDatePicker";
import GenericFormModalTitle from "./GenericFormModalTitle";
import {useCommonModalStyles} from "../utils/useCommonModalStyles";
import StockCarInfoReadiness from "./StockCarInfoReadiness";
import useDeliveryModalTranslation from "../utils/useDeliveryModalTranslation";
import {useAppSelector} from "src/state/hooks";
import {useGetUserQuery} from 'src/state/reducers/userApi';

export interface DeliveryUpdateModalProps {
  isOpen: boolean;
  onIsOpenChanged: (val: boolean) => void;
  idCarDealer?: string;
  carDealerSale: CarDealerSale | null;
}

const validationSchema = yup.object().shape({
  deliveryBeginDate: yup.date().nullable().required("Start date is required"),
  deliveryEndDate: yup.date().nullable().required("End date is required"),
});

const INITIAL_DATA = {
  deliveryBeginDate: null,
  deliveryEndDate: null,
};

const DeliveryUpdateModal: React.FC<DeliveryUpdateModalProps> = ({
  isOpen,
  onIsOpenChanged,
  carDealerSale,
}) => {
  const handleClose = () => onIsOpenChanged(false);
  const {t: tb} = useTranslation();
  const {t} = useDeliveryModalTranslation();
  const commonStyles = useCommonModalStyles();
  const [isLoading, setIsLoading] = useState(false);

  const [initialValues, setDateValues] = useState<{
    deliveryBeginDate: Date | null;
    deliveryEndDate: Date | null;
  }>(INITIAL_DATA);

  const {data: user} = useGetUserQuery();;
  const enqueueErrorSnackbar = useCustomErrorSnackbar();
  const enqueueInfoSnackbar = useCustomInfoSnackbar();

  const deliveryInfo = carDealerSale?.sales[0].deliveryInfo;

  useEffect(() => {
    if (carDealerSale?.sales[0].deliveryInfo) {
      setDateValues({
        deliveryBeginDate:
          carDealerSale?.sales[0].deliveryInfo?.deliveryBeginDate,
        deliveryEndDate: carDealerSale?.sales[0].deliveryInfo?.deliveryEndDate,
      });
    }
  }, [carDealerSale, deliveryInfo]);

  return (
    <>
      <CartagBackdrop open={isLoading} />
      <Dialog
        open={isOpen}
        TransitionComponent={Transition}
        keepMounted
        maxWidth={"md"}
        onClose={handleClose}
        classes={{
          paper: commonStyles.dialogPaper,
        }}
        aria-labelledby="alert-dialog-archive-title"
        aria-describedby="alert-dialog-archive-description"
      >
        <DialogBody>
          <Formik
            initialValues={initialValues}
            enableReinitialize
            validationSchema={validationSchema}
            onSubmit={async (values, {setSubmitting}) => {
              if (carDealerSale && user) {
                setSubmitting(true);
                setIsLoading(true);
                try {
                  // await updateInventoryData(
                  //   {
                  //     action: UpdateInventoryDataType.DELIVERY,
                  //     idCarDealer: carDealerSale.idCarDealer,
                  //     idUser: user?.id,
                  //     userName: user?.fullName,
                  //     sales: [
                  //       {
                  //         deliveryInfo: {
                  //           deliveryBeginDate: values?.deliveryBeginDate?.toISOString(),
                  //           getDeliveryEndDate: values?.deliveryEndDate?.toISOString(),
                  //         },
                  //       },
                  //     ],
                  //   },
                  //   carDealerSale.id
                  // );
                  enqueueInfoSnackbar("Update Successfully");
                  onIsOpenChanged(false);
                } catch (e) {
                } finally {
                  setIsLoading(false);
                  setSubmitting(false);
                }
              } else {
                enqueueErrorSnackbar(
                  "Invalid incoming data. Check currentDeal or user",
                );
              }
            }}
          >
            {({
              handleSubmit,
              values,
              touched,
              errors,
              setFieldValue,
              isSubmitting,
            }) => (
              <form onSubmit={handleSubmit}>
                <GenericFormModalTitle
                  title={tb("Update Delivery appointment")}
                  handleClose={handleClose}
                />
                <Box
                  className={commonStyles.mainBox}
                  padding="28px 34px 0 42px"
                >
                  <Box marginBottom="56px">
                    <Grid container spacing={5}>
                      <Grid item xs={12} sm={6}>
                        <CartagInputField
                          labelText={tb("DashboardTableHead.referenceStock")}
                          disabled
                          value={carDealerSale?.referenceStock || ""}
                          name="referenceStock"
                        />
                      </Grid>
                      <Grid item xs={3} />
                      <Grid item xs={3}>
                        <Box
                          className={commonStyles.itemReadiness}
                          flexDirection="column"
                          display="flex"
                        >
                          <Typography
                            component="div"
                            className={commonStyles.grayUppercaseText}
                          >
                            {tb("Item readiness")}
                          </Typography>
                          <Box marginTop="13px">
                            <StockCarInfoReadiness
                              stageType={
                                carDealerSale?.stageInfo.stage || "NEW_INV"
                              }
                              icons={carDealerSale?.stageInfo.icons || []}
                            />
                          </Box>
                        </Box>
                      </Grid>
                    </Grid>
                  </Box>
                  <Grid container spacing={5}>
                    <Grid item xs={12} sm={6}>
                      <CartagInputField
                        labelText={t("REFERENCE_OF_THE_SALE")}
                        disabled
                        value={carDealerSale?.sales[0].saleReference || ""}
                        name="delavente"
                      />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <CartagInputField
                        disabled
                        labelText={t("REPRESENTATIVE")}
                        name="representant"
                        value={
                          carDealerSale?.sales[0].sellerInfo.nameSeller || ""
                        }
                      />
                    </Grid>
                  </Grid>
                  <Grid container spacing={5}>
                    <Grid item xs={12} sm={6}>
                      <CartagInputField
                        labelText={t("FINANCIAL_DIRECTOR")}
                        disabled
                        value={
                          carDealerSale?.sales[0].creditInfo
                            ?.nameFinanceManager || ""
                        }
                        name="financier"
                      />
                    </Grid>
                  </Grid>
                  <FormSubtitle>{tb("Client Information")}</FormSubtitle>
                  <Grid container spacing={5}>
                    <Grid item xs={12} sm={6}>
                      <CartagInputField
                        value={carDealerSale?.sales[0].clientInfo.fullName}
                        disabled
                        labelText={t("CUSTOMER_NAME")}
                      />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <CartagInputField
                        value={carDealerSale?.sales[0].clientInfo.telNumber1}
                        disabled
                        labelText={t("PHONE")}
                      />
                    </Grid>
                  </Grid>

                  <Grid container spacing={5}>
                    <Grid item xs={12} sm={6}>
                      <CartagInputField
                        value={carDealerSale?.sales[0].clientInfo.email}
                        disabled
                        labelText={t("EMAIL")}
                      />
                    </Grid>
                  </Grid>

                  <FormSubtitle>{tb("Delivery Information")}</FormSubtitle>

                  <Grid container spacing={5}>
                    <Grid item xs={12} sm={6}>
                      <StockInputFieldBody label={t("DELIVERY_START_DATE")}>
                        <CartagDatePicker
                          isDateTimePicker
                          valueDate={values.deliveryBeginDate}
                          onChangeDate={(value) =>
                            setFieldValue("deliveryBeginDate", value)
                          }
                          margin="none"
                          error={
                            !!touched.deliveryBeginDate &&
                            !!errors.deliveryBeginDate
                          }
                          helperText={errors.deliveryBeginDate}
                        />
                      </StockInputFieldBody>
                      {/*<CustomDatePicker*/}
                      {/*  isDateTimePicker*/}
                      {/*  label={"one"}*/}
                      {/*  margin="none"*/}
                      {/*  error={*/}
                      {/*    !!touched.deliveryBeginDate &&*/}
                      {/*    !!errors.deliveryBeginDate*/}
                      {/*  }*/}
                      {/*  helperText={errors.deliveryBeginDate}*/}
                      {/*  value={values.deliveryBeginDate}*/}
                      {/*  onChange={(value) =>*/}
                      {/*    setFieldValue("deliveryBeginDate", value)*/}
                      {/*  }*/}
                      {/*/>*/}
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <StockInputFieldBody label={t("DELIVERY_END_DATE")}>
                        <CartagDatePicker
                          isDateTimePicker
                          valueDate={values.deliveryEndDate}
                          onChangeDate={(value) =>
                            setFieldValue("deliveryEndDate", value)
                          }
                          margin="none"
                          error={
                            !!touched.deliveryEndDate &&
                            !!errors.deliveryEndDate
                          }
                          helperText={errors.deliveryEndDate}
                        />
                      </StockInputFieldBody>
                    </Grid>
                  </Grid>
                </Box>
                <DialogActions className={commonStyles.dialogActions}>
                  <LoadingButton
                    style={{
                      textTransform: "uppercase",
                      padding: "7px 8px",
                    }}
                    variant="contained"
                    color="primary"
                    type="submit"
                    disabled={isSubmitting || !carDealerSale?.id}
                  >
                    {tb(`Update appointment`)}
                  </LoadingButton>
                </DialogActions>
              </form>
            )}
          </Formik>
        </DialogBody>
      </Dialog>
    </>
  );
};

export default DeliveryUpdateModal;
