import React from "react";

const WetCarIcon = (color: string) => (
  <svg width={86} height={86} viewBox="0 0 86 86" fill="none">
    <g filter="url(#prefix__filter0_d)">
      <rect x={20} y={18} width={46} height={46} rx={23} fill="#fff" />
      <rect x={20.5} y={18.5} width={45} height={45} rx={22.5} stroke={color} />
    </g>
    <path
      d="M51.492 39.958l-1.402-4.676a2.485 2.485 0 00-2.395-1.782h-.945V36h.945l1.125 3.75H37.18L38.305 36h.945v-2.5h-.945a2.485 2.485 0 00-2.395 1.782l-1.402 4.676A2.502 2.502 0 0033 42.25v5c0 .69.56 1.25 1.25 1.25v1.25c0 .69.56 1.25 1.25 1.25h1.25c.69 0 1.25-.56 1.25-1.25V48.5h10v1.25c0 .69.56 1.25 1.25 1.25h1.25c.69 0 1.25-.56 1.25-1.25V48.5c.69 0 1.25-.56 1.25-1.25v-5a2.502 2.502 0 00-1.508-2.292zM38 45.375a1.25 1.25 0 110-2.5 1.25 1.25 0 010 2.5zm10 0a1.25 1.25 0 110-2.5 1.25 1.25 0 010 2.5z"
      fill={color}
    />
    <path
      d="M44.555 34.75c.69 0 1.25-.56 1.25-1.25s-1.25-2.5-1.25-2.5-1.25 1.81-1.25 2.5.56 1.25 1.25 1.25zM41.98 37.328c.69 0 1.25-.56 1.25-1.25s-1.25-2.5-1.25-2.5-1.25 1.81-1.25 2.5.56 1.25 1.25 1.25z"
      fill={color}
    />
    <defs>
      <filter
        id="prefix__filter0_d"
        x={0}
        y={0}
        width={86}
        height={86}
        filterUnits="userSpaceOnUse"
        colorInterpolationFilters="sRGB"
      >
        <feFlood floodOpacity={0} result="BackgroundImageFix" />
        <feColorMatrix
          in="SourceAlpha"
          values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
        />
        <feOffset dy={2} />
        <feGaussianBlur stdDeviation={10} />
        <feColorMatrix values="0 0 0 0 0.796875 0 0 0 0 0.804464 0 0 0 0 0.85 0 0 0 0.5 0" />
        <feBlend in2="BackgroundImageFix" result="effect1_dropShadow" />
        <feBlend in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
      </filter>
    </defs>
  </svg>
);

export default WetCarIcon;
