import Box from "@mui/material/Box";
import React from "react";

export const GenericFormBoxHeader = ({
  children,
}: {
  children: JSX.Element | JSX.Element[];
}) => (
  <Box
    sx={{
      padding: "28px 63px 30px 24px",
      backgroundColor: "#F6F8FA",
      borderTopLeftRadius: "14px",
      borderTopRightRadius: "14px",
    }}
    display="flex"
    flexDirection="row"
    alignItems="baseline"
  >
    {children}
  </Box>
);
