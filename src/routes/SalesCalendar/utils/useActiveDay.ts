import {CarDealerSale, CarTypes} from "../../../types";
import dayjs from "dayjs";
import {useEffect, useState} from "react";

export const useActiveDay = (
  currentDate: Date,
  currentCarType: CarTypes,
  deliveriesPerDay: {
    day: dayjs.Dayjs;
    deliveries: CarDealerSale[];
  }[],
  typeOfCalendar: "DAILY" | "MONTHLY",
) => {
  const currDay = +dayjs(currentDate).format("D");

  const [activeDay, setActiveDay] = useState<dayjs.Dayjs | null>(null);
  const [currentSalesPerDay, setCurrentSalesPerDay] = useState<
    CarDealerSale[] | null
  >(null);
  const [currentSale, setCurrentSale] = useState<CarDealerSale | null>(null);

  useEffect(() => {
    setCurrentSale(null);
  }, [currentCarType]);

  useEffect(() => {
    if (deliveriesPerDay && !currentSalesPerDay) {
      setActiveDay(deliveriesPerDay[currDay - 1].day);

      setCurrentSalesPerDay(
        () => deliveriesPerDay[currDay - 1].deliveries,
        // deliveriesPerDay[currDay - 1].deliveries.filter((x) => x.inventoryType)
      );
      setCurrentSale(deliveriesPerDay[currDay - 1].deliveries[0]);
    }
  }, [
    deliveriesPerDay,
    currentSalesPerDay,
    typeOfCalendar,
    currentCarType,
    currDay,
  ]);

  return {
    activeDay,
    setActiveDay,
    currentSale,
    currentSalesPerDay,
    setCurrentSalesPerDay,
    setCurrentSale,
  };
};
