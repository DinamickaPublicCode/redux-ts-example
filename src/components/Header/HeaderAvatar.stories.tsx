import React from 'react';

import { ComponentStory, ComponentMeta } from '@storybook/react';
import HeaderAvatar from './HeaderAvatar';
import { withRouter } from 'storybook-addon-react-router-v6';

export default {
  title: 'Header Avatar',
  component: HeaderAvatar,
  decorators: [withRouter],
} as ComponentMeta<typeof HeaderAvatar>;

const Template: ComponentStory<typeof HeaderAvatar> = (args) => <HeaderAvatar {...args} />;;

export const Primary = Template.bind({});
Primary.args = {
  user: {
    fullName: 'Test User',
    login: '',
    defaultCarDealer: {
      name: 'test',
      id: 1
    },
    authorities: [],
    createdDate: new Date(),
    id: 1,
    email: 'test@mail.com',
    activated: true,
    notification: {
      channels: [],
      topics: []
    }
  },
  onLogOut: () => false
}
