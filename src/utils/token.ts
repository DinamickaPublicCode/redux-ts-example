export const getValidToken = () => {
  try {
    const token =
      window.localStorage && window.localStorage.getItem("qtag-user-token");
    // logic to chek token validity can be added here
    if (token && token === "mock token") {
      return token;
    }
    return null;
  } catch (err) {
    return null;
  }
};
