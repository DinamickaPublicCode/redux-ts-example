import {TabViewsType} from "src/types";

export type ColsDataType = {
  [tab in TabViewsType]: {
    primaryCols: string[];
    secondaryCols: string[];
  };
};

export const colsData: ColsDataType = {
  INVENTORY: {
    primaryCols: [
      "niv",
      "cle",
      "creationDate",
      "make",
      "model",
      "year",
      "km",
      "color",
      "itemReadiness",
      "stage",
    ],
    secondaryCols: ["fuel", "transmission", "price"],
  },
  MECHANIC: {
    primaryCols: [
      "cle",
      "make",
      "model",
      "year",
      "km",
      "color",
      "limitDate",
      "itemReadiness",
      "preparationStage",
    ],
    secondaryCols: ["fuel", "transmission", "price", "niv"],
  },
  ESTHETIC: {
    primaryCols: [
      "cle",
      "make",
      "model",
      "year",
      "km",
      "color",
      "limitDate",
      "itemReadiness",
      "preparationStage",
    ],
    secondaryCols: ["fuel", "transmission", "price", "niv"],
  },
  FINANCE: {
    primaryCols: [
      "cle",
      "saleReference",
      "saleDate",
      "seller",
      "financeManager",
      "Client",
      "itemReadiness",
      "preparationStage",
    ],
    secondaryCols: [
      "fuel",
      "transmission",
      "price",
      "niv",
      "make",
      "model",
      "year",
    ],
  },
  DELIVERY: {
    primaryCols: [
      "cle",
      "seller",
      "saleReference",
      "financeManager",
      "Client",
      "limitDate",
      "deliveryDate",
      "itemReadiness",
      "preparationStage",
    ],
    secondaryCols: [
      "fuel",
      "transmission",
      "price",
      "niv",
      "make",
      "model",
      "year",
    ],
  },
};
