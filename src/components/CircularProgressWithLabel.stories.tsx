import React from 'react';

import { ComponentStory, ComponentMeta } from '@storybook/react';
import CircularProgressWithLabel from './CircularProgressWithLabel';

export default {
  title: 'Progress with label',
  component: CircularProgressWithLabel,
} as ComponentMeta<typeof CircularProgressWithLabel>;



const Template: ComponentStory<typeof CircularProgressWithLabel> = (args) => <CircularProgressWithLabel {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  value: 50
}
