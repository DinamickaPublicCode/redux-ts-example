import React from "react";
import Typography from "@mui/material/Typography";

import {Theme} from "@mui/material/styles";
import makeStyles from "@mui/styles/makeStyles";
import dayjs from "dayjs";
import {CarDealerSale} from "src/types";
import {useTranslation} from "react-i18next";

interface SalesCalendarDayItemProps {
  day: {
    day: dayjs.Dayjs;
    deliveries: CarDealerSale[] | undefined;
  };
  isActive: boolean;
  onClick: (date: {
    day: dayjs.Dayjs;
    deliveries: CarDealerSale[] | undefined;
  }) => void;
}

const SalesCalendarDayItem: React.FC<SalesCalendarDayItemProps> = ({
  day,
  isActive,
  onClick,
}) => {
  const classes = useStyles({
    isActive,
  });
  const {t} = useTranslation();

  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        padding: 8,
      }}
      className={classes.wrapper}
      onClick={() => onClick(day)}
    >
      <div
        style={{
          width: 70,
          height: 70,
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          flexDirection: "column",
          marginRight: 24,
        }}
        className={classes.dateWrapper}
      >
        <span className={classes.dayOfWeek}>{day.day.format("ddd")}</span>
        <span className={classes.day}> {day.day.format("DD")}</span>
      </div>
      <Typography className={classes.delivery}>
        {day.deliveries?.length ?? 0} {t("deliveries")}
      </Typography>
    </div>
  );
};

const useStyles = makeStyles((theme: Theme) => ({
  wrapper: {
    // backgroundColor: "#F6F8FA",
    backgroundColor: ({isActive}: {isActive: boolean}) =>
      isActive ? theme.palette.primary.main : "#F6F8FA",
    borderRadius: 60,
    marginBottom: 20,
    cursor: "pointer",
    transition: "all 0.35s ease-in-out",
  },
  dateWrapper: {
    backgroundColor: "#fff",
    borderRadius: "50%",
    boxShadow: "0px 4px 12px rgba(203, 205, 217, 0.25)",
  },
  delivery: {
    fontWeight: 600,
    fontSize: 16,
    color: ({isActive}: {isActive: boolean}) => (isActive ? "#fff" : "#222238"),
  },
  dayOfWeek: {
    fontWeight: 600,
    fontSize: 14,
    color: "#98A2B7",
  },
  day: {
    fontWeight: 700,
    fontSize: 22,
    color: "#222238",
  },
}));

export default SalesCalendarDayItem;
