import {TabViewsType} from "src/types";

export const tabsViews: TabViewsType[] = [
  // "INVENTORY",
  "MECHANIC",
  "ESTHETIC",
  "FINANCE",
  "DELIVERY",
];
