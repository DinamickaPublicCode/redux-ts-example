import React from "react";
import Box from "@mui/material/Box";
import DocumentView from "./DocumentView";
import {DocumentBoxCategory} from "src/types/documentBoxInterfaces";
import {DocumentCategoryTitle} from "./DocumentCategoryTitle";

const DocumentsSubBlock = ({
  documentCategory,
}: {
  documentCategory: DocumentBoxCategory;
}) => {
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
      }}
    >
      <DocumentCategoryTitle>{documentCategory.name}</DocumentCategoryTitle>
      {documentCategory.documents.map((document, idx) => (
        <DocumentView key={idx} document={document} />
      ))}
    </Box>
  );
};

export default DocumentsSubBlock;
