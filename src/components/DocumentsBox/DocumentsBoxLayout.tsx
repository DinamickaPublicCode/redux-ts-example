import React, {useEffect, useState} from "react";
import {useDocumentBoxContext} from "./DocumentBoxProvider";
import {useTranslation} from "react-i18next";
import {
  DocumentBoxCategory,
  DocumentBoxEntryCategory,
  DocumentEntryData,
  DocumentFileTypes,
} from "../../types/documentBoxInterfaces";
import Box from "@mui/material/Box";
import DocumentsSubBlock from "./DocumentsSubBlock";
import {DOCUMENTS_BOX_TRANSLATION_KEY} from "../../system/constants";

/**
 *
 * @param props
 * @param props.edgeInsets - left and right padding added to the block. Defaults to 35
 */
export const DocumentsBoxLayout = ({
  hasTopPadding,
  edgeInsets,
  shouldRender,
}: {
  hasTopPadding?: boolean;
  shouldRender: boolean;
  edgeInsets?: number;
}) => {
  edgeInsets = edgeInsets || 35;

  // need to parse the categories
  // TODO: This actually needs caching...
  // At the moment it will re-cache everything again
  const {documents} = useDocumentBoxContext();
  const {t} = useTranslation();

  const [photoSection, setPhotoSection] = useState(
    null as DocumentBoxCategory | null,
  );
  const [documentsSection, setDocumentsSection] = useState(
    null as DocumentBoxCategory | null,
  );

  useEffect(() => {
    if (documents) {
      // extract document box category
      const photo = documents.categories.filter(
        (category) => category.type === DocumentFileTypes.PHOTO,
      );
      setPhotoSection({
        name: t(`${DOCUMENTS_BOX_TRANSLATION_KEY}.Types.PHOTO`),
        documents: [
          {
            localeFriendlyTypeString: `${DOCUMENTS_BOX_TRANSLATION_KEY}.UPLOAD_PHOTO`,
            type: DocumentFileTypes.PHOTO,
            file:
              photo.length && photo[0].file
                ? {
                    ...photo[0].file,
                    isImage: true,
                  }
                : undefined,
          },
        ],
      });

      const docs = documents.categories.filter(
        (category) => category.type !== DocumentFileTypes.PHOTO,
      );
      const docTypes = [
        DocumentFileTypes.INSPECTION,
        DocumentFileTypes.CAR_PROOF,
        DocumentFileTypes.GARANTIE,
        DocumentFileTypes.HISTO,
      ];

      setDocumentsSection({
        name: t(`${DOCUMENTS_BOX_TRANSLATION_KEY}.DOCUMENTS`),
        documents: docTypes.map((type) => {
          //
          // find file
          let file:
            | DocumentBoxEntryCategory[]
            | DocumentBoxEntryCategory
            | null = docs.filter((x) => x.type === type);

          file = file.length ? file[0] : null;

          return {
            type,
            localeFriendlyTypeString: `${DOCUMENTS_BOX_TRANSLATION_KEY}.Types.${type}`,
            file: file
              ? {
                  ...file.file,
                  isImage: false,
                }
              : undefined,
          } as DocumentEntryData;
        }),
      });
    }
  }, [documents, t]);

  if (!shouldRender) {
    return null;
  }

  return (
    <Box
      sx={{
        paddingBottom: "37px",
        backgroundColor: "rgb(246, 248, 250)",
      }}
      style={{
        ...(hasTopPadding ? {paddingTop: 24} : {}),
        paddingLeft: edgeInsets,
        paddingRight: edgeInsets,
      }}
    >
      {photoSection && <DocumentsSubBlock documentCategory={photoSection} />}
      {documentsSection && (
        <DocumentsSubBlock documentCategory={documentsSection} />
      )}
    </Box>
  );
};
