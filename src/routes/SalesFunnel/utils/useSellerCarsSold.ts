import {SellerCarEntry} from "../types/apiDataProvider";
import {useMemo} from "react";
import {getPercentage} from "../../../utils/stageData";
import {USE_FULL_PERCENTAGE_SALES_FOR_SELLER} from "./goalCache";

/**
 * Counts how many cars are attached to the seller
 * @param cars
 */
export const useSellerCarsSold = (cars?: SellerCarEntry[]) => {
  return useMemo(() => {
    if (!cars) {
      return 0;
    }
    return cars.filter((car) =>
      USE_FULL_PERCENTAGE_SALES_FOR_SELLER
        ? getPercentage(car.sale.stageInfo.stage) === 100
        : true,
    ).length;
  }, [cars]);
};
