import {useCommonModalStyles} from "../../../../utils/useCommonModalStyles";
import {useSetGoalsModalTranslation} from "../../utils/useSetGoalsModalTranslation";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import {columnSizes, columnSpacing} from "./SetGoalsSellerEntry";
import React from "react";

export const SetGoalsModalTableHeader = () => {
  const styles = useCommonModalStyles();
  const {t} = useSetGoalsModalTranslation();
  return (
    <Grid container spacing={columnSpacing}>
      <Grid item xs={columnSizes[0]}>
        <Typography className={styles.grayUppercaseText}>
          {t("Name")}
        </Typography>
      </Grid>
      <Grid item xs={columnSizes[1]}>
        <Typography className={styles.grayUppercaseText}>
          {t("Objectif")}
        </Typography>
      </Grid>
      <Grid item xs={columnSizes[2]}>
        <Typography className={styles.grayUppercaseText}>
          {t("Total Vente Mens")}
        </Typography>
      </Grid>
      <Grid item xs={columnSizes[3]}>
        <Typography className={styles.grayUppercaseText}>
          {t("Total Vente Ann")}
        </Typography>
      </Grid>
    </Grid>
  );
};
