import CameraIcon from "../assets/CameraIcon";
import WetCarIcon from "../assets/WetCarIcon";
import WrenchIcon from "../assets/WrenchIcon";
import InspectionsIcon from "../assets/InspectionsIcon";
import CompletedIcon from "../assets/CompletedIcon";

const smallStatsIcons: {
  [index: string]: (color: string) => JSX.Element;
} = {
  inv: InspectionsIcon,
  wash: WetCarIcon,
  photo: CameraIcon,
  ins: CompletedIcon,
  rep: WrenchIcon,
  deliv: InspectionsIcon,
  sale: InspectionsIcon,
};

export default smallStatsIcons;
