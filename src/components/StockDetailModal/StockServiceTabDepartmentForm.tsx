import React from "react";
import {useTranslation} from "react-i18next";
import {DepartmentSpecification} from "src/types";
import {StockServiceTabBox} from "./StockServiceTabBox";
import {StockServiceTabActionsForm} from "./StockServiceTabActionsForm";
import {SERVICE_TAB_TRANSLATION_KEY} from "src/system/constants";

export const StockServiceTabDepartmentForm = ({
  department,
}: {
  department: DepartmentSpecification;
}) => {
  const key = SERVICE_TAB_TRANSLATION_KEY;
  const {t} = useTranslation();

  return (
    <>
      {department.steps ? (
        department.steps.map((step, index) => (
          <StockServiceTabBox
            key={step.stepType}
            department={t(department.departmentType)}
            step={index + 1}
            totalSteps={department.steps?.length || 0}
            // TODO: Localize
            stepTitle={step.stepName || t(key + step.stepName)}
          >
            <StockServiceTabActionsForm
              departmentType={department.departmentType}
              step={step}
            />
          </StockServiceTabBox>
        ))
      ) : (
        <></>
      )}
    </>
  );
};
