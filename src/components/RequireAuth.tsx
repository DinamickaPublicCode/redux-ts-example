import React from "react";
import {Navigate} from "react-router-dom";
import {useAppSelector} from "src/state/hooks";

const RequireAuth: React.FC<{children: React.ReactNode}> = ({children}) => {
  const token = useAppSelector((state) => state.token.value);

  if (!token) {
    /** Redirects to the auth page from the current location */
    return <Navigate to="/login" />;
  }

  return <>{children}</>;
};

export default RequireAuth;
