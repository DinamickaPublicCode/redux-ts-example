import React from "react";
import {useTheme} from "@mui/material/styles";
import Backdrop from "@mui/material/Backdrop";
import CircularProgress from "@mui/material/CircularProgress";

interface CartagBackdropProps {
  open: boolean;
  nobg?: boolean;
}

const CartagBackdrop: React.FC<CartagBackdropProps> = ({open, nobg}) => {
  const theme = useTheme();
  return (
    <Backdrop
      invisible={nobg}
      sx={
        nobg
          ? {
              color: "#fff",
              backdropFilter: "none",
            }
          : {
              zIndex: theme.zIndex.drawer + 1,
              backdropFilter: "blur(10px)",
              color: theme.palette.primary.main,
            }
      }
      open={open}>
      <CircularProgress color="inherit" />
    </Backdrop>
  );
};

export default CartagBackdrop;
