import React, {useState, useRef} from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import ButtonBase from "@mui/material/ButtonBase";
import ClickAwayListener from "@mui/material/ClickAwayListener";
import Grow from "@mui/material/Grow";
import Paper from "@mui/material/Paper";
import Popper from "@mui/material/Popper";
import TextField, {TextFieldProps} from "@mui/material/TextField";
import {useTheme} from "@mui/material/styles";
import StaticDateRangePicker from "@mui/lab/StaticDateRangePicker";
import ArrowDropDown from "@mui/icons-material/ArrowDropDown";
import NavigateBefore from "@mui/icons-material/NavigateBefore";
import NavigateNext from "@mui/icons-material/NavigateNext";

import {queryArrayFromString} from "src/utils/queryArrFromString";
import {useNavigate, useLocation} from "react-router-dom";

interface DashboardTableFiltersDateRangeProps {
  name: string;
  filterQueryStringNamings: [string, string];
}

const DashboardTableFiltersDateRange: React.FC<
  DashboardTableFiltersDateRangeProps
> = ({name, filterQueryStringNamings}) => {
  const navigate = useNavigate();
  const location = useLocation();

  const [open, setOpen] = useState(false);
  const anchorRef = useRef<HTMLButtonElement>(null);
  const [dates, setDates] = useState<[Date | null, Date | null]>([null, null]);
  const [isSelected, setIsSelected] = useState<boolean>(false);

  const handleToggle = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>,
  ) => {
    event.stopPropagation();
    setOpen((prevOpen) => !prevOpen);
  };

  const handleFilter = () => {
    if ((!dates[0] && !dates[1]) || !dates[0] || !dates[1]) return;
    const newQueryRes = queryArrayFromString(location.search).filter(
      (i) =>
        !i.includes(`${filterQueryStringNamings[0]}`) &&
        !i.includes(`${filterQueryStringNamings[1]}`),
    );

    navigate({
      pathname: location.pathname,
      search: `${newQueryRes.join("&")}&${
        filterQueryStringNamings[0]
      }=${dates[0]?.toISOString()}&${
        filterQueryStringNamings[1]
      }=${dates[1]?.toISOString()}`,
    });

    setIsSelected(!!dates[0] && !!dates[1]);
    setOpen(false);
  };

  const theme = useTheme();

  return (
    <Box
      sx={{
        [theme.breakpoints.down("sm")]: {
          marginTop: 20,
          display: "none",
        },
      }}>
      <Button
        ref={anchorRef}
        sx={{
          backgroundColor: isSelected ? theme.palette.primary.main : "#F6F8FA",
          color: isSelected ? theme.palette.common.white : "#98A2B7",
          boxShadow: "none",
          padding: "4px 8px",
          marginRight: 20,
          "&:hover": {
            backgroundColor: isSelected
              ? theme.palette.primary.main
              : "#F6F8FA",
          },
        }}
        endIcon={<ArrowDropDown />}
        onClick={handleToggle}>
        {name}
      </Button>
      <Popper
        open={open}
        anchorEl={anchorRef.current}
        role={undefined}
        transition
        disablePortal
        placement={"bottom"}
        style={{zIndex: 100}}>
        {({TransitionProps, placement}) => (
          <Grow
            {...TransitionProps}
            style={{
              zIndex: 1000,
              transformOrigin:
                placement === "bottom" ? "center top" : "center bottom",
            }}>
            <Paper
              style={{
                boxShadow: "0px 4px 20px rgba(203, 205, 217, 0.3)",
                overflow: "hidden",
              }}>
              <ClickAwayListener
                onClickAway={(event) => {
                  if (
                    anchorRef.current &&
                    anchorRef.current.contains(event.target as HTMLElement)
                  ) {
                    return;
                  }
                  setOpen(false);
                }}>
                <Box>
                  <StaticDateRangePicker
                    disableCloseOnSelect
                    leftArrowIcon={<NavigateBefore color="primary" />}
                    rightArrowIcon={<NavigateNext color="primary" />}
                    leftArrowButtonProps={{
                      style: {
                        backgroundColor: "#F1F0FF",
                      },
                    }}
                    rightArrowButtonProps={{
                      style: {
                        backgroundColor: "#F1F0FF",
                      },
                    }}
                    displayStaticWrapperAs={"desktop"}
                    calendars={2}
                    value={dates}
                    onChange={(newValue: unknown) =>
                      setDates(newValue as [Date | null, Date | null])
                    }
                    renderInput={(
                      startProps: TextFieldProps,
                      endProps: TextFieldProps,
                    ) => (
                      <>
                        <TextField {...startProps} />
                        <span> to </span>
                        <TextField {...endProps} />
                      </>
                    )}
                  />

                  <ButtonBase
                    sx={{
                      padding: "13px",
                      width: "100%",
                      backgroundColor: theme.palette.primary.main,
                      color: theme.palette.common.white,
                      fontWeight: 600,
                      textTransform: "uppercase",
                    }}
                    onClick={handleFilter}>
                    Filter
                  </ButtonBase>
                </Box>
              </ClickAwayListener>
            </Paper>
          </Grow>
        )}
      </Popper>
    </Box>
  );
};

export default DashboardTableFiltersDateRange;
