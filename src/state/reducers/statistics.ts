import {ReduxAction, StatisticsState} from "src/types";
import {defaultStatisticsData} from "src/utils/defaultStatisticsData";

const initialState: StatisticsState = {
  mechanic: defaultStatisticsData,
  esthetic: defaultStatisticsData,
  sale: defaultStatisticsData,
  delivery: defaultStatisticsData,
  inventory: defaultStatisticsData,
  finance: defaultStatisticsData,
};

type TokenActionType = {type: 'SET_STATISTICS'; payload: { key: string, value: unknown }};

const statisticsReducer: Reducer<
  StatisticsState,
  StatisticsActionType
> = (state = initialState, action): StatisticsState => {
  switch (action.type) {
    case "SET_STATISTICS":
      return {
        ...state,
        [action.payload.key]: action.payload.value,
      };
    default:
      return state;
  }
};

export const setStatistics = (key: string, value: unknown): LoadingActionType => ({
  type: 'SET_STATISTICS',
  payload: {
    key,
    value
  },
});

export default statisticsReducer;
