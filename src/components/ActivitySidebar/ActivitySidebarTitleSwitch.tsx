import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import {useTranslation} from "react-i18next";
import {CartagSwitch} from "../CartagSwitch";
import {ACTIVITY_BOX_TRANSLATION_KEY} from "../../system/constants";

const ActivitySidebarTitleSwitch = ({
  checked,
  onCheckedSet,
}: {
  checked: boolean;
  onCheckedSet: (value: boolean) => void;
}) => {
  const {t} = useTranslation();
  const titleText = !checked
    ? t(`${ACTIVITY_BOX_TRANSLATION_KEY}.ACTIVITY`)
    : t("Documents");

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "row",
      }}
    >
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          flex: 1,
          height: 76,
          borderRadius: 0,
          border: "2px solid #F6F8FA",
          backgroundColor: "#F6F8FA",
          borderRight: "none",
          lineHeight: 1.3,
          fontSize: 13,
          fontWeight: 600,
          color: "#98A2B7",
          transition: "0.3s all ease-in-out",
        }}
      >
        <Typography
          component="div"
          sx={{
            fontWeight: 600,
            fontSize: "13px",
            marginLeft: "24px",
            letterSpacing: "0.5px",
            textTransform: "uppercase",
            color: "#98A2B7",
          }}
        >
          {titleText}
        </Typography>
      </Box>
      <Box
        sx={{
          paddingLeft: 0,
          paddingRight: "30px",
          paddingTop: "28px",
          paddingBottom: "20px",
          backgroundColor: "rgb(246, 248, 250)",
        }}
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <div>
          <CartagSwitch
            name="checkedC"
            checked={checked}
            onChange={() => {
              onCheckedSet(!checked);
            }}
          />
        </div>
      </Box>
    </Box>
  );
};

export default ActivitySidebarTitleSwitch;
