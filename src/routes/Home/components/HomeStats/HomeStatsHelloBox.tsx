import {useAppSelector} from "src/state/hooks";
import {useHomeContext} from "../../state/HomeContextProvider";
import useHomeTranslation from "../../utils/useHomeTranslation";
import {requestGeneratedAvatar} from "../../../../utils/avatarGenerator";
import HelloIllustrationAbstract from "./HomeStatsGreeting";
import React from "react";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import {useGetUserQuery} from 'src/state/reducers/userApi';

const HomeStatsHelloBox = () => {
  const {data: user} = useGetUserQuery();;
  const notificationsCount = useAppSelector(
    (state) => state.notifications.notificationsCount,
  );

  const {setDrawerOpen} = useHomeContext();
  const {t} = useHomeTranslation();

  return (
    <Box
      sx={{
        background: "#FFFFFF",
        boxShadow: "0px 4px 20px rgba(20, 32, 100, 0.08)",
        borderRadius: "14px",
        padding: "32px 30px",
      }}
      style={{
        height: "100%",
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between",
      }}
    >
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
        }}
      >
        <Box
          sx={{
            width: "61px",
            height: "61px",
            borderRadius: "50%",
            border: "2px solid white",
            boxShadow: " 0px 4px 12px rgba(203, 205, 217, 0.25)",
            mr: "26px",
            backgroundImage: `url(${requestGeneratedAvatar(
              user?.id || "",
              user?.fullName || "",
            )})`,
            backgroundPosition: "50%",
          }}
        />
        <Typography
          component="div"
          sx={{
            fontStyle: "normal",
            fontWeight: "bold",
            fontSize: "28px",
            lineHeight: "36.4px",
            color: "#222238",
          }}
        >
          {t("Hello") + " " + user?.fullName}!
        </Typography>
      </Box>
      <Box
        sx={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "center",
          marginTop: "45px",
          marginBottom: "60px",
        }}
      >
        <HelloIllustrationAbstract />
      </Box>
      <Box>
        <Box
          sx={{
            fontStyle: "normal",
            fontWeight: 600,
            fontSize: "18px",
            lineHeight: "22.32px",
            color: "#222238",
          }}
          style={{
            marginBottom: 24,
          }}
        >
          {t("We wish you a fantastic day ahead")}!
        </Box>
        {notificationsCount !== 0 && (
          <Box
            sx={{
              fontStyle: "normal",
              fontWeight: "normal",
              fontSize: "14px",
              lineHeight: "18.2px",
              color: "#98A2B7",
            }}
            style={{
              marginBottom: 16,
            }}
          >
            {t("Dont forget you have") + " "}
            <Typography
              onClick={() => setDrawerOpen(true)}
              component="span"
              sx={{
                fontStyle: "normal",
                fontWeight: "normal",
                fontSize: "14px",
                lineHeight: "18.2px",
                cursor: "pointer",
                color: "#1A06F9 !important",
              }}
            >
              {notificationsCount}
              {" notifications"}
            </Typography>
          </Box>
        )}
      </Box>
    </Box>
  );
};

export default HomeStatsHelloBox;
