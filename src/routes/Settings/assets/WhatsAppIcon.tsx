import usePublicUrl from "../../../utils/usePublicUrl";
import Box from "@mui/material/Box";
import React from "react";

export const WhatsAppIcon = () => {
  const publicUrl = usePublicUrl();

  return (
    <Box
      sx={{
        flexShrink: 0,
        width: 27,
        height: 29,
        backgroundSize: "27px 29px",
        backgroundImage: `url(${publicUrl(
          "images/icons/897px-WhatsApp_1.png",
        )})`,
      }}
    />
  );
};
