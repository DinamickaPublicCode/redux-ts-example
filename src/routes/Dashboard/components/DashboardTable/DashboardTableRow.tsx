import React, {useState, useEffect, useRef, useCallback} from "react";
import MuiTableCell from "@mui/material/TableCell";
import MoreVert from "@mui/icons-material/MoreVert";
import { ReactComponent as CheckboxIcon } from "src/assets/icons/CheckboxIcon.svg";
import { ReactComponent as CheckboxCheckedIcon } from "src/assets/icons/CheckboxCheckedIcon.svg";
import {
  FinanceTabType,
  GeneralInfo,
  StagePreparationType,
  StageType,
  TabViews,
  TabViewsType,
} from "src/types";
import dayjs from "dayjs";
import {useTranslation} from "react-i18next";
import {actionsData} from "src/utils/mockActionsData";
import DashboardTableRowItemReadinessCell from "./DashboardTableRowItemReadinessCell";
import Checkbox from "@mui/material/Checkbox";
import Box from "@mui/material/Box";
import ClickAwayListener from "@mui/material/ClickAwayListener";
import Grow from "@mui/material/Grow";
import MuiLink from "@mui/material/Link";
import MenuItem from "@mui/material/MenuItem";
import MenuList from "@mui/material/MenuList";
import Paper from "@mui/material/Paper";
import Popper from "@mui/material/Popper";
import Button from "@mui/material/Button";
import TableRow from "@mui/material/TableRow";
import Select from "@mui/material/Select";
import Typography from "@mui/material/Typography";
import InputBase from "@mui/material/InputBase";
import IconButton from "@mui/material/IconButton";
import {styled, useTheme} from "@mui/material/styles";
import {stageDataSelect, stagePreparationDataSelect} from "src/utils/stageData";

interface DashboardTableRowProps {
  tabName: TabViewsType;
  headCells: string[];
  row: FinanceTabType;
  labelId: string;
  isItemSelected: boolean;
  handleRowClick: (event: React.MouseEvent<unknown>, name: string) => void;
  createDealOpen: (row: FinanceTabType, action?: string) => void;
}

const options = [
  "CREATE_DEAL",
  "PREPARATION",
  "MARK_AS_DONE",
  "SEND_MSG",
  "OUTSOURCING",
  "WHOLESALE",
  "PREPARE_ON_DELIV",
];

const DashboardTableRow: React.FC<DashboardTableRowProps> = ({
  tabName,
  headCells,
  row,
  labelId,
  isItemSelected,
  handleRowClick,
  createDealOpen,
}) => {
  const {t} = useTranslation();

  const onNavigationClick = useCallback(() => {
    createDealOpen(row);
  }, [row, createDealOpen]);

  const [open, setOpen] = useState<boolean>(false);
  const anchorRef = useRef<HTMLButtonElement>(null);

  const [stage, setStage] = useState<StageType | StagePreparationType | "">("");

  const handleActionClick = (
    event:
      | React.MouseEvent<HTMLLIElement, MouseEvent>
      | React.MouseEvent<HTMLButtonElement>,
    action: string,
  ) => {
    event.stopPropagation();

    createDealOpen(row, action);

    setOpen(false);
  };

  const handleToggle = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>,
  ) => {
    event.stopPropagation();
    setOpen((prevOpen) => !prevOpen);
  };

  useEffect(() => {
    if (tabName === TabViews.INVENTORY) {
      const currStage = stageDataSelect.find(
        (el) => el.name === row.stageInfo.stage,
      );
      if (currStage) {
        setStage(currStage.name);
      }
    } else {
      const currStage = stagePreparationDataSelect.find(
        (el) => el.name === (row.departments && row.departments[0].status),
      );
      if (currStage) {
        setStage(currStage.name);
      }
    }
  }, [tabName, row]);

  const actionOptions =
    tabName === TabViews.INVENTORY ? actionsData[stage as StageType] : options;
  const theme = useTheme();
  return (
    <TableRow
      role="checkbox"
      aria-checked={isItemSelected}
      tabIndex={-1}
      key={row.referenceStock}
      selected={isItemSelected}
      sx={{
        "&$rowSelected, &$rowSelected:hover": {
          backgroundColor: "#F6F8FA",
        },
      }}
    >
      <TableCell
        sx={{
          position: "relative",
          border: "none",
          color: "#98A2B7",
        }}
        padding="checkbox"
      >
        <Checkbox
          color="default"
          checked={isItemSelected}
          onClick={(event) => handleRowClick(event, row.referenceStock)}
          icon={<CheckboxIcon />}
          checkedIcon={<CheckboxCheckedIcon />}
          inputProps={{
            "aria-labelledby": labelId,
          }}
        />
        <BottomBorder sx={{left: "15px"}} />
      </TableCell>
      <TableCell id={labelId} scope="row" align="left">
        <MuiLink
          style={{
            cursor: "pointer",
          }}
          onClick={() => createDealOpen(row)}
        >
          {row.referenceStock}
        </MuiLink>
        <BottomBorder />
      </TableCell>
      {headCells.map((cell) => {
        if (cell === "stage" || cell === "preparationStage") {
          return (
            <TableCell key={cell} align="left">
              <Select
                sx={{
                  "&:focus": {
                    borderRadius: theme.shape.borderRadius,
                  },
                }}
                autoWidth
                value={stage}
                onChange={(event) => {
                  setStage(event.target.value as StageType);
                }}
                input={<InputBase />}
                renderValue={(value) => {
                  const currentStageValue =
                    cell === "stage"
                      ? stageDataSelect.find((item) => item.name === value)
                      : stagePreparationDataSelect.find(
                          (item) => item.name === value,
                        );
                  return (
                    <MenuItem
                      style={{
                        whiteSpace: "normal",
                      }}
                    >
                      <SelectColor
                        sx={{
                          backgroundColor: currentStageValue?.color,
                        }}
                      />
                      <Typography
                        style={{
                          width: 150,
                        }}
                      >
                        {t(`stages.${currentStageValue?.name}`)}
                      </Typography>
                    </MenuItem>
                  );
                }}
              >
                {cell === "stage"
                  ? stageDataSelect.map((el) => {
                      return (
                        <MenuItem key={el.name} value={el.name}>
                          <SelectColor
                            sx={{
                              backgroundColor: el.color,
                            }}
                          />
                          <Typography>{t(`stages.${el.name}`)}</Typography>
                        </MenuItem>
                      );
                    })
                  : stagePreparationDataSelect.map((el) => {
                      return (
                        <MenuItem key={el.name} value={el.name}>
                          <SelectColor
                            sx={{
                              backgroundColor: el.color,
                            }}
                          />
                          <Typography>{t(`stages.${el.name}`)}</Typography>
                        </MenuItem>
                      );
                    })}
              </Select>
              <BottomBorder />
            </TableCell>
          );
        }
        if (cell === "itemReadiness") {
          return (
            <DashboardTableRowItemReadinessCell
              row={row}
              onClick={onNavigationClick}
            />
          );
        }
        if (cell === "niv") {
          return (
            <TableCell
              key={cell}
              onClick={onNavigationClick}
              sx={{
                cursor: "pointer",
              }}
              align="left"
            >
              {row.generalInfo.serialNumberNiv}
              <BottomBorder />
            </TableCell>
          );
        }
        if (cell === "cle") {
          return (
            <TableCell
              key={cell}
              onClick={onNavigationClick}
              sx={{
                cursor: "pointer",
              }}
              align="left"
            >
              {row.generalInfo.numCle}
              <BottomBorder />
            </TableCell>
          );
        }
        if (cell === "limitDate") {
          return (
            <TableCell
              key={cell}
              onClick={onNavigationClick}
              sx={{
                cursor: "pointer",
              }}
              align="left"
            >
              {"limitPreparationDate" in row
                ? dayjs(row.limitPreparationDate).format("DD/MM/YYYY")
                : ""}
              <BottomBorder />
            </TableCell>
          );
        }
        if (cell === "creationDate") {
          return (
            <TableCell
              key={cell}
              onClick={onNavigationClick}
              sx={{
                cursor: "pointer",
              }}
              align="left"
            >
              {dayjs(row.createdDate).format("DD/MM/YYYY")}
              <BottomBorder />
            </TableCell>
          );
        }
        if (cell === "saleDate") {
          return (
            <TableCell
              key={cell}
              onClick={onNavigationClick}
              sx={{
                cursor: "pointer",
              }}
              align="left"
            >
              {dayjs(row.saleInfo && row.saleInfo.saleDate).format(
                "DD/MM/YYYY",
              )}
              <BottomBorder />
            </TableCell>
          );
        }
        if (cell === "deliveryDate") {
          return (
            <TableCell
              key={cell}
              onClick={onNavigationClick}
              sx={{
                cursor: "pointer",
              }}
              align="left"
            >
              {dayjs(row?.saleInfo?.deliveryInfo?.deliveryBeginDate).format(
                "DD/MM/YYYY",
              )}
              <BottomBorder />
            </TableCell>
          );
        }
        if (cell === "seller") {
          return (
            <TableCell
              key={cell}
              onClick={onNavigationClick}
              sx={{
                cursor: "pointer",
              }}
              align="left"
            >
              {row?.saleInfo?.sellerInfo?.nameSeller}
              <BottomBorder />
            </TableCell>
          );
        }
        if (cell === "saleReference") {
          return (
            <TableCell
              key={cell}
              onClick={onNavigationClick}
              sx={{
                cursor: "pointer",
              }}
              align="left"
            >
              {row?.saleInfo?.saleReference}
              <BottomBorder />
            </TableCell>
          );
        }
        if (cell === "financeManager") {
          return (
            <TableCell
              key={cell}
              onClick={onNavigationClick}
              sx={{
                cursor: "pointer",
              }}
              align="left"
            >
              {row?.saleInfo?.creditInfo?.nameFinanceManager}
              <BottomBorder />
            </TableCell>
          );
        }
        if (cell === "Client") {
          return (
            <TableCell
              key={cell}
              onClick={onNavigationClick}
              sx={{
                cursor: "pointer",
              }}
              align="left"
            >
              {row?.saleInfo?.clientInfo?.name}
              <BottomBorder />
            </TableCell>
          );
        }
        return (
          <TableCell
            key={cell}
            onClick={onNavigationClick}
            sx={{
              cursor: "pointer",
            }}
            align="left"
          >
            {row.generalInfo[cell as keyof GeneralInfo]}
            <BottomBorder />
          </TableCell>
        );
      })}
      <TableCell sx={{cursor: "pointer"}} align="left">
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
          }}
        >
          <Button
            sx={{
              backgroundColor: "#F6F8FA",
              fontWeight: 500,
              flex: 1,
              minWidth: "125px",
            }}
            onClick={(event) => handleActionClick(event, actionOptions[0])}
          >
            {t(`actions.${actionOptions && actionOptions[0]}`)}
          </Button>
          <Box>
            <IconButton size="small" ref={anchorRef} onClick={handleToggle}>
              <MoreVert />
            </IconButton>
            <Popper
              open={open}
              anchorEl={anchorRef.current}
              role={undefined}
              transition
              disablePortal
              placement={"left-start"}
              style={{
                zIndex: 100,
              }}
            >
              {({TransitionProps, placement}) => (
                <Grow
                  {...TransitionProps}
                  style={{
                    zIndex: 1000,
                    transformOrigin:
                      placement === "bottom" ? "center top" : "center bottom",
                  }}
                >
                  <Paper
                    style={{
                      boxShadow: "0px 4px 20px rgba(203, 205, 217, 0.3)",
                    }}
                  >
                    <ClickAwayListener
                      onClickAway={(event) => {
                        if (
                          anchorRef.current &&
                          anchorRef.current.contains(
                            event.target as HTMLElement,
                          )
                        ) {
                          return;
                        }
                        setOpen(false);
                      }}
                    >
                      <MenuList id="split-button-menu">
                        {actionOptions.map((option, index) => (
                          <MenuItem
                            key={index}
                            onClick={(event) =>
                              handleActionClick(event, option)
                            }
                          >
                            {t(`actions.${option}`)}
                          </MenuItem>
                        ))}
                      </MenuList>
                    </ClickAwayListener>
                  </Paper>
                </Grow>
              )}
            </Popper>
          </Box>
        </div>
        <BottomBorder sx={{left: "-15px"}} />
      </TableCell>
    </TableRow>
  );
};

const TableCell = styled(MuiTableCell)({
  position: "relative",
  border: "none",
  color: "#98A2B7",

  //
  padding: "13px 10px",
});

const BottomBorder = styled("span")(({theme}) => ({
  display: "block",
  position: "absolute",
  bottom: 0,
  left: 0,
  right: 0,
  height: "4px",
  width: "100%",
  backgroundColor: theme.palette.background.default,
}));

const SelectColor = styled("span")({
  borderRadius: "50%",
  border: "2px solid #fff",
  height: "14px",
  width: "14px",
  marginRight: "10px",
  boxShadow: "0px 2px 6px rgba(203, 205, 217, 0.4)",
});

export default DashboardTableRow;
