import {Theme} from "@mui/material/styles";

import createStyles from "@mui/styles/createStyles";
import makeStyles from "@mui/styles/makeStyles";

export const useCommonStyles = makeStyles((theme: Theme) =>
  createStyles({
    inputLabel: {
      fontSize: 14,
      color: "#98A2B7",
    },

    /**
     * This one is a hack for making the input field behave like a button
     * Please, ignore this ugly cursor: pointer mess. Overriding miui styles is pain
     */
    inputForDatePicker: {
      backgroundColor: "#F6F8FA",
      cursor: "pointer !important",

      "& input": {
        cursor: "pointer !important",
      },
      "&.Mui-disabled": {
        cursor: "pointer !important",
        backgroundColor: "#F6F8FA",
        color: "rgba(0, 0, 0, 0.87)",
        textFillColor: "rgba(0, 0, 0, 0.87)",
      },
      "&.Mui-disabled .MuiOutlinedInput-notchedOutline": {
        borderWidth: 0,
      },

      "&:hover .MuiOutlinedInput-notchedOutline, & .MuiOutlinedInput-notchedOutline":
        {
          borderColor: "rgba(0, 0, 0, 0);",
          cursor: "pointer !important",
        },
      "&.Mui-focused .MuiOutlinedInput-notchedOutline": {
        borderColor: theme.palette.primary.main,
        borderWidth: 2,
      },
      "&.Mui-error .MuiOutlinedInput-notchedOutline": {
        borderColor: theme.palette.error.main,
        borderWidth: 2,
      },
    },

    input: {
      backgroundColor: "#F6F8FA",
      "&.Mui-disabled": {
        backgroundColor: "#EBF1F6",
        color: "#C1C7D4",
      },
      "&.Mui-disabled .MuiOutlinedInput-notchedOutline": {
        borderWidth: 0,
      },

      "&:hover .MuiOutlinedInput-notchedOutline, & .MuiOutlinedInput-notchedOutline":
        {
          borderColor: "rgba(0, 0, 0, 0);",
        },
      "&.Mui-focused .MuiOutlinedInput-notchedOutline": {
        borderColor: theme.palette.primary.main,
        borderWidth: 2,
      },
      "&.Mui-error .MuiOutlinedInput-notchedOutline": {
        borderColor: theme.palette.error.main,
        borderWidth: 2,
      },
    },
  }),
);
