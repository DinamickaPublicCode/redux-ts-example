import {SellersListEntry} from "../../types/apiDataProvider";
import React, {useEffect, useMemo, useState} from "react";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import {requestGeneratedAvatar} from "../../../../utils/avatarGenerator";
import CartagInputField from "../../../../components/CartagInputField";
import {Theme} from "@mui/material/styles";

import createStyles from "@mui/styles/createStyles";
import makeStyles from "@mui/styles/makeStyles";

export const columnSizes = [4, 4, 2, 2] as const;
export const columnSpacing = 2;
export type EditGoalEntry = {
  sellerId: string;
  month: number;
  value: string;
  newCar: boolean;
};
const SetGoalsSellerEntry = ({
  sellersListEntry,
  date,
  onValueChanged,
  newCar,
}: {
  onValueChanged: (entry: EditGoalEntry) => void;
  sellersListEntry: SellersListEntry;
  date: Date;
  newCar: boolean;
}) => {
  const styles = useSetGoalsModalStyles();
  const year = date.getFullYear();
  const month = date.getMonth() + 1;

  const onInputValueChanged = (val: string) => {
    setGoalValue(val);
    onValueChanged({
      sellerId: sellersListEntry.seller.id,
      month,
      value: val,
      newCar,
    });
  };

  const monthly = useMemo(
    () => sellersListEntry.cars?.length || 0,
    [sellersListEntry],
  );
  const goal = useMemo(
    () =>
      sellersListEntry.seller.goals
        .filter(
          (goal) =>
            +goal.month === month &&
            +goal.year === year &&
            goal.newCar === newCar,
        )
        .map((goal) => +goal.goal)
        .pop() || 0,
    [sellersListEntry, month, year, newCar],
  );
  const [goalValue, setGoalValue] = useState("" + goal);

  useEffect(() => {
    setGoalValue("" + goal);
  }, [goal]);

  return (
    <Grid alignItems="center" container spacing={columnSpacing}>
      <Grid item xs={columnSizes[0]}>
        <Box display="flex" flexDirection="row" alignItems="center">
          <Box
            width="22px"
            height="22px"
            borderRadius="50%"
            marginRight="10px"
            style={{
              border: "1px solid #FFFFFF",
              boxShadow: "0px 2px 4px rgba(203, 205, 217, 0.25)",
              backgroundSize: "cover",
              backgroundPosition: "50%",
              backgroundImage: `url(${requestGeneratedAvatar(
                sellersListEntry.seller.id,
                sellersListEntry.seller.fullName,
              )})`,
            }}
          />
          <Typography component="div" className={styles.sellerName}>
            {sellersListEntry.seller.fullName}
          </Typography>
        </Box>
      </Grid>
      <Grid item xs={columnSizes[1]}>
        <CartagInputField
          margin="none"
          disableLabel
          value={goalValue}
          onChange={(e) => onInputValueChanged(e.target.value)}
        />
      </Grid>
      <Grid item xs={columnSizes[2]}>
        <Typography component="div" className={styles.goalNumber}>
          {monthly}
        </Typography>
      </Grid>
      <Grid item xs={columnSizes[3]}>
        <Typography component="div" className={styles.goalNumber}>
          {sellersListEntry.totalYear}
        </Typography>
      </Grid>
    </Grid>
  );
};

const useSetGoalsModalStyles = makeStyles((theme: Theme) =>
  createStyles({
    sellerName: {
      fontStyle: "normal",
      fontWeight: "normal",
      fontSize: "14px",
      lineHeight: "14px",
      color: "#222238",
    },
    goalNumber: {
      fontWeight: 600,
      fontSize: "14px",
      lineHeight: "17.5px",
      color: "#222238",
    },
  }),
);

export default SetGoalsSellerEntry;
