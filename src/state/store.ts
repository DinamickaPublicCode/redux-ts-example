import {configureStore} from '@reduxjs/toolkit';
import notificationsReducer from "./reducers/notifications";
import loadingReducer from "./reducers/loading";
import statisticsReducer from "./reducers/statistics";
import tokenReducer from "./reducers/token";
import {userApi} from './reducers/userApi';
import {statisticsApi} from './reducers/statisticsApi';
import {carDealerApi} from './reducers/carDealerApi';
import {notificationsApi} from './reducers/notificationsApi';

const store = configureStore({
  reducer: {
    notifications: notificationsReducer,
    statistics: statisticsReducer,
    loading: loadingReducer,
    token: tokenReducer,
    [userApi.reducerPath]: userApi.reducer,
    [statisticsApi.reducerPath]: statisticsApi.reducer,
    [carDealerApi.reducerPath]: carDealerApi.reducer,
    [notificationsApi.reducerPath]: notificationsApi.reducer,
},
middleware: (getDefaultMiddleware) =>
  getDefaultMiddleware().concat([
    userApi.middleware,
    statisticsApi.middleware,
    carDealerApi.middleware,
    notificationsApi.middleware,
  ]),
})

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export default store;
