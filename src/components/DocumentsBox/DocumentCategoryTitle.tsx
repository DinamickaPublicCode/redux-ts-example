import React from "react";
import makeStyles from "@mui/styles/makeStyles";
import {lighten, Theme} from "@mui/material/styles";
import Typography from "@mui/material/Typography";

export const DocumentCategoryTitle = ({children}: {children: string}) => {
  const styles = useDocumentTitleStyles();
  return (
    <Typography className={styles.documentsCategoryTitle} component="div">
      {children}
    </Typography>
  );
};
// TODO: Reference colors from the theme
export const useDocumentTitleStyles = makeStyles((theme: Theme) => ({
  documentsCategoryTitle: {
    fontWeight: 600,
    fontSize: 18,
    color: "#222238",
    padding: "0 24px 31px 0",
  },
  imageView: {
    cursor: "pointer",
    width: 42,
    height: 42,
    borderRadius: "50%",
    border: "2px solid #FFFFFF",
    boxShadow: "0px 4px 14px rgba(203, 205, 217, 0.4)",
  },
  fileBox: {
    background: "#FFFFFF",
    boxShadow: "0px 4px 20px rgba(203, 205, 217, 0.4)",
    borderRadius: theme.shape.borderRadius,
    padding: "13px 5px 13px 20px",
  },
  documentIconButton: {
    height: 42,
    width: 42,
    borderRadius: 6,
    backgroundColor: lighten(theme.palette.primary.main, 0.9),
    "&:hover": {
      backgroundColor: lighten(theme.palette.primary.main, 0.9),
    },
  },
  filename: {
    fontSize: 14,
    color: "#222238",
    marginLeft: 14,
    flex: 1,
    cursor: "pointer",
  },
}));
