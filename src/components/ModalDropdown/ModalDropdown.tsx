import React from "react";
import Button from "@mui/material/Button";
import {Theme} from "@mui/material/styles";
import createStyles from "@mui/styles/createStyles";
import makeStyles from "@mui/styles/makeStyles";
import ArrowDropDown from "@mui/icons-material/ArrowDropDown";
import ModalDropdownPopper from "./ModalDropdownPopper";
import useCustomDropdownHandlers, {
  CustomDropdownHandlers,
} from "../../utils/useCustomDropdownHandlers";

export interface CustomDropdownProps {
  value?: string | number;
  options: (string | number)[];
  labels?: string[];
  onChange?: (index: number) => void;
}

export interface CustomDropdownPopperProps extends CustomDropdownHandlers {
  children: JSX.Element;
  placement?:
    | "bottom"
    | "bottom-end"
    | "bottom-start"
    | "left-end"
    | "left-start"
    | "left"
    | "right-end"
    | "right-start"
    | "right"
    | "top-end"
    | "top-start"
    | "top"
    | undefined;
}

/**
 * Sample custom dropdown
 *
 * @param props
 */
const ModalDropdown = (props: CustomDropdownProps) => {
  const classes = useCustomDropdownStyles();
  const handlers = useCustomDropdownHandlers(
    props.options,
    props.onChange,
    props.value,
    props.labels,
  );
  return (
    <ModalDropdownPopper {...handlers}>
      <Button
        className={classes.selectedItemsButtons}
        endIcon={<ArrowDropDown />}
        onClick={handlers.handleToggle}
      >
        {handlers.currentValue}
      </Button>
    </ModalDropdownPopper>
  );
};

export const useCustomDropdownStyles = makeStyles((theme: Theme) =>
  createStyles({
    selectedItemsButtons: {
      backgroundColor: (props?: {isSelected?: boolean}) =>
        props?.isSelected ? theme.palette.primary.main : "#F6F8FA",
      color: (props?: {isSelected?: boolean}) =>
        props?.isSelected ? theme.palette.common.white : "#98A2B7",
      boxShadow: "none",
      padding: "4px 8px",
      marginRight: 20,
      "&:hover": {
        backgroundColor: (props?: {isSelected?: boolean}) =>
          props?.isSelected ? theme.palette.primary.main : "#F6F8FA",
      },
    },
  }),
);

export default ModalDropdown;
