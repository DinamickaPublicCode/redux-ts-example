import React from 'react';
import Box from "@mui/material/Box";
import { ComponentStory, ComponentMeta } from '@storybook/react';
import TablePagination from './TablePagination';

export default {
  title: 'Table pagination',
  component: TablePagination,
} as ComponentMeta<typeof TablePagination>;


const Template: ComponentStory<typeof TablePagination> = (args) => <TablePagination {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  count: 100,
  page: 1,
  rowsPerPage: 10,
  rowsPerPageOptions: [10, 20, 50, 100],
  onChangePage: () => false,
  onChangeRowsPerPage: () => false,
}
