import React from "react";
import {CarDealerSale} from "src/types";
import SalesCalendarCard from "./SalesCalendarCard";

const SalesCalendarCarList = ({
  currentSaleId,
  currentSalesPerDay,
  changeCurrentCarInfo,
}: {
  currentSaleId: string;
  changeCurrentCarInfo: (car: CarDealerSale) => void;
  currentSalesPerDay: CarDealerSale[] | null;
}) => (
  <div>
    {currentSalesPerDay?.length
      ? currentSalesPerDay.map((d) => {
          return (
            <SalesCalendarCard
              key={d.id}
              car={d}
              onClick={changeCurrentCarInfo}
              isSelected={d.id === currentSaleId}
            />
          );
        })
      : null}
  </div>
);

export default SalesCalendarCarList;
