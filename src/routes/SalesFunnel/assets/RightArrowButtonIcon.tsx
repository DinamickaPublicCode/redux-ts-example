import React from "react";

export const RightArrowButtonIcon = () => (
  <svg
    width="20"
    height="20"
    viewBox="0 0 20 20"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <rect
      width="20"
      height="20"
      rx="4"
      transform="matrix(-1 0 0 1 20 0)"
      fill="#F6F8FA"
    />
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M8.32412 6.27314C8.62839 6.00333 9.10289 6.02139 9.38394 6.31349L12.4336 9.48297C12.8718 9.93844 12.8412 10.6491 12.3653 11.0682L9.34003 13.7326C9.0348 14.0014 8.56037 13.9818 8.28035 13.6888C8.00033 13.3958 8.02077 12.9403 8.326 12.6715L11.1048 10.2242L8.28208 7.29058C8.00103 6.99848 8.01984 6.54296 8.32412 6.27314Z"
      fill="#1A06F9"
    />
  </svg>
);
