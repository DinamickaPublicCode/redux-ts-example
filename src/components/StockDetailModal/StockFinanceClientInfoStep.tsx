import React, {useMemo} from "react";

import {Formik} from "formik";
import * as yup from "yup";
import {useTranslation} from "react-i18next";

import Grid from "@mui/material/Grid";
import {TelephoneMaskComponent} from "../TelephoneMaskComponent";
import StockFinanceAccordion from "./StockFinanceAccordion";
import CartagInputField from "../CartagInputField";
import {StepsCheckedIcon, StepsUpIcon} from "src/assets/icons";
import {ClientInformation} from "../../types/stockFinanceTab";
import {STOCK_DETAIL_TRANSLATION_KEY} from "../../system/constants";
import {InputBaseComponentProps} from "@mui/material/InputBase";

interface StockFinanceStepProps<T> {
  isEditing: boolean;
  isValidated: boolean;
  initialValues: T;
  onSetEditing: () => void;
  onSubmit: (values: T) => void;
}

export const StockFinanceClientInfoStep = ({
  isEditing,
  isValidated,
  initialValues,
  onSubmit,
  onSetEditing,
}: StockFinanceStepProps<ClientInformation>) => {
  const {t} = useTranslation();
  const {t: ft} = useFinanceTabTranslation();
  const shouldHideButton = !isValidated && !isEditing;

  const validationSchema: yup.ObjectSchema<
    yup.Shape<object | undefined, ClientInformation>
  > = useMemo(() => {
    return yup.object().shape({
      saleReference: yup.string().required("Sale reference is required"),
      name: yup.string().required("Name is required"),
      telephone: yup
        .string()
        .max(12, "max length 10 symbols")
        .required("Telephone is Required"),
      email: yup.string().email().required("Email is required"),
    });
  }, []);

  return (
    <Formik
      initialValues={initialValues}
      enableReinitialize
      validationSchema={validationSchema}
      onSubmit={async (values) => {
        await onSubmit(values);
      }}>
      {({handleSubmit, handleChange, handleBlur, values, touched, errors}) => (
        <StockFinanceAccordion
          onButtonClicked={
            shouldHideButton
              ? undefined
              : () => {
                  if (!isEditing) {
                    onSetEditing();
                    return;
                  }
                  handleSubmit();
                }
          }
          isEditButton={isValidated}
          stepIcon={isValidated ? <StepsCheckedIcon /> : <StepsUpIcon />}
          panelId={`${ft("Step")} 1`}
          panelName={ft("Client_Information")}
          open={isEditing}>
          <form
            style={{
              padding: 24,
              width: "100%",
            }}
            onSubmit={handleSubmit}>
            <Grid container spacing={1}>
              <Grid container item xs={12} spacing={6}>
                <Grid item xs={6}>
                  <CartagInputField
                    labelText={ft("Sale_Reference")}
                    name="saleReference"
                    error={!!touched.saleReference && !!errors.saleReference}
                    helperText={errors.saleReference}
                    value={values.saleReference}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                </Grid>
                <Grid item xs={6}></Grid>
              </Grid>
              <Grid container item xs={12} spacing={6}>
                <Grid item xs={6}>
                  <CartagInputField
                    labelText={ft("Client")}
                    name="name"
                    error={!!touched.name && !!errors.name}
                    helperText={errors.name}
                    value={values.name}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                </Grid>
              </Grid>
              <Grid container item xs={12} spacing={6}>
                <Grid item xs={6}>
                  <CartagInputField
                    labelText={t("Telephone")}
                    name="telephone"
                    type="tel"
                    InputProps={{
                      inputComponent:
                        TelephoneMaskComponent as React.ElementType<InputBaseComponentProps>,
                    }}
                    error={!!touched.telephone && !!errors.telephone}
                    helperText={errors.telephone}
                    value={values.telephone}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                </Grid>
                <Grid item xs={6}>
                  <CartagInputField
                    labelText={t("Email")}
                    name="email"
                    type="email"
                    error={!!touched.email && !!errors.email}
                    helperText={errors.email}
                    value={values.email}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                </Grid>
              </Grid>
            </Grid>
          </form>
        </StockFinanceAccordion>
      )}
    </Formik>
  );
};

const useFinanceTabTranslation = () => {
  const {t} = useTranslation();
  return {
    t: (key: string) => t(`${STOCK_DETAIL_TRANSLATION_KEY}.FINANCE_TAB.${key}`),
  };
};
