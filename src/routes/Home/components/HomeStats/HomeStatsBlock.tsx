import ExpandMore from "@mui/icons-material/ExpandMore";
import ExpandLess from "@mui/icons-material/ExpandLess";
import React, {useEffect, useMemo, useState} from "react";
import CountUp from "src/components/CountUp";
import HomeStatsSmallChart from "./HomeStatsSmallChart";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import {ParentSize} from "@visx/responsive";

export type StatsBlockData = {
  icon: (color: string) => JSX.Element;
  graph: number[];
  textA: string;
  textB: string;
  /**
   * Using rgb array since we need this color for a graph gradient
   */
  color: number[];
  number: number;
  percentString: string;
  direction: "up" | "down";
};

const HomeStatsBlock = ({data}: {data: StatsBlockData}) => {
  const htmlColor = useMemo(
    () => `rgb(${data.color[0]}, ${data.color[1]}, ${data.color[2]})`,
    [data.color],
  );
  const [loaded, setLoaded] = useState(false);
  useEffect(() => {
    const timeout = setTimeout(() => setLoaded(true), 100);
    return () => clearTimeout(timeout);
  }, [data.number]);
  return (
    <Box
      style={{
        position: "relative",
      }}
      sx={{
        background: "#FFFFFF",
        boxShadow: "0px 4px 20px rgba(20, 32, 100, 0.08)",
        borderRadius: "10px",
        padding: "30px 18px 23px 23px",
      }}
    >
      <Box
        sx={{
          background: "white",
          position: "absolute",
          right: "-20px",
          top: "-20px",
          width: "46px",
          height: "46px",
          borderRadius: "50%",
          display: "block",
          border: `1px solid rgb(${data.color[0]}, ${data.color[1]}, ${data.color[2]})`,
        }}
      >
        <Box
          sx={{
            position: "absolute",
            top: "-18px",
            left: "-21px",
          }}
        >
          {data.icon(
            `rgb(${data.color[0]}, ${data.color[1]}, ${data.color[2]})`,
          )}
        </Box>
      </Box>
      <Grid container>
        <Grid item xs={7}>
          <Box>
            <Typography
              component="div"
              sx={{
                fontStyle: "normal",
                fontWeight: "normal",
                fontSize: "14px",
                lineHeight: "18.2px",
                color: "#98A2B7",
              }}
            >
              {data.textA}
            </Typography>
            <Typography
              component="div"
              style={{
                marginTop: 2,
              }}
              sx={{
                fontStyle: "normal",
                fontWeight: 600,
                fontSize: "14px",
                lineHeight: "17.5px",
                color: "#222238",
                // Adding line truncation, for keeping the block height consistent
                whiteSpace: "nowrap",
                overflow: "hidden",
                textOverflow: "ellipsis",
              }}
            >
              {data.textB}
            </Typography>
          </Box>
          <Typography
            component="div"
            style={{
              marginTop: 20,
            }}
            sx={{
              fontStyle: "normal",
              fontWeight: "bold",
              fontSize: "28px",
              lineHeight: "36.4px",
              color: "#222238",
            }}
          >
            <CountUp end={data.number} loaded={loaded} duration={1} />
          </Typography>
        </Grid>
        <Grid item xs={5}>
          <div
            style={{
              height: "100%",
              display: "flex",
              flexDirection: "column",
            }}
          >
            <div style={{flex: 1}}>
              <ParentSize>
                {({width, height}) => {
                  return (
                    <>
                      <HomeStatsSmallChart
                        size={{
                          width,
                          height,
                        }}
                        color={data.color}
                        data={data.graph}
                      />
                    </>
                  );
                }}
              </ParentSize>
            </div>
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                justifyContent: "space-between",
              }}
            >
              <Typography
                component="div"
                sx={{
                  fontStyle: "normal",
                  fontWeight: 600,
                  fontSize: "12px",
                  lineHeight: "15px",
                }}
                style={{
                  color: htmlColor,
                  marginLeft: 11,
                }}
              >
                {data.percentString}
              </Typography>
              <Typography component="div">
                {data.direction === "down" ? (
                  <ExpandMore fontSize="small" htmlColor={htmlColor} />
                ) : (
                  <ExpandLess fontSize="small" htmlColor={htmlColor} />
                )}
              </Typography>
            </Box>
          </div>
        </Grid>
      </Grid>
    </Box>
  );
};

export default HomeStatsBlock;
