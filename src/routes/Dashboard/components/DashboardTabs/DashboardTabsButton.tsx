import React, {useRef, useState} from "react";
import {useTranslation} from "react-i18next";
import {TabViewsType} from "src/types";
import Box from "@mui/material/Box";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import {styled, useTheme} from "@mui/material/styles";
import DashboardTabsMenu from "./DashboardTabsMenu";
import DashboardTabsDialogUnpinView from "./DashboardTabsDialogUnpinView";

interface DashboardTabButtonProps {
  tabId: TabViewsType;
  tab: {
    name: TabViewsType;
    icon: JSX.Element;
  };
  handleChangeTab: (
    name: TabViewsType,
  ) => (event: React.SyntheticEvent) => void;
}

const DashboardTabButton: React.FC<DashboardTabButtonProps> = ({
  tab,
  tabId,
  handleChangeTab,
}) => {
  const {t} = useTranslation();

  const [isMenuOpen, setIsMenuOpen] = useState<boolean>(false);
  const [isDialogOpen, setIsDialogOpen] = useState<boolean>(false);
  const anchorRef = useRef<HTMLButtonElement>(null);

  const handleTabIconClick = (_event: React.MouseEvent<HTMLElement>) => {
    setIsMenuOpen((prevState) => !prevState);
  };
  const handleDialogClose = () => {
    setIsDialogOpen((prevState) => !prevState);
  };
  const theme = useTheme();
  return (
    <TabTitleBox
      sx={
        tabId === tab.name
          ? {
              "&::before": {
                boxShadow: "0px -10px 20px rgba(20, 32, 100, 0.08)",
                backgroundColor: "#fff",
              },
            }
          : undefined
      }
      onClick={handleChangeTab(tab.name)}>
      <TitleText
        sx={
          tabId === tab.name
            ? {
                color: theme.palette.primary.main,
              }
            : undefined
        }>
        {t(tab.name)}
      </TitleText>
      <IconButton
        ref={anchorRef}
        aria-controls={isMenuOpen ? "menu-list-grow" : undefined}
        aria-haspopup="true"
        size="small"
        style={{marginLeft: 5}}
        onClick={handleTabIconClick}>
        {tab.icon}
      </IconButton>

      <DashboardTabsMenu
        tabName={tab.name}
        element={anchorRef}
        open={isMenuOpen}
        setOpen={setIsMenuOpen}
        handleDialogClose={handleDialogClose}
      />

      <DashboardTabsDialogUnpinView
        tabName={tab.name}
        open={isDialogOpen}
        handleClose={handleDialogClose}
      />
    </TabTitleBox>
  );
};

const TabTitleBox = styled(Box)(({theme}) => ({
  position: "relative",
  cursor: "pointer",
  display: "flex",
  alignItems: "center",
  padding: "0 32px",
  color: "#98A2B7",
  zIndex: 10,
  [theme.breakpoints.down("sm")]: {
    width: "100%",
  },
  "&::before": {
    content: '""',
    position: "absolute",
    display: "block",
    top: 0,
    right: 0,
    bottom: -3,
    left: 0,
    borderRadius: "10px 10px 0 0",
    transform: "perspective(5px) rotateX(1deg) translateZ(-1px)",
    transformOrigin: "0 0",
    backfaceVisibility: "hidden",
    zIndex: -1,
    transition: "0.3s background-color ease-in-out",
  },
}));

const TitleText = styled(Typography)(({theme}) => ({
  lineHeight: 2.5,
  fontWeight: 500,
  textTransform: "capitalize",
  [theme.breakpoints.down("sm")]: {
    fontSize: "12px",
  },
  [theme.breakpoints.down("sm")]: {
    fontSize: "10px",
  },
  [theme.breakpoints.down("sm")]: {
    fontSize: "15px",
  },
}));

export default DashboardTabButton;
