import useHomeTranslation from "../../utils/useHomeTranslation";
import {HomeInventoryFilter} from "../../types/homeInventoryFilter";
import {useHomeContext} from "../../state/HomeContextProvider";
import React from "react";
import ModalSelectDropdown from "../../../../components/ModalDropdown/ModalSelectDropdown";

const HomeLayoutInventoryDropdown = () => {
  const {t} = useHomeTranslation();
  const BASE_KEY = "new/used/all";
  const labels: string[] = t(BASE_KEY)
    .split("/")
    .map((key) => key + " inventory");
  const values: HomeInventoryFilter[] = BASE_KEY.split(
    "/",
  ) as unknown[] as HomeInventoryFilter[];
  const {inventoryType, setInventoryType} = useHomeContext();

  return (
    <ModalSelectDropdown<HomeInventoryFilter>
      value={inventoryType}
      onChange={setInventoryType}
      labels={labels}
      values={values}
    />
  );
};
export default HomeLayoutInventoryDropdown;
