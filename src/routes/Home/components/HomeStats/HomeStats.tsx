import React from "react";
import HomeStatsHelloBox from "./HomeStatsHelloBox";
import HomeStatsBlocks from "./HomeStatsBlocks";
import HomeStatsCompletionBox from "./HomeStatsCompletionBox";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import {useTheme} from "@mui/material/styles";

const HomeStats = () => {
  const theme = useTheme();
  return (
    <Box>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={12} md={4}>
          <HomeStatsHelloBox />
        </Grid>
        <Grid
          item
          xs={12}
          sm={12}
          md={4}
          sx={{
            [theme.breakpoints.down("sm")]: {
              display: "none",
            },
          }}
        >
          <Grid
            container
            direction="column"
            justifyContent="space-between"
            style={{
              height: "100%",
            }}
          >
            <HomeStatsBlocks />
          </Grid>
        </Grid>
        <Grid item xs={12} sm={12} md={4}>
          <HomeStatsCompletionBox />
        </Grid>
      </Grid>
    </Box>
  );
};

export default HomeStats;
