/**
 * Safely return public url concatenator.
 *
 */
const usePublicUrl = () => {
  let publicUrl = "";
  try {
    publicUrl = process.env.PUBLIC_URL!;
  } catch (_) {}
  return (tail: string) => {
    if (tail.startsWith("/")) {
      tail = tail.substring(1);
    }
    return `${publicUrl}/${tail}`;
  };
};

export default usePublicUrl;
