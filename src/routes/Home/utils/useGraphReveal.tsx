import {useEffect, useMemo, useState} from "react";

const useGraphReveal = (
  data: unknown[],
  nulifier: (x: unknown) => void,
  duration = 230,
) => {
  const [revealed, setRevealed] = useState(false);
  useEffect(() => {
    const timeout = setTimeout(() => setRevealed(true), duration);
    return () => {
      clearTimeout(timeout);
    };
  }, [duration]);

  return useMemo(() => {
    if (!revealed) {
      return data.map(nulifier);
    } else {
      return data;
    }
  }, [data, nulifier, revealed]);
};

export default useGraphReveal;
