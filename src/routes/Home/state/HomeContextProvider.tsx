import React, {createContext, useState, useContext} from "react";
import {HomeInventoryFilter} from "../types/homeInventoryFilter";

interface HomeContextData {
  drawerOpen: boolean;
  setDrawerOpen: React.Dispatch<React.SetStateAction<boolean>>;
  inventoryType: HomeInventoryFilter;
  setInventoryType: React.Dispatch<React.SetStateAction<HomeInventoryFilter>>;
  globalDate: Date;
  setGlobalDate: React.Dispatch<React.SetStateAction<Date>>;
  rangeType: "year" | "month";
  setRangeType: React.Dispatch<React.SetStateAction<"year" | "month">>;
}

const HomeContext = createContext(null as null | HomeContextData);

export const useHomeContext = () => {
  return useContext(HomeContext) as HomeContextData;
};

const HomeContextProvider = ({
  children,
}: {
  children: JSX.Element | JSX.Element[];
}) => {
  const [drawerOpen, setDrawerOpen] = useState(false);
  const [inventoryType, setInventoryType] =
    useState<HomeInventoryFilter>("all");
  const [globalDate, setGlobalDate] = useState(new Date());
  const [rangeType, setRangeType] = useState<"year" | "month">("month");

  return (
    <HomeContext.Provider
      value={{
        drawerOpen,
        setDrawerOpen,
        inventoryType,
        setInventoryType,
        globalDate,
        setGlobalDate,
        rangeType,
        setRangeType,
      }}
    >
      {children}
    </HomeContext.Provider>
  );
};

export default HomeContextProvider;
