import React from "react";

import SalesCalendarLayout from "./components/SalesCalendarLayout";

const SalesCalendar = () => {
  return <SalesCalendarLayout />;
};

export default SalesCalendar;
