import React from "react";
import SettingsLayout from "./components/SettingsLayout";

const Settings = () => {
  return <SettingsLayout />;
};

export default Settings;
