import React from 'react';
import Box from "@mui/material/Box";
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { TitleButton } from './DashboardLayout';

export default {
  title: 'Button',
  component: TitleButton,
} as ComponentMeta<typeof TitleButton>;


const Template: ComponentStory<typeof TitleButton> = (args) => {
  return (
    <TitleButton sx={args.style}>{args.title}</TitleButton>
  )
}

export const Primary = Template.bind({});
Primary.args = {
  title: "Button title",
  style: {
    boxShadow: "0px 3px 14px rgba(2, 53, 236, 0.3)",
    color: '#fff',
    backgroundColor: '#1A06F9'
  }
};

export const Secondary = Template.bind({});
Secondary.args = {
  title: "Button title",
  style: {
    boxShadow: "0px 3px 14px rgba(2, 53, 236, 0.3)",
    color: '#1A06F9',
    backgroundColor: "#fff!important",
  }
};
