import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import CircularProgress from "@mui/material/CircularProgress";
import React, {useCallback, useEffect, useRef, useState} from "react";
import dayjs from "dayjs";
import clsx from "clsx";
import {useActivityBoxContext} from "src/components/ActivityBox/ActivityBox";
import {
  ActivityBoxMessage,
  ActivityBoxMessageType,
  ActivityBoxType,
} from "src/types/activityBoxInterfaces";
import {Theme} from "@mui/material/styles";
import {makeStyles} from "@mui/styles";

export type MessageContent = {
  title?: string;
  text?: string;
  date?: number;
  cancelledDate?: number;
};

const MessageContentView = ({message}: {message: MessageContent}) => {
  const styles = useMessageListStyles();
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
      }}
      className={styles.messageBoxContent}
    >
      {message.title && (
        <Typography className={styles.messageTitle} component="div">
          {message.title}
        </Typography>
      )}
      {message.cancelledDate && (
        <Box>
          <Typography
            className={clsx(styles.cancelledDate, styles.theDate)}
            component="div"
          >
            {dayjs(message.cancelledDate).format("MM-DD-YYYY")}
          </Typography>
        </Box>
      )}
      {message.date && (
        <Box>
          <Typography className={styles.theDate} component="div">
            {dayjs(message.date).format("MM-DD-YYYY")}
          </Typography>
        </Box>
      )}
      {message.text && <Typography component="div">{message.text}</Typography>}
    </div>
  );
};

function MessageItem({message}: {message: ActivityBoxMessage}) {
  const styles = useMessageListStyles();
  const [avatarUrl, setAvatarUrl] = useState("");
  const {onRequestUserAvatar} = useActivityBoxContext();
  useEffect(() => {
    onRequestUserAvatar(message.user.id, message.user.name, setAvatarUrl);
  }, [message.user.id, message.user.name, setAvatarUrl, onRequestUserAvatar]);
  return (
    <Box
      className={styles.messageItem}
      sx={{
        display: "flex",
        flexDirection: "column",
      }}
    >
      <Box
        display="flex"
        flexDirection="row"
        justifyContent="space-between"
        alignItems="center"
        className={styles.messageHeader}
      >
        <Box
          className={styles.avatar}
          style={
            avatarUrl.length !== 0
              ? {
                  backgroundImage: `url(${avatarUrl})`,
                  backgroundSize: "16px",
                  backgroundPosition: "50%",
                }
              : {}
          }
        ></Box>
        <Box sx={{flex: 1}}>
          <Typography className={styles.userName}>
            {message.user.name}
          </Typography>
        </Box>
        <Box>
          <Typography className={styles.messageDate}>
            {message.dateDisplay}
          </Typography>
        </Box>
      </Box>
      <MessageContentView message={message.message} />
    </Box>
  );
}

export function MessageSubmitForm({
  type,
}: {
  type: ActivityBoxMessageType | null;
}) {
  const [message, setMessage] = useState("");
  const styles = useMessageListStyles();
  const textareaRef = useRef(null);
  const [isLoading, setIsLoading] = useState(false);
  const {onMessageSend} = useActivityBoxContext();

  if (type === null) {
    return <Box sx={{height: 21}} />;
  }

  return (
    <Box className={styles.messageBoxContent} margin="21px 0">
      <Box display="flex" flexDirection="row" alignItems="center">
        <Box flex="1" paddingRight="10px">
          <textarea
            ref={textareaRef}
            rows={1}
            onKeyUp={(e) => {
              if (e.key === "Enter" && type !== null && !isLoading) {
                if (!e.shiftKey) {
                  e.preventDefault();
                  onMessageSend(type, message, setIsLoading);
                  setMessage("");
                }
              }
            }}
            style={{
              width: "100%",
              border: 0,
              outline: 0,
              overflow: "hidden",
              resize: "none",
              fontFamily: "inherit",
              fontSize: 14,
              lineHeight: "19.25px",
            }}
            placeholder="Leave a comment"
            value={message}
            onChange={(e) => setMessage(e.target.value)}
          />
        </Box>
        {isLoading && <CircularProgress size={12} />}
      </Box>
    </Box>
  );
}

// TODO: Decouple div logic into custom hook
// TODO: fix jerkyness on messages loading
// Check if needs to scroll to bottom on messages array refresh...
const MessageList = ({
  tabIndex,
  style,
  type,
}: {
  type: ActivityBoxType;
  tabIndex: number;
  style?: React.CSSProperties;
}) => {
  const styles = useMessageListStyles();
  const {messages, tabs, onRequestMoreMessages} = useActivityBoxContext();
  const [isLoading, setIsLoading] = useState(false);
  const tab = tabs[tabIndex];
  const [filteredMessages, setFilteredMessages] = useState(
    [] as ActivityBoxMessage[],
  );

  const scrollHeight = useRef(0);
  const messageBoxRef = useRef(null);
  const onScrollToBottom = () => {
    const theDiv = messageBoxRef.current as unknown as HTMLElement;
    theDiv.scrollTop = theDiv.scrollHeight;
  };

  const onScrollToTop = useCallback(() => {
    if (isLoading) {
      return;
    }
    const theDiv = messageBoxRef.current as unknown as HTMLElement;
    // first store the total height of the div
    scrollHeight.current = theDiv.scrollHeight;
    // request stuff
    onRequestMoreMessages(setIsLoading);
  }, [isLoading, onRequestMoreMessages]);

  useEffect(() => {
    const theDiv = messageBoxRef.current as unknown as HTMLElement;

    const scrollTopCallback = () => {
      if (theDiv.scrollTop === 0) {
        onScrollToTop();
      }
    };
    theDiv.addEventListener("scroll", scrollTopCallback);
    return () => {
      theDiv.removeEventListener("scroll", scrollTopCallback);
    };
  }, [onScrollToTop]);

  useEffect(() => {
    onScrollToBottom();
  }, [filteredMessages]);

  useEffect(() => {
    const theDiv = messageBoxRef.current as unknown as HTMLElement;

    if (
      scrollHeight.current !== 0 &&
      theDiv.scrollHeight - scrollHeight.current > 150
    ) {
      theDiv.scrollTop = theDiv.scrollHeight - scrollHeight.current;
      scrollHeight.current = 0;
    }
  });

  useEffect(() => {
    const filtered = messages.filter(tab.filter).reverse();
    setFilteredMessages(filtered);
  }, [messages, tabIndex, tab.filter]);

  return (
    <Box padding="0 24px" style={style}>
      <Box
        ref={messageBoxRef}
        className={
          type === ActivityBoxType.MODAL
            ? styles.messagesContainer
            : styles.messagesContainerFullheight
        }
      >
        {isLoading && (
          <Box
            display="flex"
            flexDirection="row"
            justifyContent="center"
            marginTop="10px"
          >
            <CircularProgress size={32} />
          </Box>
        )}
        {filteredMessages.length ? (
          filteredMessages.map((message, _) => (
            <MessageItem key={message.id} message={message} />
          ))
        ) : (
          <Box height="400px" />
        )}
      </Box>
      <MessageSubmitForm type={tab.sendMessageType} />
    </Box>
  );
};

// TODO: Reference colors from the theme
export const useMessageListStyles = makeStyles((theme: Theme) => ({
  avatar: {
    width: 20,
    height: 20,
    border: "2px solid #FFFFFF",
    boxShadow: "0px 4px 4px rgba(203, 205, 217, 0.25)",
    marginRight: 10,
    borderRadius: "50%",
    backgroundColor: "#fff",
  },
  messageBoxContent: {
    borderRadius: "0 14px 14px 14px",
    padding: "14px 20px 16px 20px",
    backgroundColor: theme.palette.common.white,
    fontSize: 14,
    lineHeight: "19.25px",
  },
  messagesContainerFullheight: {
    height: "calc(100vh - 422px)",
    overflowY: "scroll",
    overflowX: "hidden",
  },
  messagesContainer: {
    height: 568,
    // eslint-disable-next-line no-useless-computed-key
    ["@media (max-height:950px)"]: {
      height: 420,
    },
    // eslint-disable-next-line no-useless-computed-key
    ["@media (max-height:760px)"]: {
      height: 320,
    },
    overflowY: "scroll",
    overflowX: "hidden",

    "&::-webkit-scrollbar-track": {
      background: "rgb(246, 248, 250)" /* color of the tracking area */,
    },
  },
  messageHeader: {
    marginBottom: 10,
  },
  messageItem: {
    marginTop: 18,
    marginBottom: 6,
  },
  messageTitle: {
    color: "#98A2B7",
    textTransform: "uppercase",
    lineHeight: "17.5px",
    fontWeight: 600,
    marginBottom: 8,
  },
  theDate: {
    padding: "5px 9px",
    borderRadius: "4px",
    display: "inline-block",
  },
  cancelledDate: {
    background: "#F1F0FF",
    textDecoration: "line-through",
  },
  userName: {
    color: "#222238",
    fontWeight: 600,
    fontSize: 14,
    lineHeight: "17.25px",
  },
  messageDate: {
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: "14px",
    lineHeight: "18.2px",

    textAlign: "right",
    color: "#98A2B7",
  },
}));

export default MessageList;
