import {Action} from "redux";

export interface LogInUser {
  username: string;
  password: string;
}

export type CarTypes = "ALL" | "NEW" | "USED";

export type SellersRoleType = "ROLE_SELLER_NEW" | "ROLE_SELLER_USED";
export type FinanceManagersRoleType = "ROLE_FINANCE_MANAGER";
export type UsersRoleType = "ROLE_ADMIN" | "ROLE_MANAGER";

export type StageType =
  | "NEW_INV"
  | "FIRST_PREPARATION_NOT_DONE"
  | "READY_FOR_SALE"
  | "WAITING_APV_CREDIT"
  | "PREPARATION_DELIVERY"
  | "READY_FOR_DELIVERY"
  | "DELIVERY_DONE"
  | "COUNTED"
  | "WHOLESALE"
  | "OUTSOURCING";

export type StagePreparationType =
  | "REQUEST_NEW"
  | "REQUEST_NEW_WITH_WO"
  | "REQUEST_HIGH_PRIORITY"
  | "REQUEST_HIGH_PRIORITY_WITH_WO"
  | "REQUEST_DONE"
  | "REQUEST_REFUSED"
  | "REQUEST_TO_COMPLETE";

export type ItemReadinessIcons =
  | "MEC_INS"
  | "MEC_REP"
  | "EST_WASH"
  | "EST_PHOTO"
  | "CREDIT"
  | "DELIV";

export type TabViewsType =
  | "INVENTORY"
  | "MECHANIC"
  | "ESTHETIC"
  | "FINANCE"
  | "DELIVERY";

export enum TabViews {
  INVENTORY = "INVENTORY",
  MECHANIC = "MECHANIC",
  ESTHETIC = "ESTHETIC",
  FINANCE = "FINANCE",
  DELIVERY = "DELIVERY",
}

export type DepartmentType =
  | "mechanic"
  | "esthetic"
  | "sale"
  | "delivery"
  | "inventory"
  | "finance";

export interface UserType {
  fullName: string;
  login: string;
  defaultCarDealer: {
    name: string;
    id: string;
  };
  authorities: UsersRoleType | SellersRoleType | FinanceManagersRoleType[];
  createdDate: Date;
  langKey?: string;
  createdBy?: string;
  imageUrl?: string;
  id: string;
  email: string;
  activated: boolean;
  views?: TabViewsType[];
  password?: string;
  lastConnexionDate?: string;
  notification: NotificationSettings;
}

export interface NotificationSettings {
  channels: NotificationChannel[];
  topics: NotificationTopic[];
}

export interface NotificationChannel {
  type: string;
  value?: string;
}

export interface NotificationTopic {
  type: string;
}

export type DocumentInfoType =
  | "ACCESSORIES"
  | "PHOTO"
  | "INSPECTION"
  | "CAR_PROOF"
  | "GARANTIE"
  | "HISTO";

export interface DocumentInfo {
  id: string;
  type: DocumentInfoType;
  name: string;
}

export interface CarDealer {
  id: string;
  carDealerName: string;
  departments: DepartmentSpecification[];
  groupAccessories: CarDealerGroupAccessories[];
  legends: CarDealerLegend[];
}

export interface DepartmentSpecification {
  departmentName: string;
  departmentType: string;
  steps?: StepSpecification[];
}

export interface CarDealerLegend {
  colorLegend: string;
  idLegend: string;
  labelLegend: string;
}

export interface AccessoriesType {
  idAcc: string;
  name: string;
  price: number;
}
export interface CarDealerGroupAccessories {
  accessories: AccessoriesType[];
  idGroupAcc: string;
  name: string;
}

export interface RequestSpecification {
  idRequest: string;
  labelRequest: string;
  carType: string;
}

export interface ActionSpecification {
  idAction: string;
  labelAction: string;
  carType: string;
}

export interface StepSpecification {
  stepDescription: string;
  stepName: string;
  stepType: string;
  iwWorkOrder: boolean;
  requests?: RequestSpecification[];
  actions?: ActionSpecification[];
}

export interface ActionValue {
  idAction: string;
}

export interface StepValue {
  stepType: string;
  workOrderInfo?: {
    workOrderDate?: string;
    workOrderValue: string;
  };
  actions?: ActionValue[];
  requests?: RequestValue[];
}

export interface RequestValue {
  idRequest: string;
  dateRequest: string;
}

export interface GeneralInfo {
  color: string;
  door: number;
  fuel: string;
  km: number;
  make: string;
  model: string;
  newCar: boolean;
  numCle: string;
  price: number;
  serialNumberNiv: string;
  transmission: string;
  year: number;
}

export interface InventoryAndMechanicAndEstheticTabType {
  id: string;
  createdDate: Date;
  idCarDealer: string;
  inventoryType: string;
  location: string;
  referenceStock: string;
  generalInfo: GeneralInfo;
  files?: DocumentInfo[];
  stageInfo: {
    icons: ItemReadinessIcons[];
    stage: StageType;
    pipelineStages?: {
      abv: string;
      description: string;
      order: number;
      status: string;
      stageDate?: string;
    }[];
  };
  limitPreparationDate?: Date;
  departments?: {
    departmentType: string;
    status: string;
    limitPreparationDate?: string;
    steps?: StepValue[];
  }[];
}

interface DeliverySaleInfo {
  accessories: AccessoriesType[];
  legends: string[];
  clientInfo?: {
    // This is strange, but the "name" may come as full name field. It's worth to check if it exists first.
    fullName?: string;
    name?: string;
    email?: string;
    telNumber1?: string;
  };
  creditInfo?: {
    idFinanceManager?: string;
    nameFinanceManager?: string;
  };
  saleDate?: Date;
  saleReference?: string;
  sellerInfo?: {
    idSeller?: string;
    nameSeller?: string;
  };
}

export interface FinanceSaleInfo extends DeliverySaleInfo {
  deliveryInfo?: {
    deliveryBeginDate?: Date;
    deliveryEndDate?: Date;
    getDeliveryEndDate?: Date;
  };
}

export interface FinanceTabType extends InventoryAndMechanicAndEstheticTabType {
  /**
   * This field was only added from the documentation, no one knows if it's correct
   */
  sales: FinanceSaleInfo[];
  saleInfo?: FinanceSaleInfo;
}

export type RowDataType = FinanceTabType[];

export interface PagedRowDataType {
  content: RowDataType;
  empty: boolean;
  first: boolean;
  last: boolean;
  number: number;
  numberOfElements: number;
  size: number;
  totalElements: number;
  totalPages: number;
  pageable: {
    offset: number;
    pageNumber: number;
    pageSize: number;
    paged: boolean;
    sort: {
      sorted: boolean;
      unsorted: boolean;
      empty: boolean;
    };
    unpaged: boolean;
  };
  sort: {
    empty: boolean;
    sorted: boolean;
    unsorted: boolean;
  };
}

/**
 * Inventory-related actions listed here.
 */
export enum UpdateInventoryDataType {
  DELIVERY = "DELIVERY",
  // Preparation request actions
  ADD_REQUEST = "ADD_REQUEST",
  SELL_AS_IS = "SELL_AS_IS",
  PREPARE_ON_DELIV = "PREPARE_ON_DELIV", // This action is used in Service tab as well
  // Service Tab
  PREPARATION = "PREPARATION",

  APV_CREDIT = "APV_CREDIT",
  REFUSE_CREDIT = "REFUSE_CREDIT",
}

/**
 * This interface is used for sending inventory data to the backend
 * currently used in delivery, service and prep. request tabs (CreateDealModal)
 */
export interface UpdateInventoryDataAction {
  action: UpdateInventoryDataType;
  /** Apparently, these are optional */
  idUser?: string;
  userName?: string;
  idCarDealer?: string;
}

export interface UpdateFinanceRequest extends UpdateInventoryDataAction {
  id: string;
  /** Since dates are specified as ISO strings, it's necessary to duplicate the interface code.  */
  limitPreparationDate: string;
  sales: {
    clientInfo: {
      name: string;
      telNumber1: string;
      email: string;
    };
    creditInfo: {
      idFinanceManager: string;
      nameFinanceManager: string;
    };
    saleDate: string;
    saleReference: string;
    sellerInfo: {
      idSeller: string;
      nameSeller: string;
    };
    deliveryInfo: {
      deliveryBeginDate: string;
    };
  }[];
  files: DocumentInfo[];
}

export interface CarDealerSeller {
  id: string;
  fullName: string;
  email: string;
  goals: Goal[];
  orderNew: string;
  orderUsed: string;
  authorities?: string[];
}

export interface Goal {
  year: string;
  month: string;
  goal: string;
  newCar: boolean;
}

export interface CarDealerSale {
  id: string;
  createdDate: Date | null;
  generalInfo: GeneralInfo;
  sales: CarDealerSaleEntry[];
  stageInfo: CarDealerSaleStageInfo;
  referenceStock: string;
  idCarDealer: string;
  limitPreparationDate: Date | null;
  inventoryType: string;
}

export interface CarDealerSaleEntry {
  saleReference: string;
  saleDate: string;
  legends: string[];
  sellerInfo: CarDealerSellerInfo;
  creditInfo?: {
    nameFinanceManager: string;
  };
  clientInfo: {
    fullName: string;
    email: string;
    telNumber1: string;
  };
  deliveryInfo: {
    deliveryBeginDate: Date | null;
    deliveryEndDate: Date | null;
  } | null;
}

export interface CarDealerSellerInfo {
  idSeller: string;
  nameSeller: string;
}

export interface CarDealerSaleStageInfo {
  icons: ItemReadinessIcons[];
  stage: StageType;
}

export interface TopSeller {
  id: string;
  fullName: string;
  total: string;
  totalYear: string;
}

export interface StatisticsData {
  department: string;
  totalDep: number | string;
  goalDep: number | null;
  details: Detail[];
}

export interface Detail {
  typeAction: string;
  labelAction: string;
  iconAction: string;
  total: string | number;
  goal: string | null;
  evolution: Evolution;
  avg: number[];
}

export interface Evolution {
  percent: string;
  data: number[];
  highlights: number[];
}

export type NotificationType =
  | "SALE"
  | "FINANCE"
  | "DELIVERY"
  | "MECHANIC"
  | "ESTHETIC";

export interface CTA {
  type: CtaType;
  ctaParams: {
    name: string;
    value: string;
  }[];
}

export type CtaType =
  | "GO_TO_SALE_PAGE"
  | "GO_TO_DELIVERY_CALENDAR"
  | "GO_TO_DASHBOARD_FINANCE"
  | "GO_TO_DASHBOARD_ALL_INV"
  | "GO_TO_DASHBOARD_DELIVERY"
  | "GO_TO_DASHBOARD_MECANIC"
  | "GO_TO_DASHBOARD_ESTHETIC"
  | "OPEN_TO_STOCK_DETAIL";

export interface Notification {
  id: string;
  type: NotificationType;
  idUser: string;
  message: string;
  status: "DELIVERED" | "READ";
  active: boolean;
  cta: CTA;
  details: {
    key: string;
    value: string;
  }[];
  createdDate: string;
}

export interface PagedNotifications {
  content: Notification[];
  empty: boolean;
  first: boolean;
  last: boolean;
  number: number;
  numberOfElements: number;
  size: number;
  totalElements: number;
  totalPages: number;
  pageable: {
    offset: number;
    pageNumber: number;
    pageSize: number;
    paged: boolean;
    sort: {
      sorted: boolean;
      unsorted: boolean;
      empty: boolean;
    };
    unpaged: boolean;
  };
  sort: {
    empty: boolean;
    sorted: boolean;
    unsorted: boolean;
  };
}
export interface PagedCarDealerSale {
  content: CarDealerSale[];
  empty: boolean;
  first: boolean;
  last: boolean;
  number: number;
  numberOfElements: number;
  size: number;
  totalElements: number;
  totalPages: number;
  pageable: {
    offset: number;
    pageNumber: number;
    pageSize: number;
    paged: boolean;
    sort: {
      sorted: boolean;
      unsorted: boolean;
      empty: boolean;
    };
    unpaged: boolean;
  };
  sort: {
    empty: boolean;
    sorted: boolean;
    unsorted: boolean;
  };
}
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export interface ReduxAction<T = string, P = any> extends Action<T> {
  type: T;
  payload?: P;
}


export type LoadingState = {
  loading: boolean;
};

export type CarDealerState = {
  inventories: PagedRowDataType | undefined;
  inventory: FinanceTabType | null;
  topSellers: TopSeller[];
  carDealerSalesNew: PagedCarDealerSale | undefined;
  carDealerSalesUsed: PagedCarDealerSale | undefined;
  carDealerSellers: CarDealerSeller[];
  carDealer: CarDealer | null;
  financeManagers: UserType[];
};

export type NotificationsState = {
  notificationsAll: Notification[] | undefined;
  notifications: Notification[] | undefined;
  pagesCount: number;
  notificationsCount: number;
  error: Error | null;
  isValidating: boolean;
  page: number;
  perPage: number;
  activeFilters: NotificationType[];
  switchCheck: "DELIVERED" | "ALL";
  switchCheckArchived: boolean;
};

export type StatisticsState = {
  mechanic: StatisticsData | undefined;
  esthetic: StatisticsData | undefined;
  sale: StatisticsData | undefined;
  delivery: StatisticsData | undefined;
  inventory: StatisticsData | undefined;
  finance: StatisticsData | undefined;
};

export enum UserActionTypes {
  FETCH_USER_START = "FETCH_USER_START",
  FETCH_USER_SUCCESS = "FETCH_USER_SUCCESS",
  FETCH_USER_ERROR = "FETCH_USER_ERROR",
  DELETE_USER = "DELETE_USER",
  SET_TOKEN = "SET_TOKEN",
}

export enum CarDealerActionTypes {
  FETCH_CAR_DEALER_SELLERS_START = "FETCH_CAR_DEALER_SELLERS_START",
  FETCH_CAR_DEALER_SELLERS_SUCCESS = "FETCH_CAR_DEALER_SELLERS_SUCCESS",
  FETCH_CAR_DEALER_SELLERS_ERROR = "FETCH_CAR_DEALER_SELLERS_ERROR",

  FETCH_CAR_DEALER_SALES_START = "FETCH_CAR_DEALER_SALES_START",
  FETCH_CAR_DEALER_SALES_SUCCESS = "FETCH_CAR_DEALER_SALES_SUCCESS",
  FETCH_CAR_DEALER_SALES_ERROR = "FETCH_CAR_DEALER_SALES_ERROR",

  FETCH_CAR_DEALER_START = "FETCH_CAR_DEALER_START",
  FETCH_CAR_DEALER_SUCCESS = "FETCH_CAR_DEALER_SUCCESS",
  FETCH_CAR_DEALER_ERROR = "FETCH_CAR_DEALER_ERROR",

  FETCH_TOP_SELLERS_START = "FETCH_TOP_SELLERS_START",
  FETCH_TOP_SELLERS_SUCCESS = "FETCH_TOP_SELLERS_SUCCESS",
  FETCH_TOP_SELLERS_ERROR = "FETCH_TOP_SELLERS_ERROR",

  FETCH_FINANCE_MANAGERS_START = "FETCH_FINANCE_MANAGERS_START",
  FETCH_FINANCE_MANAGERS_SUCCESS = "FETCH_FINANCE_MANAGERS_SUCCESS",
  FETCH_FINANCE_MANAGERS_ERROR = "FETCH_FINANCE_MANAGERS_ERROR",

  FETCH_INVENTORY_START = "FETCH_INVENTORY_START",
  FETCH_INVENTORY_SUCCESS = "FETCH_INVENTORY_SUCCESS",
  FETCH_INVENTORY_ERROR = "FETCH_INVENTORY_ERROR",

  FETCH_INVENTORIES_START = "FETCH_INVENTORIES_START",
  FETCH_INVENTORIES_SUCCESS = "FETCH_INVENTORIES_SUCCESS",
  FETCH_INVENTORIES_ERROR = "FETCH_INVENTORIES_ERROR",
}

export enum NotificationsActionTypes {
  FETCH_NOTIFICATIONS_START = "FETCH_NOTIFICATIONS_START",
  FETCH_NOTIFICATIONS_SUCCESS = "FETCH_NOTIFICATIONS_SUCCESS",
  FETCH_NOTIFICATIONS_ERROR = "FETCH_NOTIFICATIONS_ERROR",
  SET_NOTIFICATIONS_SWITCH_CHECK = "SET_NOTIFICATIONS_SWITCH_CHECK",
  SET_NOTIFICATIONS_ARCHIVED_SWITCH_CHECK = "SET_NOTIFICATIONS_ARCHIVED_SWITCH_CHECK",
  SET_NOTIFICATIONS_PAGE = "SET_NOTIFICATIONS_PAGE",
  SET_NOTIFICATIONS_PER_PAGE = "SET_NOTIFICATIONS_PER_PAGE",
  SET_NOTIFICATIONS_FILTERS = "SET_NOTIFICATIONS_FILTERS",
  SET_NOTIFICATION_STATUS_READ = "SET_NOTIFICATION_STATUS_READ",
}

export interface ApiResponse<T> {
  response: T;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  error?: any;
}
