import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import React, {useState, useMemo} from "react";
import {SpaceX} from "./SpaceX";
import {useSalesFunnelStyles} from "../../utils/salesFunnelStyles";
import MonthPicker from "src/components/MonthPicker";
import clsx from "clsx";
import CartagSearch from "src/components/CartagSearch";
import CreateDealDialog from "src/components/CreateDealDialog";
import {useTranslation} from "react-i18next";
import {useSalesFunnelContext} from "../../state/SalesFunnelProvider";
import ModalSelectDropdown from "../../../../components/ModalDropdown/ModalSelectDropdown";

const UsedNewCarSelect = () => {
  const labels = ["Used", "New"];
  const values: ("USED" | "NEW")[] = useMemo(() => ["USED", "NEW"], []);
  const {carFilter, onCarFilterChanged} = useSalesFunnelContext();

  return (
    <ModalSelectDropdown<"USED" | "NEW">
      value={carFilter}
      onChange={onCarFilterChanged}
      labels={labels}
      values={values}
    />
  );
};

export const SalesFunnelTopRow = () => {
  const styles = useSalesFunnelStyles();
  const {
    onShowSetGoalsModal,
    currentDate,
    onCurrentDateChanged,
    searchText,
    onSearchTextChanged,
  } = useSalesFunnelContext();
  const [, setTextState] = useState(() => searchText);
  const [openCreateDeal, setOpenCreateDeal] = useState(false);
  const {t} = useTranslation();
  return (
    <>
      <CreateDealDialog
        open={openCreateDeal}
        handleClose={() => {
          setOpenCreateDeal(false);
        }}
      />
      <div className={styles.boxOnTable}>
        <div className={styles.boxOnTableSalesTable}>
          <Typography className={styles.salesTable}>Sales table</Typography>
          <SpaceX size={29} />
          <div className={styles.monthPicker}>
            <MonthPicker
              date={currentDate}
              onDateChanged={onCurrentDateChanged}
            />
          </div>
          <SpaceX size={35} />
          <UsedNewCarSelect />
        </div>
        <div className={styles.boxOnTableSearch}>
          <div className={styles.search}>
            <CartagSearch
              noCleanOnSubmit
              onChange={(value) => {
                if (value.length >= 3 || value.length === 0) {
                  onSearchTextChanged(value);
                  setTextState(value);
                }
              }}
              onSubmit={(value) => {
                onSearchTextChanged(value);
                setTextState(value);
              }}
            />
          </div>

          <SpaceX size={14} />
          <div className={styles.buttons}>
            <Button
              className={clsx(styles.buttonBase, styles.buttonWhite)}
              onClick={() => setOpenCreateDeal(true)}
            >
              {t("buttonNames.CreateDeal")}
            </Button>
            <SpaceX size={14} />
            <Button
              onClick={onShowSetGoalsModal}
              className={clsx(styles.buttonBase, styles.buttonBlue)}
            >
              {t("buttonNames.SetGoals")}
            </Button>
          </div>
        </div>
      </div>
    </>
  );
};
