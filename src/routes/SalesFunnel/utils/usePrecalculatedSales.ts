import {CarDealerSale, CarDealerSeller, TopSeller} from "../../../types";
import {useMemo} from "react";
import {SellerCarEntry, SellersListEntry} from "../types/apiDataProvider";
import {useCarDealerSalesStats} from "./useCarDealerSalesStats";

/**
 * Parses sellers and sales structures into the table-friendly format.
 * Additionally, the stats are calculated
 *
 * @param sellers
 * @param sales
 * @param topSellers
 * @param currentMonth Month is needed to calculate the goals
 */
export const usePrecalculatedSales = (
  sellers: CarDealerSeller[] | undefined,
  sales: CarDealerSale[] | undefined,
  topSellers: TopSeller[] | undefined,
  currentMonth: number,
) => {
  const sellersList = useMemo(() => {
    if (!sellers || !sales || !topSellers) {
      return undefined;
    }

    const sellersMap = new Map<string, SellerCarEntry[]>();
    sellers?.forEach((seller) => sellersMap.set(seller.id, []));
    sales?.forEach((sale) => {
      sale.sales.forEach((reference) => {
        const refs = sellersMap.get(reference.sellerInfo.idSeller);
        refs?.push({
          reference,
          sale,
        });
      });
    });

    return sellers?.map((seller) => {
      const totalYear =
        topSellers.filter((topSeller) => topSeller.id === seller.id).pop()
          ?.totalYear || "0";

      const cars = sellersMap.get(seller.id);
      return {
        seller,
        cars,
        totalYear: +totalYear,
      };
    }) as SellersListEntry[];
  }, [sellers, sales, topSellers]) as SellersListEntry[] | undefined;

  const stats = useCarDealerSalesStats(sellers, sales, currentMonth);

  return {sellersList, stats};
};
