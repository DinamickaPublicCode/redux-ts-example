import React, {useState} from "react";
import IconButton from "@mui/material/IconButton";
import Paper from "@mui/material/Paper";
import InputBase from "@mui/material/InputBase";
import SearchIcon from "@mui/icons-material/Search";

interface CartagSearchProps {
  onSubmit: (value: string) => void;
  noCleanOnSubmit?: boolean;
  /** An additional callback for the search, for being able optionally search as user types */
  onChange?: (value: string) => void;
}

const CartagSearch: React.FC<CartagSearchProps> = ({
  onSubmit,
  onChange,
  noCleanOnSubmit,
}) => {
  const [value, setValue] = useState<string>("");

  const handleSubmitGlobalSearch = (event: React.FormEvent) => {
    event.preventDefault();
    onSubmit(value);
    if (!noCleanOnSubmit) {
      setValue("");
    }
  };

  return (
    <Paper
      component="form"
      onSubmit={handleSubmitGlobalSearch}
      sx={{
        padding: "2px 4px",
        display: "flex",
        alignItems: "center",
        width: "100%",
      }}
    >
      <InputBase
        sx={{
          marginLeft: "30px",
          flex: 1,
        }}
        placeholder="Search"
        value={value}
        name={"globalSearch"}
        onChange={(event) => {
          setValue(event.target.value);
          if (onChange) {
            onChange(event.target.value);
          }
        }}
      />
      <IconButton
        type="submit"
        sx={{
          padding: "7px",
          marginRight: "10px",
        }}
        size="large"
      >
        <SearchIcon />
      </IconButton>
    </Paper>
  );
};

export default CartagSearch;
