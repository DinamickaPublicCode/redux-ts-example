import React, {useState, useMemo, useCallback} from "react";
import Box from "@mui/material/Box";
import Slide from "@mui/material/Slide";
import IconButton from "@mui/material/IconButton";
import chunk from "lodash/chunk";
import HomePerformanceGraph from "./HomePerformanceGraph";
import ArrowBackIos from "@mui/icons-material/ArrowBackIos";
import ArrowForwardIos from "@mui/icons-material/ArrowForwardIos";
import {styled} from "@mui/material/styles";

const HomePerformanceCarousel = ({
  rawData,
}: {
  rawData: number[][] | undefined;
}) => {
  const [index, setIndex] = useState(0);
  const [slideIn, setSlideIn] = useState(true);

  const [slideDirection, setSlideDirection] = useState<
    "down" | "left" | "right" | "up" | undefined
  >("down");

  /**
   * This would split any month without adding the third slide.
   */
  const CHUNK_SPLIT = 16;
  const data: number[][][] = useMemo(
    () => chunk(rawData, CHUNK_SPLIT),
    [rawData],
  );

  const numSlides = data.length;

  const onArrowClick = useCallback(
    (
      direction:
        | "down"
        | "left"
        | "right"
        | "up"
        | ((
            prevState: "down" | "left" | "right" | "up" | undefined,
          ) => "down" | "left" | "right" | "up" | undefined)
        | undefined,
    ) => {
      const increment = direction === "left" ? -1 : 1;
      const newIndex = (index + increment + numSlides) % numSlides;

      const oppDirection = direction === "left" ? "right" : "left";
      setSlideDirection(direction);
      setSlideIn(false);

      setTimeout(() => {
        setIndex(newIndex);
        setSlideDirection(oppDirection);
        setSlideIn(true);
      }, 500);
    },
    [index, numSlides],
  );

  return (
    <Box
      sx={{
        width: "100%",
        height: "300px",
        position: "relative",
      }}>
      <>
        {index !== 0 && (
          <Box
            sx={{
              zIndex: 20,
              left: 0,
              top: "calc(50% - 45px)",
              position: "absolute",
            }}>
            <ArrowButton onClick={() => onArrowClick("left")} size="large">
              <ArrowBackIos
                style={{
                  marginLeft: 5,
                }}
                color="primary"
              />
            </ArrowButton>
          </Box>
        )}
        {index !== numSlides - 1 && (
          <Box
            sx={{
              zIndex: 20,
              right: 0,
              top: "calc(50% - 45px)",
              position: "absolute",
            }}>
            <ArrowButton onClick={() => onArrowClick("right")} size="large">
              <ArrowForwardIos color="primary" />
            </ArrowButton>
          </Box>
        )}
        <Box
          sx={{
            overflow: "hidden",
            width: "100%",
            height: "100%",
          }}>
          <Slide in={slideIn} direction={slideDirection}>
            <Box
              sx={{
                display: "flex",
                justifyContent: "center",
                height: "100%",
                flex: 1,
                p: "0 30px",
              }}>
              {data.length && (
                <HomePerformanceGraph
                  data={data[index]}
                  labelIndexStart={1 + index * CHUNK_SPLIT}
                />
              )}
            </Box>
          </Slide>
        </Box>
      </>
    </Box>
  );
};

const ArrowButton = styled(IconButton)({
  background: "#FFFFFF",
  boxShadow: "0px 3px 12px rgba(203, 205, 217, 0.7)",
  borderRadius: "6px",
});

export default HomePerformanceCarousel;
