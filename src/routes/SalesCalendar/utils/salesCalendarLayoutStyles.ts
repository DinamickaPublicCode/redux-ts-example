import {Theme} from "@mui/material/styles";
import makeStyles from "@mui/styles/makeStyles";

export const useSalesCalendarLayoutStyles = makeStyles((theme: Theme) => ({
  titleWrapper: {
    margin: theme.spacing(3, -1.5),
    display: "flex",
    alignItems: "center",
    [theme.breakpoints.down("sm")]: {
      flexDirection: "column",
    },
  },
  listItemGutters: {
    paddingLeft: 20,
    paddingRight: 20,
    "& .MuiListItemIcon-root": {
      minWidth: 45,
    },
    "& .MuiListItemText-root .MuiTypography-root": {
      fontWeight: 600,
      fontSize: 16,
      color: "#222238",
      textTransform: "capitalize",
    },
  },
  activeCarType: {
    backgroundColor: "#F6F8FA",
    "& .MuiListItemText-root .MuiTypography-root": {
      color: theme.palette.primary.main,
    },
  },
  openDeliveryText: {
    marginLeft: 5,
    fontSize: 16,
    fontWeight: 600,
    color: theme.palette.primary.main,
  },
  box: {
    width: "20%",
    mx: 1.5,
    [theme.breakpoints.down("sm")]: {
      width: "100%",
    },
  },
  saleCalendarStyle: {
    fontWeight: 700,
    fontSize: 28,
    [theme.breakpoints.down("sm")]: {
      fontSize: 20,
      // marginLeft:15
    },
    // [theme.breakpoints.down(950)]: {
    //   fontSize:24,
    // },
  },
  monthPickerStyle: {
    mx: 1.5,
    [theme.breakpoints.up(950)]: {
      width: "20%",
    },
    [theme.breakpoints.down("sm")]: {
      width: "100%",
      marginTop: 20,
    },
  },
  globalSearchStyle: {
    mx: 1.5,
    [theme.breakpoints.up(950)]: {
      width: "30%",
      marginLeft: "2%",
    },
    [theme.breakpoints.down("sm")]: {
      width: "100%",
      marginTop: 20,
    },
  },
  titleButtonsBox: {
    mx: 1.5,
    display: "flex",
    justifyContent: "space-between",
    [theme.breakpoints.up(950)]: {
      width: "30%",
      marginLeft: "2%",
    },
    [theme.breakpoints.down("sm")]: {
      marginTop: 20,
      width: "100%",
    },
  },
}));
