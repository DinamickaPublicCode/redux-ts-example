import ChevronLeft from "@mui/icons-material/ChevronLeft";
import ChevronRight from "@mui/icons-material/ChevronRight";
import Box from "@mui/material/Box";
import IconButton from "@mui/material/IconButton";
import Paper from "@mui/material/Paper";
import React from "react";

const MonthPickerButton = ({
  date,
  children,
  onClickLeft,
  onClickRight,
}: {
  date: Date;
  children: JSX.Element | JSX.Element[] | string;
  onClickLeft: () => void;
  onClickRight: () => void;
}) => {
  return (
    <Paper
      sx={{
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "space-between",
        padding: "6px 16px",
        fontWeight: 600,
        fontSize: "14px",
        color: "#222238",
      }}
    >
      <IconButton size="small" onClick={onClickLeft}>
        <ChevronLeft color={"primary"} style={{padding: 0}} />
      </IconButton>
      <Box
        sx={{
          mx: "14px",
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        {children}
      </Box>
      <IconButton size="small" onClick={onClickRight}>
        <ChevronRight color={"primary"} style={{padding: 0}} />
      </IconButton>
    </Paper>
  );
};

export default MonthPickerButton;
