import {StageType} from "src/types";

export type ActionsDataType = {
  [stage in StageType]: string[];
};

export const actionsData: ActionsDataType = {
  NEW_INV: [
    "CREATE_DEAL",
    "ADD_REQUEST",
    "SEND_MSG",
    "WHOLESALE",
    "SELL_AS_IS",
    "MARK_AS_DONE",
    "PREPARE_ON_DELIV",
  ],
  FIRST_PREPARATION_NOT_DONE: [
    "CREATE_DEAL",
    "PREPARATION",
    "MARK_AS_DONE",
    "SEND_MSG",
    "OUTSOURCING",
    "WHOLESALE",
    "PREPARE_ON_DELIV",
  ],
  READY_FOR_SALE: ["CREATE_DEAL", "SEND_MSG"],
  WAITING_APV_CREDIT: ["APV_CREDIT", "CANCEL_SALE", "SEND_MSG"],
  PREPARATION_DELIVERY: [
    "PREPARATION",
    "MARK_AS_DONE",
    "CANCEL_SALE",
    "SEND_MSG",
  ],
  READY_FOR_DELIVERY: ["DELIVERY", "CANCEL_SALE"],
  DELIVERY_DONE: ["COUNT", "SEND_MSG"],
  COUNTED: ["ARCHIVE", "SEND_MSG"],
  WHOLESALE: ["ARCHIVE", "SEND_MSG"],
  OUTSOURCING: ["BACK_FROM_OUTSC", "SEND_MSG"],
};
