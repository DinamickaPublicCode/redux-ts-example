import InputBase from "@mui/material/InputBase";
import MenuItem from "@mui/material/MenuItem";
import Pagination from "@mui/material/Pagination";
import PaginationItem from "@mui/material/PaginationItem";
import Select from "@mui/material/Select";
import {styled, useTheme} from "@mui/material/styles";
import Toolbar from "@mui/material/Toolbar";
import React from "react";

interface TablePaginationProps {
  count: number;
  page: number;
  rowsPerPage: number;
  rowsPerPageOptions: number[];
  onChangePage: (
    event: React.MouseEvent<HTMLButtonElement> | null,
    page: number,
  ) => void;
  onChangeRowsPerPage?: (event: unknown) => void;

  /**
   * Styling related props for specialized use
   */
  nopadding?: boolean;
  noshadow?: boolean;
  noselect?: boolean;
}

const TablePagination: React.FC<TablePaginationProps> = ({
  count,
  page,
  rowsPerPage,
  rowsPerPageOptions,
  onChangePage,
  onChangeRowsPerPage,
  nopadding,
  noshadow,
  noselect,
}) => {
  const handlePageClick = (
    event: React.ChangeEvent<unknown>,
    pageNum: number,
  ) => {
    onChangePage(
      event as React.MouseEvent<HTMLButtonElement> | null,
      pageNum - 1,
    );
  };

  const theme = useTheme();

  return (
    <Footer sx={noshadow ? {boxShadow: "none"} : {}}>
      <Toolbar
        sx={{
          justifyContent: "center",
        }}
        // classes={{root: classes.paginationToolbar}}
        style={nopadding ? {padding: 0} : {}}
      >
        <Pagination
          count={count}
          shape="rounded"
          color="primary"
          page={page + 1}
          onChange={handlePageClick}
          renderItem={(item) => {
            if (item.type === "previous" || item.type === "next") {
              return (
                <PaginationItem
                  {...item}
                  sx={{
                    height: "28px",
                    minWidth: "48px",
                    borderRadius: "6px",
                    margin: "0 10px",
                    boxShadow: "0px 3px 12px rgba(203, 205, 217, 0.7)",
                    color: theme.palette.primary.main,
                  }}
                />
              );
            }
            return (
              <PaginationItem
                {...item}
                sx={{
                  "&.Mui-selected": {
                    boxShadow: "0px 3px 14px rgba(2, 53, 236, 0.3)",
                  },
                }}
              />
            );
          }}
        />
        {!noselect && (
          <Select
            autoWidth
            sx={{
              paddingLeft: "10px",
              marginLeft: "20px",
              color: "#98A2B7",
              fontSize: "14px",
              "&:focus": {
                borderRadius: theme.shape.borderRadius,
              },
            }}
            // selectIcon: {
            //   color: "#98A2B7",
            // },
            value={rowsPerPage}
            onChange={(event, _child: React.ReactNode) =>
              onChangeRowsPerPage?.(event)
            }
            input={<InputBase />}
          >
            {rowsPerPageOptions.map((item) => {
              return (
                <MenuItem key={item} value={item}>
                  {item} per page
                </MenuItem>
              );
            })}
          </Select>
        )}
      </Toolbar>
    </Footer>
  );
};

const Footer = styled("footer")(({theme}) => ({
  backgroundColor: theme.palette.common.white,
  boxShadow: "0px -8px 25px rgba(20, 32, 100, 0.06)",
}));

export default TablePagination;
