import React from 'react';

import { ComponentStory, ComponentMeta } from '@storybook/react';
import MonthPicker from './MonthPicker';

export default {
  title: 'Month Picker',
  component: MonthPicker,
} as ComponentMeta<typeof MonthPicker>;



const Template: ComponentStory<typeof MonthPicker> = (args) => <MonthPicker {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  date: new Date(),
  onDateChanged: () => false
}
