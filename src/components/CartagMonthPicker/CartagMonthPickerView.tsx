import React from "react";
import chunk from "lodash/chunk";
import ArrowForwardIos from "@mui/icons-material/ArrowForwardIos";
import ArrowBackIos from "@mui/icons-material/ArrowBackIos";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import {CartagSwitch} from "../CartagSwitch";
import {styled} from "@mui/material/styles";

type CustomMonthPickerRangeType = "year" | "month";

interface CartagMonthPickerViewProps {
  date: Date;
  immediate?: boolean;
  noswitch?: boolean;
  sliceTitle: string;
  currentIndex: number;
  onApplyButtonPressed?: () => void;
  onIndexChange: (index: number) => void;
  onChangeRange: (type: "positive" | "negative") => void;
  onRangeTypeChange: (rangeType: CustomMonthPickerRangeType) => void;
  rangeType: CustomMonthPickerRangeType;
  setRangeType: React.Dispatch<
    React.SetStateAction<CustomMonthPickerRangeType>
  >;
  onChange: (date: Date) => void;
  itemTitles: string[];
}

const CartagMonthPickerView: React.FC<CartagMonthPickerViewProps> = ({
  date,
  currentIndex,
  onApplyButtonPressed,
  noswitch,
  sliceTitle,
  onChangeRange,
  immediate,
  onRangeTypeChange,
  onIndexChange,
  rangeType,
  setRangeType,
  itemTitles,
  onChange,
}) => (
  <Wrapper>
    {!noswitch && (
      <Header>
        <RangeTypeTitle as="div">{rangeType}</RangeTypeTitle>
        <CartagSwitch
          checked={rangeType === "year"}
          onChange={() => {
            const newRangeType = rangeType === "year" ? "month" : "year";
            setRangeType(newRangeType);
            if (immediate) {
              onRangeTypeChange(newRangeType);
            }
          }}
        />
      </Header>
    )}
    <Box
      sx={{
        p: "27px 35px 23px 35px",
      }}
    >
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          mb: 2.7,
        }}
      >
        <ArrowButton onClick={() => onChangeRange("negative")} size="large">
          <ArrowBackIos
            color="primary"
            style={{
              marginLeft: 4,
            }}
            sx={{
              width: "14px",
              height: "auto",
            }}
          />
        </ArrowButton>
        <SliceTitle>{sliceTitle}</SliceTitle>
        <ArrowButton onClick={() => onChangeRange("positive")} size="large">
          <ArrowForwardIos
            color="primary"
            sx={{
              width: "14px",
              height: "auto",
            }}
          />
        </ArrowButton>
      </Box>
      {chunk(itemTitles, 4).map((row, rowIndex) => (
        <Grid key={rowIndex} container>
          {row.map((item, itemIndex) => (
            <Grid key={item} item xs={3}>
              <ItemButton
                sx={{
                  backgroundColor:
                    rowIndex * 4 + itemIndex === currentIndex
                      ? "#F1F0FF"
                      : "#F6F8FA",
                  color:
                    rowIndex * 4 + itemIndex === currentIndex
                      ? "#1A06F9"
                      : "#222238",
                }}
                onClick={() => onIndexChange(rowIndex * 4 + itemIndex)}
              >
                {item}
              </ItemButton>
            </Grid>
          ))}
        </Grid>
      ))}
    </Box>
    {!immediate && (
      <ApplyButton
        onClick={() => {
          onRangeTypeChange(rangeType);
          onChange(date);
          if (onApplyButtonPressed) {
            onApplyButtonPressed();
          }
        }}
      >
        Apply
      </ApplyButton>
    )}
  </Wrapper>
);

const Header = styled(Box)({
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
  justifyContent: "space-between",
  padding: "16px 24px",
  height: "61px",
  backgroundColor: "#F6F8FA",
});

const RangeTypeTitle = styled(Typography)({
  textTransform: "uppercase",
  fontStyle: "normal",
  fontWeight: 600,
  fontSize: "13px",
  lineHeight: "16.9px",
  letterSpacing: "0.5px",
  color: "#222238",
});

const Wrapper = styled(Box)({
  width: "315px",
  backgroundColor: "#fff",
  borderTopRightRadius: 0,
  borderTopLeftRadius: 0,
  borderBottomLeftRadius: "14px",
  borderBottomRightRadius: "14px",
});

const SliceTitle = styled(Typography)({
  fontWeight: 600,
  fontSize: "16px",
  lineHeight: "10%",
  display: "flex",
  alignItems: "center",
  textAlign: "center",
  color: "#1A06F9",
});

const ApplyButton = styled(Button)(({theme}) => ({
  "&:hover": {
    color: "white",
    background: "#1807DC",
  },
  textTransform: "uppercase",
  backgroundColor: theme.palette.primary.main,
  color: "white",
  padding: "15px 0",
  width: "100%",
  borderRadius: 0,

  fontStyle: "normal",
  fontWeight: 600,
  fontSize: "14px",
  lineHeight: "14px",
  textAlign: "center",
  borderBottomLeftRadius: 14,
  borderBottomRightRadius: 14,
}));

const ArrowButton = styled(IconButton)({
  width: 24,
  height: 24,
  borderRadius: "50%",
  background: "#F1F0FF",
});

const ItemButton = styled(Button)({
  "& span:first-child": {
    fontStyle: "normal",
    fontWeight: 600,
    fontSize: "10px",
    lineHeight: "10px",
    textTransform: "capitalize",
  },

  display: "flex",
  borderRadius: 0,
  justifyContent: "flex-start",
  alignItems: "flex-start",
  width: 60,
  height: 60,
  verticalAlign: "text-top",
  borderRight: "1px solid white",
  borderBottom: "1px solid white",
});

export default CartagMonthPickerView;
