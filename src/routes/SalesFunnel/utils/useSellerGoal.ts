import {CarDealerSeller} from "../../../types";
import {useMemo} from "react";
import dayjs from "dayjs";
import {CarFilter} from "../types/salesFunnelContext";

/**
 * Since goals provided via api come as an array without any specialization
 * It's necessary to extract the data for the quick access.
 *
 * @param currentDate
 * @param seller
 * @param carFilter
 */
export const useSellerGoal = (
  currentDate: Date,
  seller: CarDealerSeller | undefined,
  carFilter: CarFilter,
) => {
  return useMemo(() => {
    if (!seller) {
      return 0;
    }
    const currentMoment = dayjs(currentDate);
    /** Moment counts months from 0 */
    const month = currentMoment.month() + 1;
    const year = currentMoment.year();

    const goals = seller.goals.filter((goal) => {
      const filter = goal.newCar ? "NEW" : "USED";
      return (
        +goal.month === month && +goal.year === year && filter === carFilter
      );
    });
    if (goals.length === 0) {
      return 0;
    }
    return +goals[0].goal;
  }, [seller, currentDate, carFilter]);
};
