import React, {useState} from "react";
import HeaderLayout from "./HeaderLayout";

const Header = () => {
  const [drawerOpen, setDrawerOpen] = useState(false);
  return <HeaderLayout drawerOpen={drawerOpen} setDrawerOpen={setDrawerOpen} />;
};

export default Header;
