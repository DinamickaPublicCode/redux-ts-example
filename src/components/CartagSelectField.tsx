import React from "react";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import Typography from "@mui/material/Typography";
import {TextFieldProps} from "@mui/material/TextField/TextField";
import {useCommonStyles} from "../utils/commonStyles";

interface CartagSelectFieldProps {
  labelText?: string;
  disableLabel?: boolean;
  margin?: "normal" | "dense" | "none" | undefined;
  select?: boolean;
}

const CartagSelectField: React.FC<TextFieldProps & CartagSelectFieldProps> = ({
  labelText,
  disableLabel,
  margin = "normal",
  children,
  ...rest
}) => {
  const classes = useCommonStyles();
  return (
    <Box sx={{width: "100%"}}>
      <Typography className={classes.inputLabel}>{labelText}</Typography>
      <TextField
        select
        fullWidth
        InputProps={{
          className: classes.input,
        }}
        margin={margin}
        variant="outlined"
        size="small"
        {...rest}
      >
        {children}
      </TextField>
    </Box>
  );
};

export default CartagSelectField;
