import React from "react";
import Avatar from "@mui/material/Avatar";
import Box from "@mui/material/Box";
import CircularProgress from "@mui/material/CircularProgress";
import IconButton from "@mui/material/IconButton";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";
import {Theme} from "@mui/material/styles";
import makeStyles from "@mui/styles/makeStyles";
import ChevronRight from "@mui/icons-material/ChevronRight";
import {CarDealerSale} from "src/types";
import {getColor, getPercentage} from "src/utils/stageData";
import {useTranslation} from "react-i18next";
import {stageInfoIcons} from "src/utils/stageInfoIcons";
import dayjs from "dayjs";
import usePublicUrl from "src/utils/usePublicUrl";

interface SalesCalendarCardProps {
  car: CarDealerSale;
  onClick: (car: CarDealerSale) => void;
  isSelected: boolean;
}

const SalesCalendarCard: React.FC<SalesCalendarCardProps> = ({
  car,
  onClick,
  isSelected,
}) => {
  const classes = useStyles({
    isSelected,
  });
  const {t} = useTranslation();
  const publicUrl = usePublicUrl();

  return (
    <Paper className={classes.wrapper} onClick={() => onClick(car)}>
      <div className={classes.imageWrapper}>
        <Avatar
          src={publicUrl(`images/car-logos/${car.generalInfo.make}.png`)}
          style={{
            width: 84,
            height: 84,
          }}
        />
        <span className={classes.progressUnder} />
        {typeof getPercentage(car.stageInfo.stage) === "number" ? (
          getPercentage(car.stageInfo.stage) === 0 ? null : (
            <CircularProgress
              variant="determinate"
              size={100}
              thickness={2}
              value={getPercentage(car.stageInfo.stage) as number}
              style={{
                color: getColor(car.stageInfo.stage),
              }}
              className={classes.progress}
              classes={{
                circle: classes.circle,
              }}
            />
          )
        ) : (
          <CircularProgress
            variant="determinate"
            size={100}
            thickness={2}
            value={100}
            style={{
              color: "#000",
              opacity: 0.1,
            }}
            className={classes.progress}
            classes={{
              circle: classes.circle,
            }}
          />
        )}
      </div>
      <div className={classes.carNameWrapper}>
        <Typography variant="h6" className={classes.model}>
          {car.generalInfo.make}
        </Typography>
        <Typography
          style={{
            color: "#98A2B7",
          }}>
          {makeCarStockText(t, car)}
        </Typography>
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
          }}>
          <span className={classes.percentage}>
            {getPercentage(car.stageInfo.stage)}%
          </span>
          <div
            style={{
              display: "flex",
              alignItems: "center",
            }}>
            {car.stageInfo.icons.map((i) => (
              <div
                style={{
                  width: "max-content",
                  display: "flex",
                  alignItems: "center",
                  margin: "0 5px",
                }}
                key={i}>
                {stageInfoIcons[i]}
              </div>
            ))}
          </div>
        </Box>
      </div>
      <IconButton size="small">
        <ChevronRight
          color={isSelected ? undefined : "primary"}
          htmlColor={isSelected ? "#fff" : undefined}
          style={{padding: 0}}
        />
      </IconButton>
    </Paper>
  );
};

export function makeCarStockText(t: (m: string) => string, car: CarDealerSale) {
  return `${t("DashboardTableHead.referenceStock")} ${
    car.sales[0].saleReference
  }\n    \n  ${
    car.sales[0].deliveryInfo &&
    car.sales[0].deliveryInfo.deliveryBeginDate &&
    dayjs(car.sales[0].deliveryInfo.deliveryBeginDate).format("hh:mm")
  }`;
}

const useStyles = makeStyles((theme: Theme) => ({
  wrapper: {
    padding: 22,
    marginBottom: 20,
    display: "flex",
    alignItems: "center",
    cursor: "pointer",
    transition: "all 0.35s ease-in-out",
    backgroundColor: ({isSelected}: {isSelected: boolean}) =>
      isSelected ? theme.palette.primary.main : "#fff",
  },
  imageWrapper: {
    backgroundColor: "#fff",
    borderRadius: "50%",
    position: "relative",
    marginRight: 33,
  },
  progressUnder: {
    width: 96,
    height: 96,
    position: "absolute",
    top: -6,
    left: -6,
    borderRadius: "50%",
    border: "1px solid #F3F6F9",
  },
  progress: {
    position: "absolute",
    top: -8,
    left: -8,
  },
  circle: {
    strokeLinecap: "round",
  },
  carNameWrapper: {
    flex: 1,
  },
  model: {
    fontWeight: 600,
    fontSize: 22,
    color: ({isSelected}: {isSelected: boolean}) =>
      isSelected ? "#fff" : "#222238",
  },
  percentage: {
    fontWeight: 600,
    fontSize: 18,
    marginRight: 15,
    color: ({isSelected}: {isSelected: boolean}) =>
      isSelected ? "#fff" : "#222238",
  },
}));

export default SalesCalendarCard;
