import CartagMonthPicker from "src/components/CartagMonthPicker";
import React, {useState} from "react";

type CustomMonthPickerRangeType = "year" | "month";

const HomeBottomTablePicker = (
  props: Omit<
    React.ComponentProps<typeof CartagMonthPicker>,
    "rangeType" | "setRangeType"
  > & {
    /**
     * Hides month/year switch and locks a specific mode
     */
    forceRangeType?: CustomMonthPickerRangeType;
  },
) => {
  const [rangeType, setRangeType] = useState<CustomMonthPickerRangeType>(
    props.forceRangeType || "month",
  );

  return (
    <CartagMonthPicker
      {...props}
      rangeType={rangeType}
      setRangeType={setRangeType}
    />
  );
};

export default HomeBottomTablePicker;
