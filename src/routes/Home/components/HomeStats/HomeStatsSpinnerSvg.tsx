import React from "react";

const HomeStatsSpinnerSvg = () => (
  <svg
    width="216"
    height="216"
    viewBox="0 0 216 216"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <g filter="url(#filter0_i)">
      <circle cx="108" cy="108" r="108" fill="white" />
    </g>
    <g filter="url(#filter1_d)">
      <circle cx="108" cy="108" r="73.0588" fill="white" />
    </g>
    <defs>
      <filter
        id="filter0_i"
        x="0"
        y="0"
        width="216"
        height="220.533"
        filterUnits="userSpaceOnUse"
        colorInterpolationFilters="sRGB"
      >
        <feFlood floodOpacity="0" result="BackgroundImageFix" />
        <feBlend
          mode="normal"
          in="SourceGraphic"
          in2="BackgroundImageFix"
          result="shape"
        />
        <feColorMatrix
          in="SourceAlpha"
          type="matrix"
          values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
          result="hardAlpha"
        />
        <feOffset dy="4.53333" />
        <feGaussianBlur stdDeviation="11.3333" />
        <feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1" />
        <feColorMatrix
          type="matrix"
          values="0 0 0 0 0.796875 0 0 0 0 0.804464 0 0 0 0 0.85 0 0 0 0.25 0"
        />
        <feBlend mode="normal" in2="shape" result="effect1_innerShadow" />
      </filter>
      <filter
        id="filter1_d"
        x="14.3444"
        y="18.4638"
        width="187.312"
        height="187.312"
        filterUnits="userSpaceOnUse"
        colorInterpolationFilters="sRGB"
      >
        <feFlood floodOpacity="0" result="BackgroundImageFix" />
        <feColorMatrix
          in="SourceAlpha"
          type="matrix"
          values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
        />
        <feOffset dy="4.1194" />
        <feGaussianBlur stdDeviation="10.2985" />
        <feColorMatrix
          type="matrix"
          values="0 0 0 0 0.796875 0 0 0 0 0.804464 0 0 0 0 0.85 0 0 0 0.4 0"
        />
        <feBlend
          mode="normal"
          in2="BackgroundImageFix"
          result="effect1_dropShadow"
        />
        <feBlend
          mode="normal"
          in="SourceGraphic"
          in2="effect1_dropShadow"
          result="shape"
        />
      </filter>
    </defs>
  </svg>
);

export default HomeStatsSpinnerSvg;
