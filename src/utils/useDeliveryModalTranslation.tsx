import {useTranslation} from "react-i18next";
import {DELIVERY_TAB_TRANSLATION_KEY} from "../system/constants";

const useDeliveryModalTranslation = () => {
  const {t} = useTranslation();
  return {
    t: (key: string) => t(`${DELIVERY_TAB_TRANSLATION_KEY}.FORM.` + key),
  };
};

export default useDeliveryModalTranslation;
