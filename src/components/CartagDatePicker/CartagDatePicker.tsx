import React, {useEffect, useState} from "react";
import IconButton from "@mui/material/IconButton";
import TextField from "@mui/material/TextField";
import StaticDatePicker from "@mui/lab/StaticDatePicker";
import StaticTimePicker from "@mui/lab/StaticTimePicker";
import {CustomInputFieldForDatePicker} from "../CartagInputField";
import {CalendarOutlinedIcon} from "src/assets/icons";
import {TextFieldProps} from "@mui/material/TextField";
import dayjs from "dayjs";
import CartagDatePickerPopper from "./CartagDatePickerPopper";

interface CartagDatePickerProps {
  valueDate: Date | null;
  onChangeDate: (date: Date | null) => void;
  isDateTimePicker?: boolean;
}

const CartagDatePicker: React.FC<CartagDatePickerProps & TextFieldProps> = ({
  valueDate,
  onChangeDate,
  isDateTimePicker,
  ...rest
}) => {
  const [open, setOpen] = useState(false);
  const [date, setDate] = useState<Date | null>(null);
  const [inputValue, setInputValue] = useState<string>("");

  const [dateToTimeSteps, setDateToTimeSteps] = useState("step1");

  const innerFormat = isDateTimePicker ? "DD-MM-YYYY hh:mm" : "DD-MM-YYYY";

  const inputValueToDate = (value: string) => {
    const newDate = dayjs(value, innerFormat, true).toDate();
    setDate(newDate);
    onChangeDate(newDate);
  };

  useEffect(() => {
    if (valueDate) setDate(new Date(valueDate));
    if (!valueDate) setDate(null);
  }, [valueDate]);

  useEffect(() => {
    if (dayjs(date).isValid()) {
      setInputValue(dayjs(date).format(innerFormat));
    }
    if (!date) setInputValue("");
  }, [date, innerFormat]);

  useEffect(() => {
    if (!open) {
      setDateToTimeSteps("step1");
    }
  }, [open]);

  return (
    <CartagDatePickerPopper
      open={open}
      setOpen={setOpen}
      monthPickerBody={
        dateToTimeSteps === "step1" ? (
          <StaticDatePicker
            displayStaticWrapperAs="desktop"
            openTo="day"
            value={date}
            onChange={(newValue) => {
              onChangeDate(newValue);
              setDate(newValue);
              if (isDateTimePicker) setDateToTimeSteps("step2");
            }}
            renderInput={(props) => <TextField {...props} />}
          />
        ) : (
          <StaticTimePicker
            displayStaticWrapperAs="desktop"
            openTo="hours"
            views={["hours", "minutes"]}
            ampm={false}
            value={date}
            onChange={(newValue) => {
              onChangeDate(newValue);
              setDate(newValue);
            }}
            renderInput={(props) => <TextField {...props} />}
          />
        )
      }
    >
      <CustomInputFieldForDatePicker
        placeholder={innerFormat}
        disableLabel
        disabled
        value={inputValue}
        onChange={(event) => {
          setInputValue(event.target.value);
          inputValueToDate(event.target.value);
        }}
        InputProps={{
          startAdornment: (
            <IconButton
              size="small"
              style={{
                marginRight: 15,
              }}
            >
              <CalendarOutlinedIcon />
            </IconButton>
          ),
        }}
        {...rest}
      />
    </CartagDatePickerPopper>
  );
};

export default CartagDatePicker;
