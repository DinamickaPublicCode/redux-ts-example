import React, {useMemo, useState} from "react";
import {useTranslation} from "react-i18next";
import {
  DepartmentSpecification,
  FinanceTabType,
  StepValue,
  UpdateInventoryDataAction,
  UpdateInventoryDataType,
} from "src/types";
import {StockServiceTabContext} from "./StockServiceTabContext";
import CartagBackdrop from "../CartagBackdrop";
import {StockServiceTabDepartmentForm} from "./StockServiceTabDepartmentForm";
import {GenericFormSubmitButton} from "../GenericFormSubmitButton";
import {SERVICE_TAB_TRANSLATION_KEY} from "src/system/constants";
import {useAppSelector} from "src/state/hooks";
import useCustomInfoSnackbar from "../../utils/useCustomInfoSnackbar";
import useCustomErrorSnackbar from "../../utils/useCustomErrorSnackbar";
import {useGetCarDealerQuery} from 'src/state/reducers/carDealerApi'; 

const StockServiceTab = ({
  showStep,
  currentDeal,
  revalidateInventory,
}: {
  showStep?: number;
  currentDeal: FinanceTabType | null;
  revalidateInventory: () => void;
}) => {
  const {t} = useTranslation();
  const key = SERVICE_TAB_TRANSLATION_KEY;

  const {
    onChecked,
    onWorkOrderInfoChanged,
    getActionValue,
    getWorkOrderValue,
    isLoading,
    departmentsSchema,
    onSubmitAction,
  } = useServiceTabHandlers(currentDeal);

  return departmentsSchema && departmentsSchema?.length ? (
    <StockServiceTabContext.Provider
      value={{
        onChecked,
        onWorkOrderInfoChanged,
        getActionValue,
        getWorkOrderValue,
      }}>
      <CartagBackdrop open={isLoading} />
      <form
        action="/src/routes"
        onSubmit={(e) => {
          e.preventDefault();
          onSubmitAction(
            UpdateInventoryDataType.PREPARATION,
            revalidateInventory,
          );
        }}>
        {departmentsSchema ? (
          departmentsSchema.map((department, index) => (
            <StockServiceTabDepartmentForm
              key={index}
              department={department}
            />
          ))
        ) : (
          <></>
        )}
        <GenericFormSubmitButton
          secondaryButtons={[
            {
              title: t(key + UpdateInventoryDataType.PREPARE_ON_DELIV),
              onClick: () =>
                onSubmitAction(
                  UpdateInventoryDataType.PREPARE_ON_DELIV,
                  revalidateInventory,
                ),
            },
          ]}>
          {t(key + UpdateInventoryDataType.PREPARATION)}
        </GenericFormSubmitButton>
      </form>
    </StockServiceTabContext.Provider>
  ) : (
    <></>
  );
};

interface ServiceTabOutDataJson extends UpdateInventoryDataAction {
  departments: {
    departmentType: string;
    steps?: StepValue[];
  }[];
}

/**
 * An unnecessary complicated data handling for this tab.
 *
 * @param currentDeal
 */
export const useServiceTabHandlers = (currentDeal: FinanceTabType | null) => {
  const {data: user} = useGetUserQuery();
  const {data:carDealer} = useGetCarDealerQuery();

  const enqueueInfoSnackbar = useCustomInfoSnackbar();
  const enqueueErrorSnackbar = useCustomErrorSnackbar();

  const departmentsSchema: DepartmentSpecification[] | undefined =
    carDealer?.departments;

  const departmentsState = useMemo(() => {
    const res: ServiceTabOutDataJson = {
      action: UpdateInventoryDataType.PREPARE_ON_DELIV,
      idUser: "",
      userName: "",
      idCarDealer: "",
      departments: currentDeal?.departments || [],
    };
    return res;
  }, [currentDeal]);

  const [isLoading, setIsLoading] = useState(false);
  const onSubmitAction = async (
    actionType: UpdateInventoryDataType,
    revalidateInventory: () => void,
  ) => {
    // filter out  empty actions
    const departmentsToSend = departmentsState.departments.filter(
      (department) =>
        department.steps?.filter((step) => step.actions?.length).length,
    );
    if (departmentsToSend.length === 0) {
      // TODO: Use localized string in here
      enqueueErrorSnackbar("Nothing was selected");
      return;
    }

    if (!currentDeal) {
      // TODO: Use localized string in here
      enqueueErrorSnackbar("Invalid deal id");
      return;
    }

    if (!user?.id || !user?.fullName) {
      // TODO: Use localized string in here
      enqueueErrorSnackbar("Couldn't fetch the user data");
      return;
    }
    departmentsState.action = actionType;
    departmentsState.idUser = user?.id || "user";
    departmentsState.userName = user?.fullName || "name";
    departmentsState.idCarDealer = currentDeal?.idCarDealer || "id";

    setIsLoading(true);
    try {
      enqueueInfoSnackbar("Success");
    } catch (e) {
      enqueueErrorSnackbar(e as string);
    } finally {
      revalidateInventory();
      setIsLoading(false);
    }
  };

  const onChecked = (
    departmentType: string,
    stepType: string,
    idAction: string,
    checked: boolean,
  ) => {
    if (departmentsState.departments.length === 0) {
      departmentsState.departments.push({
        departmentType,
        steps: [
          {
            stepType,
            actions: [{idAction}],
          },
        ],
      });
      return;
    }
    departmentsState.departments = departmentsState.departments.map(
      // Pls be correct, don't want to fix this
      (department) =>
        department.departmentType === departmentType
          ? {
              ...department,
              steps: department.steps?.map((step) =>
                step.stepType === stepType
                  ? {
                      ...step,
                      actions: checked
                        ? [
                            ...(step.actions?.filter(
                              (action) => action.idAction !== idAction,
                            ) || []),
                            {
                              idAction,
                            },
                          ]
                        : step.actions?.filter(
                            (action) => action.idAction !== idAction,
                          ),
                    }
                  : step,
              ),
            }
          : department,
    );
  };

  const onWorkOrderInfoChanged = (
    departmentType: string,
    stepType: string,
    workOrderValue: string,
  ) => {
    if (departmentsState.departments.length === 0) {
      departmentsState.departments.push({
        departmentType,
        steps: [
          {
            stepType,
            workOrderInfo: {
              workOrderValue,
            },
          },
        ],
      });
      return;
    }
    departmentsState.departments = departmentsState.departments.map(
      (department) =>
        department.departmentType === departmentType
          ? {
              ...department,
              steps: department.steps?.map((step) =>
                step.stepType === stepType
                  ? {
                      ...step,
                      workOrderInfo: {
                        workOrderValue,
                      },
                    }
                  : step,
              ),
            }
          : department,
    );
  };

  const getActionValue = (
    departmentType: string,
    stepType: string,
    idAction: string,
  ) => {
    try {
      const length = departmentsState.departments
        .filter((d) => d.departmentType === departmentType)[0]
        .steps?.filter((step) => step.stepType === stepType)[0]
        .actions?.filter((action) => action.idAction === idAction).length;
      // ugh
      return !!length;
    } catch (e) {
      return false;
    }
  };

  const getWorkOrderValue = (departmentType: string, stepType: string) => {
    try {
      return (
        departmentsState.departments
          .filter((d) => d.departmentType === departmentType)[0]
          .steps?.filter((step) => step.stepType === stepType)[0].workOrderInfo
          ?.workOrderValue || ""
      );
    } catch (e) {
      return "";
    }
  };
  return {
    isLoading,
    onSubmitAction,
    onChecked,
    onWorkOrderInfoChanged,
    getActionValue,
    getWorkOrderValue,
    departmentsSchema,
  };
};

export default StockServiceTab;
