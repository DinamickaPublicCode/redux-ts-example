import {
  DocumentsApiData,
  DocumentBoxActionHandlers,
  DocumentInputFieldData,
  DocumentFileTypes,
} from "src/types/documentBoxInterfaces";
import {
  useUploadFilesDispatch,
  useUploadFilesState,
} from "src/components/UploadFilesProvider";
import {DocumentInfo, FinanceTabType} from "src/types";
import {useState} from "react";
import {downloadFileFromServer} from "./downloadFileFromServer";

export function useApiDocumentBoxHandlers(deal?: FinanceTabType | null) {
  const docs = useUploadFilesState();
  const dispatchMethods = useUploadFilesDispatch();

  return {
    documents: docs as unknown as DocumentsApiData,
    actions: {
      ...dispatchMethods,
    } as DocumentBoxActionHandlers,
  };
}

const MAX_SIZE = 5000000;

export function useApiDocumentBoxHandlersNoContext() {
  const [documents, setDocuments] = useState({
    files: [] as DocumentInfo[],
  });

  const onFileUpdated = async (e: DocumentInputFieldData, type: string) => {
    if (!e.files?.length) {
      throw new Error("e.target?.files?.length");
    }
    const {validity, files} = e;
    const file = files[0];

    if (file.size > MAX_SIZE) {
      // onShowFileSizeWarning();
      throw new Error("Wrong file size");
    }
    // const res = await uploadDocumentOrImage(file);
    if (validity.valid) {
      return {
        file,
        fileData: {
          name: file.name,
          url: file.name,
          isImage: type === DocumentFileTypes.PHOTO,
          id: `${Math.floor(Math.random() * 1000000000)}`,
        },
      };
    }
  };
  return {
    documents,
    actions: {
      onDownload: downloadFileFromServer,
      onUploadFile: async (id, fileData, onLoadingStateChanged, onComplete) => {
        // First delete the old file
        onLoadingStateChanged(true);
        try {
          const result = await onFileUpdated(fileData, id.type);
          if (!result) {
            /// snackbar
            console.error("Broken ", result);
            return;
          }
          const {fileData: metadata} = result;
          const newDocs = {
            files: [
              ...documents.files.filter((x) => x.type !== id.type),
              {
                id: metadata.id,
                type: id.type,
                name: metadata.url,
              },
            ],
          };
          setDocuments(newDocs);
          onComplete();
          onLoadingStateChanged(false);
        } catch (error) {
          console.error(error);
        }
      },
      onDeleteButtonClicked: (id, onLoadingStateChanged) => {
        onLoadingStateChanged(true);
        setTimeout(() => {
          const newDocs = {
            files: documents.files.filter((x) => x.type !== id.type),
          };
          setDocuments(newDocs);
          // need to remove document
          onLoadingStateChanged(false);
        }, 1200);
      },
    } as DocumentBoxActionHandlers,
  };
}
