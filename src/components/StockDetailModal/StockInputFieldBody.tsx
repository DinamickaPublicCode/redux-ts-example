import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import React from "react";

const StockInputFieldBody = ({
  children,
  label,
}: {
  label: string;
  children: JSX.Element;
}) => {
  return (
    <Box>
      <Typography
        sx={{
          fontSize: "14px",
          marginBottom: "12px",
          color: "#98A2B7",
        }}>
        {label}
      </Typography>
      {children}
    </Box>
  );
};

export default StockInputFieldBody;
