import React from "react";
import useHomeTranslation from "../../utils/useHomeTranslation";
import ModalSelectDropdown from "../../../../components/ModalDropdown/ModalSelectDropdown";

/**
 * A special dropdown for selecting a department
 *
 * @param param0
 */
const HomePerformanceFilter = ({
  departmentType,
  setDepartmentType,
}: {
  departmentType: "mechanic" | "esthetic";
  setDepartmentType: React.Dispatch<
    React.SetStateAction<"mechanic" | "esthetic">
  >;
}) => {
  const {t} = useHomeTranslation();
  const BASE_KEY = "mechanic/esthetic";
  const labels: string[] = BASE_KEY.split("/").map(t);
  const values: ("mechanic" | "esthetic")[] = BASE_KEY.split(
    "/",
  ) as unknown[] as ("mechanic" | "esthetic")[];

  return (
    <ModalSelectDropdown<"mechanic" | "esthetic">
      value={departmentType}
      onChange={setDepartmentType}
      labels={labels}
      values={values}
    />
  );
};

export default HomePerformanceFilter;
