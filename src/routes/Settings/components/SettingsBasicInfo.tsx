import React, {useEffect, useState} from "react";
import Box from "@mui/material/Box";
import Divider from "@mui/material/Divider";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import PersonIcon from "@mui/icons-material/Person";
import CartagInputField from "../../../components/CartagInputField";
import {LoadingButton} from "@mui/lab";
import {useTranslation} from "react-i18next";
import {useAppSelector} from "src/state/hooks";
import {Formik} from "formik";
import * as yup from "yup";
import useCustomInfoSnackbar from "src/utils/useCustomInfoSnackbar";
import makeStyles from "@mui/styles/makeStyles";
import {Theme} from "@mui/material/styles";
import createStyles from "@mui/styles/createStyles";
import {useGetUserQuery} from 'src/state/reducers/userApi';

const INITIAL_STATE = {
  fullName: "",
  email: "",
  newPassword: "",
  confirmPassword: "",
};

const validationSchema = yup.object().shape({
  fullName: yup.string().required("Full Name is Required"),
  email: yup.string().email().required("Email is Required"),
  newPassword: yup.string().min(8, "Seems a bit short..."),
  confirmPassword: yup
    .string()
    .when("newPassword", (password: unknown, field: yup.StringSchema) =>
      password
        ? field
            .oneOf([yup.ref("newPassword")])
            .required("Password does not match")
        : field,
    ),
});

const BasicInfo = () => {
  const classes = useBasicInfoStyles();
  const {t} = useTranslation();
  const {data: user} = useGetUserQuery();;
  const enqueueInfoSnackbar = useCustomInfoSnackbar();

  const [inputData, setInputData] = useState(INITIAL_STATE);

  useEffect(() => {
    if (user) {
      setInputData({
        fullName: user.fullName,
        email: user.email,
        newPassword: "",
        confirmPassword: "",
      });
    }
  }, [user]);

  console.log("dss");
  return (
    <Box sx={{maxWidth: 780}}>
      <Formik
        initialValues={inputData}
        enableReinitialize
        validationSchema={validationSchema}
        onSubmit={async (values, {setSubmitting, resetForm}) => {
          if (!user?.id) return;
          if (
            values.fullName === user.fullName &&
            values.email === user.email &&
            values.newPassword.trim() === "" &&
            values.confirmPassword.trim() === ""
          ) {
            return setSubmitting(false);
          } else {
            try {
              // await updateUserData({
              //   id: user.id,
              //   fullName: values.fullName,
              //   email: values.email,
              //   password: values.newPassword,
              // });
              enqueueInfoSnackbar("Successfully updated");
              setInputData((prevState) => ({
                ...prevState,
                newPassword: "",
                confirmPassword: "",
              }));
            } catch (e) {
              console.log(e);
            } finally {
              // resetForm({
              //   values: {
              //     newPassword: "",
              //     confirmPassword: "",
              //   },
              // });
              setSubmitting(false);
            }
          }
        }}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          /* and other goodies */
        }) => (
          <form onSubmit={handleSubmit}>
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                mb: 3,
              }}
            >
              <div
                style={{
                  marginRight: 20,
                }}
              >
                <input
                  accept="image/*"
                  style={{
                    display: "none",
                  }}
                  id="icon-button-file"
                  type="file"
                  // onChange={handleImgChange}
                />
                <label htmlFor="icon-button-file">
                  <IconButton
                    color="default"
                    component="span"
                    className={classes.imageUploader}
                    // classes={{
                    //   label: classes.imageUploaderLabel as unknown as string,
                    // }}
                    size="large"
                  >
                    <PersonIcon className={classes.imageUploaderIcon} />
                    <Typography
                      style={{
                        fontSize: 10,
                      }}
                      variant="body2"
                    >
                      {t("Upload")}
                    </Typography>
                  </IconButton>
                </label>
              </div>
              <Typography
                variant="body1"
                style={{
                  fontSize: 18,
                  fontWeight: 600,
                }}
              >
                {user?.fullName}
              </Typography>
            </Box>
            <div className={classes.inputWrapper}>
              <CartagInputField
                labelText={"Full name"}
                name="fullName"
                error={!!touched.fullName && !!errors.fullName}
                helperText={errors.fullName}
                value={values.fullName}
                onChange={handleChange}
                onBlur={handleBlur}
              />
            </div>
            <div className={classes.inputWrapper}>
              <CartagInputField
                labelText={"Email"}
                name="email"
                type="email"
                error={!!touched.email && !!errors.email}
                helperText={errors.email}
                value={values.email}
                onChange={handleChange}
                onBlur={handleBlur}
              />
            </div>
            <Divider
              style={{
                borderWidth: 1,
              }}
            />
            <div className={classes.inputWrapper}>
              <CartagInputField
                labelText={"New password"}
                name="newPassword"
                type="password"
                error={!!touched.newPassword && !!errors.newPassword}
                helperText={errors.newPassword}
                value={values.newPassword}
                onChange={handleChange}
                onBlur={handleBlur}
              />
            </div>
            <div className={classes.inputWrapper}>
              <CartagInputField
                labelText={"Confirm password"}
                name="confirmPassword"
                type="password"
                error={!!touched.confirmPassword && !!errors.confirmPassword}
                helperText={errors.confirmPassword}
                value={values.confirmPassword}
                onChange={handleChange}
                onBlur={handleBlur}
              />
            </div>
            <Box
              sx={{
                display: "flex",
                justifyContent: "flex-end",
              }}
            >
              <LoadingButton
                type="submit"
                variant="contained"
                disabled={isSubmitting}
                className={classes.submitButton}
              >
                Save
              </LoadingButton>
            </Box>
          </form>
        )}
      </Formik>
    </Box>
  );
};

export const useBasicInfoStyles = makeStyles((theme: Theme) =>
  createStyles({
    imageUploader: {
      padding: 0,
      backgroundColor: "#F6F8FA",
      boxShadow: "0px 4px 12px rgba(203, 205, 217, 0.25)",
      "& $imageUploaderLabel": {
        width: 68,
        height: 68,
        display: "flex",
        flexDirection: "column",
      },
    },
    imageUploaderLabel: {},
    imageUploaderIcon: {
      color: theme.palette.action.disabled,
    },
    inputWrapper: {
      margin: "13px 0",
    },
    submitButton: {
      padding: "8px 30px",
      textTransform: "uppercase",
      fontSize: 14,
      fontWeight: 600,
    },
  }),
);

export default BasicInfo;
