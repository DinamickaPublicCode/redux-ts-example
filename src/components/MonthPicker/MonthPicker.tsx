import React, {useMemo} from "react";
import dayjs from "dayjs";
import Box from "@mui/material/Box";
import MonthPickerPopper from "./MonthPickerPopper";
import MonthPickerButton from "../MonthPickerButton";

const MonthPicker = ({
  date,
  onDateChanged,
}: {
  date: Date;
  onDateChanged: (date: Date) => void;
}) => {
  const dateMoment = useMemo(() => dayjs(date), [date]);

  return (
    <MonthPickerButton
      date={date}
      onClickRight={() => onDateChanged(dateMoment.add(1, "month").toDate())}
      onClickLeft={() =>
        onDateChanged(dateMoment.subtract(1, "month").toDate())
      }
    >
      <MonthPickerPopper value={date} onChange={onDateChanged}>
        <Box
          sx={{
            cursor: "pointer",
            mr: "7px",
          }}
        >
          {dateMoment.format("MMMM")}
        </Box>
      </MonthPickerPopper>
      <MonthPickerPopper
        isYear
        value={date}
        onChange={(dateChanged) => {
          onDateChanged(
            dateMoment.set("year", dayjs(dateChanged).get("year")).toDate(),
          );
        }}
      >
        <Box
          sx={{
            cursor: "pointer",
          }}
        >
          {dateMoment.year()}
        </Box>
      </MonthPickerPopper>
    </MonthPickerButton>
  );
};

export default MonthPicker;
