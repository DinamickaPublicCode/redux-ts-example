import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import React from "react";

/**
 * A circle with a label for the graph legend indication
 */
const HomePerformanceLegendsCircle = ({
  htmlColor,
  children,
}: {
  htmlColor: string;
  children: string;
}) => {
  return (
    <Box
      sx={{
        display: "flex",
        alignItems: "center",
        mr: "30px",
      }}
    >
      <Box
        sx={{
          width: "12px",
          height: "12px",
          marginRight: "10px",
          borderRadius: "50%",
          border: "2px solid white",
          boxShadow: "0px 2px 6px rgba(203, 205, 217, 0.4)",
        }}
        style={{
          flexShrink: 0,
          backgroundColor: htmlColor,
        }}
      />
      <Typography
        sx={{
          fontStyle: "normal",
          fontWeight: "normal",
          fontSize: "14px",
          lineHeight: "16.8px",
          color: "#222238",
        }}
      >
        {children}
      </Typography>
    </Box>
  );
};

export default HomePerformanceLegendsCircle;
