import {CreateDealTab, CreateDealTabs} from "./createDealTabs";
import {StageType} from "src/types";
import {StageEnum} from "./stageData";

export const enableTabByStage = (
  tab: CreateDealTab,
  stage: StageType | undefined,
) => {
  if (tab === CreateDealTabs.GENERAL) {
    return false;
  }
  if (tab === CreateDealTabs.SALES) {
    return (
      stage !== StageEnum.NEW_INV &&
      stage !== StageEnum.FIRST_PREPARATION_NOT_DONE &&
      stage !== StageEnum.READY_FOR_SALE
    );
  }
  if (tab === CreateDealTabs.PREPARATION_REQUEST) {
    return stage !== StageEnum.NEW_INV;
  }
  if (tab === CreateDealTabs.SERVICE) {
    return (
      stage !== StageEnum.FIRST_PREPARATION_NOT_DONE &&
      stage !== StageEnum.PREPARATION_DELIVERY
    );
  }
  if (tab === CreateDealTabs.DELIVERY) {
    return stage !== StageEnum.READY_FOR_DELIVERY;
  }
  if (tab === CreateDealTabs.FINANCE) {
    return stage !== StageEnum.WAITING_APV_CREDIT;
  }
  return false;
};
