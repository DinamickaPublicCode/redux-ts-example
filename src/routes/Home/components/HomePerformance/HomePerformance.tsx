import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import React, {useState, useEffect, useMemo} from "react";
import {CustomMonthPickerRangeType} from "src/components/CartagMonthPicker";
import HomePerformanceHeader from "./HomePerformanceHeader";
import HomePerformanceCarousel from "./HomePerformanceCarousel";
import {useAppSelector} from "src/state/hooks";
import {useDispatch} from "react-redux";
export const LEGEND_COLORS = ["#1A06F9", "#FF134C"];

const HomePerformance = () => {
  const [date, setDate] = useState(new Date());
  const [rangeType, setRangeType] =
    useState<CustomMonthPickerRangeType>("month");

  const [departmentType, setDepartmentType] = useState<"mechanic" | "esthetic">(
    "mechanic",
  );

  const stats = useAppSelector((state) => state.statistics[departmentType]);

  const dispatch = useDispatch();

  const labels = useMemo(() => {
    return stats?.details.map((detail) => detail.labelAction) || [];
  }, [stats]);

  const graphData = useMemo(() => {
    const graphs = stats?.details.map((detail) => detail.avg);
    if (!graphs) {
      return undefined;
    }
    const length = graphs[0].length;
    const items: number[][] = [];
    for (let x = 0; x < length; x++) {
      const elem = [];
      for (let y = 0; y < graphs.length; y++) {
        elem.push(graphs[y][x]);
      }
      items.push(elem);
    }
    return items;
  }, [stats]);

  return (
    <Grid container>
      <Grid item xs={12}>
        <Box
          sx={{
            background: "#FFFFFF",
            boxShadow: "0px 4px 20px rgba(20, 32, 100, 0.08)",
            borderRadius: "14px",
            padding: "32px 30px",
          }}
          style={{
            marginTop: "30px",
            paddingTop: "20px !important",
          }}
        >
          <HomePerformanceHeader
            legendLabels={labels}
            date={date}
            setDate={setDate}
            rangeType={rangeType}
            setRangeType={setRangeType}
            departmentType={departmentType}
            setDepartmentType={setDepartmentType}
          />
          <HomePerformanceCarousel rawData={graphData} />
        </Box>
      </Grid>
    </Grid>
  );
};

export default HomePerformance;
