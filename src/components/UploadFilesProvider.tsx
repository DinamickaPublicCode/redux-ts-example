import React, {createContext, useContext, useEffect, useState} from "react";
import {DocumentInfo, FinanceTabType} from "src/types";
import {
  DocumentBoxActionCallback,
  DocumentFileTypes,
  DocumentInputFieldData,
  OnLoadingStateChangedCallback,
} from "src/types/documentBoxInterfaces";
import {downloadFileFromServer} from "src/utils/downloadFileFromServer";

interface UploadFilesState {
  files: DocumentInfo[];
}

type OnUploadFile = (
  id: DocumentInfo,
  fileData: DocumentInputFieldData,
  onLoadingStateChanged: OnLoadingStateChangedCallback,
  onComplete: () => void,
) => void;

type OnDeleteButtonClicked = (
  id: DocumentInfo,
  onLoadingStateChanged: OnLoadingStateChangedCallback,
) => void;

interface UploadFilesDispatch {
  onDownload: DocumentBoxActionCallback;
  onUploadFile: OnUploadFile;
  onDeleteButtonClicked: OnDeleteButtonClicked;
}

const UploadFilesStateContext = createContext<UploadFilesState>({files: []});
const UploadFilesDispatchContext = createContext<UploadFilesDispatch | null>(
  null,
);

const UploadFilesProvider = ({
  children,
  deal,
}: {
  children: React.ReactNode;
  deal: FinanceTabType | null;
}) => {
  const [documents, setDocuments] = useState<UploadFilesState>({
    files: [],
  });

  const onShowFileSizeWarning = () => {
    console.error("Maximum file size is 5MB");
  };

  const MAX_SIZE = 5000000;

  const onFileUpdated = async (e: DocumentInputFieldData, type: string) => {
    if (!e.files?.length) {
      throw new Error("e.target?.files?.length");
    }
    const {validity, files} = e;
    const file = files[0];

    if (file.size > MAX_SIZE) {
      onShowFileSizeWarning();
      throw new Error("Wrong file size");
    }
    // const res = await uploadDocumentOrImage(file);
    if (validity.valid) {
      return {
        file,
        fileData: {
          name: file.name,
          url: file.name,
          isImage: type === DocumentFileTypes.PHOTO,
          id: `${Math.floor(Math.random() * 1000000000)}`,
        },
      };
    }
  };

  useEffect(() => {
    if (deal && "files" in deal && Array.isArray(deal.files)) {
      setDocuments(
        deal as {
          files: DocumentInfo[];
        },
      );
    }
  }, [deal]);

  const onUploadFile: OnUploadFile = async (
    id,
    fileData,
    onLoadingStateChanged,
    onComplete,
  ) => {
    onLoadingStateChanged(true);
    try {
      const result = await onFileUpdated(fileData, id.type);
      if (!result) {
        /// TODO: snackbar
        console.error("Broken ", result);
        return;
      }
      const {fileData: metadata} = result;
      const newDocs = {
        files: [
          ...documents.files.filter((x) => x.type !== id.type),
          {
            id: metadata.id,
            type: id.type,
            name: metadata.url,
          },
        ],
      };
      setDocuments(newDocs);
      onComplete();
      onLoadingStateChanged(false);
    } catch (error) {
      console.error(error);
    }
  };
  const onDeleteButtonClicked: OnDeleteButtonClicked = (
    id,
    onLoadingStateChanged,
  ) => {
    onLoadingStateChanged(true);
    setTimeout(() => {
      const newDocs = {
        files: documents.files.filter((x) => x.type !== id.type),
      };
      setDocuments(newDocs);
      onLoadingStateChanged(false);
    }, 1200);
  };

  return (
    <UploadFilesStateContext.Provider value={documents}>
      <UploadFilesDispatchContext.Provider
        value={{
          onDownload: downloadFileFromServer,
          onUploadFile,
          onDeleteButtonClicked,
        }}
      >
        {children}
      </UploadFilesDispatchContext.Provider>
    </UploadFilesStateContext.Provider>
  );
};

export const useUploadFilesState = () => {
  return useContext(UploadFilesStateContext);
};
export const useUploadFilesDispatch = () => {
  return useContext(
    UploadFilesDispatchContext,
  ) as unknown as UploadFilesDispatch;
};

export default UploadFilesProvider;
