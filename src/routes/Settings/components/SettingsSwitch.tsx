import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import {CartagSwitch} from "../../../components/CartagSwitch";
import React from "react";

export const SettingsSwitch = ({
  label,
  icon,
  value,
  onChange: setValue,
}: {
  icon?: () => JSX.Element;
  label: string;
  value: boolean;
  onChange?: (val: boolean) => void;
}) => {
  return (
    <Box
      sx={{
        mb: 3.2,
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
      }}
    >
      <Box sx={{mr: "20px"}}>
        <CartagSwitch
          checked={value}
          onChange={() => {
            if (setValue) {
              setValue(!value);
            }
          }}
        />
      </Box>
      {icon && <Box sx={{mr: 1}}>{icon()}</Box>}
      <Typography>{label}</Typography>
    </Box>
  );
};
