import React from 'react';

import { ComponentStory, ComponentMeta } from '@storybook/react';
import HomeStatsSmallChart from './HomeStatsSmallChart';

export default {
  title: 'Line chart',
  component: HomeStatsSmallChart,
} as ComponentMeta<typeof HomeStatsSmallChart>;


const Template: ComponentStory<typeof HomeStatsSmallChart> = (args) => <HomeStatsSmallChart {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  size: {
    width: 500,
    height: 400
  },
  data: [-1, 65, -1, -1, 73, -1, 50, 30],
  color: [217, 33, 32]
}
