import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import clsx from "clsx";
import React, {useState} from "react";
import {SpaceX} from "../SalesFunnelLayout/SpaceX";
import {useSalesFunnelStyles} from "../../utils/salesFunnelStyles";
import {useSalesFunnelContext} from "../../state/SalesFunnelProvider";
import {requestGeneratedAvatar} from "src/utils/avatarGenerator";
import {useTranslation} from "react-i18next";
import {LeftArrowButtonIcon} from "../../assets/LeftArrowButtonIcon";
import {RightArrowButtonIcon} from "../../assets/RightArrowButtonIcon";

interface GrayBlackTextProps {
  grayText: string;
  blackText: string;
}

interface StatsBlockProps extends GrayBlackTextProps {
  value: number;
}

const GrayBlackTextBlock = ({grayText, blackText}: GrayBlackTextProps) => {
  const styles = useSalesFunnelStyles();

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
      }}
    >
      <Typography className={styles.yourStatsSold}>{grayText}</Typography>
      <Typography className={styles.yourStatsSoldNew}>{blackText}</Typography>
    </Box>
  );
};

const StatsBlock = (props: StatsBlockProps) => {
  const styles = useSalesFunnelStyles();

  return (
    <Box
      sx={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <GrayBlackTextBlock {...props} />
      <div style={{marginLeft: 17}} className={styles.yourStatsBigNumber}>
        {props.value}
      </div>
    </Box>
  );
};

const PercentBar = ({percent}: {percent: number}) => {
  const styles = useSalesFunnelStyles();

  return (
    <div
      style={{
        flex: 1,
        height: 14,
      }}
      className={styles.percentBarUnfilled}
    >
      <div
        className={styles.percentBar}
        style={{
          width: `${percent}%`,
        }}
      />
    </div>
  );
};

export const PercentBarWidthText = ({percent}: {percent: number}) => {
  const styles = useSalesFunnelStyles();
  return (
    <Box
      sx={{
        display: "flex",
        flex: 1,
        alignItems: "center",
      }}
    >
      <PercentBar percent={percent} />
      <SpaceX size={25} />
      <div style={{flexShrink: 1}} className={styles.bigPercent}>
        {Math.floor(percent)}%
      </div>
    </Box>
  );
};

const TopPerformers = () => {
  const styles = useSalesFunnelStyles();
  const [index, setIndex] = useState(0);
  const {
    dataHandlers: {topSellers},
  } = useSalesFunnelContext();

  const onNextIndex = (diff: number) =>
    setIndex(
      (index + diff < 0 ? (topSellers?.length || 1) - 1 : index + diff) %
        (topSellers?.length || 1),
    );

  return (
    <Box
      sx={{
        display: "flex",
        alignItems: "center",
        justifyContent: "space-betwen",
      }}
    >
      <GrayBlackTextBlock grayText="Top" blackText="Performers" />
      <SpaceX size={36} />
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
        }}
      >
        <div
          style={{
            backgroundImage: `url(${
              topSellers
                ? requestGeneratedAvatar(
                    topSellers[index] && topSellers[index].id,
                    (topSellers[index] && topSellers[index].fullName) || "",
                  )
                : ""
            })`,
            backgroundSize: "cover",
          }}
          className={styles.yourStatsAvatar}
        />
        <Typography className={styles.yourStatsUserName} component="div">
          {topSellers && topSellers[index] ? topSellers[index].fullName : ""}
        </Typography>
      </Box>
      <SpaceX size={36} />
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
        }}
      >
        <IconButton
          onClick={() => onNextIndex(-1)}
          size="small"
          className={styles.arrowButton}
        >
          <LeftArrowButtonIcon />
        </IconButton>
        <SpaceX size={8} />
        <IconButton
          size="small"
          onClick={() => onNextIndex(1)}
          className={styles.arrowButton}
        >
          <RightArrowButtonIcon />
        </IconButton>
      </Box>
    </Box>
  );
};

const YourStats = () => {
  const styles = useSalesFunnelStyles();
  const {
    dataHandlers: {stats},
  } = useSalesFunnelContext();

  const {t} = useTranslation();

  return (
    <>
      <div
        style={{
          display: "flex",
        }}
        className={styles.yourStatsBlock}
      >
        <div
          style={{
            display: "flex",
          }}
        >
          <div style={{width: 142}} className={styles.yourStatsGridItem}>
            <StatsBlock
              grayText={t("Sales_Funnel.Sold_Total.Sold")}
              blackText={t("Sales_Funnel.Sold_Total.Total")}
              value={stats.soldTotal}
            />
          </div>
          <div style={{width: 142}} className={styles.yourStatsGridItem}>
            <StatsBlock
              grayText={t("Sales_Funnel.Goal_Total.Goal")}
              blackText={t("Sales_Funnel.Goal_Total.Total")}
              value={stats.goalTotal}
            />
          </div>
        </div>
        <Grid container className={styles.yourStatsGrid}>
          <Grid
            item
            xs={12}
            sm={6}
            className={clsx(
              styles.yourStatsGridItem,
              styles.yourStatsGridItemLarger,
            )}
          >
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                justifyContent: "space-between",
              }}
            >
              <GrayBlackTextBlock
                grayText={t("Sales_Funnel.Goal_Completion.Goal")}
                blackText={t("Sales_Funnel.Goal_Completion.Completion")}
              />
              <SpaceX size={23} />
              <PercentBarWidthText percent={stats.goalCompletion} />
            </Box>
          </Grid>
          <Grid
            item
            xs={12}
            sm={6}
            className={clsx(
              styles.yourStatsGridItem,
              styles.yourStatsGridItemLarger,
            )}
          >
            <TopPerformers />
          </Grid>
        </Grid>
      </div>
    </>
  );
};

export default YourStats;
