import Box from "@mui/material/Box";
import range from "lodash/range";
import React from "react";

const HomePerformanceVerticalAxis = ({
  step,
  divisions,
}: {
  step: number;
  divisions: number;
}) => {
  return (
    <Box
      sx={{
        height: "100%",
        display: "flex",
        flexDirection: "column",
        justifyContent: "flex-start",
        mr: "40px",
        mb: "25px",
      }}
    >
      {range(0, divisions)
        .map((x) => x * step)
        .reverse()
        .map((item) => (
          <Box
            sx={{
              width: "18px",
              flex: 1,
              display: "flex",
              flexDirection: "column",
              justifyContent: "flex-end",
            }}
            key={item}
          >
            <Box
              sx={{
                fontStyle: "normal",
                fontWeight: "normal",
                fontSize: "14px",
                lineHeight: "18.2px",
                color: "#98A2B7",
              }}
            >
              {item}
            </Box>
          </Box>
        ))}
    </Box>
  );
};

export default HomePerformanceVerticalAxis;
