import React, {useEffect, useState} from "react";
import Box from "@mui/material/Box";
import Fade from "@mui/material/Fade";
import HomePerformanceLegendsCircle from "./HomePerformanceLegendsCircle";

const HomePerformanceBar = ({
  visible,
  value,
  name,
  heightPercent,
  htmlColor,
}: {
  visible: boolean;
  value: number;
  name: string;
  heightPercent: number;
  htmlColor: string;
}) => {
  const [hover, setHover] = useState(false);
  const [percent, setPercent] = useState(0);
  useEffect(() => {
    if (visible) {
      setPercent(heightPercent);
    } else {
      setPercent(0);
    }
  }, [visible, heightPercent]);
  return (
    <div
      className="the-bar"
      onMouseEnter={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
      style={{
        transition: "height 0.5s ease",
        position: "relative",
        borderRadius: "20px",
        width: "18px",
        backgroundColor: htmlColor,
        height: `${percent}%`,
        marginRight: "10px",
      }}
    >
      <Fade in={hover}>
        <Box
          sx={{
            position: "absolute",
            left: "40px",
            top: "-50px",
            padding: "19px 15px 15px 15px",
            zIndex: 20,
            background: "#FFFFFF",
            boxShadow: "0px 4px 20px rgba(203, 205, 217, 0.4)",
            borderRadius: "8px",
          }}
        >
          <Box sx={{width: 134}}>
            <HomePerformanceLegendsCircle htmlColor={htmlColor}>
              {Math.floor(value) + " Units"}
            </HomePerformanceLegendsCircle>
          </Box>
          <Box sx={{ml: 3}}>
            <b>{name}</b>
          </Box>
        </Box>
      </Fade>
    </div>
  );
};

export default HomePerformanceBar;
