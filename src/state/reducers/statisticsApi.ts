import baseQuery from "src/state/baseQuery";
import {createApi} from '@reduxjs/toolkit/query/react';
import {setLoading} from './loading';
import {setStatistics} from './statistics';

export const statisticsApi = createApi({
  reducerPath: 'statisticsApi',
  baseQuery,
  endpoints: (builder) => ({
    getStatistics: builder.query<any, string>({
      query: (departmentType) => ({
        url: `/mock/statistics/cardealer/carDealer1/department/${departmentType}.json`,
        method: 'GET',
      }),
      async onQueryStarted(arg, {dispatch, queryFulfilled}) {
        try {
          dispatch(setLoading(true));
          const res = await queryFulfilled;
          const key = (res?.data?.department || "").toLowerCase();
          dispatch(setStatistics(key, res.data));
          dispatch(setLoading(false));
        } catch (error) {
          dispatch(setLoading(false));
        }
      },
    }),
  }),
});

export const {useGetStatisticsQuery} = statisticsApi;
