import Box from "@mui/material/Box";
import IconButton from "@mui/material/IconButton";
import React from "react";

const SalesFunnelItemCircle = ({
  color,
  checked,
  onClick,
}: {
  onClick: () => void;
  color: string;
  checked: boolean;
}) => {
  return (
    <Box
      flexShrink="0"
      borderRadius="50%"
      width="23px"
      height="23px"
      display="flex"
      flexDirection="row"
      alignItems="center"
      justifyContent="center"
      border={checked ? "none" : "3px solid white"}
      boxShadow={"0px 2px 6px rgba(203, 205, 217, 0.4)"}
      style={{
        backgroundColor: color,
      }}
    >
      <IconButton size="small" onClick={onClick}>
        {checked ? (
          <svg
            width="14"
            height="9"
            viewBox="0 0 14 9"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M13.1251 0.661121C13.4121 0.974952 13.4011 1.47221 13.1004 1.77177L6.53482 8.31263C6.16111 8.68493 5.58653 8.72723 5.16645 8.41335L0.314596 4.78814C-0.0234105 4.53559 -0.101254 4.04488 0.140728 3.69211C0.38271 3.33934 0.852883 3.25809 1.19089 3.51064L5.75309 6.91943L12.0609 0.635292C12.3616 0.335726 12.8381 0.34729 13.1251 0.661121Z"
              fill="white"
            />
          </svg>
        ) : (
          <Box width="14px" height="14px" />
        )}
      </IconButton>
    </Box>
  );
};

export default SalesFunnelItemCircle;
