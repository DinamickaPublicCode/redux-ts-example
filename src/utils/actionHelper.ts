import {ReduxAction} from "src/types";

export function action<T = string, P = {}>(
  type: T,
  payload?: P,
): ReduxAction<T, P> {
  return {type, payload};
}
