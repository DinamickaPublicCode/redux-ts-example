import Button from "@mui/material/Button";
import {useTheme} from "@mui/material/styles";
import React from "react";
import {NavLink} from "react-router-dom";

interface NavigationLinkProps {
  to: string;
  buttonText: string;
  icon?: JSX.Element;
}

const NavigationLink: React.FC<NavigationLinkProps> = ({
  buttonText,
  icon,
  to,
}) => {
  const theme = useTheme();
  return (
    <Button
      component={NavLink}
      to={to ?? "/"}
      color="inherit"
      sx={{
        "&.active": {
          backgroundColor: "#1807DC",
          [theme.breakpoints.down("sm")]: {
            width: "98%",
            height: 100,
          },
        },
        margin: "0 5px",
        padding: "11px 20px",
        fontSize: 15,
        lineHeight: "16px",
        [theme.breakpoints.down("lg")]: {
          margin: "0 20px",
          padding: "11px 0px",
          fontSize: 10,
          lineHeight: "16px",
        },
        [theme.breakpoints.down("sm")]: {
          margin: "0 10px",
          padding: "11px 0px",
          fontSize: 20,
          lineHeight: "16px",
          marginTop: 0,
          fontWeight: 700,
          width: "98%",
          height: 100,
        },
      }}
      startIcon={icon}
      /*    isActive={(
        _: unknown,
        location: {
          pathname: string;
        },
      ) => {
        const isActive = location.pathname === to;
        // setIsActive(isActive);
        return isActive;
      }} */
    >
      {buttonText}
    </Button>
  );
};
export default NavigationLink;
