import React from "react";
import {Navigate, NavLink, Route, Routes} from "react-router-dom";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";
import Header from "src/components/Header";
import {useTranslation} from "react-i18next";
import {useAppSelector} from "src/state/hooks";
import {useDispatch} from "react-redux";
import {setToken} from "src/state/reducers/token";
import {adminArea, personalPreference} from "../utils/listOfLinks";
import SettingsBasicInfo from "./SettingsBasicInfo";
import SettingsNotification from "./SettingsNotification";
import {styled} from "@mui/material/styles";
import {useGetUserQuery} from 'src/state/reducers/userApi';

const SettingsLayout = () => {
  const {t} = useTranslation();
  const isLoggedIn = useAppSelector((state) => !!state.token.value);
  const dispatch = useDispatch();
  const handleLogOut = () => {
    dispatch(setToken(null));
  };

  return (
    <>
      <Header />
      <Container maxWidth="xl">
        <TitleWrapper>
          <Typography
            variant="h4"
            style={{
              fontWeight: 700,
              fontSize: 28,
            }}>
            {t("Navigation.Settings")}
          </Typography>
          {isLoggedIn && false && (
            <div>
              <TitleButton
                sx={{
                  backgroundColor: "#fff",
                  boxShadow: "0px 4px 20px rgba(203, 205, 217, 0.3)",
                }}
                onClick={handleLogOut}>
                {t("Log Out")}
              </TitleButton>
            </div>
          )}
        </TitleWrapper>
        <Grid container spacing={3}>
          <PersonalPreferencesBox item xs={12} md={3}>
            <Paper
              style={{
                padding: "10px 0",
                boxShadow: "0px 4px 20px rgba(20, 32, 100, 0.08)",
              }}>
              <Typography
                variant="h6"
                style={{
                  fontWeight: 600,
                  fontSize: 22,
                  marginLeft: 16,
                }}>
                {t("Personal Preferences")}
              </Typography>
              <List component="nav">
                {personalPreference.map(({name, slag: slug}) => (
                  <ListItem
                    key={name}
                    sx={{
                      p: 0,
                    }}>
                    <ActiveListItem
                      sx={{
                        padding: "8px 16px",
                      }}
                      key={slug}
                      // className={({isActive}) =>
                      //   isActive ? "active-class" : ""
                      // }
                      to={`/settings/${slug}`}>
                      <ListItemText primary={name} />
                    </ActiveListItem>
                  </ListItem>
                ))}
              </List>

              <Typography
                variant="h6"
                style={{
                  fontWeight: 600,
                  fontSize: 22,
                  marginLeft: 16,
                  opacity: 0,
                }}>
                {t("Admin Area")}
              </Typography>
              <List
                style={{
                  opacity: 0,
                }}
                component="nav">
                {adminArea.map(({name, slag}) => (
                  <ListItem key={slag}>
                    <ListItemText primary={name} />
                  </ListItem>
                ))}
              </List>
            </Paper>
          </PersonalPreferencesBox>
          <Grid item xs={12} md={9}>
            <Paper
              style={{
                padding: 30,
                height: "100%",
                boxShadow: "0px 4px 20px rgba(20, 32, 100, 0.08)",
              }}>
              <Routes>
                <Route
                  path="/"
                  element={<Navigate to="/settings/basic-info" />}
                />
                <Route
                  path="/notification"
                  element={<SettingsNotification />}
                />
                <Route path="/basic-info" element={<SettingsBasicInfo />} />
              </Routes>
            </Paper>
          </Grid>
        </Grid>
      </Container>
    </>
  );
};

const TitleWrapper = styled(Box)(({theme}) => ({
  margin: theme.spacing(3, 0),
  display: "flex",
  justifyContent: "space-between",
}));

const TitleButton = styled(Button)(({theme}) => ({
  marginLeft: theme.spacing(3),
  padding: "8px 20px",
  textTransform: "uppercase",
  minWidth: "164px",
}));

const ActiveListItem = styled(NavLink)(({theme}) => ({
  textDecoration: "none",
  color: "inherit",
  display: "block",
  width: "100%",
  "&.active": {
    backgroundColor: "#F6F8FA",

    "& .MuiTypography-root": {
      color: theme.palette.primary.main,
      fontWeight: 600,
    },
  },
}));

const PersonalPreferencesBox = styled(Grid)(({theme}) => ({
  [theme.breakpoints.down("sm")]: {
    height: 150,
  },
}));

export default SettingsLayout;
