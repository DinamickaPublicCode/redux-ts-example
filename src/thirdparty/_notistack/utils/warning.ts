/* eslint-disable */
//@ts-ignore
const __DEV__ = process.env.NODE_ENV !== "production";

export default (message: string) => {
  if (!__DEV__) return;

  if (typeof console !== "undefined") {
    console.error(message);
  }
};
