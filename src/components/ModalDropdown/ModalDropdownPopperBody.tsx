import React from "react";
import ClickAwayListener from "@mui/material/ClickAwayListener";
import Grow from "@mui/material/Grow";
import Paper from "@mui/material/Paper";
import Popper, {PopperProps} from "@mui/material/Popper";

export interface ModalDropdownPopperBodyProps {
  open: boolean;
  anchorRef: React.RefObject<HTMLButtonElement>;
  children: JSX.Element | JSX.Element[];
  handleClose: (event: React.MouseEvent<Document, MouseEvent>) => void;

  placement?:
    | "bottom"
    | "bottom-end"
    | "bottom-start"
    | "left-end"
    | "left-start"
    | "left"
    | "right-end"
    | "right-start"
    | "right"
    | "top-end"
    | "top-start"
    | "top"
    | undefined;
}

const ModalDropdownPopperBody = ({
  open,
  anchorRef,
  handleClose,
  placement,
  children,
  modifiers,
}: ModalDropdownPopperBodyProps & Pick<PopperProps, "modifiers">) => {
  return (
    <Popper
      open={open}
      anchorEl={anchorRef.current}
      role={undefined}
      transition
      disablePortal
      modifiers={modifiers}
      placement={placement || "bottom"}
      style={{zIndex: 100}}
    >
      {({TransitionProps, placement}) => (
        <Grow
          {...TransitionProps}
          style={{
            zIndex: 1000,
            transformOrigin:
              placement === "bottom" ? "center top" : "center bottom",
          }}
        >
          <Paper
            style={{
              boxShadow: "0px 4px 20px rgba(203, 205, 217, 0.3)",
            }}
          >
            <ClickAwayListener
              onClickAway={
                handleClose as unknown as React.ComponentProps<
                  typeof ClickAwayListener
                >["onClickAway"]
              }
            >
              {
                children as unknown as React.ComponentProps<
                  typeof ClickAwayListener
                >["children"]
              }
            </ClickAwayListener>
          </Paper>
        </Grow>
      )}
    </Popper>
  );
};

export default ModalDropdownPopperBody;
