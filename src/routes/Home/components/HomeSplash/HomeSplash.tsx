import React, {useState} from "react";
import {Link} from "react-router-dom";
import Box from "@mui/material/Box";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import {useAppSelector} from "src/state/hooks";
import CrossIcon from "../../assets/CrossIcon";
import HomeSplashImage from "./HomeSplashImage";
import {styled} from "@mui/material/styles";
import {useGetUserQuery} from 'src/state/reducers/userApi';

/**
 * A splash screen for a new user. Has a close button and only shown
 * when a lastConnectionDate in the user structure is not null
 */
const HomeSplash = () => {
  const {data: user} = useGetUserQuery();;

  const [isOpen, setIsOpen] = useState(user?.lastConnexionDate === undefined);

  if (!isOpen) {
    return <Box sx={{height: 36}} />;
  }

  return (
    <Box
      sx={{
        position: "relative",
        mb: 9,
      }}
    >
      <Box
        sx={{
          paddingTop: "25px",
          paddingBottom: "27px",
          paddingLeft: "38px",
          fontStyle: "normal",
          fontWeight: "bold",
          fontSize: "28px",
          lineHeight: "34px",
          color: "#222238",
        }}
      >
        Home
      </Box>
      <Box
        sx={{
          height: 480,
          pt: 3.2,
          pl: 9.8,
          background: "white",
        }}
      >
        <Box
          sx={{
            position: "absolute",
            top: 120,
            left: 40,
          }}
        >
          <IconButton size="small" onClick={() => setIsOpen(false)}>
            <CrossIcon />
          </IconButton>
        </Box>
        <Box sx={{mt: 11}}>
          <Box
            sx={{
              textDecoration: "none",
              fontStyle: "normal",
              fontWeight: "bold",
              fontSize: "50px",
              lineHeight: "55px",
              color: "#222238",
            }}
            style={{
              width: 459,
            }}
          >
            Welcome to your new{" "}
            <DashboardLink to="/dashboard">dashboard</DashboardLink>
          </Box>
          <Typography
            component="div"
            sx={{
              fontStyle: "normal",
              fontWeight: "normal",
              fontSize: "16px",
              lineHeight: "25.6px",
              color: "#222238",
              width: 377,
              marginTop: 20,
            }}
          >
            We overhauled the experience of the <b>CarTag</b> so you can get
            your works faster than ever before. Way faster than all the other
            competitors.
          </Typography>
        </Box>
      </Box>
      <Box
        sx={{
          right: 0,
          top: 53,
          zIndex: 2,
          position: "absolute",
        }}
      >
        <HomeSplashImage />
      </Box>
    </Box>
  );
};

const DashboardLink = styled(Link)({
  textDecoration: "none",
  fontStyle: "normal",
  fontWeight: "bold",
  fontSize: "50px",
  lineHeight: "55px",
  color: "#1A06F9 !important",
});
export default HomeSplash;
