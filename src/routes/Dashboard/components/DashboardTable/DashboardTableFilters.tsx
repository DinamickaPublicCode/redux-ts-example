import React, {useEffect, useState} from "react";
import SearchIcon from "@mui/icons-material/Search";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import IconButton from "@mui/material/IconButton";
import InputBase from "@mui/material/InputBase";
import {styled, useTheme} from "@mui/material/styles";
import Toolbar from "@mui/material/Toolbar";
import {useAppSelector} from "src/state/hooks";
import DashboardTableFiltersButton from "./DashboardTableFiltersButton";
import DashboardTableFiltersDateRange from "./DashboardTableFiltersDateRange";
import {stageDataSelect, stagePreparationDataSelect} from "src/utils/stageData";
import DashboardTableFiltersDialogEdit from "./DashboardTableFiltersDialogEdit";
import {TabViews, TabViewsType} from "src/types";
import {useTranslation} from "react-i18next";
import {useLocation, useNavigate} from "react-router-dom";
import {queryArrayFromString} from "src/utils/queryArrFromString";
import {useGetFinanceManagersQuery} from 'src/state/reducers/carDealerApi'; 

interface DashboardTableFiltersProps {
  tabName: TabViewsType;
  headCells: string[];
  setHeadCells: React.Dispatch<React.SetStateAction<string[]>>;
}

const DashboardTableFilters: React.FC<DashboardTableFiltersProps> = ({
  tabName,
  headCells,
  setHeadCells,
}) => {
  const {t} = useTranslation();
  const navigate = useNavigate();
  const location = useLocation();

  const [editTableOpen, setEditTableOpen] = useState<boolean>(false);

  const [globalSearchValue, setGlobalSearchValue] = useState<string>("");

  const {data:allFinanceManagers} = useGetFinanceManagersQuery();

  const theme = useTheme();

  const handleEditTable = () => {
    setEditTableOpen((prevState) => !prevState);
  };

  const newQueryRes = queryArrayFromString(location.search).filter(
    (i) => !i.includes("text"),
  );
  useEffect(() => {
    if (globalSearchValue.length > 3) {
      if (location.search.includes("text") && !globalSearchValue.trim()) {
        navigate({
          pathname: location.pathname,
          search: `${newQueryRes.join("&")}`,
        });
      } else {
        navigate({
          pathname: location.pathname,
          search: `${newQueryRes.join("&")}&text=${globalSearchValue}`,
        });
      }
    }
    // eslint-disable-next-line
  }, [globalSearchValue]);

  const globalSearchSubmit = (event: React.FormEvent) => {
    event.preventDefault();

    setGlobalSearchValue("");
  };

  const currentTabNameFilters = () => {
    switch (tabName) {
      case TabViews.INVENTORY:
        return (
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
            }}>
            <DashboardTableFiltersButton
              name={t("DashboardTableHead.stage")}
              options={stageDataSelect}
              filterQueryStringNamings={"stage"}
            />
            <DashboardTableFiltersDateRange
              name={t("DashboardTableHead.creationDate")}
              filterQueryStringNamings={["cdtBg", "cdtEd"]}
            />
          </Box>
        );
      case TabViews.MECHANIC:
      case TabViews.ESTHETIC:
        return (
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
            }}>
            <DashboardTableFiltersButton
              name={t("DashboardTableHead.preparationStage")}
              options={stagePreparationDataSelect}
              filterQueryStringNamings={"depStatus"}
            />
            <DashboardTableFiltersDateRange
              name={t("DashboardTableHead.limitDate")}
              filterQueryStringNamings={["ldtBg", "ldtEd"]}
            />
          </Box>
        );
      case TabViews.FINANCE:
        return (
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              [theme.breakpoints.down("sm")]: {
                marginLeft: "30px",
                display: "flex",
                alignItems: "center",
              },
            }}>
            <DashboardTableFiltersButton
              name={t("DashboardTableHead.financeManager")}
              options={allFinanceManagers}
              filterQueryStringNamings={"idFinMan"}
            />
            <DashboardTableFiltersButton
              name={t("DashboardTableHead.preparationStage")}
              options={stagePreparationDataSelect}
              filterQueryStringNamings={"depStatus"}
            />
            <DashboardTableFiltersDateRange
              name={t("DashboardTableHead.saleDate")}
              filterQueryStringNamings={["sdtBg", "sdtEd"]}
            />
          </Box>
        );
      case TabViews.DELIVERY:
        return (
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
            }}>
            <DashboardTableFiltersDateRange
              name={t("DashboardTableHead.deliveryDate")}
              filterQueryStringNamings={["ddtBg", "ddtEn"]}
            />
          </Box>
        );
      default:
        return (
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
            }}
          />
        );
    }
  };

  return (
    <Toolbar
      sx={{
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(1),
      }}>
      <Grid
        container
        spacing={2}
        alignItems="center"
        sx={{
          [theme.breakpoints.down("sm")]: {
            display: "flex",
            alignItems: "center",
            flexDirection: "column",
            width: "100%",
          },
        }}>
        <Grid item xs={12} sm={3}>
          <InputForm onSubmit={globalSearchSubmit}>
            <InputBase
              placeholder={t("search")}
              sx={{
                backgroundColor: "#F6F8FA",
                borderRadius: `${theme.shape.borderRadius}px`,
                padding: "5px 16px",
                marginRight: "10px",
                flex: "1 1 100%",
                [theme.breakpoints.down("sm")]: {
                  width: "90%",
                },
              }}
              value={globalSearchValue}
              onChange={(event) => {
                setGlobalSearchValue(event.target.value);
              }}
            />
            <IconButton
              type="submit"
              sx={{
                padding: "9px",
                backgroundColor: "#F6F8FA",
                borderRadius: `${theme.shape.borderRadius}px`,
              }}
              size="large">
              <SearchIcon color="primary" />
            </IconButton>
          </InputForm>
        </Grid>
        <Grid
          item
          xs={12}
          sm={9}
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
          sx={{
            [theme.breakpoints.down("sm")]: {
              marginBottom: "20px",
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            },
          }}>
          <Box>{currentTabNameFilters()}</Box>
          <Button
            sx={{
              [theme.breakpoints.down("sm")]: {
                marginTop: "20px",
              },
            }}
            style={{
              color: "#98A2B7",
            }}
            onClick={handleEditTable}>
            Edit table
          </Button>
        </Grid>
      </Grid>
      <DashboardTableFiltersDialogEdit
        tabName={tabName}
        headCells={headCells}
        setHeadCells={setHeadCells}
        open={editTableOpen}
        handleClose={handleEditTable}
      />
    </Toolbar>
  );
};

const InputForm = styled("form")(({theme}) => ({
  display: "flex",
  alignItems: "center",
  [theme.breakpoints.down("sm")]: {
    marginTop: 20,
    width: "100%",
  },
}));

export default DashboardTableFilters;
