import {useTranslation} from "react-i18next";

/**
 * A trivial wrapper on the useTranslation hook for accessing the nested home page-related key
 */
const useHomeTranslation = () => {
  const {t} = useTranslation();

  return {
    t: (key: string) => t("HomePage." + key),
  };
};

export default useHomeTranslation;
