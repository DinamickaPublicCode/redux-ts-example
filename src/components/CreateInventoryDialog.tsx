import React, {useEffect, useState} from "react";
import * as yup from "yup";
import {useFormik} from "formik";
import Box from "@mui/material/Box";
import Checkbox from "@mui/material/Checkbox";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import FormControlLabel from "@mui/material/FormControlLabel";
import Grid from "@mui/material/Grid";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import mockStock from "../utils/mockStockForAutocomplete";
import Transition from "./Transition";
import CloseIcon from "@mui/icons-material/Close";
import {useTranslation} from "react-i18next";
import {Theme} from "@mui/material/styles";
import makeStyles from "@mui/styles/makeStyles";
import { ReactComponent as CheckboxIcon } from "src/assets/icons/CheckboxIcon.svg";
import { ReactComponent as CheckboxCheckedIcon } from "src/assets/icons/CheckboxCheckedIcon.svg";
import CartagInputField from "./CartagInputField";
import {searchStockList} from "src/state/api";
import {LoadingButton} from "@mui/lab";
import {FinanceTabType} from "src/types";
import CartagAutocomplete from "./CartagAutocomplete";
import DocumentsBox from "./DocumentsBox";
import {useApiDocumentBoxHandlersNoContext} from "../utils/useApiDocumentBoxHandlers";
import useCustomInfoSnackbar from "src/utils/useCustomInfoSnackbar";
import {useAppSelector} from "src/state/hooks";
import {useGetUserQuery} from 'src/state/reducers/userApi';
import {useGetCarDealerQuery} from 'src/state/reducers/carDealerApi';

const INITIAL_DATA = {
  niv: "",
  cle: "",
  make: "",
  model: "",
  color: "",
  fuel: "",
  year: 0,
  km: 0,
  price: 0,
  transmission: "",
  doors: 0,
  newCar: false,
};

const validationSchema = yup.object().shape({
  niv: yup
    .string()
    .matches(/^[0-9a-zA-Z]+$/, "NIV must be alphanumeric")
    .required("NIV is Required"),
  cle: yup.string(),
  make: yup.string().required("Make is Required"),
  model: yup.string().required("Model is Required"),
  color: yup.string().required("Color is Required"),
  fuel: yup.string().required("Fuel is Required"),
  year: yup.number().required("Year is Required"),
  km: yup.number().required("km is Required"),
  price: yup.number().required("Price is Required"),
  transmission: yup.string(),
  doors: yup.number(),
  newCar: yup.boolean(),
});

interface CreateInventoryDialogProps {
  open: boolean;
  handleClose: () => void;
}

const CreateInventoryDialog: React.FC<CreateInventoryDialogProps> = ({
  open,
  handleClose,
}) => {
  const classes = useStyles();
  const {t} = useTranslation();
  const enqueueInfoSnackbar = useCustomInfoSnackbar();

  const {data: user} = useGetUserQuery();;
  const {data:carDealer} = useGetCarDealerQuery();

  const [stockSearchValue, setStockSearchValue] = useState<string>("");
  useEffect(() => {
    setStockSearchValue("L0954");
    setCurrentStock(mockStock);
  }, [open]);
  const [currentStock, setCurrentStock] = useState<FinanceTabType | null>(null);
  const [stockResults, setStockResults] = useState<FinanceTabType[]>([]);
  const [loadingStock, setLoadingStock] = useState<boolean>(false);

  const documentBoxHandlers = useApiDocumentBoxHandlersNoContext();

  const {
    values,
    errors,
    touched,
    handleChange,
    handleBlur,
    handleSubmit,
    isSubmitting,
    resetForm,
  } = useFormik({
    initialValues: INITIAL_DATA,
    enableReinitialize: true,
    validationSchema,
    onSubmit: async (values, {setSubmitting, resetForm}) => {
      if (currentStock?.id && user?.id) {
        const data = {
          idUser: user.id,
          idCarDealer: carDealer?.id,
          files: documentBoxHandlers.documents.files,
          referenceStock: currentStock.referenceStock,
          generalInfo: {
            color: values.color,
            serialNumberNiv: values.niv,
            numCle: values.cle,
            make: values.make,
            model: values.model,
            fuel: values.fuel,
            year: values.year,
            km: values.km,
            price: values.price,
            transmission: values.transmission,
            door: values.doors,
            newCar: values.newCar,
          },
        };
        console.log("submit", data);
        try {
          // await postNewInventory(data);
          enqueueInfoSnackbar("Inventory successfully created");
        } catch (err) {
          console.log(err);
        } finally {
          setCurrentStock(null);
          setStockSearchValue("");
          setStockResults([]);
          resetForm({
            values: INITIAL_DATA,
          });
          setSubmitting(false);
          handleClose();
        }
      }
    },
  });

  useEffect(() => {
    if (!open) {
      setCurrentStock(null);
      setStockSearchValue("");
      setStockResults([]);
      resetForm({
        values: INITIAL_DATA,
      });
    }
  }, [open, resetForm]);

  useEffect(() => {
    (async () => {
      if (user && stockSearchValue && stockSearchValue.length > 2) {
        try {
          setLoadingStock(true);
          const res = await searchStockList(
            user.defaultCarDealer.id,
            stockSearchValue,
          );
          setStockResults(res.data.content);
        } catch (e) {
          console.log(e);
        } finally {
          setLoadingStock(false);
        }
      }
    })();
  }, [stockSearchValue, user]);

  const onStockValueChange = (value: string) => {
    setStockSearchValue(value);
  };
  const onStockObjectChange = (value: FinanceTabType | null) => {
    setCurrentStock(value);
    setStockSearchValue(value?.referenceStock as string);
  };

  return (
    <Dialog
      scroll="body"
      open={open}
      TransitionComponent={Transition}
      keepMounted
      maxWidth="md"
      onClose={handleClose}
      classes={{
        paper: classes.dialogPaper,
      }}
    >
      <DialogTitle
        classes={{
          root: classes.dialogTitle,
        }}
      >
        <Typography variant="h6" className={classes.titleText}>
          {t("Create new inventory")}
        </Typography>
        <IconButton size="small" onClick={handleClose}>
          <CloseIcon />
        </IconButton>
      </DialogTitle>

      <form onSubmit={handleSubmit} style={{width: "100%"}}>
        <Box className={classes.mainBox}>
          <Box
            sx={{
              width: "100%",
              maxWidth: 878,
              pt: "30px",
            }}
          >
            <Grid
              container
              style={{
                padding: "0 20px",
              }}
            >
              <Grid item xs={12} md={6} className={classes.gridSpacing}>
                <CartagAutocomplete
                  loading={loadingStock}
                  options={stockResults}
                  inputValue={stockSearchValue}
                  stockValue={currentStock}
                  labelText={t("DashboardTableHead.referenceStock")}
                  name="referenceStock"
                  onValueChange={onStockValueChange}
                  onStockChange={onStockObjectChange}
                />
              </Grid>

              <Grid item xs={12} md={6} className={classes.gridSpacing}>
                <CartagInputField
                  labelText={t("DashboardTableHead.niv")}
                  name="niv"
                  error={!!touched.niv && !!errors.niv}
                  helperText={errors.niv}
                  value={values.niv}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
              </Grid>
              <Grid item xs={12} className={classes.gridSpacing}>
                <FormControlLabel
                  label={t("newCar")}
                  onChange={handleChange}
                  checked={values.newCar}
                  value={values.newCar}
                  control={
                    <Checkbox
                      icon={<CheckboxIcon />}
                      checkedIcon={<CheckboxCheckedIcon />}
                      name="newCar"
                      color="primary"
                    />
                  }
                />
              </Grid>

              <Grid item xs={12} md={6} className={classes.gridSpacing}>
                <CartagInputField
                  labelText={t("DashboardTableHead.cle")}
                  name="cle"
                  error={!!touched.cle && !!errors.cle}
                  helperText={errors.cle}
                  value={values.cle}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
              </Grid>
              <Grid item xs={12} md={6} className={classes.gridSpacing}>
                <CartagInputField
                  labelText={t("DashboardTableHead.make")}
                  name="make"
                  error={!!touched.make && !!errors.make}
                  helperText={errors.make}
                  value={values.make}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
              </Grid>

              <Grid item xs={12} md={6} className={classes.gridSpacing}>
                <CartagInputField
                  labelText={t("DashboardTableHead.model")}
                  name="model"
                  error={!!touched.model && !!errors.model}
                  helperText={errors.model}
                  value={values.model}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
              </Grid>
              <Grid item xs={12} md={6} className={classes.gridSpacing}>
                <CartagInputField
                  labelText={t("DashboardTableHead.color")}
                  name="color"
                  error={!!touched.color && !!errors.color}
                  helperText={errors.color}
                  value={values.color}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
              </Grid>

              <Grid item xs={12} md={6} className={classes.gridSpacing}>
                <CartagInputField
                  labelText={t("DashboardTableHead.fuel")}
                  name="fuel"
                  error={!!touched.fuel && !!errors.fuel}
                  helperText={errors.fuel}
                  value={values.fuel}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
              </Grid>
              <Grid item xs={12} md={6} className={classes.gridSpacing}>
                <CartagInputField
                  labelText={t("DashboardTableHead.year")}
                  name="year"
                  type="number"
                  error={!!touched.year && !!errors.year}
                  helperText={errors.year}
                  value={values.year}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
              </Grid>

              <Grid item xs={12} md={6} className={classes.gridSpacing}>
                <CartagInputField
                  labelText={t("DashboardTableHead.km")}
                  name="km"
                  type="number"
                  error={!!touched.km && !!errors.km}
                  helperText={errors.km}
                  value={values.km}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
              </Grid>
              <Grid item xs={12} md={6} className={classes.gridSpacing}>
                <CartagInputField
                  labelText={t("DashboardTableHead.price")}
                  name="price"
                  type="number"
                  inputProps={{
                    min: "0",
                    step: "0.01",
                  }}
                  error={!!touched.price && !!errors.price}
                  helperText={errors.price}
                  value={values.price}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
              </Grid>

              <Grid item xs={12} md={6} className={classes.gridSpacing}>
                <CartagInputField
                  labelText={t("DashboardTableHead.transmission")}
                  name="transmission"
                  error={!!touched.transmission && !!errors.transmission}
                  helperText={errors.transmission}
                  value={values.transmission}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
              </Grid>
              <Grid item xs={12} md={6} className={classes.gridSpacing}>
                <CartagInputField
                  labelText={t("door")}
                  name="doors"
                  type="number"
                  error={!!touched.doors && !!errors.doors}
                  helperText={errors.doors}
                  value={values.doors}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
              </Grid>
            </Grid>
            <Box className={classes.photoBox2}>
              {open && (
                <DocumentsBox
                  shouldRender={true}
                  styling={{
                    hasTopPadding: false,
                    edgeInsets: 24,
                  }}
                  {...documentBoxHandlers}
                />
              )}
            </Box>
            <div
              style={{
                width: "100%",
              }}
              className={classes.submitButtonWrapper}
            >
              <LoadingButton
                type="submit"
                variant="contained"
                color="primary"
                disabled={isSubmitting || !currentStock?.id}
                className={classes.submitButton}
              >
                {t("Create Inventory")}
              </LoadingButton>
            </div>
          </Box>
          <Box className={classes.photoBox1}>
            {open && (
              <DocumentsBox
                shouldRender={true}
                styling={{
                  hasTopPadding: false,
                  edgeInsets: 24,
                }}
                {...documentBoxHandlers}
              />
            )}
          </Box>
        </Box>
      </form>
    </Dialog>
  );
};

export const useStyles = makeStyles((theme: Theme) => ({
  mainBox: {
    display: "flex",
    justifyContent: "space-between",
    [theme.breakpoints.down("sm")]: {
      display: "flex",
      flexDirection: "column",
    },
  },
  dialogPaper: {
    width: "100%",
  },
  dialogTitle: {
    padding: "20px 35px",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    borderBottom: "1px solid #F6F8FA",
  },
  titleText: {
    fontWeight: 600,
    fontSize: 22,
    color: "#222238",
  },
  gridSpacing: {
    padding: "0 20px 30px",
  },
  salesSubTitle: {
    fontSize: 18,
    fontWeight: 600,
    margin: "30px 0",
    width: "100%",
  },
  inputLabel: {
    fontSize: 14,
    color: "#98A2B7",
  },
  submitButtonWrapper: {
    display: "flex",
    justifyContent: "flex-end",
    backgroundColor: "#F6F8FA",
    padding: "25px 32px",
  },
  submitButton: {
    padding: "8px 30px",
    textTransform: "uppercase",
    fontSize: 14,
    fontWeight: 600,
  },
  input: {
    backgroundColor: "#F6F8FA",
    "&:hover .MuiOutlinedInput-notchedOutline, & .MuiOutlinedInput-notchedOutline":
      {
        borderColor: "rgba(0, 0, 0, 0);",
      },
    "&.Mui-focused .MuiOutlinedInput-notchedOutline": {
      borderColor: theme.palette.primary.main,
      borderWidth: 2,
    },
    "&.Mui-error .MuiOutlinedInput-notchedOutline": {
      borderColor: theme.palette.error.main,
      borderWidth: 2,
    },
  },
  grayUppercaseText: {
    fontWeight: 600,
    fontSize: 13,
    textTransform: "uppercase",
    color: "#98A2B7",
  },
  photoBox1: {
    width: "100%",
    maxWidth: 322,
    pt: "30px",
    backgroundColor: "rgb(246, 248, 250)",
    [theme.breakpoints.down("sm")]: {
      maxWidth: 960,
      width: "100%",
      display: "none",
    },
  },
  photoBox2: {
    width: "100%",
    maxWidth: 322,
    pt: "30px",
    backgroundColor: "rgb(246, 248, 250)",
    [theme.breakpoints.down("sm")]: {
      maxWidth: 960,
      width: "100%",
    },
    [theme.breakpoints.up(960)]: {
      display: "none",
    },
  },
}));

export default CreateInventoryDialog;
