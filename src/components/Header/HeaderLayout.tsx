import React, {useState} from "react";
import {Link} from "react-router-dom";
import {useDispatch} from "react-redux";
import {setToken} from "src/state/reducers/token";
import DashboardIcon from "@mui/icons-material/Dashboard";
import HomeRounded from "@mui/icons-material/HomeRounded";
import { ReactComponent as SalesIcon } from "src/assets/icons/SalesIcon.svg";
import HeaderLogo from "src/assets/icons/HeaderLogo";
import { ReactComponent as CalendarIcon } from "src/assets/icons/CalendarIcon.svg";
import NavigationLink from "../NavigationLink";
import {useTranslation} from "react-i18next";
import NotificationsIcon from "@mui/icons-material/Notifications";
import HeaderAvatar from "./HeaderAvatar";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Badge from "@mui/material/Badge";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Collapse from "@mui/material/Collapse";
import {styled, useTheme} from "@mui/material/styles";
import suspended from "src/utils/suspended";
import NotificationsSidebarContext from "src/components/NotificationsSidebar/NotificationsSidebarContext";
import {useAppSelector} from "src/state/hooks";
import {useGetUserQuery} from 'src/state/reducers/userApi';

const NotificationsSidebar = suspended(
  () => import("../NotificationsSidebar"),
  {fallback: <></>},
);

const HeaderLayout = ({
  drawerOpen,
  setDrawerOpen,
}: {
  drawerOpen: boolean;
  setDrawerOpen: React.Dispatch<React.SetStateAction<boolean>>;
}) => {
  const {t} = useTranslation();
  const {data: user} = useGetUserQuery();
  const token = useAppSelector((state) => state.token.value);
  const {notificationsCount} = useAppSelector((state) => state.notifications);
  const dispatch = useDispatch();

  const handleLogOut = () => {
    dispatch(setToken(null));
  };

  const [menuActive, setMenuActive] = useState(false);
  const theme = useTheme();

  return (
    <StyledAppBar position="static">
      <Toolbar
        sx={{
          padding: "11px 20px",
        }}
      >
        <Destkop>
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
            }}
          >
            <Link to="/">
              <HeaderLogo
                style={{
                  margin: "10px 20px 10px 15px",
                }}
              />
            </Link>
            <nav>
              <Grid
                sx={{
                  [theme.breakpoints.down("sm")]: {
                    display: "none",
                  },
                }}
              >
                <NavigationLink
                  to="/"
                  buttonText={t(`Navigation.Home`)}
                  icon={<HomeRounded />}
                />
                <NavigationLink
                  to="/dashboard"
                  buttonText={t(`Navigation.Dashboard`)}
                  icon={<DashboardIcon />}
                />
                <NavigationLink
                  to="/sales-performance"
                  buttonText={t(`Navigation.SalesPerformance`)}
                  icon={<SalesIcon />}
                />
                <NavigationLink
                  to="/sales-calendar"
                  buttonText={t(`Navigation.Calendar`)}
                  icon={<CalendarIcon />}
                />
              </Grid>
            </nav>
          </Box>
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
            }}
          >
            {token ? (
              <>
                <IconButton
                  onClick={() => setDrawerOpen((prevState) => !prevState)}
                  size="large"
                >
                  <Badge
                    color="secondary"
                    badgeContent={notificationsCount}
                    sx={{
                      "& .MuiBadge-standard": {
                        height: "23px",
                        minWidth: "23px",
                        border: "2px solid #fff",
                        fontSize: "11px",
                        fontWeight: 600,
                        padding: "3px",
                      },
                    }}
                  >
                    <NotificationsIcon htmlColor="#fff" />
                  </Badge>
                </IconButton>
                <HeaderAvatar user={user} onLogOut={handleLogOut} />
                <Box
                  sx={{
                    float: "right",
                    cursor: "pointer",
                    paddingLeft: "7%",
                  }}
                  onClick={() => setMenuActive(!menuActive)}
                >
                  <MenuButtonSpan></MenuButtonSpan>
                  <MenuButtonSpan></MenuButtonSpan>
                  <MenuButtonSpan></MenuButtonSpan>
                </Box>
              </>
            ) : (
              <NavigationLink to="/login" buttonText={t(`Navigation.LogIn`)} />
            )}
          </Box>
        </Destkop>
      </Toolbar>
      <Collapse in={menuActive}>
        <NavMobileMenu>
          <NavigationLink
            to="/"
            buttonText={t(`Navigation.Home`)}
            icon={<HomeRounded />}
          />
          <NavigationLink
            to="/dashboard"
            buttonText={t(`Navigation.Dashboard`)}
            icon={<DashboardIcon />}
          />
          <NavigationLink
            to="/sales-performance"
            buttonText={t(`Navigation.SalesPerformance`)}
            icon={<SalesIcon />}
          />
          <NavigationLink
            to="/sales-calendar"
            buttonText={t(`Navigation.Calendar`)}
            icon={<CalendarIcon />}
          />
        </NavMobileMenu>
      </Collapse>
      <NotificationsSidebarContext.Provider
        value={{
          open: drawerOpen,
          onClose: setDrawerOpen,
        }}
      >
        <NotificationsSidebar />
      </NotificationsSidebarContext.Provider>
    </StyledAppBar>
  );
};

const StyledAppBar = styled(AppBar)(({theme}) => ({
  zIndex: theme.zIndex.drawer + 1,
  boxShadow: "none",
  [theme.breakpoints.down("md")]: {
    boxShadow: "none",
  },
}));

const Destkop = styled(Box)({
  width: "100%",
  display: "flex",
  justifyContent: "space-between",
  alignItems: "center",
});

const NavMobileMenu = styled(Box)({
  width: "100%",
  height: 400,
  backgroundColor: "#1A06F9",
});

const MenuButtonSpan = styled("span")(({theme}) => ({
  width: "30px",
  height: "5px",
  backgroundColor: "white",
  margin: "5px",
  display: "none",

  [theme.breakpoints.down("sm")]: {
    display: "block",
  },
}));

export default HeaderLayout;
