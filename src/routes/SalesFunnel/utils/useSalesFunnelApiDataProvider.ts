import {useMemo} from "react";
import {useAppSelector} from "src/state/hooks";
import {CarDealerSeller} from "src/types";
import {
  SaleRefenceUpdateParams,
  SalesFunnelDataHandlers,
  SalesFunnelDataProviderOptions,
} from "../types/apiDataProvider";
import {useSalesFunnelDataProviderInputs} from "./useSalesFunnelDataProviderInputs";
import {usePrecalculatedSales} from "./usePrecalculatedSales";
import {getSellerRoleFromCarFilter} from "./getSellerRoleFromCarFilter";
import {
  useGetCarDealerSellersQuery,
  useGetTopSellersQuery,
  useGetCarDealerSalesNewQuery,
  useGetCarDealerSalesUsedQuery,
} from 'src/state/reducers/carDealerApi'

export const useSalesFunnelApiDataProvider = (
  options: SalesFunnelDataProviderOptions,
) => {
  const inputs = useSalesFunnelDataProviderInputs(options);
  const {carType, legends} = inputs;

  const {data:topSellers} = useGetTopSellersQuery();
  const {data:sellersAll} = useGetCarDealerSellersQuery();

  const {data:salesNew} = useGetCarDealerSalesNewQuery();
  const {data:salesUsed} = useGetCarDealerSalesUsedQuery();

  const sales = useMemo(
    () => (carType === "NEW" ? salesNew : salesUsed),
    [salesNew, salesUsed, carType],
  );
  const role = getSellerRoleFromCarFilter(carType);

  const sellers = useMemo(() => {
    return (sellersAll || []).filter((el: CarDealerSeller) =>
      el.authorities?.includes(role),
    );
  }, [sellersAll, role]);

  const onSaleRefenceUpdated = async (
    idInventory: string,
    idUser: string,
    saleReference: string,
    params: SaleRefenceUpdateParams,
  ) => {
    console.log("sale reference updated");
  };

  const {sellersList, stats} = usePrecalculatedSales(
    sellers,
    sales?.content,
    topSellers,
    options.date.getMonth() + 1,
  );

  const onSellersOrderChanged = async (sellersOrderedIndices: {
    [index: string]: number;
  }) => {
    console.log("sellers order changed", sellersOrderedIndices);
  };

  return {
    legends,
    topSellers,
    sellersList,
    stats,
    onSellersOrderChanged,
    onSaleRefenceUpdated,
  } as SalesFunnelDataHandlers;
};
