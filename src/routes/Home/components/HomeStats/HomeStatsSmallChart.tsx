import Box from "@mui/material/Box";
import Fade from "@mui/material/Fade";
import {LinearGradient} from "@visx/gradient";
import {
  AnimatedAreaSeries,
  AnimatedGlyphSeries,
  AnimatedLineSeries,
  buildChartTheme,
  XYChart,
} from "@visx/xychart";
import React, {useEffect, useMemo, useState} from "react";
import {curveCardinal} from "@visx/curve";
import {GlyphDot} from "@visx/glyph";
import useGraphReveal from "../../utils/useGraphReveal";

const HomeStatsSmallChart = ({
  size: {width, height},
  data: rawData,
  color,
}: {
  size: {
    width: number;
    height: number;
  };
  data: number[];
  color: number[];
}) => {
  const htmlColorKey = useMemo(
    () => "" + color.map((a) => "" + a).reduce((a, b) => `${a}${b}`),
    [color],
  );
  const htmlColor = useMemo(
    () => `rgb(${color[0]}, ${color[1]}, ${color[2]})`,
    [color],
  );
  const preprocessedData = useMemo(
    () =>
      rawData.map((main, index) => ({
        main: main < 0.1 ? 0.1 : main > 0.85 ? 0.85 : main,
        index,
      })),
    [rawData],
  );
  const data = useGraphReveal(preprocessedData, (x) => ({
    ...(x as object),
    main: 0,
  }));
  const theme = useMemo(
    () =>
      buildChartTheme({
        backgroundColor: "#ffffff",
        colors: [htmlColor],
        gridColor: "#336d88",
        gridColorDark: "#1d1b38",
        svgLabelBig: {
          fill: "#1d1b38",
        },
        tickLength: 0,
      }),
    [htmlColor],
  );
  const [relativePoint, setRelativePoint] = useState<{
    x: number;
    y: number;
    index: number;
  } | null>(null);
  const [tooltipVisible, setTooltipVisible] = useState(false);
  useEffect(() => {
    setTooltipVisible(relativePoint !== null);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [relativePoint]);

  const dataHandlers = useMemo(
    () => ({
      dataKey: "main",
      data: data,
      xAccessor: (e: {index: unknown}) => e.index,
      yAccessor: (e: {main: unknown}) => e.main,
    }),
    [data],
  );

  return (
    <Box
      onMouseLeave={() => {
        setTooltipVisible(false);
      }}
      sx={{
        width: "100%",
        height: "100%",
        position: "relative",
      }}
    >
      <Fade in={tooltipVisible}>
        <div>
          <Box
            sx={{
              borderRadius: "4px",
              zIndex: 500,
              boxShadow: "0px 4px 20px rgba(20, 32, 100, 0.08)",
              position: "absolute",
              top: relativePoint?.y || 0,
              left: relativePoint?.x || 0,
              backgroundColor: "white",
              padding: "10px 20px",
              transition:
                "top 195ms cubic-bezier(0.4, 0, 0.2, 1) 0ms, left 195ms cubic-bezier(0.4, 0, 0.2, 1) 0ms",
              paddingLeft: "10px",
            }}
          >
            <Box
              style={{
                display: "flex",
                flexDirection: "row",
              }}
            >
              <Box
                style={{
                  marginTop: "5px",
                }}
              >
                <Box
                  sx={{
                    width: "12px",
                    height: "12px",
                    marginRight: "10px",
                    borderRadius: "50%",
                    border: "2px solid white",
                    boxShadow: "0px 2px 6px rgba(203, 205, 217, 0.4)",
                  }}
                  style={{
                    flexShrink: 0,
                    backgroundColor: htmlColor,
                  }}
                />
              </Box>
              <div>
                <span
                  style={{
                    color: "#98A2B7",
                  }}
                >
                  Highlights
                </span>
                <br />
                <span
                  style={{
                    fontWeight: 600,
                  }}
                >
                  {relativePoint &&
                    Math.floor(rawData[relativePoint?.index] * 100) / 100}
                </span>
              </div>
            </Box>
          </Box>
        </div>
      </Fade>
      <XYChart
        theme={theme}
        width={width}
        height={Math.min(height, 66)}
        xScale={{type: "band"}}
        yScale={{
          type: "linear",
          domain: [0, 1],
        }}
        margin={{
          top: 0,
          bottom: 0,
          left: 0,
          right: 0,
        }}
      >
        <LinearGradient
          id={`area-gradient-${htmlColorKey}`}
          from={htmlColor}
          to={"#ffffff"}
          fromOpacity={0.7}
          toOpacity={0}
        />
        <AnimatedAreaSeries
          {...(dataHandlers as unknown as React.ComponentProps<
            typeof AnimatedAreaSeries
          >)}
          fillOpacity={0.4}
          curve={curveCardinal}
          fill={`url(#area-gradient-${htmlColorKey})`}
          renderLine={false}
        />
        <AnimatedLineSeries
          strokeWidth={2}
          {...(dataHandlers as unknown as React.ComponentProps<
            typeof AnimatedLineSeries
          >)}
          curve={curveCardinal}
          points="1"
        />
        <AnimatedGlyphSeries
          {...(dataHandlers as unknown as React.ComponentProps<
            typeof AnimatedGlyphSeries
          >)}
          size={5}
          onPointerMove={(e) => {
            if (e.index !== relativePoint?.index) {
              setRelativePoint({
                x: e.svgPoint?.x || 0,
                y: e.svgPoint?.y || 0,
                index: e.index,
              });
            }
          }}
          onPointerOut={(e) => {}}
          renderGlyph={({size, color, onPointerUp}) => (
            <GlyphDot
              style={{
                boxShadow: "-1px 0px 3px rgba(20, 32, 100, 0.08)",
              }}
              strokeWidth={1}
              stroke="white"
              fill={color}
              r={size / 2}
            />
          )}
        />
      </XYChart>
    </Box>
  );
};

export default HomeStatsSmallChart;
