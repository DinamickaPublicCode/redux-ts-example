import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import CircularProgress from "@mui/material/CircularProgress";
import React, {useMemo} from "react";
import {getColor, getPercentage} from "src/utils/stageData";
import clsx from "clsx";
import {
  useSalesFunnelCarItemStyles,
  useSalesFunnelCarItemSelectedStyles,
  useSalesFunnelCarItemSpecialStyles,
} from "../../utils/salesFunnelCarItemStyles";
import SalesFunnelItemCirclesDropdown from "./SalesFunnelItemCirclesDropdown";
import {SalesFunnelItemDotsDropdown} from "./SalesFunnelItemDotsDropdown";
import {stageInfoIcons} from "src/utils/stageInfoIcons";
import {CarDealerLegend, ItemReadinessIcons} from "src/types";
import {useSalesFunnelContext} from "../../state/SalesFunnelProvider";
import usePublicUrl from "src/utils/usePublicUrl";
import {
  SellerCarEntry,
  SaleReferenceAction,
} from "src/routes/SalesFunnel/types/apiDataProvider";
import useCustomInfoSnackbar from "src/utils/useCustomInfoSnackbar";
import {KingIcon} from "../../assets/KingIcon";

const SalesFunnelItem = ({
  idUser,
  car,
  isKing,
}: {
  isKing: boolean;
  idUser: string;
  car: SellerCarEntry;
}) => {
  const styles = useSalesFunnelCarItemStyles();
  const specialStyles = useSalesFunnelCarItemSpecialStyles();
  const selectedStyles = useSalesFunnelCarItemSelectedStyles();
  const {
    dataHandlers: {onSaleRefenceUpdated},
  } = useSalesFunnelContext();

  const {
    dataHandlers: {legends},
    carSelection,
    setCarSelection,
  } = useSalesFunnelContext();

  const selected =
    car.reference.sellerInfo.idSeller === carSelection?.sellerId &&
    car.sale.id === carSelection?.inventoryId;

  const setSelected = (value: boolean) => {
    setCarSelection(
      value
        ? {
            sellerId: car.reference.sellerInfo.idSeller,
            inventoryId: car.sale.id,
          }
        : null,
    );
  };

  const carLegends = useMemo(() => {
    return car.reference.legends
      .map((legend) =>
        legends
          ?.filter((filteredLegend) => filteredLegend.idLegend === legend)
          .pop(),
      )
      .filter((legend) => legend !== undefined) as CarDealerLegend[];
  }, [car.reference.legends, legends]);

  const getClasses = (className: string) =>
    clsx(
      styles[className as keyof typeof styles],
      selected
        ? selectedStyles[className as keyof typeof selectedStyles] || ""
        : "",
    );

  const {percentage, color} = useMemo(() => {
    return {
      percentage: getPercentage(car.sale.stageInfo.stage),
      color: getColor(car.sale.stageInfo.stage),
    };
  }, [car]);

  const publicUrl = usePublicUrl();

  const enqueueInfoSnackbar = useCustomInfoSnackbar();

  return (
    <div
      className={
        isKing && !selected
          ? clsx(specialStyles.carItemBody, styles.carItemBody)
          : getClasses("carItemBody")
      }
    >
      <Grid container>
        <Grid item xs={4}>
          <div
            style={{
              marginBottom: 15,
              marginRight: 14,
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
            onClick={() => setSelected(!selected)}
            className={styles.pointer}
          >
            <Box
              sx={{
                width: "48px",
                marginBottom: "11px",
                height: "48px",
                position: "relative",
              }}
            >
              <Box
                sx={{
                  position: "absolute",
                  width: 38,
                  height: 38,
                  borderRadius: "50%",
                  top: 5,
                  left: 5,
                  backgroundImage: `url(${publicUrl(
                    `images/car-logos/${car.sale.generalInfo.make}.png`,
                  )})`,
                  backgroundSize: "85%",
                  backgroundRepeat: "no-repeat",
                  backgroundPosition: "50%",
                }}
              />
              <CircularProgress
                size="48px"
                variant="determinate"
                thickness={1}
                style={{
                  color: "#F3F6F9",
                  position: "absolute",
                }}
                value={100}
              />
              <CircularProgress
                style={{
                  color: selected ? "white" : color,
                  position: "absolute",
                }}
                size="48px"
                variant="determinate"
                thickness={2}
                value={percentage || 0}
              />
            </Box>
            <div className={getClasses("carItemPercent")}>{percentage}%</div>
          </div>
        </Grid>
        <Grid item xs={7}>
          <div
            style={{
              position: "relative",
            }}
            onClick={() => setSelected(!selected)}
            className={styles.pointer}
          >
            <Typography component="div" className={getClasses("carNameTitle")}>
              {car.sale.generalInfo.make}
            </Typography>
            <Typography component="div" className={getClasses("stockNumber")}>
              Stock {car.reference.saleReference}
            </Typography>
            <Grid container>
              {car.sale.stageInfo.icons.map((x) => (
                <Grid
                  key={x}
                  item
                  style={{
                    paddingRight: 8,
                  }}
                >
                  {stageInfoIcons[x as unknown as ItemReadinessIcons]}
                </Grid>
              ))}
            </Grid>

            {isKing && (
              <Box
                sx={{
                  position: "absolute",
                  zIndex: 10,
                  top: -3,
                  right: carLegends.length === 0 ? -10 : 5,
                }}
              >
                <KingIcon />
              </Box>
            )}
          </div>
        </Grid>
        <Grid item xs={1}>
          <Box display="flex" flexDirection="column" alignItems="flex-end">
            <SalesFunnelItemCirclesDropdown
              onUpdateLegends={async (legends) => {
                try {
                  await onSaleRefenceUpdated(
                    car.sale.id,
                    idUser,
                    car.reference.saleReference,
                    {
                      action: SaleReferenceAction.UPDATE_SALE,
                      legends: legends.map((legend) => legend.idLegend),
                    },
                  );
                  enqueueInfoSnackbar("Success");
                } catch (e) {}
              }}
              selected={selected}
              legends={carLegends}
            />

            <SalesFunnelItemDotsDropdown idUser={idUser} car={car} />
          </Box>
        </Grid>
      </Grid>
    </div>
  );
};
export default SalesFunnelItem;
