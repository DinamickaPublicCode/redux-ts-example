import React from "react";
import {useTranslation} from "react-i18next";
import MuiTableHead from "@mui/material/TableHead";
import { ReactComponent as SortIcon } from "src/assets/icons/SortIcon.svg";
import { ReactComponent as CheckboxIcon } from "src/assets/icons/CheckboxIcon.svg";
import { ReactComponent as CheckboxCheckedIcon } from "src/assets/icons/CheckboxCheckedIcon.svg";
import { ReactComponent as CheckboxIndeterminateIcon } from "src/assets/icons/CheckboxIndeterminateIcon.svg";
import TableRow from "@mui/material/TableRow";
import MuiTableCell from "@mui/material/TableCell";
import Checkbox from "@mui/material/Checkbox";
import TableSortLabel from "@mui/material/TableSortLabel";
import {styled} from "@mui/material/styles";

type Order = "asc" | "desc";

interface DashboardTableHeadProps {
  headCells: string[];
  numSelected: number;
  onRequestSort: (event: React.MouseEvent<unknown>, property: string) => void;
  onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>) => void;
  order: Order;
  orderBy: string;
  rowCount: number;
}

const DashboardTableHead: React.FC<DashboardTableHeadProps> = ({
  headCells,
  numSelected,
  onRequestSort,
  onSelectAllClick,
  order,
  orderBy,
  rowCount,
}) => {
  headCells = ["referenceStock", ...headCells, "actions"];
  const {t} = useTranslation();

  const createSortHandler =
    (property: string) => (event: React.MouseEvent<unknown>) => {
      onRequestSort(event, property);
    };

  return (
    <MuiTableHead>
      <TableRow>
        <TableCell variant="head" padding="checkbox">
          <Checkbox
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={rowCount > 0 && numSelected === rowCount}
            onChange={onSelectAllClick}
            color="default"
            icon={<CheckboxIcon />}
            checkedIcon={<CheckboxCheckedIcon />}
            indeterminateIcon={<CheckboxIndeterminateIcon />}
          />
        </TableCell>
        {headCells.map((headCell) => (
          <TableCell
            variant="head"
            key={headCell}
            align="left"
            padding="normal"
            sortDirection={orderBy === headCell ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell}
              direction={orderBy === headCell ? order : "asc"}
              onClick={createSortHandler(headCell)}
              hideSortIcon={false}
              IconComponent={SortIcon}
              sx={{
                color: "#98A2B7",
                justifyContent: "space-between",
                width: "100%",
                minWidth: 60,
                "&$sortLabelActive": {
                  color: "#98A2B7",
                },
              }}
            >
              {t(`TableHead.${headCell}`).toUpperCase()}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </MuiTableHead>
  );
};

const TableCell = styled(MuiTableCell)({
  backgroundColor: "#F6F8FA",
  borderRight: "2px solid #fff",
  borderLeft: "2px solid #fff",
  "&:first-child": {
    borderLeft: "none",
    borderTopLeftRadius: "14px",
  },
  "&:last-child": {
    borderRight: "none",
    borderTopRightRadius: "14px",
  },
  //
  padding: "13px 10px",
});

export default DashboardTableHead;
