import React, {useEffect, useState} from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Checkbox from "@mui/material/Checkbox";
import Collapse from "@mui/material/Collapse";
import FormControlLabel from "@mui/material/FormControlLabel";
import Typography from "@mui/material/Typography";
import {useTranslation} from "react-i18next";
import {useAppSelector} from "src/state/hooks";
import {NotificationSettings} from "src/types";
import {Formik} from "formik";
import * as yup from "yup";
import {CheckboxCheckedIcon, CheckboxIcon} from "src/assets/icons";
import clsx from "clsx";
import WhiteInputField from "./SettingsPhoneInput";
import {TelephoneMaskComponent} from "../../../components/TelephoneMaskComponent";
import useCustomErrorSnackbar from "src/utils/useCustomErrorSnackbar";
import useCustomInfoSnackbar from "src/utils/useCustomInfoSnackbar";
import {WhatsAppIcon} from "../assets/WhatsAppIcon";
import {SlackIcon} from "../assets/SlackIcon";
import makeStyles from "@mui/styles/makeStyles";
import {Theme} from "@mui/material/styles";
import createStyles from "@mui/styles/createStyles";
import {SettingsSwitch} from "./SettingsSwitch";
import {InputBaseComponentProps} from "@mui/material/InputBase";
import {useGetUserQuery} from 'src/state/reducers/userApi';

const INITIAL_STATE = {
  web: false,
  email: false,
  sms: false,
  whatsapp: false,
  phone: "",
  phone2: "",
  slack: false,
  slackUrl: "",
  finance: false,
  delivery: false,
  sale: false,
  mechanic: false,
  esthetic: false,
};

const validationSchema = yup.object().shape({
  web: yup.boolean(),
  email: yup.boolean(),
  sms: yup.boolean(),
  whatsapp: yup.boolean(),
  phone: yup.string().when("whatsapp", {
    is: true,
    then: yup.string().required("Enter your phone"),
  }),
  phone2: yup.string().when("sms", {
    is: true,
    then: yup.string().required("Enter your phone"),
  }),
  slack: yup.boolean(),
  slackUrl: yup.string().when("slack", {
    is: true,
    then: yup.string().required("Enter your slack url"),
  }),
  finance: yup.boolean(),
  delivery: yup.boolean(),
  sale: yup.boolean(),
  mechanic: yup.boolean(),
  esthetic: yup.boolean(),
});

const SettingsNotification = () => {
  const {data: user} = useGetUserQuery();;
  const {t} = useNotificationSettingsTranslation();

  const [inputData, setInputData] = useState(INITIAL_STATE);
  const styles = useNotificationStyles();

  useEffect(() => {
    const newState = {
      web: false,
      email: false,
      sms: false,
      whatsapp: false,
      phone: "",
      phone2: "",
      slack: false,
      slackUrl: "",
      finance: false,
      delivery: false,
      sale: false,
      mechanic: false,
      esthetic: false,
    };
    user?.notification.channels.forEach((channel) => {
      if (channel.type === "WEB") {
        newState.web = true;
      }
      if (channel.type === "EMAIL") {
        newState.email = true;
      }
      if (channel.type === "SMS") {
        newState.sms = true;
        newState.phone2 = channel.value || "";
      }
      if (channel.type === "WHATSAPP") {
        newState.whatsapp = true;
        newState.phone = channel.value || "";
      }
      if (channel.type === "SLACK") {
        newState.slack = true;
        newState.slackUrl = channel.value || "";
      }
    });
    user?.notification.topics.forEach((topic) => {
      if (topic.type === "SALE") {
        newState.sale = true;
      }
      if (topic.type === "DELIVERY") {
        newState.delivery = true;
      }
      if (topic.type === "MECHANIC") {
        newState.mechanic = true;
      }
      if (topic.type === "FINANCE") {
        newState.finance = true;
      }
      if (topic.type === "ESTHETIC") {
        newState.esthetic = true;
      }
    });
    setInputData(newState);
  }, [user]);

  const enqueueErrorSnackbar = useCustomErrorSnackbar();
  const enqueueInfoSnackbar = useCustomInfoSnackbar();

  return (
    <Formik
      initialValues={inputData}
      enableReinitialize
      validationSchema={validationSchema}
      onSubmit={async (values, {setSubmitting}) => {
        // create outgoing data
        const idUser = user?.id;

        if (!idUser) {
          enqueueErrorSnackbar("Invalid used ID");
          return;
        }

        const outgoing: {
          notification: NotificationSettings;
        } = {
          notification: {
            channels: [],
            topics: [],
          },
        };
        if (values.web) {
          outgoing.notification.channels.push({type: "WEB"});
        }
        if (values.email) {
          outgoing.notification.channels.push({type: "EMAIL"});
        }
        if (values.sms) {
          outgoing.notification.channels.push({
            type: "SMS",
            value: values.phone2,
          });
        }
        if (values.whatsapp) {
          outgoing.notification.channels.push({
            type: "WHATSAPP",
            value: values.phone,
          });
        }
        if (values.slack) {
          outgoing.notification.channels.push({
            type: "SLACK",
            value: values.slackUrl,
          });
        }
        if (values.sale) {
          outgoing.notification.topics.push({
            type: "SALE",
          });
        }
        if (values.delivery) {
          outgoing.notification.topics.push({
            type: "DELIVERY",
          });
        }
        if (values.mechanic) {
          outgoing.notification.topics.push({
            type: "MECHANIC",
          });
        }
        if (values.finance) {
          outgoing.notification.topics.push({
            type: "FINANCE",
          });
        }
        if (values.esthetic) {
          outgoing.notification.topics.push({
            type: "ESTHETIC",
          });
        }
        try {
          setSubmitting(true);
          // await updateUserDataById(idUser, outgoing);
          setSubmitting(false);
          enqueueInfoSnackbar("Success");
        } catch (e) {}
      }}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        setFieldValue,
      }) => (
        <form onSubmit={handleSubmit}>
          <Box>
            <Typography className={clsx(styles.sectionTitle, styles.mainTitle)}>
              {t(`Configure your notification channels`)}
            </Typography>
            <Typography className={styles.sectionSubtitle}>
              {t(
                `Choose your channel that works with you the best to get notifcation`,
              )}
            </Typography>
            <Box
              sx={{
                mt: "31px",
              }}
            >
              <SettingsSwitch
                value={values.web}
                onChange={(value: boolean) => setFieldValue("web", value)}
                label={t("Web")}
              />
              <SettingsSwitch
                value={values.email}
                onChange={(value: boolean) => setFieldValue("email", value)}
                label={t("Email")}
              />
              <SettingsSwitch
                value={values.whatsapp}
                onChange={(value: boolean) => setFieldValue("whatsapp", value)}
                icon={WhatsAppIcon}
                label={t("WhatsApp")}
              />
              <Collapse
                in={values.whatsapp}
                className={styles.addNumberCollapse}
              >
                <Box className={styles.addUrlBox}>
                  <Typography className={styles.sectionTitle}>
                    {t("Please Add your Phone number")}
                  </Typography>
                  <WhiteInputField
                    className={styles.textField}
                    InputProps={{
                      inputComponent:
                        TelephoneMaskComponent as unknown as React.ElementType<InputBaseComponentProps>,
                    }}
                    placeholder={t("Add your phone number")}
                    name="phone"
                    error={!!touched.phone && !!errors.phone}
                    helperText={errors.phone}
                    value={values.phone}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                </Box>
              </Collapse>
              <SettingsSwitch
                value={values.sms}
                onChange={(value: boolean) => setFieldValue("sms", value)}
                label={t("Sms")}
              />
              <Collapse in={values.sms}>
                <Box className={styles.addNumberBox}>
                  <Typography className={styles.sectionTitle}>
                    {t("Please Add your Phone number")}
                  </Typography>
                  <WhiteInputField
                    className={styles.textField}
                    InputProps={{
                      // tslint:disable-next-line:no-any
                      inputComponent:
                        TelephoneMaskComponent as unknown as React.ElementType<InputBaseComponentProps>,
                    }}
                    placeholder={t("Add your phone number")}
                    name="phone2"
                    error={!!touched.phone2 && !!errors.phone2}
                    helperText={errors.phone2}
                    value={values.phone2}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                </Box>
              </Collapse>
            </Box>
            <SettingsSwitch
              value={values.slack}
              onChange={(value: boolean) => setFieldValue("slack", value)}
              icon={SlackIcon}
              label={t("Slack")}
            />
            <Collapse in={values.slack}>
              <Box className={styles.addUrlBox}>
                <Typography className={styles.sectionTitle}>
                  Please Add your slack URL
                </Typography>
                <WhiteInputField
                  className={styles.textField}
                  placeholder="Add your slack url"
                  name="slackUrl"
                  error={!!touched.slackUrl && !!errors.slackUrl}
                  helperText={errors.slackUrl}
                  value={values.slackUrl}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
              </Box>
            </Collapse>
          </Box>
          <Box>
            <Typography className={clsx(styles.sectionTitle, styles.mainTitle)}>
              {t("Configure your subscription purpose")}
            </Typography>
            <Typography className={styles.sectionSubtitle}>
              Choose for what kind of purpose you’d like to be notified for
            </Typography>
            <Box
              sx={{
                mt: "30px",
              }}
            >
              <Box>
                <FormControlLabel
                  style={{
                    marginBottom: 10,
                  }}
                  label={t("Notification about credit validation")}
                  checked={values.finance}
                  onChange={() => setFieldValue("finance", !values.finance)}
                  value={values.finance}
                  control={
                    <Checkbox
                      icon={<CheckboxIcon />}
                      checkedIcon={<CheckboxCheckedIcon />}
                      name="finance"
                      color="primary"
                    />
                  }
                />
              </Box>
              <Box>
                <FormControlLabel
                  style={{
                    marginBottom: 10,
                  }}
                  label={t(
                    "Notifications about the tasks assigned to delivery team",
                  )}
                  checked={values.delivery}
                  onChange={() => setFieldValue("delivery", !values.delivery)}
                  value={values.delivery}
                  control={
                    <Checkbox
                      icon={<CheckboxIcon />}
                      checkedIcon={<CheckboxCheckedIcon />}
                      name="delivery"
                      color="primary"
                    />
                  }
                />
              </Box>
              <Box>
                <FormControlLabel
                  style={{
                    marginBottom: 10,
                  }}
                  label={t("Notification Sale team")}
                  checked={values.sale}
                  onChange={() => setFieldValue("sale", !values.sale)}
                  value={values.sale}
                  control={
                    <Checkbox
                      icon={<CheckboxIcon />}
                      checkedIcon={<CheckboxCheckedIcon />}
                      name="sale"
                      color="primary"
                    />
                  }
                />
              </Box>
              <Box>
                <FormControlLabel
                  style={{
                    marginBottom: 10,
                  }}
                  label={t("Notification Mechanic Team")}
                  checked={values.mechanic}
                  onChange={() => setFieldValue("mechanic", !values.mechanic)}
                  value={values.mechanic}
                  control={
                    <Checkbox
                      icon={<CheckboxIcon />}
                      checkedIcon={<CheckboxCheckedIcon />}
                      name="mechanic"
                      color="primary"
                    />
                  }
                />
              </Box>
              <Box>
                <FormControlLabel
                  style={{
                    marginBottom: 10,
                  }}
                  label={t("Notification Esthetic Team")}
                  checked={values.esthetic}
                  onChange={() => setFieldValue("esthetic", !values.esthetic)}
                  value={values.esthetic}
                  control={
                    <Checkbox
                      icon={<CheckboxIcon />}
                      checkedIcon={<CheckboxCheckedIcon />}
                      name="esthetic"
                      color="primary"
                    />
                  }
                />
              </Box>
            </Box>
          </Box>
          <Box sx={{mt: "30px"}}>
            <Button
              type="submit"
              className={styles.submitButton}
              variant="contained"
            >
              Save
            </Button>
          </Box>
        </form>
      )}
    </Formik>
  );
};

export const useNotificationStyles = makeStyles((theme: Theme) =>
  createStyles({
    sectionTitle: {
      fontStyle: "normal",
      fontWeight: 600,
      fontSize: "18px",
      lineHeight: "22.32px",
      color: "#222238",
    },
    mainTitle: {
      marginBottom: 14,
    },
    sectionSubtitle: {
      fontStyle: "normal",
      fontWeight: "normal",
      fontSize: "14px",
      lineHeight: "17px",
      color: "#98A2B7",
    },
    submitButton: {
      textTransform: "uppercase",
      width: 156,
    },
    textField: {
      fontSize: "12px !important",
      backgroundColor: "#ffffff",
    },

    root: {
      backgroundColor: "transparent",
    },
    input: {
      backgroundColor: "#ffffff",
      fontSize: "12px",
      borderRadius: "10px",
      "&.Mui-disabled": {
        backgroundColor: "#EBF1F6",
        color: "#C1C7D4",
      },
      "&.Mui-disabled .MuiOutlinedInput-notchedOutline": {
        borderWidth: 0,
      },

      "&:hover .MuiOutlinedInput-notchedOutline, & .MuiOutlinedInput-notchedOutline":
        {
          borderColor: "rgba(0, 0, 0, 0);",
        },
      "&.Mui-focused .MuiOutlinedInput-notchedOutline": {
        borderColor: theme.palette.primary.main,
        borderWidth: 2,
      },
      "&.Mui-error .MuiOutlinedInput-notchedOutline": {
        borderColor: theme.palette.error.main,
        borderWidth: 2,
      },
    },
    addNumber: {
      borderRadius: "10px",
      backgroundColor: "#F6F8FA",
      width: 470,
      p: "24px",
      mb: "36px",
    },
    addNumberCollapse: {
      // [theme.breakpoints.down(600)]:{
      //     display:"none"
      // }
    },
    addNumberBox: {
      borderRadius: "10px",
      backgroundColor: "#F6F8FA",
      width: 470,
      p: "24px",
      mb: "36px",
      padding: 25,
      [theme.breakpoints.down("sm")]: {
        width: 230,
        height: 150,
      },
    },
    addUrlBox: {
      borderRadius: "10px",
      backgroundColor: "#F6F8FA",
      width: 470,
      p: "24px",
      mb: "36px",
      padding: 25,
      [theme.breakpoints.down("sm")]: {
        width: 230,
        height: 165,
      },
    },
  }),
);

const useNotificationSettingsTranslation = () => {
  const {t} = useTranslation();
  return {
    t: (key: string) => t("NotificationSettings." + key),
  };
};

export default SettingsNotification;
