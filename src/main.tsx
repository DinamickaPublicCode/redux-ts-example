import React from "react";
import {createRoot} from "react-dom/client";
import App from "./system/App";
import "./utils/i18n";
import "./thirdparty/dayjs";

const container = document.getElementById("root");
const root = createRoot(container!);

root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
);
