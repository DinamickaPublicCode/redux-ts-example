import {Theme} from "@mui/material/styles";

import createStyles from "@mui/styles/createStyles";
import makeStyles from "@mui/styles/makeStyles";

export const useSalesFunnelStyles = makeStyles((theme: Theme) =>
  createStyles({
    buttonBase: {
      textTransform: "uppercase",
      fontStyle: "normal",
      fontWeight: 600,
      fontSize: "14px",
      lineHeight: "17px",
      textAlign: "center",
      padding: "13px 45px",
      borderRadius: "10px",
      [theme.breakpoints.down("sm")]: {
        width: "49%",
      },
    },
    buttonBlue: {
      color: "white",
      backgroundColor: theme.palette.primary.main,
      boxShadow: "0px 3px 14px rgba(2, 53, 236, 0.3)",
      "&:hover": {
        backgroundColor: "#1807DC",
      },
    },
    buttonWhite: {
      color: theme.palette.primary.main,
      backgroundColor: "white",
      boxShadow: "0px 3px 20px rgba(203, 205, 217, 0.4)",
    },
    chatBox: {
      position: "fixed",
      width: 300,
      height: "100%",
      right: 0,
      zIndex: 5,
      backgroundColor: "rgb(246, 248, 250)",
      boxShadow: "-10px 10px 40px rgba(20, 32, 100, 0.06)",
    },
    yourStatsBlock: {
      backgroundColor: "white",
      borderTopLeftRadius: 10,
      borderTopRightRadius: 10,
      [theme.breakpoints.down("sm")]: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      },
    },
    yourStatsUserName: {
      fontStyle: "normal",
      fontWeight: 600,
      fontSize: "16px",
      lineHeight: "19px",
      color: "#000000",
    },
    yourStatsAvatar: {
      flexShrink: 0,
      borderRadius: "50%",
      marginRight: "10px",
      width: "30px",
      height: "30px",
      border: "2px solid white",
      background: "white",
      boxSizing: "border-box",
      boxShadow: "0px 2px 10px rgba(203, 205, 217, 0.25)",
    },
    yourStatsGridItem: {
      paddingTop: 16,
      paddingBottom: 14,
      paddingLeft: 32,
      paddingRight: 32,
      borderRight: "2px solid #F6F8FA",
      [theme.breakpoints.down("sm")]: {
        paddingLeft: 5,
        paddingRight: 5,
      },
      "&:last-child": {
        border: "none",
      },
      // [theme.breakpoints.down(960)]:{
      //   display:"flex",
      //     alignItems:"center",
      //     justifyContent:"space-between",
      // }
    },
    yourStatsGrid: {
      [theme.breakpoints.down("sm")]: {
        display: "flex",
      },
    },
    yourStatsGridItemLarger: {
      paddingTop: 24,
    },
    arrowButton: {
      padding: 0,
    },
    salesFunnelContent: {
      backgroundColor: "#F6F8FA",
      borderRadius: 10,
      boxShadow: "0px 4px 20px rgba(203, 205, 217, 0.3)",
      marginLeft: 20,
      marginRight: 20,
    },
    bigPercent: {
      fontStyle: "normal",
      fontWeight: "bold",
      fontSize: "22px",
      lineHeight: "28.6px",
      textAlign: "right",
      color: "#222238",
    },
    percentBarUnfilled: {
      background: "rgba(255,19,76,0.06)",
      borderRadius: "10px",
      padding: "2px 1px",
    },
    percentBar: {
      background: "#FF134C",
      borderRadius: "10px",
      height: "100%",
    },
    yourStatsBigNumber: {
      fontStyle: "normal",
      fontWeight: "bold",
      fontSize: "22px",
      lineHeight: "28.6px",
      textAlign: "center",
      color: "#222238",
      padding: "11px 14px",
      background: "#FFFFFF",
      boxShadow: "0px 2px 16px rgba(203, 205, 217, 0.4)",
      borderRadius: "8px",
      width: 50,
      height: 50,
    },
    yourStatsSold: {
      fontStyle: "normal",
      fontWeight: "normal",
      fontSize: "14px",
      lineHeight: "137.5%",
      color: "#98A2B7",
    },
    yourStatsSoldNew: {
      fontStyle: "normal",
      fontWeight: 600,
      fontSize: "14px",
      lineHeight: "125%",
      color: "#222238",
    },
    searchBar: {
      background: "#FFFFFF",
      boxShadow: "0px 4px 20px rgba(203, 205, 217, 0.3)",
      borderRadius: "10px",
    },
    markerText: {
      display: "flex",
      alignItems: "center",
      marginRight: 36,
      fontSize: 14,
      color: "#98A2B7",
    },
    salesTable: {
      fontStyle: "normal",
      fontWeight: "bold",
      fontSize: "28px",
      lineHeight: "34px",
      color: "#222238",
      [theme.breakpoints.down("sm")]: {},
    },
    boxOnTable: {
      minWidth: 320,
      flexDirection: "row",
      display: "flex",
      alignItems: "center",
      justifyContent: "space-between",
      paddingLeft: "20px",
      paddingRight: "20px",
      paddingTop: "21px",
      paddingBottom: "23px",
      [theme.breakpoints.down("sm")]: {
        flexDirection: "column",
      },
    },
    boxOnTableSalesTable: {
      display: "flex",
      alignItems: "center",
      [theme.breakpoints.down("sm")]: {
        flexDirection: "column",
        width: "100%",
      },
    },
    monthPicker: {
      flexShrink: 1,
      [theme.breakpoints.down("sm")]: {
        width: "100%",
        marginTop: 20,
        marginBottom: 20,
      },
    },
    boxOnTableSearch: {
      display: "flex",
      alignItems: "center",
      justifyContent: "space-between",
      [theme.breakpoints.down("sm")]: {
        flexDirection: "column",
        width: "100%",
      },
    },
    search: {
      flexShrink: 1,
      [theme.breakpoints.down("sm")]: {
        marginTop: 20,
        marginBottom: 20,
        width: "100%",
      },
    },
    buttons: {
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
      [theme.breakpoints.down("sm")]: {
        width: "100%",
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
      },
    },
  }),
);
