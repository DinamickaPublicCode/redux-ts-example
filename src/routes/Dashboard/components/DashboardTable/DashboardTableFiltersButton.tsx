import React, {useEffect, useState} from "react";
import ArrowDropDown from "@mui/icons-material/ArrowDropDown";
import SearchIcon from "@mui/icons-material/Search";
import { ReactComponent as CheckboxIcon } from "src/assets/icons/CheckboxIcon.svg";
import { ReactComponent as CheckboxCheckedIcon } from "src/assets/icons/CheckboxCheckedIcon.svg";
import {useTranslation} from "react-i18next";
import {useLocation, useNavigate} from "react-router-dom";
import {queryArrayFromString} from "src/utils/queryArrFromString";
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
import InputBase from "@mui/material/InputBase";
import IconButton from "@mui/material/IconButton";
import ButtonBase from "@mui/material/ButtonBase";
import Typography from "@mui/material/Typography";
import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import FormControl from "@mui/material/FormControl";
import Avatar from "@mui/material/Avatar";
import {useTheme} from "@mui/material/styles";
import Menu from "@mui/material/Menu";

interface MenuOption {
  id?: string;
  name?: string;
  fullName?: string;
  logo?: string;
  color?: string;
}

interface DashboardTableFiltersButtonProps {
  name?: string;
  options?: MenuOption[];
  filterQueryStringNamings: string;
}

const DashboardTableFiltersButton: React.FC<
  DashboardTableFiltersButtonProps
> = ({name, options, filterQueryStringNamings}) => {
  const {t} = useTranslation();
  const navigate = useNavigate();

  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const [selectedOptions, setSelectedOptions] = useState<string[]>([]);
  const [searchText, setSearchText] = useState<string>("");
  const [optionsArr, setOptionsArr] = useState<MenuOption[] | undefined>(
    options,
  );
  const [isSelected, setIsSelected] = useState<boolean>(false);

  useEffect(() => {
    const newOptions = options?.filter((item) => {
      if (filterQueryStringNamings === "idFinMan") {
        return item.fullName
          ? item.fullName.toLowerCase().includes(searchText.toLowerCase())
          : false;
      }
      if (filterQueryStringNamings === "stage") {
        return t(`stages.${item.name}`)
          .toLowerCase()
          .includes(searchText.toLowerCase());
      }
      return item.name
        ? item.name.toLowerCase().includes(searchText.toLowerCase())
        : false;
    });
    setOptionsArr(newOptions);
  }, [searchText, options, filterQueryStringNamings, t]);

  const handleMenuItemClick = (
    event: React.ChangeEvent<object>,
    checked: boolean,
  ) => {
    const {name: checkboxName} = event.target as HTMLInputElement;
    if (!checked) {
      return setSelectedOptions((prevState) => [
        ...prevState.filter((el) => el !== checkboxName),
      ]);
    }
    setSelectedOptions((prevState) => [...prevState, checkboxName]);
  };

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  const onFilterChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchText(event.target.value);
  };

  const checkIsSelected = (option: MenuOption) => {
    if (filterQueryStringNamings === "idFinMan") {
      return selectedOptions.some((el) => el === option.id);
    }
    return selectedOptions.some((el) => el === option.name);
  };
  const theme = useTheme();

  const logoOrColor = (option: MenuOption) => {
    if (typeof option?.logo === "string") {
      return (
        <Avatar
          style={{
            height: "22px",
            width: "22px",
            marginRight: "10px",
          }}
          alt={option.logo}
          src={option.logo}>
          {option?.name?.slice(0, 1)}
        </Avatar>
      );
    }
    if (typeof option?.color === "string") {
      return (
        <span
          style={{
            borderRadius: "50%",
            backgroundColor: option.color,
            border: "2px solid #fff",
            height: "14px",
            width: "14px",
            marginRight: "10px",
            boxShadow: "0px 2px 6px rgba(203, 205, 217, 0.4)",
          }}
        />
      );
    }
    return;
  };
  const location = useLocation();
  const handleFilter = () => {
    const newQueryRes = queryArrayFromString(location.search).filter(
      (i) => !i.includes(filterQueryStringNamings),
    );

    if (
      location.search.includes(filterQueryStringNamings) &&
      !selectedOptions.length
    ) {
      navigate({
        pathname: location.pathname,
        search: `${newQueryRes.join("&")}`,
      });
    } else {
      navigate({
        pathname: location.pathname,
        search: `${newQueryRes.join(
          "&",
        )}&${filterQueryStringNamings}=${selectedOptions.join(",")}`,
      });
    }

    setIsSelected(!!selectedOptions.length);
    setAnchorEl(null);
  };

  return (
    <Box
      sx={{
        [theme.breakpoints.down("sm")]: {
          marginTop: "20px",
        },
      }}>
      <Button
        sx={{
          backgroundColor: isSelected ? theme.palette.primary.main : "#F6F8FA",
          color: isSelected ? theme.palette.common.white : "#98A2B7",
          boxShadow: "none",
          padding: "4px 8px",
          marginRight: "20px",
          "&:hover": {
            backgroundColor: isSelected
              ? theme.palette.primary.main
              : "#F6F8FA",
          },
        }}
        endIcon={<ArrowDropDown />}
        onClick={handleClick}>
        {name}
      </Button>
      <Menu
        anchorEl={anchorEl}
        variant={"selectedMenu"}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
        sx={{
          p: 0,
          boxShadow: "0px 4px 20px rgba(203, 205, 217, 0.3)",
        }}>
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            pt: 3.8,
            minWidth: 270,
            outline: "none",
          }}>
          <Box
            sx={{
              position: "relative",
              display: "flex",
              mx: 3,
            }}>
            <InputBase
              placeholder={t("search")}
              sx={{
                backgroundColor: "#F6F8FA",
                borderRadius: theme.shape.borderRadius,
                padding: "5px 16px",
                marginRight: "10px",
                flex: "1 1 100%",
                [theme.breakpoints.down("sm")]: {
                  width: "90%",
                },
              }}
              style={{
                marginRight: 0,
                paddingRight: "42px",
              }}
              value={searchText}
              onChange={onFilterChange}
            />
            <IconButton
              type="submit"
              sx={{
                padding: "9px",
                backgroundColor: "#F6F8FA",
                borderRadius: theme.shape.borderRadius,
              }}
              style={{
                position: "absolute",
                right: 0,
                backgroundColor: "transparent",
              }}
              size="large">
              <SearchIcon color="primary" />
            </IconButton>
          </Box>
          <FormControl
            component="div"
            sx={{
              margin: "10px 0",
              maxHeight: "245px",
              height: "100%",
              width: "100%",
              overflowY: "auto",
              border: "none",
              background: "#F6F8FA",
            }}>
            <FormGroup>
              {optionsArr?.map((option) => (
                <FormControlLabel
                  key={
                    filterQueryStringNamings === "idFinMan"
                      ? option.id
                      : option.name
                  }
                  style={{
                    padding: "0 20px",
                    marginRight: 0,
                    backgroundColor: checkIsSelected(option)
                      ? "#F6F8FA"
                      : "#fff",
                  }}
                  checked={checkIsSelected(option)}
                  control={
                    <Checkbox
                      color="default"
                      icon={<CheckboxIcon />}
                      checkedIcon={<CheckboxCheckedIcon />}
                      onChange={handleMenuItemClick}
                      name={
                        filterQueryStringNamings === "idFinMan"
                          ? option.id
                          : option.name
                      }
                    />
                  }
                  label={
                    <Box
                      sx={{
                        display: "flex",
                        alignItems: "center",
                        ml: 1,
                      }}>
                      {logoOrColor(option)}
                      <Typography
                        style={{
                          textTransform: "capitalize",
                        }}>
                        {filterQueryStringNamings === "stage" ||
                        filterQueryStringNamings === "depStatus"
                          ? t(`stages.${option.name}`).toLowerCase()
                          : filterQueryStringNamings === "idFinMan"
                          ? option?.fullName?.toLowerCase()
                          : option?.name?.toLowerCase()}
                      </Typography>
                    </Box>
                  }
                />
              ))}
            </FormGroup>
          </FormControl>
          <ButtonBase
            sx={{
              padding: "13px",
              width: "100%",
              backgroundColor: theme.palette.primary.main,
              color: theme.palette.common.white,
              fontWeight: 600,
              textTransform: "uppercase",
            }}
            onClick={handleFilter}>
            {t(`buttonNames.filter`)}
          </ButtonBase>
        </Box>
      </Menu>
    </Box>
  );
};

export default DashboardTableFiltersButton;
