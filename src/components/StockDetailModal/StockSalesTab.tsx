import React, {useEffect, useState, useMemo} from "react";
import * as yup from "yup";
import {Formik} from "formik";
import Box from "@mui/material/Box";
import Checkbox from "@mui/material/Checkbox";
import FormControlLabel from "@mui/material/FormControlLabel";
import Grid from "@mui/material/Grid";
import MenuItem from "@mui/material/MenuItem";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";
import {LoadingButton} from "@mui/lab";
import {useTranslation} from "react-i18next";
import { ReactComponent as CheckboxIcon } from "src/assets/icons/CheckboxIcon.svg";
import { ReactComponent as CheckboxCheckedIcon } from "src/assets/icons/CheckboxCheckedIcon.svg";
import CartagDatePicker from "../CartagDatePicker";
import StockSalesTabTable from "./StockSalesTabTable";
import CartagInputField from "../CartagInputField";
import {AccessoriesType, FinanceTabType, CarDealerSeller} from "src/types";
import StockUploadFileButton from "./StockUploadFileButton";
import {useFileUpload} from "src/utils/useFileUpload";
import {TelephoneMaskComponent} from "../TelephoneMaskComponent";
import CartagSelectField from "../CartagSelectField";
import useCustomInfoSnackbar from "src/utils/useCustomInfoSnackbar";
import {useDispatch} from "react-redux";
import {useTabsStyles} from "./StockDeliveryTab";
import {DocumentDisplayWithDownloadFeature} from "../DocumentDisplay";
import {InputBaseComponentProps} from "@mui/material/InputBase";
import {useAppSelector} from "src/state/hooks";
import {
  useGetCarDealerSellersQuery,
  useGetCarDealerQuery,
} from 'src/state/reducers/carDealerApi';

interface InitialDataTypes {
  client: string;
  email: string;
  telephone: string;
  seller: string;
  manager: string;
  saleDate: Date | null;
}

const INITIAL_DATA = {
  client: "",
  email: "",
  telephone: "",
  seller: "",
  manager: "",
  saleDate: null,
};

const validationSchema = yup.object().shape({
  client: yup.string().required("Client is Required"),
  email: yup.string().email().required("Email is Required"),
  telephone: yup
    .string()
    .max(12, "max length 10 symbols")
    .required("Telephone is Required"),
  seller: yup.string().required("Seller is Required"),
  manager: yup.string().required("Manager is Required"),
  saleDate: yup.date().nullable().required("saleDate is Required"),
});

interface SalesTabProps {
  currentDeal: FinanceTabType | null;
  revalidateInventory: () => void;
}

const StockSalesTab: React.FC<SalesTabProps> = ({
  currentDeal,
  revalidateInventory,
}) => {
  const classes = useTabsStyles();
  const {t} = useTranslation();
  const dispatch = useDispatch();

  const enqueueInfoSnackbar = useCustomInfoSnackbar();

  const {data:sellersAll} = useGetCarDealerSellersQuery();
  const {data:managers} = useGetCarDealerSellersQuery();
  const {data:carDealer} = useGetCarDealerQuery();

  const sellers = useMemo(() => {
    return (sellersAll || []).filter((el: CarDealerSeller) =>
      el.authorities?.includes("ROLE_FINANCE_MANAGER"),
    );
  }, [sellersAll]);

  const [legendsIds, setLegendsIds] = useState<string[]>([]);
  const [accessories, setAccessories] = useState<AccessoriesType[]>([]);

  const [inputValues, setInputValues] =
    useState<InitialDataTypes>(INITIAL_DATA);

  const {documents, isLoading, onUploadFile, onDeleteFromList} =
    useFileUpload();

  useEffect(() => {
    dispatch(getCarDealer(currentDeal?.idCarDealer));
  }, [currentDeal, dispatch]);

  useEffect(() => {
    if (currentDeal) {
      currentDeal.files?.forEach((f) => {
        // onAddExistingToList(f);
      });
      if (!!currentDeal.sales?.length) {
        setAccessories(currentDeal.sales[0].accessories ?? []);
        setLegendsIds(currentDeal.sales[0].legends ?? []);
        setInputValues({
          client: currentDeal.sales[0].clientInfo?.fullName ?? "",
          email: currentDeal.sales[0].clientInfo?.email ?? "",
          telephone: currentDeal.sales[0].clientInfo?.telNumber1 ?? "",
          seller: currentDeal.sales[0].sellerInfo?.nameSeller ?? "",
          manager: currentDeal.sales[0].creditInfo?.nameFinanceManager ?? "",
          saleDate: currentDeal.sales[0]?.saleDate ?? null,
        });
      }
    }
  }, [currentDeal]);

  const onLegendsClick = (
    event: React.ChangeEvent<HTMLInputElement>,
    checked: boolean,
  ) => {
    const {name} = event.target;
    if (checked) {
      setLegendsIds((prevState) => [
        ...Array.from(new Set([...prevState, name])),
      ]);
    } else {
      setLegendsIds((prevState) => [...prevState.filter((l) => l !== name)]);
    }
  };

  return (
    <Paper elevation={0} className={classes.tabPaper}>
      <Box
        display="flex"
        alignItems="center"
        justifyContent="space-between"
        style={{
          padding: "20px 40px",
          backgroundColor: "#F6F8FA",
        }}
      >
        <Typography
          variant="h6"
          style={{
            fontSize: 18,
            fontWeight: 600,
          }}
        >
          {t("actions.CREATE_DEAL")}
        </Typography>
      </Box>

      <Grid
        container
        style={{
          padding: "0 20px",
        }}
      >
        <Typography
          variant="h6"
          style={{
            fontSize: 18,
            fontWeight: 600,
            margin: "30px 0",
            width: "100%",
          }}
        >
          {t("General")}
        </Typography>

        <Grid item xs={12} sm={6} className={classes.gridSpacing}>
          <FormControlLabel
            label={t("Stock not available")}
            checked={currentDeal?.inventoryType === "COMING"}
            disabled
            control={
              <Checkbox
                icon={<CheckboxIcon />}
                checkedIcon={<CheckboxCheckedIcon />}
                name="stockNotAvailable"
                color="primary"
              />
            }
          />
        </Grid>

        <Grid item xs={12} sm={6} className={classes.gridSpacing}>
          <FormControlLabel
            label={t("newCar")}
            checked={currentDeal?.generalInfo?.newCar}
            value={currentDeal?.generalInfo?.newCar}
            disabled
            control={
              <Checkbox
                icon={<CheckboxIcon />}
                checkedIcon={<CheckboxCheckedIcon />}
                name="newCar"
                color="primary"
              />
            }
          />
        </Grid>

        <Grid item xs={12} sm={6} className={classes.gridSpacing}>
          <CartagInputField
            labelText={t("DashboardTableHead.cle")}
            name="cle"
            value={currentDeal?.generalInfo?.numCle}
            disabled
          />
        </Grid>
        <Grid item xs={12} sm={6} className={classes.gridSpacing}>
          <CartagInputField
            labelText={t("DashboardTableHead.make")}
            name="make"
            value={currentDeal?.generalInfo?.make}
            disabled
          />
        </Grid>

        <Grid item xs={12} sm={6} className={classes.gridSpacing}>
          <CartagInputField
            labelText={t("DashboardTableHead.model")}
            name="model"
            value={currentDeal?.generalInfo?.model}
            disabled
          />
        </Grid>
      </Grid>

      <Formik
        initialValues={inputValues}
        enableReinitialize
        validationSchema={validationSchema}
        onSubmit={async (values, {setSubmitting}) => {
          if (currentDeal?.id) {
            const currentManager = managers?.find(
              (m) => m.fullName === values.manager,
            );
            const currentSeller = sellers?.find(
              (m) => m.fullName === values.seller,
            );
            const data = {
              action: "CREATE_DEAL",
              idUser: "idUser1",
              userName: "Name User",
              idCarDealer: carDealer?.id,
              sales: [
                {
                  clientInfo: {
                    fullName: values.client,
                    email: values.email,
                    telNumber1: values.telephone,
                  },
                  creditInfo: {
                    idFinanceManager: currentManager?.id,
                    nameFinanceManager: currentManager?.fullName,
                  },
                  saleDate: values.saleDate,
                  sellerInfo: {
                    idSeller: currentSeller?.id,
                    nameSeller: currentSeller?.fullName,
                  },
                  accessories,
                  legends: legendsIds,
                },
              ],
              files: documents,
            };
            console.log("submit", data);
            try {
              enqueueInfoSnackbar("Successfully updated");
            } catch (err) {
              console.log(err);
            } finally {
              revalidateInventory();
              setSubmitting(false);
            }
          }
        }}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          setFieldValue,
          isSubmitting,
          /* and other goodies */
        }) => (
          <form
            onSubmit={handleSubmit}
            style={{
              width: "100%",
            }}
          >
            <Grid
              container
              style={{
                padding: "0 20px",
              }}
            >
              <Typography variant="h6" className={classes.salesSubTitle}>
                {t("Client Information")}
              </Typography>

              <Grid item xs={12} sm={6} className={classes.gridSpacing}>
                <CartagInputField
                  labelText={t("DashboardTableHead.Client")}
                  name="client"
                  error={!!touched.client && !!errors.client}
                  helperText={errors.client}
                  value={values.client}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
              </Grid>
              <Grid item xs={12} sm={6} className={classes.gridSpacing}>
                <CartagInputField
                  labelText={t("Email")}
                  name="email"
                  type="email"
                  error={!!touched.email && !!errors.email}
                  helperText={errors.email}
                  value={values.email}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
              </Grid>

              <Grid item xs={12} sm={6} className={classes.gridSpacing}>
                <CartagInputField
                  labelText={t("Telephone")}
                  name="telephone"
                  type="tel"
                  InputProps={{
                    inputComponent:
                      TelephoneMaskComponent as React.ElementType<InputBaseComponentProps>,
                  }}
                  error={!!touched.telephone && !!errors.telephone}
                  helperText={errors.telephone}
                  value={values.telephone}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
              </Grid>

              <Typography variant="h6" className={classes.salesSubTitle}>
                {t("Sale information")}
              </Typography>

              {sellers && (
                <Grid item xs={12} sm={6} className={classes.gridSpacing}>
                  <CartagSelectField
                    labelText={t("Seller")}
                    name="seller"
                    error={!!touched.seller && !!errors.seller}
                    helperText={errors.seller}
                    value={values.seller}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  >
                    {sellers.map(({fullName}) => (
                      <MenuItem key={fullName} value={fullName}>
                        {fullName}
                      </MenuItem>
                    ))}
                  </CartagSelectField>
                </Grid>
              )}
              {managers && (
                <Grid item xs={12} sm={6} className={classes.gridSpacing}>
                  <CartagSelectField
                    labelText={t("Finance Manager")}
                    name="manager"
                    error={!!touched.manager && !!errors.manager}
                    helperText={errors.manager}
                    value={values.manager}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  >
                    {managers.map(({fullName}) => (
                      <MenuItem key={fullName} value={fullName}>
                        {fullName}
                      </MenuItem>
                    ))}
                  </CartagSelectField>
                </Grid>
              )}

              <Grid item xs={12} sm={6} className={classes.gridSpacing}>
                <Typography className={classes.inputLabel}>
                  {t("Sale Date")}
                </Typography>
                <CartagDatePicker
                  name="saleDate"
                  valueDate={values.saleDate}
                  error={!!touched.saleDate && !!errors.saleDate}
                  helperText={errors.saleDate}
                  onChangeDate={(value) => setFieldValue("saleDate", value)}
                />
              </Grid>

              <Typography variant="h6" className={classes.salesSubTitle}>
                {t("Accessories")}
              </Typography>

              <Box
                sx={{
                  width: "100%",
                  ml: 3,
                  mb: 3,
                }}
              >
                <StockUploadFileButton
                  accept={"application/pdf"}
                  title="Accessories"
                  loading={isLoading}
                  onFileSelectChanged={(event) =>
                    onUploadFile(event, "ACCESSORIES")
                  }
                />
              </Box>

              {!!documents.length &&
                documents.map((el) => {
                  return (
                    <Grid
                      key={el.id}
                      item
                      xs={5}
                      className={classes.gridSpacing}
                    >
                      <DocumentDisplayWithDownloadFeature
                        documentInfo={el}
                        isLoading={false}
                        hasPendingFile={false}
                        onDocumentAction={() => onDeleteFromList([el.id])}
                      />
                    </Grid>
                  );
                })}

              <Typography variant="h6" className={classes.salesSubTitle}>
                {t("List of accessories")}
              </Typography>

              <Grid item xs={12} className={classes.gridSpacing}>
                <StockSalesTabTable
                  groupAccessories={carDealer?.groupAccessories}
                  selected={accessories}
                  setSelected={setAccessories}
                />
              </Grid>

              <Typography variant="h6" className={classes.salesSubTitle}>
                {t("Identification")}
              </Typography>

              <Grid item xs={12} className={classes.gridSpacing}>
                {carDealer &&
                  carDealer.legends.map(
                    ({colorLegend, labelLegend, idLegend}) => {
                      return (
                        <FormControlLabel
                          key={idLegend}
                          style={{
                            marginRight: 20,
                          }}
                          name={idLegend}
                          checked={legendsIds.includes(idLegend)}
                          label={
                            <Box
                              sx={{
                                display: "flex",
                                alignItems: "center",
                              }}
                            >
                              <span
                                style={{
                                  height: 10,
                                  width: 10,
                                  backgroundColor: colorLegend,
                                  borderRadius: "50%",
                                  marginRight: 10,
                                }}
                              />{" "}
                              {labelLegend}
                            </Box>
                          }
                          control={
                            <Checkbox
                              icon={<CheckboxIcon />}
                              checkedIcon={<CheckboxCheckedIcon />}
                              onChange={onLegendsClick}
                              color="primary"
                            />
                          }
                        />
                      );
                    },
                  )}
              </Grid>
            </Grid>

            <div
              style={{
                width: "100%",
              }}
              className={classes.submitButtonWrapper}
            >
              <LoadingButton
                type="submit"
                variant="contained"
                color="primary"
                disabled={isSubmitting}
                className={classes.submitButton}
              >
                {t("actions.CREATE_DEAL")}
              </LoadingButton>
            </div>
          </form>
        )}
      </Formik>
    </Paper>
  );
};

export default StockSalesTab;
