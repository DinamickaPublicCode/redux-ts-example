import {
  DocumentEntryData,
  DocumentFileTypes,
} from "../../types/documentBoxInterfaces";
import {useTranslation} from "react-i18next";
import {useDocumentBoxActions} from "./DocumentBoxProvider";
import React, {useState} from "react";
import StockUploadFileButton from "../StockDetailModal/StockUploadFileButton";
import {makeStyles} from "@mui/styles";
import {Theme} from "@mui/material/styles";
import DocumentDisplay from "../DocumentDisplay";

const DocumentView = ({document}: {document: DocumentEntryData}) => {
  const styles = useDocumentsBoxStyles();
  const {t} = useTranslation();
  const {onDownload, onUploadFile, onDeleteButtonClicked} =
    useDocumentBoxActions();

  const [isLoading, setIsLoading] = useState(false);

  const fileToDisplay = document.file;

  return (
    <div className={styles.documentView}>
      <StockUploadFileButton
        typeId={document.type}
        accept={
          document.type === DocumentFileTypes.PHOTO
            ? "image/png,image/jpg"
            : "image/png,image/jpg,application/pdf"
        }
        onFileSelectChanged={(event) => {
          onUploadFile(
            {
              id: document.file?.id || "",
              type: document.type,
              name: document.file?.name || "",
            },
            event,
            setIsLoading,
            () => {},
          );
        }}
        title={t(document.localeFriendlyTypeString)}
      />
      {fileToDisplay && (
        <DocumentDisplay
          isLoading={isLoading}
          filename={fileToDisplay.name}
          imageSrc={fileToDisplay.isImage ? fileToDisplay.url : undefined}
          onDocumentAction={() => {
            onDeleteButtonClicked(
              {
                id: fileToDisplay?.id,
                type: document.type,
                name: document.file?.name || "",
              },
              setIsLoading,
            );
          }}
          onDownload={() =>
            onDownload(
              {
                id: fileToDisplay?.id,
                type: document.type,
                name: document.file?.name || "",
              },
              setIsLoading,
            )
          }
        />
      )}
    </div>
  );
};

const useDocumentsBoxStyles = makeStyles((theme: Theme) => ({
  documentView: {
    marginBottom: 30,
    "&:last-child": {
      marginBottom: 24,
    },
  },
}));

export default DocumentView;
