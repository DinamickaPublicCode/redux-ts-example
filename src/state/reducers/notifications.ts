import {Reducer} from "redux";
import {
  ReduxAction,
  NotificationsState,
  Notification,
  PagedNotifications,
  NotificationsActionTypes,
} from "src/types";
import {action} from "src/utils/actionHelper";
import {filterNotifications} from "src/utils/notificationsFilter";

export const SET_NOTIFICATIONS = "SET_NOTIFICATIONS";
export const SET_NOTIFICATIONS_SWITCH_CHECK = "SET_NOTIFICATIONS_SWITCH_CHECK";
export const SET_NOTIFICATIONS_ARCHIVED_SWITCH_CHECK =
  "SET_NOTIFICATIONS_ARCHIVED_SWITCH_CHECK";
export const SET_NOTIFICATIONS_PAGE = "SET_NOTIFICATIONS_PAGE";
export const SET_NOTIFICATIONS_PER_PAGE = "SET_NOTIFICATIONS_PER_PAGE";
export const SET_NOTIFICATIONS_FILTERS = "SET_NOTIFICATIONS_FILTERS";
export const SET_NOTIFICATION_STATUS_READ = "SET_NOTIFICATION_STATUS_READ";

const initialState: NotificationsState = {
  notifications: [],
  notificationsAll: [],
  pagesCount: 0,
  notificationsCount: 0,
  error: null,
  isValidating: false,
  page: 0,
  perPage: 0,
  activeFilters: [],
  switchCheck: "ALL",
  switchCheckArchived: false,
};

const notificationsReducer: Reducer<
  NotificationsState,
  ReduxAction<NotificationsActionTypes>
> = (state = initialState, action) => {
  switch (action.type) {
    case SET_NOTIFICATIONS:
      return {
        ...state,
        notificationsAll: action.payload?.content ?? [],
        notifications: filterNotifications(
          action.payload?.content ?? [],
          state.switchCheck,
          state.switchCheckArchived,
          state.activeFilters,
        ),
        pagesCount: action.payload?.totalPages ?? 0,
        notificationsCount:
          action.payload?.content?.filter(
            (x: Notification) => x.status === "DELIVERED",
          ).length ?? 0,
      };
    case SET_NOTIFICATIONS_SWITCH_CHECK:
      return {
        ...state,
        switchCheck: action.payload,
        notifications: filterNotifications(
          state.notificationsAll,
          action.payload,
          state.switchCheckArchived,
          state.activeFilters,
        ),
      };
    case SET_NOTIFICATIONS_ARCHIVED_SWITCH_CHECK:
      return {
        ...state,
        switchCheckArchived: action.payload,
        notifications: filterNotifications(
          state.notificationsAll,
          state.switchCheck,
          action.payload,
          state.activeFilters,
        ),
      };
    case SET_NOTIFICATIONS_PAGE:
      return {
        ...state,
        page: action.payload,
      };
    case SET_NOTIFICATIONS_PER_PAGE:
      return {
        ...state,
        perPage: action.payload,
      };
    case SET_NOTIFICATIONS_FILTERS:
      return {
        ...state,
        activeFilters: action.payload,
        notifications: filterNotifications(
          state.notificationsAll,
          state.switchCheck,
          state.switchCheckArchived,
          action.payload,
        ),
      };
    case SET_NOTIFICATION_STATUS_READ:
      return {
        ...state,
        notifications: state.notifications?.map((x: Notification) =>
          x.id === action.payload
            ? {
                ...x,
                status: "READ",
              }
            : x,
        ),
      };
    default:
      return state;
  }
};

export const setNotifications = (payload: PagedNotifications) =>
  action(SET_NOTIFICATIONS, payload);
export const setNotificationSwitchCheck = (payload: "DELIVERED" | "ALL") =>
  action(SET_NOTIFICATIONS_SWITCH_CHECK, payload);
export const setNotificationArchivedSwitchCheck = (payload: boolean) =>
  action(SET_NOTIFICATIONS_ARCHIVED_SWITCH_CHECK, payload);
export const setNotificationsPage = (payload: number) =>
  action(SET_NOTIFICATIONS_PAGE, payload);
export const setNotificationsPerPage = (payload: number) =>
  action(SET_NOTIFICATIONS_PER_PAGE, payload);
export const setActiveNotificationFilters = (payload: NotificationType[]) =>
  action(SET_NOTIFICATIONS_FILTERS, payload);
export const setNotificationStatusRead = (payload: string) =>
  action(SET_NOTIFICATION_STATUS_READ, payload);


export default notificationsReducer;
