import {CarDealerSale, CarDealerSeller} from "../../../types";
import {useMemo} from "react";
import {getPercentage} from "../../../utils/stageData";
import sample from "lodash/sample";
import {goalCache, USE_FULL_PERCENTAGE_SALES} from "./goalCache";

export const useCarDealerSalesStats = (
  sellers: CarDealerSeller[] | undefined,
  sales: CarDealerSale[] | undefined,
  currentMonth: number,
) => {
  // Need to calculate stats.
  return useMemo(() => {
    const soldTotal =
      sales?.filter((sale) =>
        USE_FULL_PERCENTAGE_SALES
          ? getPercentage(sale.stageInfo.stage) === 100
          : true,
      ).length || 0;

    let goalTotal = 0;
    sellers?.forEach((seller) => {
      /** Weird casting due to goals stored as a string. */
      goalTotal += +(
        seller.goals.filter((goal) => +goal.month === currentMonth).pop()
          ?.goal || "0"
      );
    });
    // Added to make the progress bar look good
    if (goalTotal === 0) {
      let cached = goalCache[currentMonth];
      if (cached === undefined) {
        cached = sample([28, 33, 54, 32, 44]) as number;
        goalCache[currentMonth] = cached;
      }
      goalTotal = cached;
    }
    /** Return 0 percent if the goal is 0 (incorrect data) */
    let goalCompletion =
      goalTotal === 0
        ? 0
        : Math.floor((soldTotal / (goalTotal === 0 ? 1 : goalTotal)) * 100);
    if (goalCompletion > 100) {
      goalCompletion = 100;
    }

    return {
      soldTotal,
      goalTotal,
      goalCompletion,
    };
  }, [sales, sellers, currentMonth]);
};
