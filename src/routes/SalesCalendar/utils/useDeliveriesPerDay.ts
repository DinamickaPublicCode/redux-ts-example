import {PagedCarDealerSale} from "../../../types";
import {useMemo} from "react";
import dayjs from "dayjs";

export const useDeliveriesPerDay = (
  currentDate: Date,
  deliveries: PagedCarDealerSale | undefined,
) =>
  useMemo(() => {
    return Array.from(
      {
        length: dayjs(currentDate).daysInMonth(),
      },
      (x, i) => {
        const day = dayjs(currentDate).startOf("month").add(i, "days");
        const deliveriesByDay = deliveries
          ? deliveries?.content.filter((el) =>
              dayjs(el.sales[0].saleDate).isSame(day, "day"),
            )
          : [];
        return {
          day,
          deliveries: deliveriesByDay,
        };
      },
    );
  }, [currentDate, deliveries]);
