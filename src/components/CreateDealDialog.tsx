import React, {useEffect, useState, useMemo} from "react";
import Box from "@mui/material/Box";
import Checkbox from "@mui/material/Checkbox";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import FormControlLabel from "@mui/material/FormControlLabel";
import Grid from "@mui/material/Grid";
import IconButton from "@mui/material/IconButton";
import MenuItem from "@mui/material/MenuItem";
import Typography from "@mui/material/Typography";
import Transition from "./Transition";
import CloseIcon from "@mui/icons-material/Close";
import {useTranslation} from "react-i18next";
import {Theme} from "@mui/material/styles";
import makeStyles from "@mui/styles/makeStyles";
import { ReactComponent as CheckboxIcon } from "src/assets/icons/CheckboxIcon.svg";
import { ReactComponent as CheckboxCheckedIcon } from "src/assets/icons/CheckboxCheckedIcon.svg";
import CartagInputField from "./CartagInputField";
import {useFormik} from "formik";
import mockStock from "../utils/mockStockForAutocomplete";
import {searchStockList} from "src/state/api";
import {TelephoneMaskComponent} from "./TelephoneMaskComponent";
import CartagSelectField from "./CartagSelectField";
import CartagDatePicker from "./CartagDatePicker";
import StockUploadFileButton from "./StockDetailModal/StockUploadFileButton";
import StockSalesTabTable from "./StockDetailModal/StockSalesTabTable";
import {LoadingButton} from "@mui/lab";
import {AccessoriesType, FinanceTabType} from "src/types";
import {useFileUpload} from "src/utils/useFileUpload";
import * as yup from "yup";
import {CarDealerSeller} from "src/types";
import CartagAutocomplete from "./CartagAutocomplete";
import useCustomInfoSnackbar from "src/utils/useCustomInfoSnackbar";
import {DocumentDisplayWithDownloadFeature} from "./DocumentDisplay";
import StockCarInfoReadiness from "./StockCarInfoReadiness";
import {InputBaseComponentProps} from "@mui/material/InputBase";
import {useAppSelector} from "src/state/hooks";
import {useGetUserQuery} from 'src/state/reducers/userApi';
import {
  useGetCarDealerSellersQuery,
  useGetCarDealerQuery,
} from 'src/state/reducers/carDealerApi'

export const useStyles = makeStyles((theme: Theme) => ({
  dialogPaper: {
    width: "100%",
  },
  dialogTitle: {
    padding: "20px 35px",
    marginBottom: 30,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    borderBottom: "1px solid #F6F8FA",
  },
  titleText: {
    fontWeight: 600,
    fontSize: 22,
    color: "#222238",
  },
  gridSpacing: {
    padding: "0 20px 30px",
  },
  salesSubTitle: {
    fontSize: 18,
    fontWeight: 600,
    margin: "30px 0",
    width: "100%",
  },
  inputLabel: {
    fontSize: 14,
    color: "#98A2B7",
  },
  submitButtonWrapper: {
    display: "flex",
    justifyContent: "flex-end",
    backgroundColor: "#F6F8FA",
    padding: "25px 32px",
  },
  submitButton: {
    padding: "8px 30px",
    textTransform: "uppercase",
    fontSize: 14,
    fontWeight: 600,
  },
  input: {
    backgroundColor: "#F6F8FA",
    "&:hover .MuiOutlinedInput-notchedOutline, & .MuiOutlinedInput-notchedOutline":
      {
        borderColor: "rgba(0, 0, 0, 0);",
      },
    "&.Mui-focused .MuiOutlinedInput-notchedOutline": {
      borderColor: theme.palette.primary.main,
      borderWidth: 2,
    },
    "&.Mui-error .MuiOutlinedInput-notchedOutline": {
      borderColor: theme.palette.error.main,
      borderWidth: 2,
    },
  },
  grayUppercaseText: {
    fontWeight: 600,
    fontSize: 13,
    textTransform: "uppercase",
    color: "#98A2B7",
  },
}));

const INITIAL_DATA = {
  client: "",
  email: "",
  telephone: "",
  seller: "",
  manager: "",
  saleDate: null,
};

const validationSchema = yup.object().shape({
  client: yup.string().required("Client is Required"),
  email: yup.string().email().required("Email is Required"),
  telephone: yup
    .string()
    .max(12, "max length 10 symbols")
    .required("Telephone is Required"),
  seller: yup.string().required("Seller is Required"),
  manager: yup.string().required("Manager is Required"),
  saleDate: yup.date().nullable().required("saleDate is Required"),
});

interface CreateDealDialogProps {
  open: boolean;
  handleClose: () => void;
}

const CreateDealDialog: React.FC<CreateDealDialogProps> = ({
  open,
  handleClose,
}) => {
  const classes = useStyles();
  const {t} = useTranslation();
  const enqueueInfoSnackbar = useCustomInfoSnackbar();

  const {data: user} = useGetUserQuery();
  const {data:carDealer}= useGetCarDealerQuery();

  const [legendsIds, setLegendsIds] = useState<string[]>([]);
  const [accessories, setAccessories] = useState<AccessoriesType[]>([]);

  const [stockSearchValue, setStockSearchValue] = useState<string>("");
  useEffect(() => {
    onStockValueChange("L0954");
    setDisabledValues({
      cle: "H DISPONIBLE",
      make: "HONDA",
      model: "CIVIC SDN LX",
    });
    setCurrentStock(mockStock);
  }, [open]);
  const [currentStock, setCurrentStock] = useState<FinanceTabType | null>(null);
  const [stockResults, setStockResults] = useState<FinanceTabType[]>([]);
  const [loadingStock, setLoadingStock] = useState<boolean>(false);

  const [disabledValues, setDisabledValues] = useState({
    cle: "",
    make: "",
    model: "",
  });

  const {data:sellersAll} = useGetCarDealerSellersQuery();

  const sellers = useMemo(() => {
    return (sellersAll || []).filter((el: CarDealerSeller) =>
      el.authorities?.includes(
        currentStock?.generalInfo.newCar
          ? "ROLE_SELLER_NEW"
          : "ROLE_SELLER_USED",
      ),
    );
  }, [sellersAll, currentStock]);

  const managers = useMemo(() => {
    return (sellersAll || []).filter((el: CarDealerSeller) =>
      el.authorities?.includes("ROLE_FINANCE_MANAGER"),
    );
  }, [sellersAll]);

  const {
    documents,
    isLoading,
    onUploadFile,
    onAddExistingToList,
    onDeleteFromList,
  } = useFileUpload();

  const {
    values,
    errors,
    touched,
    handleChange,
    handleBlur,
    handleSubmit,
    setFieldValue,
    resetForm,
  } = useFormik({
    initialValues: INITIAL_DATA,
    enableReinitialize: true,
    validationSchema,
    onSubmit: async (values, {setSubmitting, resetForm}) => {
      if (currentStock?.id && user?.id) {
        const currentManager = managers?.find(
          (m) => m.fullName === values.manager,
        );
        const currentSeller = sellers?.find(
          (m) => m.fullName === values.seller,
        );
        const data = {
          action: "CREATE_DEAL",
          idUser: user.id,
          userName: user.fullName,
          idCarDealer: carDealer?.id,
          sales: [
            {
              clientInfo: {
                fullName: values.client,
                email: values.email,
                telNumber1: values.telephone,
              },
              creditInfo: {
                idFinanceManager: currentManager?.id,
                nameFinanceManager: currentManager?.fullName,
              },
              saleDate: values.saleDate,
              sellerInfo: {
                idSeller: currentSeller?.id,
                nameSeller: currentSeller?.fullName,
              },
              accessories,
              legends: legendsIds,
            },
          ],
          files: documents,
        };
        console.log("submit", data);
        try {
          // await updateInventory(currentStock.id, data);
          enqueueInfoSnackbar("Successfully updated");
        } catch (err) {
          console.log(err);
        } finally {
          setCurrentStock(null);
          setStockSearchValue("");
          setLegendsIds([]);
          setAccessories([]);
          setStockResults([]);
          setDisabledValues({
            cle: "",
            make: "",
            model: "",
          });
          resetForm({
            values: INITIAL_DATA,
          });
          setSubmitting(false);
          handleClose();
        }
      }
    },
  });

  useEffect(() => {
    if (!open) {
      setCurrentStock(null);
      setStockSearchValue("");
      setLegendsIds([]);
      setAccessories([]);
      setStockResults([]);
      setDisabledValues({
        cle: "",
        make: "",
        model: "",
      });
      resetForm({
        values: INITIAL_DATA,
      });
    }
  }, [open, resetForm]);

  useEffect(() => {
    if (currentStock) {
      setDisabledValues({
        cle: currentStock?.generalInfo?.numCle,
        make: currentStock?.generalInfo?.make,
        model: currentStock?.generalInfo?.model,
      });
    }
  }, [currentStock]);

  useEffect(() => {
    (async () => {
      if (user && stockSearchValue && stockSearchValue.length > 2) {
        try {
          setLoadingStock(true);
          const res = await searchStockList(
            user.defaultCarDealer.id,
            stockSearchValue,
          );
          setStockResults(res.data.content);
        } catch (e) {
          console.log(e);
        } finally {
          setLoadingStock(false);
        }
      }
    })();
  }, [stockSearchValue, user]);

  useEffect(() => {
    if (currentStock?.id) {
      currentStock.files?.forEach((f) => {
        onAddExistingToList(f);
      });
      if (!!currentStock.sales?.length) {
        setAccessories(currentStock.sales[0].accessories ?? []);
        setLegendsIds(currentStock.sales[0].legends ?? []);
      }
    }
  }, [currentStock, user, onAddExistingToList]);

  const onStockValueChange = (value: string) => {
    setStockSearchValue(value);
  };
  const onStockObjectChange = (value: FinanceTabType | null) => {
    setCurrentStock(value);
    setStockSearchValue(value?.referenceStock as string);
  };

  const onLegendsClick = (
    event: React.ChangeEvent<HTMLInputElement>,
    checked: boolean,
  ) => {
    const {name} = event.target;
    if (checked) {
      setLegendsIds((prevState) => [
        ...Array.from(new Set([...prevState, name])),
      ]);
    } else {
      setLegendsIds((prevState) => [...prevState.filter((l) => l !== name)]);
    }
  };

  return (
    <Dialog
      scroll="body"
      open={open}
      TransitionComponent={Transition}
      keepMounted
      maxWidth={"md"}
      onClose={handleClose}
      classes={{
        paper: classes.dialogPaper,
      }}
    >
      <DialogTitle
        classes={{
          root: classes.dialogTitle,
        }}
      >
        <Typography variant="h6" className={classes.titleText}>
          {t("actions.CREATE_DEAL")}
        </Typography>
        <IconButton size="small" onClick={handleClose}>
          <CloseIcon />
        </IconButton>
      </DialogTitle>

      <Grid
        container
        style={{
          padding: "0 20px",
        }}
      >
        <Grid item xs={12} sm={6} className={classes.gridSpacing}>
          <CartagAutocomplete
            loading={loadingStock}
            options={stockResults}
            inputValue={stockSearchValue}
            stockValue={currentStock}
            labelText={t("DashboardTableHead.referenceStock")}
            name="referenceStock"
            onValueChange={onStockValueChange}
            onStockChange={onStockObjectChange}
          />
        </Grid>
        <Grid item xs={12} sm={6} className={classes.gridSpacing}>
          {currentStock?.id && (
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <Typography className={classes.grayUppercaseText}>
                {t("Item readiness")}
              </Typography>
              <Box
                sx={{
                  mt: "13px",
                }}
              >
                <StockCarInfoReadiness
                  stageType={currentStock.stageInfo.stage}
                  icons={currentStock.stageInfo.icons}
                />
              </Box>
            </Box>
          )}
        </Grid>
        <Grid item xs={12} className={classes.gridSpacing}>
          {currentStock?.id && (
            <FormControlLabel
              label={t("Stock not available")}
              checked={currentStock?.inventoryType === "COMING"}
              disabled
              control={
                <Checkbox
                  icon={<CheckboxIcon />}
                  checkedIcon={<CheckboxCheckedIcon />}
                  name="stockNotAvailable"
                  color="primary"
                />
              }
            />
          )}
        </Grid>
        <Typography
          variant="h6"
          style={{
            fontSize: 18,
            fontWeight: 600,
            margin: "30px 0",
          }}
        >
          {t("General")}
        </Typography>

        <Grid item xs={12} className={classes.gridSpacing}>
          <FormControlLabel
            label={t("newCar")}
            checked={currentStock?.generalInfo?.newCar}
            disabled
            control={
              <Checkbox
                icon={<CheckboxIcon />}
                checkedIcon={<CheckboxCheckedIcon />}
                name="newCar"
                color="primary"
              />
            }
          />
        </Grid>

        <Grid item xs={12} sm={6} className={classes.gridSpacing}>
          <CartagInputField
            labelText={t("DashboardTableHead.cle")}
            name="cle"
            value={disabledValues.cle}
            disabled
          />
        </Grid>
        <Grid item xs={12} sm={6} className={classes.gridSpacing}>
          <CartagInputField
            labelText={t("DashboardTableHead.make")}
            name="make"
            value={disabledValues.make}
            disabled
          />
        </Grid>

        <Grid item xs={12} sm={6} className={classes.gridSpacing}>
          <CartagInputField
            labelText={t("DashboardTableHead.model")}
            name="model"
            value={disabledValues.model}
            disabled
          />
        </Grid>
      </Grid>

      <form onSubmit={handleSubmit} style={{width: "100%"}}>
        <Grid
          container
          style={{
            padding: "0 20px",
          }}
        >
          <Typography variant="h6" className={classes.salesSubTitle}>
            {t("Client Information")}
          </Typography>

          <Grid item xs={12} sm={6} className={classes.gridSpacing}>
            <CartagInputField
              labelText={t("DashboardTableHead.Client")}
              name="client"
              error={!!touched.client && !!errors.client}
              helperText={errors.client}
              value={values.client}
              onChange={handleChange}
              onBlur={handleBlur}
            />
          </Grid>
          <Grid item xs={12} sm={6} className={classes.gridSpacing}>
            <CartagInputField
              labelText={t("Email")}
              name="email"
              type="email"
              error={!!touched.email && !!errors.email}
              helperText={errors.email}
              value={values.email}
              onChange={handleChange}
              onBlur={handleBlur}
            />
          </Grid>

          <Grid item xs={12} sm={6} className={classes.gridSpacing}>
            <CartagInputField
              labelText={t("Telephone")}
              name="telephone"
              type="tel"
              InputProps={{
                inputComponent:
                  TelephoneMaskComponent as unknown as React.ElementType<InputBaseComponentProps>,
              }}
              error={!!touched.telephone && !!errors.telephone}
              helperText={errors.telephone}
              value={values.telephone}
              onChange={handleChange}
              onBlur={handleBlur}
            />
          </Grid>

          <Typography variant="h6" className={classes.salesSubTitle}>
            {t("Sale information")}
          </Typography>

          {sellers && (
            <Grid item xs={12} sm={6} className={classes.gridSpacing}>
              <CartagSelectField
                labelText={t("Seller")}
                name="seller"
                error={!!touched.seller && !!errors.seller}
                helperText={errors.seller}
                value={values.seller}
                onChange={handleChange}
                onBlur={handleBlur}
              >
                {sellers.map(({fullName}) => (
                  <MenuItem key={fullName} value={fullName}>
                    {fullName}
                  </MenuItem>
                ))}
              </CartagSelectField>
            </Grid>
          )}
          {managers && (
            <Grid item xs={12} sm={6} className={classes.gridSpacing}>
              <CartagSelectField
                labelText={t("Finance Manager")}
                name="manager"
                error={!!touched.manager && !!errors.manager}
                helperText={errors.manager}
                value={values.manager}
                onChange={handleChange}
                onBlur={handleBlur}
              >
                {managers.map(({fullName}) => (
                  <MenuItem key={fullName} value={fullName}>
                    {fullName}
                  </MenuItem>
                ))}
              </CartagSelectField>
            </Grid>
          )}

          <Grid item xs={12} sm={6} className={classes.gridSpacing}>
            <Typography className={classes.inputLabel}>
              {t("Sale Date")}
            </Typography>
            <CartagDatePicker
              name="saleDate"
              valueDate={values.saleDate}
              error={!!touched.saleDate && !!errors.saleDate}
              helperText={errors.saleDate}
              onChangeDate={(value) => setFieldValue("saleDate", value)}
            />
          </Grid>

          <Typography variant="h6" className={classes.salesSubTitle}>
            {t("Accessories")}
          </Typography>

          <Box
            sx={{
              width: "100%",
              ml: 3,
              mb: 3,
            }}
          >
            {/**
             * For some unknown reason, react messes up which Upload button is which, thus doing wrong types updates
             * Having this button rendering the whole time in the DOM tree makes it breaking for the sales/accessory tab.
             * Because once the file input select is fired, it runs for the wrong react component.
             *
             * Super weird bug. Hiding this button if the modal is not visible.
             */}
            {open && (
              <StockUploadFileButton
                title="Accessories"
                loading={isLoading}
                onFileSelectChanged={(event) =>
                  onUploadFile(event, "ACCESSORIES")
                }
              />
            )}
          </Box>

          {!!documents.length &&
            documents.map((el) => {
              return (
                <Grid key={el.id} item xs={5} className={classes.gridSpacing}>
                  <DocumentDisplayWithDownloadFeature
                    documentInfo={el}
                    isLoading={isLoading}
                    hasPendingFile={false}
                    onDocumentAction={() => onDeleteFromList([el.id])}
                  />
                </Grid>
              );
            })}

          <Typography variant="h6" className={classes.salesSubTitle}>
            {t("List of accessories")}
          </Typography>

          <Grid item xs={12} className={classes.gridSpacing}>
            {open && (
              /**
               * A table with a variable number of rows shouldn't be rendered
               * when it's not visible to not trigger the nasty react 'key' errors.
               */
              <StockSalesTabTable
                groupAccessories={carDealer?.groupAccessories}
                selected={accessories}
                setSelected={setAccessories}
              />
            )}
          </Grid>

          <Typography variant="h6" className={classes.salesSubTitle}>
            {t("Identification")}
          </Typography>

          <Grid item xs={12} className={classes.gridSpacing}>
            {carDealer &&
              carDealer.legends.map(({colorLegend, labelLegend, idLegend}) => {
                return (
                  <FormControlLabel
                    key={idLegend}
                    style={{
                      marginRight: 20,
                    }}
                    name={idLegend}
                    checked={legendsIds.includes(idLegend)}
                    label={
                      <Box
                        sx={{
                          display: "flex",
                          alignItems: "center",
                        }}
                      >
                        <span
                          style={{
                            height: 10,
                            width: 10,
                            backgroundColor: colorLegend,
                            borderRadius: "50%",
                            marginRight: 10,
                          }}
                        />{" "}
                        {labelLegend}
                      </Box>
                    }
                    control={
                      <Checkbox
                        icon={<CheckboxIcon />}
                        checkedIcon={<CheckboxCheckedIcon />}
                        onChange={onLegendsClick}
                        color="primary"
                      />
                    }
                  />
                );
              })}
          </Grid>
        </Grid>

        <div
          style={{
            width: "100%",
          }}
          className={classes.submitButtonWrapper}
        >
          <LoadingButton
            type="submit"
            variant="contained"
            color="primary"
            // pending={isSubmitting}
            className={classes.submitButton}
            disabled={!currentStock?.id}
          >
            {t("actions.CREATE_DEAL")}
          </LoadingButton>
        </div>
      </form>
    </Dialog>
  );
};

export default CreateDealDialog;
