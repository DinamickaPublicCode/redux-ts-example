import usePublicUrl from "../../../../utils/usePublicUrl";
import Box from "@mui/material/Box";
import React from "react";

export const HomeStatsGreetingImage = () => {
  const publicUrl = usePublicUrl();

  return (
    <Box
      sx={{
        position: "absolute",
        top: "20px",
        left: "calc(50% - 77px)",
      }}
    >
      <Box
        sx={{
          position: "relative",
          width: "154px",
          height: "154px",
        }}
      >
        <Box
          sx={{
            background: `url(${publicUrl("images/home/image_1.png")})`,
            backgroundSize: "267px",
            backgroundPosition: "-138px ",
            transform: "scale(-1.1, 1.1)",
            border: "6px solid white",
            top: "calc(50% - 58px)",
            left: "calc(50% - 58px)",
            position: "absolute",
            width: "116px",
            height: "116px",
            borderRadius: "50%",
          }}
        />

        <svg
          width="154"
          height="154"
          viewBox="0 0 154 154"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M77.0015 154C119.535 154 154 119.525 154 76.994C154 34.4626 119.535 0 77.0015 0C34.4679 0 0 34.4746 0 76.994C0 119.513 34.4769 154 77.0015 154ZM77.0015 38.6913C84.5784 38.6913 91.9851 40.9382 98.2849 45.1479C104.585 49.3575 109.495 55.3409 112.394 62.3413C115.293 69.3416 116.052 77.0445 114.573 84.4759C113.094 91.9072 109.445 98.7331 104.087 104.09C98.7292 109.448 91.9028 113.096 84.4714 114.573C77.0399 116.051 69.3373 115.291 62.3375 112.391C55.3378 109.491 49.3553 104.579 45.1467 98.2789C40.938 91.9782 38.6923 84.571 38.6935 76.994C38.6959 66.8352 42.7328 57.0932 49.9166 49.9104C57.1004 42.7276 66.8429 38.6921 77.0015 38.6913Z"
            fill="#1807DA"
          />
        </svg>
      </Box>
    </Box>
  );
};
