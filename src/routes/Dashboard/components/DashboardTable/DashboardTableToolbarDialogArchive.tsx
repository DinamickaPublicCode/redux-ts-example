import React from "react";
import {Link} from "react-router-dom";
import MUILink from "@mui/material/Link";
import Box from "@mui/material/Box";
import CloseIcon from "@mui/icons-material/Close";
import IconButton from "@mui/material/IconButton";
import DialogTitle from "@mui/material/DialogTitle";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import Typography from "@mui/material/Typography";
import {useTheme} from "@mui/material/styles";
import Transition from "src/components/Transition";

interface DashboardTableToolbarDialogArchiveProps {
  open: boolean;
  handleClose: () => void;
  selected: string[];
}

const DashboardTableToolbarDialogArchive: React.FC<
  DashboardTableToolbarDialogArchiveProps
> = ({open, selected, handleClose}) => {
  const theme = useTheme();

  return (
    <Dialog
      open={open}
      TransitionComponent={Transition}
      keepMounted
      maxWidth={"xs"}
      onClose={handleClose}
      sx={{
        padding: "35px",
        width: "100%",
      }}
      aria-labelledby="alert-dialog-archive-title"
      aria-describedby="alert-dialog-archive-description"
    >
      <DialogTitle
        sx={{
          padding: 0,
          marginBottom: "30px",
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
        id="alert-dialog-archive-title"
      >
        <Typography variant="h6">Archive items</Typography>
        <IconButton size="small" onClick={handleClose}>
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent
        sx={{
          padding: 0,
          marginBottom: "25px",
        }}
      >
        <Box
          sx={{
            marginBottom: "25px",
            padding: "15px",
            color: theme.palette.primary.main,
            fontWeight: 500,
            fontSize: "16px",
            backgroundColor: "rgba(26, 6, 249, 0.06)",
            borderRadius: theme.shape.borderRadius,
          }}
        >
          {selected.length === 1
            ? `${selected.length} Item Selected`
            : `${selected.length} Items Selected`}
        </Box>
        <DialogContentText id="alert-dialog-archive-description">
          You can always see the archived items under{" "}
          <MUILink
            style={{
              whiteSpace: "nowrap",
            }}
            to={"/"}
            component={Link}
          >
            All inventory
          </MUILink>{" "}
          tab
        </DialogContentText>
      </DialogContent>
      <DialogActions style={{padding: 0}}>
        <Button
          style={{
            textTransform: "uppercase",
            padding: 15,
          }}
          fullWidth
          variant="contained"
          onClick={handleClose}
        >
          Submit
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default DashboardTableToolbarDialogArchive;
