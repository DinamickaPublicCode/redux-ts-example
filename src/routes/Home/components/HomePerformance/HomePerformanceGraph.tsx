import React, {useMemo, useState} from "react";
import Box from "@mui/material/Box";
import VisibilitySensor from "react-visibility-sensor";
import {LEGEND_COLORS} from "./HomePerformance";
import HomePerformanceBar from "./HomePerformanceBar";
import HomePerformanceVerticalAxis from "./HomePerformanceVerticalAxis";

const HomePerformanceGraph = ({
  data,
  labelIndexStart,
}: {
  data: number[][];
  labelIndexStart: number;
}) => {
  const legendNames = ["Mechanics", "Aesthetics"];
  const max = useMemo(() => {
    let max = 0;
    data.forEach((c) =>
      c.forEach((i) => {
        max = i > max ? i : max;
      }),
    );
    max += 10;
    return max;
  }, [data]);

  const step = Math.floor(max / 4);

  const [visible, setVisible] = useState(false);
  return (
    <VisibilitySensor
      onChange={(newVisible: boolean | ((prevState: boolean) => boolean)) => {
        if (!visible) {
          setVisible(newVisible);
        }
      }}
    >
      <Box
        sx={{
          display: "flex",
          flexDirection: "row",
          alignItems: "flex-end",
          height: "100%",
          width: "100%",
        }}
      >
        <HomePerformanceVerticalAxis divisions={5} step={step} />
        {data.map((column, columnIndex) => (
          <Box
            key={columnIndex}
            sx={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              height: "100%",
              width: "100%",
            }}
          >
            <Box
              sx={{
                display: "flex",
                alignItems: "flex-end",
                flex: 1,
              }}
            >
              {column.map((col, id) => (
                <HomePerformanceBar
                  visible={visible}
                  key={id}
                  value={col}
                  name={legendNames[id]}
                  htmlColor={LEGEND_COLORS[id]}
                  heightPercent={Math.floor((col / max) * 100)}
                />
              ))}
            </Box>
            <Box
              style={{
                marginTop: 11,
              }}
              sx={{
                textAlign: "center",
                fontStyle: "normal",
                fontWeight: "normal",
                fontSize: "14px",
                lineHeight: "18.2px",
                color: "#98A2B7",
              }}
            >
              {(columnIndex + labelIndexStart).toString().length === 1
                ? "0" + (columnIndex + labelIndexStart)
                : columnIndex + labelIndexStart}
            </Box>
          </Box>
        ))}
      </Box>
    </VisibilitySensor>
  );
};
export default HomePerformanceGraph;
