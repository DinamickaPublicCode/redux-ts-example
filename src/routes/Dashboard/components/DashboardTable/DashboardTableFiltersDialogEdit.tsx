import React, {Dispatch, SetStateAction, useEffect, useState} from "react";
import SearchIcon from "@mui/icons-material/Search";
import CloseIcon from "@mui/icons-material/Close";
import DragIndicator from "@mui/icons-material/DragIndicator";
import { ReactComponent as CheckboxWhiteBackIcon } from "src/assets/icons/CheckboxWhiteBackIcon.svg";
import { ReactComponent as CheckboxCheckedGrayBackIcon } from "src/assets/icons/CheckboxCheckedGrayBackIcon.svg";
import {
  DragDropContext,
  Droppable,
  Draggable,
  DroppableProvided,
  DroppableStateSnapshot,
  DraggableProvided,
  DraggableStateSnapshot,
  DropResult,
} from "react-beautiful-dnd";
import {TabViewsType} from "src/types";
import {colsData} from "src/utils/mockDataEditTable";
import {useTranslation} from "react-i18next";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Checkbox from "@mui/material/Checkbox";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogTitle from "@mui/material/DialogTitle";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormGroup from "@mui/material/FormGroup";
import Grid from "@mui/material/Grid";
import IconButton from "@mui/material/IconButton";
import InputBase from "@mui/material/InputBase";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";
import {useTheme} from "@mui/material/styles";
import Transition from "src/components/Transition";

interface DashboardTableFiltersDialogEditProps {
  tabName: TabViewsType;
  headCells: string[];
  setHeadCells: Dispatch<SetStateAction<string[]>>;
  open: boolean;
  handleClose: () => void;
}

const DashboardTableFiltersDialogEdit: React.FC<
  DashboardTableFiltersDialogEditProps
> = ({tabName, headCells, setHeadCells, open, handleClose}) => {
  const {t} = useTranslation();

  const [searchText, setSearchText] = useState<string>("");

  const [secondaryArr, setSecondaryArr] = useState<string[]>(
    colsData[tabName].secondaryCols,
  );
  const [secondaryArrFiltered, setSecondaryArrFiltered] = useState<string[]>(
    [],
  );
  const [primaryArr, setPrimaryArr] = useState<string[]>(headCells);

  useEffect(() => {
    if (searchText) {
      const newOptions = secondaryArr.filter((item) => {
        return item.toLowerCase().includes(searchText.toLowerCase());
      });
      setSecondaryArrFiltered(newOptions);
    } else {
      setSecondaryArrFiltered([]);
    }
  }, [searchText, secondaryArr]);

  const handleMenuItemClick = (
    event: React.ChangeEvent<object>,
    _checked: boolean,
  ) => {
    const {value} = event.target as HTMLInputElement;
    setPrimaryArr((prevState) => [...prevState, value]);
    setSecondaryArr((prevState) => [...prevState.filter((el) => el !== value)]);
    if (secondaryArrFiltered.length) {
      setSecondaryArrFiltered((prevState) => [
        ...prevState.filter((el) => el !== value),
      ]);
    }
  };
  const handleDeleteFromPrimary = (value: string) => () => {
    setPrimaryArr((prevState) => [...prevState.filter((el) => el !== value)]);
    setSecondaryArr((prevState) => [...prevState, value]);
  };

  const onFilterChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchText(event.target.value);
  };

  const reorder = (list: string[], startIndex: number, endIndex: number) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
  };
  const onDragEnd = (result: DropResult) => {
    // dropped outside the list
    if (!result.destination) {
      return;
    }

    const items = reorder(
      primaryArr,
      result.source.index,
      result.destination.index,
    );
    setPrimaryArr(items);
  };

  const handleEditTable = () => {
    setHeadCells(primaryArr);
    handleClose();
  };

  const secondArrOrFilteredArr = searchText
    ? secondaryArrFiltered
    : secondaryArr;

  const theme = useTheme();

  return (
    <Dialog
      open={open}
      TransitionComponent={Transition}
      keepMounted
      maxWidth={"md"}
      onClose={handleClose}
      // classes={{paper: classes.dialogPaper}}
      sx={{
        padding: "35px",
        width: "100%",
        maxWidth: "730px",
      }}
      aria-labelledby="alert-dialog-archive-title"
      aria-describedby="alert-dialog-archive-description"
    >
      <DialogTitle
        sx={{
          position: "relative",
          padding: 0,
          marginBottom: "30px",
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
        id="alert-dialog-archive-title"
      >
        <Grid container spacing={2} alignItems="center">
          <Grid item xs={6}>
            <Typography
              variant="h6"
              style={{
                fontSize: 18,
              }}
            >
              {t("DialogEditTableTitle")}
            </Typography>
          </Grid>
          <Grid item xs={3}>
            <Typography
              sx={{
                [theme.breakpoints.down("sm")]: {
                  display: "none",
                },
              }}
              style={{
                color: "#98A2B7",
              }}
              variant="subtitle2"
            >
              {t("selectedColumns")}
            </Typography>
          </Grid>
        </Grid>
        <IconButton
          size="small"
          style={{
            position: "absolute",
            right: 0,
          }}
          onClick={handleClose}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>

      <Grid
        container
        spacing={2}
        style={{
          marginBottom: 20,
        }}
      >
        <Grid
          item
          xs={12}
          sm={6}
          style={{
            paddingRight: 50,
            boxSizing: "border-box",
          }}
        >
          <Box
            sx={{
              position: "relative",
              display: "flex",
            }}
          >
            <InputBase
              placeholder={t("search")}
              sx={{
                backgroundColor: "#F6F8FA",
                borderRadius: theme.shape.borderRadius,
                padding: "5px 16px",
                paddingRight: "42px",
                marginRight: "10px",
                flex: "1 1 100%",
                [theme.breakpoints.down("sm")]: {
                  width: "90%",
                },
              }}
              value={searchText}
              onChange={onFilterChange}
            />
            <IconButton
              type="submit"
              sx={{
                padding: "9px",
                backgroundColor: "#F6F8FA",
                borderRadius: theme.shape.borderRadius,
              }}
              style={{
                position: "absolute",
                right: 0,
                backgroundColor: "transparent",
              }}
              size="large"
            >
              <SearchIcon color="primary" />
            </IconButton>
          </Box>
          <Paper
            variant="outlined"
            sx={{
              margin: "10px 0",
              maxHeight: "245px",
              height: "100%",
              width: "100%",
              overflowY: "auto",
              border: "none",
              background: "#F6F8FA",
            }}
          >
            <Typography
              style={{
                fontWeight: 700,
                padding: "10px 20px 10px",
              }}
            >
              {t("InventoryItems")}
            </Typography>
            <FormGroup>
              {secondArrOrFilteredArr?.map((option) => (
                <FormControlLabel
                  style={{
                    padding: "0 20px",
                    marginRight: 0,
                    backgroundColor: secondaryArr.some(
                      (el) => el.toLowerCase() === option.toLowerCase(),
                    )
                      ? "#fff"
                      : "#F6F8FA",
                  }}
                  key={option}
                  checked={secondaryArr.some((el) => el === option)}
                  onChange={handleMenuItemClick}
                  value={option}
                  control={
                    <Checkbox
                      color="default"
                      icon={<CheckboxWhiteBackIcon />}
                      checkedIcon={<CheckboxCheckedGrayBackIcon />}
                      name={option}
                    />
                  }
                  label={
                    <Box
                      sx={{
                        display: "flex",
                        alignItems: "center",
                        ml: 1,
                      }}
                    >
                      <Typography
                        style={{
                          textTransform: "capitalize",
                        }}
                      >
                        {t(`TableHead.${option}`)}
                      </Typography>
                    </Box>
                  }
                />
              ))}
            </FormGroup>
          </Paper>
        </Grid>
        <Grid
          sx={{
            [theme.breakpoints.down("sm")]: {
              marginTop: "70px",
            },
          }}
          item
          xs={11}
          sm={6}
        >
          <Paper
            variant="outlined"
            style={{
              border: "none",
            }}
          >
            <Typography
              sx={{
                position: "relative",
                borderRadius: theme.shape.borderRadius,
                backgroundColor: "#F6F8FA",
                padding: "10px 32px",
                fontSize: "13px",
                color: "#98A2B7",
                marginBottom: "10px",
                "&:last-child": {
                  marginBottom: 0,
                },
                textTransform: "uppercase",
                marginRight: "10px",
              }}
            >
              {t(`TableHead.referenceStock`)}
            </Typography>
            <DragDropContext onDragEnd={onDragEnd}>
              <Droppable droppableId="droppable">
                {(
                  provided: DroppableProvided,
                  _snapshot: DroppableStateSnapshot,
                ) => (
                  <Paper
                    variant="outlined"
                    ref={provided.innerRef}
                    {...provided.droppableProps}
                    sx={{
                      maxHeight: "242px",
                      overflowY: "auto",
                      backgroundColor: "#fff",
                      border: "none",
                    }}
                  >
                    {primaryArr.map((item, index) => (
                      <Draggable key={item} draggableId={item} index={index}>
                        {(
                          dragProvided: DraggableProvided,
                          _dragSnapshot: DraggableStateSnapshot,
                        ) => (
                          <Typography
                            sx={{
                              position: "relative",
                              borderRadius: theme.shape.borderRadius,
                              backgroundColor: "#F6F8FA",
                              padding: "10px 32px",
                              fontSize: 13,
                              color: "#98A2B7",
                              marginBottom: "10px",
                              "&:last-child": {
                                marginBottom: 0,
                              },
                            }}
                            ref={dragProvided.innerRef}
                            {...dragProvided.draggableProps}
                            {...dragProvided.dragHandleProps}
                          >
                            <DragIndicator
                              sx={{
                                position: "absolute",
                                left: "5px",
                                top: "50%",
                                transform: "translateY(-50%)",
                                color: "#CBD1DD",
                              }}
                            />
                            {t(`TableHead.${item}`)}
                            <IconButton
                              size="small"
                              sx={{
                                position: "absolute",
                                right: "5px",
                                top: "50%",
                                transform: "translateY(-50%)",
                                backgroundColor: "#CBD1DD",
                              }}
                              onClick={handleDeleteFromPrimary(item)}
                            >
                              <CloseIcon
                                style={{
                                  color: "#fff",
                                  width: 18,
                                  height: 18,
                                }}
                              />
                            </IconButton>
                          </Typography>
                        )}
                      </Draggable>
                    ))}
                    {provided.placeholder}
                  </Paper>
                )}
              </Droppable>
            </DragDropContext>
          </Paper>
        </Grid>
      </Grid>
      <DialogActions sx={{padding: 0}}>
        <Button
          sx={{
            textTransform: "uppercase",
            padding: "7px 60px",
          }}
          variant="contained"
          onClick={handleEditTable}
        >
          {t(`buttonNames.Apply`)}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default DashboardTableFiltersDialogEdit;
