import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import React from "react";
import {useTranslation} from "react-i18next";
import {GenericFormBoxHeader} from "../GenericFormBoxHeader";
import {GenericFormBox} from "../GenericFormBox";
import {GenericFormDepartmentHeader} from "../GenericFormDepartmentHeader";
import {SERVICE_TAB_TRANSLATION_KEY} from "src/system/constants";

interface StockServiceTabBoxProps {
  department: string;
  step: number;
  totalSteps: number;
  stepTitle: string;
  children: JSX.Element | JSX.Element[];
}

export const StockServiceTabBox = ({
  department,
  step,
  totalSteps,
  stepTitle,
  children,
}: StockServiceTabBoxProps) => {
  const {t} = useTranslation();
  return (
    <GenericFormBox
      header={
        <GenericFormBoxHeader>
          <GenericFormDepartmentHeader>
            {department}
          </GenericFormDepartmentHeader>
          <Box>
            <Typography
              sx={{
                fontWeight: 600,
                fontSize: "13px",
                lineHeight: "16.9px",
                letterSpacing: "0.5px",
                textTransform: "uppercase",
                color: "#1A06F9",
              }}
              component="span"
            >
              {t(SERVICE_TAB_TRANSLATION_KEY + "STEP")}
              {` ${step}/${totalSteps}:`}
            </Typography>{" "}
            <Typography
              sx={{
                fontWeight: 600,
                fontSize: "13px",
                lineHeight: "16.9px",
                letterSpacing: "0.5px",
                textTransform: "uppercase",
                color: "#98A2B7",
              }}
              component="span"
            >
              {stepTitle}
            </Typography>
          </Box>
        </GenericFormBoxHeader>
      }
    >
      {children}
    </GenericFormBox>
  );
};
