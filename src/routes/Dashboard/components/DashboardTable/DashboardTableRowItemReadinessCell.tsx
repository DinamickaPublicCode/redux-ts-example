import React, {useMemo, useState} from "react";
import {FinanceTabType} from "src/types";
import Remove from "@mui/icons-material/Remove";
import VisibilitySensor from "react-visibility-sensor";
import Box from "@mui/material/Box";
import {styled} from "@mui/material/styles";
import MuiTableCell from "@mui/material/TableCell";
import {getColor, getPercentage} from "src/utils/stageData";
import {stageInfoIcons} from "src/utils/stageInfoIcons";
import CircularProgressWithLabel from "src/components/CircularProgressWithLabel";

const DashboardTableRowItemReadinessCell = ({
  row,
  onClick,
}: {
  row: FinanceTabType;
  onClick: () => void;
}) => {
  const [visible, setVisible] = useState(false);

  const cachedPercentage = useMemo(() => {
    const percentage = getPercentage(row.stageInfo.stage);
    if (typeof percentage === "number") {
      return percentage;
    }
    return 100;
  }, [row.stageInfo.stage]);

  return (
    <TableCell onClick={onClick} sx={{cursor: "pointer"}} align="left">
      <VisibilitySensor onChange={setVisible}>
        <div
          style={{
            display: "flex",
            alignItems: "center",
          }}
        >
          {visible && typeof getPercentage(row.stageInfo.stage) === "number" ? (
            getPercentage(row.stageInfo.stage) === 0 ? (
              <Box
                sx={{
                  width: 42,
                  height: 42,
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <Remove
                  style={{
                    color: "#222238",
                  }}
                />
              </Box>
            ) : (
              <CircularProgressWithLabel
                size={42}
                thickness={2}
                style={{
                  color: getColor(row.stageInfo.stage),
                  marginRight: 5,
                }}
                value={visible ? cachedPercentage : 0}
              />
            )
          ) : (
            <CircularProgressWithLabel
              size={42}
              thickness={2}
              style={{
                color: "#000",
                opacity: 0.1,
                marginRight: 5,
              }}
              value={visible ? cachedPercentage : 0}
            />
          )}
          {row.stageInfo.icons.map((el) => {
            return (
              <div
                style={{
                  width: "max-content",
                  margin: "0 2px",
                }}
                key={el}
              >
                {stageInfoIcons[el]}
              </div>
            );
          })}
        </div>
      </VisibilitySensor>
      <BottomBorder />
    </TableCell>
  );
};

const TableCell = styled(MuiTableCell)({
  position: "relative",
  border: "none",
  color: "#98A2B7",

  //
  padding: "13px 10px",
});

const BottomBorder = styled("span")(({theme}) => ({
  display: "block",
  position: "absolute",
  bottom: 0,
  left: 0,
  right: 0,
  height: "4px",
  width: "100%",
  backgroundColor: theme.palette.background.default,
}));

export default DashboardTableRowItemReadinessCell;
