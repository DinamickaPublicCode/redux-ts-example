import Box from "@mui/material/Box";
import React from "react";

const SalesFunnelMarkerCircle = ({
  htmlColor,
  nomargin,
  noborder,
}: {
  htmlColor?: string;
  noborder?: boolean;
  nomargin?: boolean;
}) => (
  <Box
    sx={{
      borderRadius: "50%",
      border: noborder ? "none" : "2px solid white",
      mr: nomargin ? 0 : 1,
      width: noborder ? 10 : 14,
      height: noborder ? 10 : 14,
      opacity: htmlColor ? 1 : 0,
      backgroundColor: htmlColor ? htmlColor : "#98A2B7",
      boxShadow: noborder ? "none" : "0px 2px 6px rgba(203, 205, 217, 0.4)",
    }}
  />
);
export default SalesFunnelMarkerCircle;
