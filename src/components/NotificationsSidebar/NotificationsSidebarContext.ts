import React from "react";
import type {NotificationsSidebarProps} from "./NotificationsSidebar";

const NotificationsSidebarContext =
  React.createContext<NotificationsSidebarProps>({
    open: false,
    onClose: () => {},
  });

export default NotificationsSidebarContext;
