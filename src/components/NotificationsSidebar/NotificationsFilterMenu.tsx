import React, {useRef, useState} from "react";
import {useTranslation} from "react-i18next";
import Badge from "@mui/material/Badge";
import Box from "@mui/material/Box";
import ButtonBase from "@mui/material/ButtonBase";
import Checkbox from "@mui/material/Checkbox";
import ClickAwayListener from "@mui/material/ClickAwayListener";
import FormControl from "@mui/material/FormControl";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormGroup from "@mui/material/FormGroup";
import Grow from "@mui/material/Grow";
import IconButton from "@mui/material/IconButton";
import Paper from "@mui/material/Paper";
import Popper from "@mui/material/Popper";
import Typography from "@mui/material/Typography";
import FilterListIcon from "@mui/icons-material/FilterList";
import { ReactComponent as CheckboxIcon } from "src/assets/icons/CheckboxIcon.svg";
import { ReactComponent as CheckboxCheckedIcon } from "src/assets/icons/CheckboxCheckedIcon.svg";
import {NotificationType} from "src/types";
import {useDispatch} from "react-redux";
import {setActiveNotificationFilters} from "src/state/reducers/notifications";
import {useTheme} from "@mui/material/styles";
import {useAppSelector} from "src/state/hooks";

const NotificationsFilterMenu: React.FC = () => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  const {activeFilters} = useAppSelector((state) => state.notifications);

  const [checked, setChecked] = useState<NotificationType[]>([]);

  const [open, setOpen] = useState<boolean>(false);
  const anchorRef = useRef<HTMLButtonElement>(null);

  const handleMenuItemClick = (
    event: React.ChangeEvent<HTMLInputElement>,
    type: NotificationType,
  ) => {
    event.stopPropagation();
    if (event.target.checked) {
      return setChecked((prevState) => [
        ...Array.from(new Set([...prevState, type])),
      ]);
    }
    return setChecked((prevState) => [
      ...prevState.filter((el) => el !== type),
    ]);
  };

  const handleToggle = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>,
  ) => {
    event.stopPropagation();
    setOpen((prevOpen) => !prevOpen);
  };

  const handleFilter = () => {
    dispatch(setActiveNotificationFilters(checked));
    setOpen(false);
  };
  const theme = useTheme();
  return (
    <>
      <IconButton
        sx={{
          margin: "0 15px",
          backgroundColor: theme.palette.common.white,
          boxShadow: "0px 2px 12px rgba(203, 205, 217, 0.4)",
          borderRadius: "8px",
          padding: "6px",
          [theme.breakpoints.down("sm")]: {
            marginLeft: "30%",
            marginTop: "10px",
            marginBottom: "20px",
          },
        }}
        ref={anchorRef}
        onClick={handleToggle}
        size="large"
      >
        <Badge
          color="primary"
          sx={{
            "& .MuiBadge-standard": {
              height: "23px",
              minWidth: "23px",
              border: "2px solid #fff",
              fontSize: "11px",
              fontWeight: 600,
              padding: "3px",
            },
          }}
          badgeContent={activeFilters.length}
        >
          <FilterListIcon color="primary" />
        </Badge>
      </IconButton>
      <Popper
        open={open}
        anchorEl={anchorRef.current}
        role={undefined}
        transition
        disablePortal
        placement={"bottom-start"}
        style={{zIndex: 100}}
      >
        {({TransitionProps, placement}) => (
          <Grow
            {...TransitionProps}
            style={{
              zIndex: 1000,
              transformOrigin:
                placement === "bottom" ? "center top" : "center bottom",
            }}
          >
            <Paper
              style={{
                boxShadow: "0px 4px 20px rgba(203, 205, 217, 0.3)",
                overflow: "hidden",
              }}
            >
              <ClickAwayListener
                onClickAway={(event) => {
                  if (
                    anchorRef.current &&
                    anchorRef.current.contains(event.target as HTMLElement)
                  ) {
                    return;
                  }
                  setOpen(false);
                }}
              >
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                    pt: 3.8,
                    minWidth: 280,
                    outline: "none",
                  }}
                >
                  <FormControl
                    component="div"
                    sx={{
                      padding: "0 22px 22px",
                      width: "100%",
                      overflowY: "auto",
                      border: "none",
                    }}
                  >
                    <Typography
                      style={{
                        fontWeight: 600,
                        fontSize: 16,
                        color: "#222238",
                        marginBottom: 15,
                      }}
                    >
                      {t("FilterNotification")}
                    </Typography>
                    <FormGroup>
                      {(
                        [
                          "SALE",
                          "FINANCE",
                          "MECHANIC",
                          "ESTHETIC",
                          "DELIVERY",
                        ] as NotificationType[]
                      ).map((el) => {
                        return (
                          <FormControlLabel
                            key={el}
                            sx={{
                              "& .MuiFormControlLabel-label": {
                                fontWeight: 600,
                                marginLeft: "10px",
                                marginRight: "10px",
                              },
                            }}
                            checked={!!checked.length && checked.includes(el)}
                            control={
                              <Checkbox
                                color="default"
                                icon={<CheckboxIcon />}
                                checkedIcon={<CheckboxCheckedIcon />}
                                onChange={(event) =>
                                  handleMenuItemClick(event, el)
                                }
                                name={el}
                              />
                            }
                            label={t(`${el}`)}
                          />
                        );
                      })}
                    </FormGroup>
                  </FormControl>
                  <ButtonBase
                    sx={{
                      padding: "13px",
                      width: "100%",
                      backgroundColor: theme.palette.primary.main,
                      color: theme.palette.common.white,
                      fontWeight: 600,
                      textTransform: "uppercase",
                    }}
                    onClick={handleFilter}
                  >
                    {t(`buttonNames.Apply`)}
                  </ButtonBase>
                </Box>
              </ClickAwayListener>
            </Paper>
          </Grow>
        )}
      </Popper>
    </>
  );
};

export default NotificationsFilterMenu;
