import {
  SalesFunnelDataProviderInputs,
  SalesFunnelDataProviderOptions,
} from "../types/apiDataProvider";
import {useAppSelector} from "src/state/hooks";
import {useOneMonthDateRange} from "./useOneMonthDateRange";
import {useGetCarDealerQuery} from 'src/state/reducers/carDealerApi';

/**
 * Preprocessed data which is handled by react UI.
 * Stored in the upper context and pre-processed for the "sales funnel" endpoints
 *
 * @param options
 */
export const useSalesFunnelDataProviderInputs = (
  options: SalesFunnelDataProviderOptions,
) => {
  const {carType, date, searchText, idCarDealer} = options;
  const [saleDateBegin, saleDateEnd] = useOneMonthDateRange(date);
  const {data:carDealer} = useGetCarDealerQuery();
  const legends = carDealer?.legends;

  return {
    legends,
    idCarDealer,
    saleDateBegin,
    saleDateEnd,
    carType,
    searchText: searchText.length === 0 ? undefined : searchText,
  } as SalesFunnelDataProviderInputs;
};
