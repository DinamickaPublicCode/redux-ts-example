import React from "react";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";

interface SalesCalendarCarItemProps {
  icon: JSX.Element;
  title: string;
  descr: string;
}

const SalesCalendarCarItem: React.FC<SalesCalendarCarItemProps> = ({
  icon,
  title,
  descr,
}) => {
  return (
    <Box
      sx={{
        display: "flex",
        alignItems: "center",
        my: 2,
      }}
    >
      <Paper
        style={{
          width: 46,
          height: 46,
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          marginRight: 20,
        }}
      >
        {icon}
      </Paper>
      <div>
        <Typography
          style={{
            color: "#98A2B7",
          }}
        >
          {title}
        </Typography>
        <Typography
          style={{
            fontWeight: 600,
            fontSize: 18,
          }}
        >
          {descr}
        </Typography>
      </div>
    </Box>
  );
};

export default SalesCalendarCarItem;
