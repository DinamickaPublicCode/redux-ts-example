import React, {useEffect, useRef, useState} from "react";
import Portal from "@mui/material/Portal";
import Box from "@mui/material/Box";
import {useSalesFunnelCarItemStyles} from "../../utils/salesFunnelCarItemStyles";
import {CarDealerLegend} from "src/types";
import {useSalesFunnelContext} from "../../state/SalesFunnelProvider";
import SalesFunnelMarkerCircle from "../SalesFunnelMarker/SalesFunnelMarkerCircle";
import SalesFunnelItemCircleCheckbox from "./SalesFunnelItemCircleCheckbox";
import ModalDropdownPopperBody from "../../../../components/ModalDropdown/ModalDropdownPopperBody";

const SalesFunnelItemCirclesDropdown = ({
  legends,
  selected,
  onUpdateLegends: onUpdateCircles,
}: {
  legends: CarDealerLegend[];
  selected: boolean;
  onUpdateLegends: (circles: CarDealerLegend[]) => void;
}) => {
  const {
    dataHandlers: {legends: allLegends},
  } = useSalesFunnelContext();
  const styles = useSalesFunnelCarItemStyles();
  const anchor = useRef(null);
  const [isOpen, setIsOpen] = useState(false);
  const [shouldUpdate, setShouldUpdate] = useState(false);
  const onHandleClose = () => {
    setIsOpen(false);
    if (shouldUpdate) {
      onUpdateCircles(
        items.filter((item) => item.checked).map((item) => item.legend),
      );
      setShouldUpdate(false);
    }
  };

  const [items, setItems] = useState(() =>
    buildCurrentItems(allLegends, legends),
  );

  useEffect(() => {
    setItems(() => buildCurrentItems(allLegends, legends));
  }, [allLegends, legends]);

  return (
    <>
      <Box
        ref={anchor}
        className={styles.pointer}
        display="flex"
        flexDirection="row"
        alignItems="center"
        onClick={(e) => {
          e.stopPropagation();
          setIsOpen(true);
        }}
      >
        {legends.length === 0 ? (
          <Box
            style={{
              marginRight: selected ? -1 : -4,
              marginTop: selected ? 2 : 0,
            }}
          >
            <SalesFunnelMarkerCircle
              noborder={selected}
              nomargin
            />
          </Box>
        ) : (
          legends.map((legend, index) => (
            <Box
              style={{
                marginRight: selected ? -1 : -4,
                marginTop: selected ? 2 : 0,
              }}
              key={legend.idLegend}
              zIndex={legends.length - index}
            >
              <SalesFunnelMarkerCircle
                noborder={selected}
                nomargin
                htmlColor={legend.colorLegend}
              />
            </Box>
          ))
        )}
      </Box>

      <Portal container={document.body}>
        <ModalDropdownPopperBody
          handleClose={onHandleClose}
          open={isOpen}
          anchorRef={anchor}
        >
          <Box padding="17px 39px 0 21px">
            {items.map(({legend, checked}, index) => (
              <SalesFunnelItemCircleCheckbox
                key={legend.idLegend}
                index={index}
                legend={legend}
                onChange={(checked: boolean, index: number) => {
                  setItems(
                    items.map((item, itemIndex) =>
                      itemIndex === index
                        ? {
                            ...item,
                            checked,
                          }
                        : item,
                    ),
                  );
                  setShouldUpdate(true);
                }}
                title={"Ident " + index}
                checked={checked}
              />
            ))}
          </Box>
        </ModalDropdownPopperBody>
      </Portal>
    </>
  );
};

/**
 * Creates a checklist array for the car legends dropdown list.
 *
 * @param allLegends
 * @param legends
 */
const buildCurrentItems = (
  allLegends: CarDealerLegend[] | undefined,
  legends: CarDealerLegend[],
) => {
  if (!allLegends) {
    return [];
  }
  return allLegends.map((legend) => ({
    checked: legends.indexOf(legend) !== -1,
    legend,
  }));
};

export default SalesFunnelItemCirclesDropdown;
