import axios, {AxiosRequestConfig, AxiosResponse} from "axios";
import useCustomErrorSnackbar from "src/utils/useCustomErrorSnackbar";
import {useAppSelector} from "src/state/hooks";
import {useEffect} from "react";

const useConfigureInterceptors = () => {
  const token = useAppSelector((state) => state.token.value);
  const errorSnackbar = useCustomErrorSnackbar();

  useEffect(() => {
    // Add a request interceptor
    axios.interceptors.request.use(
      (config: AxiosRequestConfig) => {
        // to not add token for public url
        if (!config?.url?.includes("authenticate")) {
          config.headers.common.Authorization = token;
        }
        return config;
      },
      (error) => {
        errorSnackbar(
          error?.response?.data?.message ||
            error?.response?.data ||
            error?.response?.statusText ||
            "Unknown error",
        );
        return Promise.reject(error);
      },
    );
    // Add a response interceptor
    axios.interceptors.response.use(
      (response: AxiosResponse) => {
        return response;
      },
      (error) => {
        errorSnackbar(
          error?.response?.data?.message ||
            error?.response?.data ||
            error?.response?.statusText ||
            "Unknown error",
        );
        return Promise.reject(error);
      },
    );
  }, [token, errorSnackbar]);
};

export default useConfigureInterceptors;
