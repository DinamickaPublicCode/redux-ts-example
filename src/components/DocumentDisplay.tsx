import React, {useState} from "react";
import {DocumentInfo} from "../types";
import {useDocumentTitleStyles} from "./DocumentsBox/DocumentCategoryTitle";
import Box from "@mui/material/Box";
import CircularProgress from "@mui/material/CircularProgress";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import {DocumentIcon, TrashIcon} from "../assets/icons";
import CheckIcon from "@mui/icons-material/Check";
import {downloadFileFromServer} from "../utils/downloadFileFromServer";

interface DocumentDisplayProps {
  /**
   * External loading flag may come if the file is managed outside the component
   * and for providing a meaningful indication of some action progress
   */
  isLoading: boolean;
  onDownload?: () => void;
  onDocumentAction?: () => void;
  filename: string;
  hasPendingFile?: boolean;
  imageSrc?: string;
}

/**
 * This document display component has a downloading feature built-in.
 * Depends on DocumentInfo, combines the loading types with an incoming one
 *
 * TODO: pick a shorter name.
 *
 * @param props
 */
export const DocumentDisplayWithDownloadFeature: React.FC<
  {
    documentInfo: DocumentInfo;
  } & Omit<DocumentDisplayProps, "onDownload" | "filename">
> = (props) => {
  const [isLoading, setIsLoading] = useState(false);

  return (
    <DocumentDisplay
      {...props}
      filename={props.documentInfo.name}
      onDownload={async () => {
        await downloadFileFromServer(props.documentInfo, setIsLoading);
      }}
      isLoading={isLoading || props.isLoading}
    />
  );
};

const DocumentDisplay: React.FC<DocumentDisplayProps> = ({
  onDownload,
  isLoading,
  filename,
  onDocumentAction,
  hasPendingFile,
  imageSrc,
}) => {
  const styles = useDocumentTitleStyles();
  return (
    <Box className={styles.fileBox} display="flex" alignItems="center">
      {imageSrc ? (
        <img
          src={imageSrc}
          alt="img"
          className={styles.imageView}
          onClick={onDownload}
        />
      ) : (
        <IconButton
          className={styles.documentIconButton}
          onClick={onDownload}
          size="large"
        >
          <DocumentIcon />
        </IconButton>
      )}
      <Typography className={styles.filename} onClick={onDownload}>
        {filename
          ? filename?.length < 20
            ? filename
            : `${filename.slice(0, 19)}...`
          : ""}
      </Typography>
      <IconButton onClick={onDocumentAction} size="large">
        {isLoading ? (
          <CircularProgress size={22} />
        ) : hasPendingFile ? (
          <CheckIcon />
        ) : (
          <TrashIcon color={"#CBD1DD"} />
        )}
      </IconButton>
    </Box>
  );
};
export default DocumentDisplay;
