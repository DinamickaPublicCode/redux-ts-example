import Grid from "@mui/material/Grid";
import React, {useState} from "react";
import {useTranslation} from "react-i18next";
import {StepSpecification} from "src/types";
import {useStockServiceTabContext} from "./StockServiceTabContext";
import CartagInputField from "../CartagInputField";
import {SERVICE_TAB_TRANSLATION_KEY} from "../../system/constants";
import {GenericFormSection} from "../GenericFormSection";
import {GenericFormCheckbox} from "../GenericFormCheckbox";

export const StockServiceTabActionsForm = ({
  departmentType,
  step,
}: {
  departmentType: string;
  step: StepSpecification;
}) => {
  const {getActionValue, getWorkOrderValue, onChecked, onWorkOrderInfoChanged} =
    useStockServiceTabContext();

  const key = SERVICE_TAB_TRANSLATION_KEY;
  const {t} = useTranslation();
  const [workInput, setWorkInput] = useState(
    getWorkOrderValue(departmentType, step.stepType),
  );
  const refresh = useState(Math.random());

  return (
    <Grid container>
      <Grid item xs>
        <GenericFormSection title={"ETAT"}>
          {step.actions ? (
            step.actions.map((action) => (
              <GenericFormCheckbox
                key={action.idAction}
                checked={getActionValue(
                  departmentType,
                  step.stepType,
                  action.idAction,
                )}
                onChange={(checked) => {
                  onChecked(
                    departmentType,
                    step.stepType,
                    action.idAction,
                    checked,
                  );
                  refresh[1](Math.random());
                }}
                disabled={workInput.length === 0}
                // TODO: Localize
                label={action.labelAction || t(key + action.labelAction)}
              />
            ))
          ) : (
            <></>
          )}
        </GenericFormSection>
      </Grid>
      {step.iwWorkOrder && (
        <Grid item xs>
          <GenericFormSection title={t(key + "WORK_ORDER")}>
            <CartagInputField
              disableLabel
              placeholder={t(key + "WORK_ORDER_PLACEHOLDER")}
              value={workInput}
              onChange={(e) => {
                setWorkInput(e.target.value);
                onWorkOrderInfoChanged(
                  departmentType,
                  step.stepType,
                  e.target.value,
                );
              }}
            />
          </GenericFormSection>
        </Grid>
      )}
    </Grid>
  );
};
