import React, {forwardRef} from "react";
import {TransitionProps} from "@mui/material/transitions";
import Slide, {SlideProps} from "@mui/material/Slide";

const Transition = forwardRef(
  (props: TransitionProps, ref: React.Ref<unknown>) => {
    return (
      <Slide direction="up" ref={ref} {...(props as unknown as SlideProps)} />
    );
  },
);

export default Transition;
