import React from 'react';

import { ComponentStory, ComponentMeta } from '@storybook/react';
import NotificationsFilterMenu from './NotificationsFilterMenu';

export default {
  title: 'Notification Filter',
  component: NotificationsFilterMenu,
} as ComponentMeta<typeof NotificationsFilterMenu>;

const Template: ComponentStory<typeof NotificationsFilterMenu> = () => <NotificationsFilterMenu />;

export const Primary = Template.bind({});
