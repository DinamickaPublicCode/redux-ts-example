import React from 'react';

import { addDecorator } from '@storybook/react';
import { ThemeProvider, StyledEngineProvider } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import AdapterDayjs from "@mui/lab/AdapterDayjs";
import {Provider} from "react-redux";
import {SnackbarProvider} from "../src/thirdparty/_notistack";
import {CartagSnackbarProvider} from "../src/components/CartagSnackbar";
import { theme, GlobalCss } from "../src/utils/theme";
import store from "../src/state/store";
import "../src/utils/i18n";

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
  backgrounds: {
   default: 'default',
   values: [
     {
       name: 'default',
       value: '#F2F2F2',
     },
   ],
 },
}

addDecorator((story) => (
  <StyledEngineProvider injectFirst>
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <GlobalCss />
        <LocalizationProvider dateAdapter={AdapterDayjs}>
          <SnackbarProvider maxSnack={8} autoHideDuration={6000}>
            <CartagSnackbarProvider>
                <Provider store={store}>
                    {story()}
                </Provider>
              </CartagSnackbarProvider>
            </SnackbarProvider>
        </LocalizationProvider>
    </ThemeProvider>
  </StyledEngineProvider>
));
