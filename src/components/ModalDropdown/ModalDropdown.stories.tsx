import React from 'react';

import { ComponentStory, ComponentMeta } from '@storybook/react';
import ModalDropdown from './ModalDropdown';

export default {
  title: 'Modal Dropdown',
  component: ModalDropdown,
} as ComponentMeta<typeof ModalDropdown>;



const Template: ComponentStory<typeof ModalDropdown> = (args) => <div style={{width: 64}}><ModalDropdown {...args} /></div>;

export const Primary = Template.bind({});
Primary.args = {
  value: 'test',
  options: ['test1', 'test2', 'test3'],
  labels: ['label1', 'label2', 'label3'],
  onChange: () => false
}
