import React from "react";
import IconButton from "@mui/material/IconButton";
import DialogTitle from "@mui/material/DialogTitle";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import Typography from "@mui/material/Typography";
import CloseIcon from "@mui/icons-material/Close";
import {useTranslation} from "react-i18next";
import Transition from "./Transition";

interface ConfirmationDialogProps {
  open: boolean;
  handleClose: () => void;
  handleRequest: () => void;
  action?: string | undefined;
}

const ConfirmationDialog: React.FC<ConfirmationDialogProps> = ({
  open,
  handleClose,
  handleRequest,
  action,
}) => {
  const {t} = useTranslation();

  return (
    <Dialog
      open={open}
      TransitionComponent={Transition}
      keepMounted
      maxWidth={"xs"}
      onClose={handleClose}
      // classes={{paper: classes.dialogPaper}}
      aria-labelledby="alert-dialog-archive-title"
      aria-describedby="alert-dialog-archive-description"
    >
      <DialogTitle
        // classes={{root: classes.dialogTitle}}
        id="alert-dialog-archive-title"
      >
        <Typography variant="h6">
          {action ? t(`actions.${action}`) : t("Delivery done")}
        </Typography>
        <IconButton size="small" onClick={handleClose}>
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent
        style={{
          padding: 0,
          marginBottom: 25,
        }}
      >
        <DialogContentText id="alert-dialog-archive-description">
          {action
            ? `Do you confirm action ${t(`actions.${action}`)}`
            : "Do you want to mark this delivery as done?"}
        </DialogContentText>
      </DialogContent>
      <DialogActions style={{padding: 0}}>
        <Button
          style={{
            textTransform: "uppercase",
            padding: 15,
            boxShadow: "0px 4px 20px rgba(203, 205, 217, 0.3)",
          }}
          fullWidth
          onClick={handleClose}
        >
          {t("Cancel")}
        </Button>
        <Button
          style={{
            textTransform: "uppercase",
            padding: 15,
            boxShadow: "0px 4px 20px rgba(203, 205, 217, 0.3)",
          }}
          fullWidth
          variant="contained"
          onClick={handleRequest}
        >
          {t("Agree")}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default ConfirmationDialog;
