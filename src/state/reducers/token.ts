import {getValidToken} from "src/utils/token";

const initialState: TokenState = {
  value: getValidToken(),
};

type TokenActionType = {type: 'SET_TOKEN'; payload?: unknown};

const tokenReducer = (
  state: TokenState = initialState,
  action: TokenActionType,
): TokenState => {
  switch (action.type) {
    case 'SET_TOKEN': {
      return {
        ...state,
        value: action.payload as TokenState["token"],
      };
    }
    default:
      return state;
  }
};

export const setToken = (token: string): TokenActionType => ({
  type: 'SET_TOKEN',
  payload: token,
});

export default tokenReducer;
