import React from "react";

const CompletedIcon = (color: string) => (
  <svg width={86} height={86} viewBox="0 0 86 86" fill="none">
    <g filter="url(#prefix__filter0_d)">
      <rect x={20} y={18} width={46} height={46} rx={23} fill="#fff" />
      <rect x={20.5} y={18.5} width={45} height={45} rx={22.5} stroke={color} />
    </g>
    <path
      d="M49.125 40.25a4.88 4.88 0 00-4.875 4.875A4.88 4.88 0 0049.125 50 4.88 4.88 0 0054 45.125a4.88 4.88 0 00-4.875-4.875zm2.44 3.682l-2.626 3a.75.75 0 01-.54.256h-.024a.75.75 0 01-.53-.22l-1.5-1.5a.75.75 0 111.06-1.06l.933.932 2.097-2.397a.75.75 0 011.13.989z"
      fill={color}
    />
    <path
      d="M49.126 38.75c.127 0 .247 0 .375.008v-.008h-.375zM36 43.25h7.028a6.388 6.388 0 016.098-4.5H36v4.5zM42.75 45.125c0-.127 0-.248.008-.375H36v3.188c0 1.14.922 2.062 2.063 2.062h6.952a6.365 6.365 0 01-2.265-4.875zM47.438 32h-9.375c-1.14 0-2.063.922-2.063 2.063v3.187h13.5v-3.187c0-1.14-.922-2.063-2.062-2.063z"
      fill={color}
    />
    <defs>
      <filter
        id="prefix__filter0_d"
        x={0}
        y={0}
        width={86}
        height={86}
        filterUnits="userSpaceOnUse"
        colorInterpolationFilters="sRGB"
      >
        <feFlood floodOpacity={0} result="BackgroundImageFix" />
        <feColorMatrix
          in="SourceAlpha"
          values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
        />
        <feOffset dy={2} />
        <feGaussianBlur stdDeviation={10} />
        <feColorMatrix values="0 0 0 0 0.796875 0 0 0 0 0.804464 0 0 0 0 0.85 0 0 0 0.5 0" />
        <feBlend in2="BackgroundImageFix" result="effect1_dropShadow" />
        <feBlend in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
      </filter>
    </defs>
  </svg>
);

export default CompletedIcon;
