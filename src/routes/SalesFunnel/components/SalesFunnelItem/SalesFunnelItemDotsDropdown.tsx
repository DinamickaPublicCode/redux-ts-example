import Box from "@mui/material/Box";
import Divider from "@mui/material/Divider";
import IconButton from "@mui/material/IconButton";
import MenuItem from "@mui/material/MenuItem";
import MenuList from "@mui/material/MenuList";
import Typography from "@mui/material/Typography";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import Button from "@mui/material/Button";
import Portal from "@mui/material/Portal";
import React, {useCallback, useMemo, useRef, useState} from "react";
import MoreVert from "@mui/icons-material/MoreVert";
import {useSalesFunnelContext} from "../../state/SalesFunnelProvider";
import {useSalesFunnelCarItemStyles} from "../../utils/salesFunnelCarItemStyles";
import {
  SellerCarEntry,
  SaleReferenceAction,
} from "src/routes/SalesFunnel/types/apiDataProvider";
import useCustomInfoSnackbar from "src/utils/useCustomInfoSnackbar";
import ModalDropdownPopperBody from "../../../../components/ModalDropdown/ModalDropdownPopperBody";

export const SalesFunnelItemDotsDropdown = ({
  idUser,
  car,
}: {
  idUser: string;
  car: SellerCarEntry;
}) => {
  const {
    onChatInventoryIdChanged,
    dataHandlers: {onSaleRefenceUpdated},
  } = useSalesFunnelContext();
  const anchor = useRef(null);
  const [isOpen, setIsOpen] = useState(false);
  const onHandleClose = () => {
    setIsOpen(false);
  };
  const [confirmationDialogAction, setConfirmationDialogAction] = useState<{
    action: () => Promise<void> | undefined;
  }>();

  const styles = useSalesFunnelCarItemStyles();

  const enqueueInfoSnackbar = useCustomInfoSnackbar();
  const onActionClicked = useCallback(
    (action: SaleReferenceAction) => {
      setIsOpen(false);
      setConfirmationDialogAction({
        action: async () => {
          try {
            await onSaleRefenceUpdated(
              car.sale.id,
              idUser,
              car.reference.saleReference,
              {
                action,
                saleDate: car.reference.saleDate,
              },
            );
            enqueueInfoSnackbar("Success");
          } catch (e) {
          } finally {
            setConfirmationDialogAction(undefined);
          }
        },
      });
    },
    [
      car.reference.saleDate,
      car.reference.saleReference,
      car.sale.id,
      enqueueInfoSnackbar,
      idUser,
      onSaleRefenceUpdated,
    ],
  );

  const actionItems: [string, () => void][] = useMemo(
    () => [
      ["Carry Over", () => onActionClicked(SaleReferenceAction.CARRY_OVER)],
      ["Count", () => onActionClicked(SaleReferenceAction.COUNT)],
      ["Cancel", () => onActionClicked(SaleReferenceAction.CANCEL_SALE)],
    ],
    [onActionClicked],
  );

  const onShowChat = () => {
    setIsOpen(false);
    onChatInventoryIdChanged(car.sale.id);
  };

  return (
    <>
      <Box
        ref={anchor}
        sx={{
          width: "24px",
          height: "24px",
          marginTop: "20px",
          position: "relative",
        }}
      >
        <Box
          sx={{
            position: "absolute",
            right: "-12px",
            top: 0,
          }}
        >
          <IconButton
            size="small"
            onClick={(e) => {
              e.stopPropagation();
              setIsOpen(true);
            }}
          >
            <MoreVert htmlColor="#CBD1DD" />
          </IconButton>
        </Box>
      </Box>
      <Portal container={document.body}>
        <ModalDropdownPopperBody
          handleClose={onHandleClose}
          open={isOpen}
          anchorRef={anchor}
        >
          <Box>
            <Box
              sx={{
                p: "0px 19px 0px 21px",
              }}
            >
              <Typography
                component="div"
                className={styles.confirmationDropDownText}
              >
                Confirmation
              </Typography>
              <MenuList id="split-button-menu">
                {actionItems.map(([title, onClick]) => (
                  <MenuItem key={title} onClick={() => onClick()}>
                    {title}
                  </MenuItem>
                ))}
              </MenuList>
            </Box>
            <Divider light />
            <Box
              sx={{
                p: "2px 21px",
              }}
            >
              <MenuItem onClick={onShowChat}>Message</MenuItem>
            </Box>
          </Box>
        </ModalDropdownPopperBody>
        <Dialog open={confirmationDialogAction !== undefined}>
          <DialogTitle id="alert-dialog-title">{"Confirmation"}</DialogTitle>
          <DialogContent>Please, confirm the action</DialogContent>
          <DialogActions>
            <Button
              onClick={() => setConfirmationDialogAction(undefined)}
              color="primary"
            >
              Cancel
            </Button>
            <Button
              onClick={() => {
                if (confirmationDialogAction) {
                  confirmationDialogAction.action();
                }
              }}
              color="primary"
              variant="contained"
              autoFocus
            >
              OK
            </Button>
          </DialogActions>
        </Dialog>
      </Portal>
    </>
  );
};
