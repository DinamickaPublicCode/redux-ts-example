import Box from "@mui/material/Box";
import React, {useMemo, useState} from "react";
import {useSalesFunnelContext} from "../../state/SalesFunnelProvider";
import {useSalesFunnelStyles} from "../../utils/salesFunnelStyles";
import {ScrollableItemsList, UserProfile, UserStats} from "./SalesFunnelRow";
import {useSalesFunnelRowStyles} from "../../utils/salesFunnelRowStyles";
import YourStats from "./YourStats";

import {
  DragDropContext,
  Droppable,
  Draggable,
  DroppableProvided,
  DroppableStateSnapshot,
  DraggableProvided,
  DraggableStateSnapshot,
  DropResult,
} from "react-beautiful-dnd";
import {CarDealerSeller} from "src/types";
import {
  SellerCarEntry,
  SellersListEntry,
} from "src/routes/SalesFunnel/types/apiDataProvider";
import {useSellerGoal} from "../../utils/useSellerGoal";
import {useSellerCarsSold} from "../../utils/useSellerCarsSold";
import {useSellerTotalYear} from "../../utils/useSellerTotalYear";

const SalesFunnelRow = ({
  seller,
  cars,
}: {
  seller: CarDealerSeller;
  cars: SellerCarEntry[] | undefined;
}) => {
  const styles = useSalesFunnelRowStyles();
  const {
    currentDate,
    carFilter,
    dataHandlers: {topSellers},
  } = useSalesFunnelContext();

  const currentGoal = useSellerGoal(currentDate, seller, carFilter);
  const carsSold = useSellerCarsSold(cars);
  const totalYear = useSellerTotalYear(topSellers, seller);

  return (
    <Box display="flex" flexDirection="row">
      <Box width="162px" className={styles.userProfileGridItem}>
        <Box>
          <UserProfile person={seller} />
        </Box>
      </Box>

      {cars ? (
        <ScrollableItemsList
          idUser={seller.id}
          cars={cars}
          carsSold={carsSold}
          currentGoal={currentGoal}
        />
      ) : (
        <Box flex="1" />
      )}
      <Box
        className={
          styles.userProfileGridItem + " " + styles.userProfileGridItem1
        }
      >
        {seller && cars && (
          <UserStats
            currentGoal={currentGoal}
            carsSold={carsSold}
            totalYear={totalYear}
          />
        )}
      </Box>
    </Box>
  );
};

const UserTable = () => {
  const {
    dataHandlers: {sellersList: sellersListUnsorted, onSellersOrderChanged},
    carFilter,
  } = useSalesFunnelContext();

  // A good idea would be to locally store the sellers list
  const sellersList = useMemo(() => {
    if (carFilter === "NEW") {
      return sellersListUnsorted?.sort(
        (a, b) => +a.seller.orderNew - +b.seller.orderNew,
      );
    }
    return sellersListUnsorted?.sort(
      (a, b) => +a.seller.orderUsed - +b.seller.orderUsed,
    );
  }, [carFilter, sellersListUnsorted]);

  // Now we have a sorted list of sellers
  // For now trigger thing
  const [, setStateUpdate] = useState(false);

  const onDragEnd = async (result: DropResult) => {
    if (!result.destination || !sellersList) {
      return;
    }
    const reorder = (
      list: SellersListEntry[],
      startIndex: number,
      endIndex: number,
    ) => {
      const result = Array.from(list);
      const [removed] = result.splice(startIndex, 1);
      result.splice(endIndex, 0, removed);

      return result;
    };
    const ordererd = reorder(
      [...sellersList],
      result.source.index,
      result.destination.index,
    );
    sellersList.splice(0);
    sellersList.push(...ordererd);
    setStateUpdate((state) => !state);
    // Give a newcar
    // list of id: order
    const orderFlags: {
      [index: string]: number;
    } = {};
    sellersList.forEach((seller, index) => {
      orderFlags[seller.seller.id] = index;
    });
    await onSellersOrderChanged(orderFlags);
  };

  return (
    <Box
      display="flex"
      flexDirection="column"
      style={{
        overflowY: "scroll",
        height: "calc(100vh - 324px)",
      }}
    >
      <DragDropContext onDragEnd={onDragEnd}>
        <Droppable droppableId="droppable">
          {(provided: DroppableProvided, _snapshot: DroppableStateSnapshot) => (
            <Box
              {...provided.droppableProps}
              ref={provided.innerRef}
              paddingBottom="140px"
            >
              {sellersList?.map(({seller, cars}, index) => (
                <Draggable
                  key={seller.id}
                  draggableId={seller.id}
                  index={index}
                >
                  {(
                    dragProvided: DraggableProvided,
                    _dragSnapshot: DraggableStateSnapshot,
                  ) => (
                    <Box
                      ref={dragProvided.innerRef}
                      {...dragProvided.draggableProps}
                      {...dragProvided.dragHandleProps}
                    >
                      <SalesFunnelRow seller={seller} cars={cars} />
                    </Box>
                  )}
                </Draggable>
              ))}
            </Box>
          )}
        </Droppable>
      </DragDropContext>
    </Box>
  );
};
const SalesFunnelContent = () => {
  const styles = useSalesFunnelStyles();

  return (
    <Box className={styles.salesFunnelContent}>
      <YourStats />
      <UserTable />
    </Box>
  );
};

export default SalesFunnelContent;
