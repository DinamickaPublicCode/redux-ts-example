import React from "react";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import {useSalesCalendarLayoutStyles} from "../utils/salesCalendarLayoutStyles";
import {useTranslation} from "react-i18next";
import CartagSearch from "src/components/CartagSearch";
import MonthPicker from "src/components/MonthPicker";

const SalesCalendarTitle: React.FC<{
  onGlobalSearch: (e: string) => void;
  onSetCurrentDateChanged: (date: Date) => void;
  currentDate: Date;
  typeOfCalendar: "MONTHLY" | "DAILY";
  onTypeOfCalendarChange: (type: "MONTHLY" | "DAILY") => void;
}> = ({
  onGlobalSearch,
  onSetCurrentDateChanged: setCurrentDateChanged,
  currentDate,
  typeOfCalendar,
  onTypeOfCalendarChange: setTypeOfCalendar,
}) => {
  const {t} = useTranslation();
  const classes = useSalesCalendarLayoutStyles();
  return (
    <div className={classes.titleWrapper}>
      <Box sx={{mx: 1.5}}>
        <Typography className={classes.saleCalendarStyle}>
          {t("Navigation.Calendar")}
        </Typography>
      </Box>
      <div className={classes.monthPickerStyle}>
        <MonthPicker date={currentDate} onDateChanged={setCurrentDateChanged} />
      </div>
      <div className={classes.globalSearchStyle}>
        <CartagSearch onSubmit={onGlobalSearch} onChange={onGlobalSearch} />
      </div>
      <div className={classes.titleButtonsBox}>
        <Button
          style={{
            padding: "7px 20px",
            width: "100%",
            marginRight: 15,
            backgroundColor: typeOfCalendar === "MONTHLY" ? "#fff" : "#1A06F9",
            color: typeOfCalendar === "MONTHLY" ? "#1A06F9" : "#fff",
            boxShadow: "0px 3px 14px rgba(2, 53, 236, 0.3)",
            fontWeight: 600,
            fontSize: 16,
          }}
          variant="contained"
          onClick={() => setTypeOfCalendar("DAILY")}
        >
          {t("Daily")}
        </Button>
        <Button
          style={{
            padding: "7px 20px",
            width: "100%",
            marginLeft: 15,
            backgroundColor: typeOfCalendar === "DAILY" ? "#fff" : "#1A06F9",
            color: typeOfCalendar === "DAILY" ? "#1A06F9" : "#fff",
            boxShadow: "0px 4px 20px rgba(203, 205, 217, 0.3)",
            fontWeight: 600,
            fontSize: 16,
          }}
          variant="contained"
          onClick={() => setTypeOfCalendar("MONTHLY")}
        >
          Monthly
        </Button>
      </div>
    </div>
  );
};

export default SalesCalendarTitle;
