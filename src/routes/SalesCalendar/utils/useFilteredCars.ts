import {CarDealerSale, CarTypes} from "../../../types";
import {useTranslation} from "react-i18next";
import {useEffect, useState} from "react";
import {makeCarStockText} from "../components/SalesCalendarCard";

export const useFilteredCars = (
  currentSalesPerDay: CarDealerSale[] | null,
  currentCarType: CarTypes,
  globalSearchValue: string,
) => {
  const {t} = useTranslation();
  const [filteredCars, setFilteredCars] = useState(() => currentSalesPerDay);
  useEffect(() => {
    setFilteredCars(() => {
      const filteredCarsProcessed =
        currentSalesPerDay?.filter((x) =>
          currentCarType === "NEW"
            ? x.generalInfo.newCar
            : currentCarType === "ALL"
            ? true
            : !x.generalInfo.newCar,
        ) || null;

      return (
        filteredCarsProcessed?.filter((car) => {
          if (globalSearchValue === "") {
            return true;
          }
          const text = makeCarStockText(t, car);
          return (
            car.generalInfo.make
              .toLowerCase()
              .includes(globalSearchValue.toLowerCase()) ||
            text.toLowerCase().includes(globalSearchValue.toLowerCase())
          );
        }) || null
      );
    });
  }, [currentSalesPerDay, currentCarType, globalSearchValue, t]);

  return filteredCars;
};
