import React from "react";
import { ReactComponent as TrashIcon } from "src/assets/icons/TrashIcon.svg";
import { ReactComponent as ListIcon } from "src/assets/icons/ListIcon.svg";
import { ReactComponent as OutsourcingIcon } from "src/assets/icons/OutsourcingIcon.svg";
import DashboardTableToolbarDialogArchive from "./DashboardTableToolbarDialogArchive";
import DashboardTableToolbarDialogAddReference from "./DashboardTableToolbarDialogAddReference";
import Button from "@mui/material/Button";
import Collapse from "@mui/material/Collapse";
import Grid from "@mui/material/Grid";
import {styled, useTheme} from "@mui/material/styles";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";

interface DashboardTableToolbarProps {
  selected: string[];
}

const DashboardTableToolbar: React.FC<DashboardTableToolbarProps> = ({
  selected,
}) => {
  const [openArchiveDialog, setOpenArchiveDialog] = React.useState(false);
  const [openReferenceDialog, setOpenReferenceDialog] = React.useState(false);

  const handleClickOpenArchive = () => {
    setOpenArchiveDialog(true);
  };
  const handleClickOpenAddReference = () => {
    setOpenReferenceDialog(true);
  };

  const handleCloseArchive = () => {
    setOpenArchiveDialog(false);
  };
  const handleCloseReference = () => {
    setOpenReferenceDialog(false);
  };

  const theme = useTheme();

  return (
    <Collapse orientation="vertical" in={selected.length > 0}>
      <Toolbar
        sx={{
          paddingLeft: theme.spacing(2),
          paddingRight: theme.spacing(1),
          transition: theme.transitions.create(["background-color"], {
            duration: theme.transitions.duration.standard,
          }),
          ...(selected.length > 0
            ? {
                color: "#98A2B7",
                backgroundColor: "#F3F6F9",
              }
            : {}),
        }}
      >
        <Grid container spacing={2} alignItems="center">
          <Grid item xs={2}>
            <Typography
              sx={{
                flex: "1 1 100%",
              }}
              color="inherit"
              variant="subtitle1"
              component="div"
            >
              {selected.length === 1
                ? `${selected.length} Item Selected`
                : `${selected.length} Items Selected`}
            </Typography>
          </Grid>
          <Grid item xs={10}>
            <SelectedItemsButton
              startIcon={<TrashIcon />}
              onClick={handleClickOpenArchive}
            >
              Archive
            </SelectedItemsButton>
            <SelectedItemsButton
              startIcon={<ListIcon />}
              onClick={handleClickOpenAddReference}
            >
              Add Work/order Reference
            </SelectedItemsButton>
            <SelectedItemsButton startIcon={<OutsourcingIcon />}>
              Send to Outsourcing
            </SelectedItemsButton>
          </Grid>
        </Grid>
      </Toolbar>

      <DashboardTableToolbarDialogArchive
        open={openArchiveDialog}
        handleClose={handleCloseArchive}
        selected={selected}
      />
      <DashboardTableToolbarDialogAddReference
        open={openReferenceDialog}
        handleClose={handleCloseReference}
        selected={selected}
      />
    </Collapse>
  );
};

const SelectedItemsButton = styled(Button)(({theme}) => ({
  backgroundColor: theme.palette.common.white,
  boxShadow: "none",
  fontSize: 13,
  color: "#98A2B7",
  padding: "7px 30px",
  marginRight: 20,
}));

export default DashboardTableToolbar;
