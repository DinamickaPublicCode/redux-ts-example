import React, {Dispatch, RefObject, useEffect, useRef} from "react";
import {useLocation, useNavigate} from "react-router-dom";
import Grow from "@mui/material/Grow";
import MenuItem from "@mui/material/MenuItem";
import MenuList from "@mui/material/MenuList";
import Paper from "@mui/material/Paper";
import Popper from "@mui/material/Popper";
import Box from "@mui/material/Box";
import ClickAwayListener from "@mui/material/ClickAwayListener";
import {useTranslation} from "react-i18next";
import {TabViews, TabViewsType} from "src/types";
import {queryArrayFromString} from "src/utils/queryArrFromString";

const carTypes = ["ALL", "NEW", "USED"];

interface DashboardTabsMenuProps {
  tabName: TabViewsType;
  element: RefObject<HTMLButtonElement>;
  open: boolean;
  setOpen: Dispatch<boolean>;
  handleDialogClose: () => void;
}

const DashboardTabsMenu: React.FC<DashboardTabsMenuProps> = ({
  tabName,
  element,
  open,
  setOpen,
  handleDialogClose,
}) => {
  const {t} = useTranslation();
  const navigate = useNavigate();
  const location = useLocation();
  const handleTypeChange = (type: string) => () => {
    const witOutCarType = queryArrayFromString(location.search).filter(
      (i) => !i.includes("carType"),
    );
    navigate({
      pathname: location.pathname,
      search: `?${witOutCarType.join("&")}&carType=${type}`,
    });
    setOpen(false);
  };
  const handleDeleteTabDialog = () => {
    handleDialogClose();
    setOpen(false);
  };

  // return focus to the button when we transitioned from !open -> open
  const prevOpen = useRef(open);
  useEffect(() => {
    if (prevOpen.current && !open) {
      element.current!.focus();
    }
    prevOpen.current = open;
  }, [open, element]);

  return (
    <Box
      sx={{
        zIndex: 1000,
      }}
    >
      <Popper
        open={open}
        anchorEl={element.current}
        role={undefined}
        transition
        disablePortal
      >
        {({TransitionProps, placement}) => (
          <Grow
            {...TransitionProps}
            style={{
              transformOrigin:
                placement === "bottom" ? "center top" : "center bottom",
            }}
          >
            <Paper
              variant="outlined"
              sx={{
                boxShadow: "0px 4px 20px rgba(20, 32, 100, 0.08)",
                border: "none",
                minWidth: 140,
              }}
            >
              <ClickAwayListener
                onClickAway={(event) => {
                  if (element?.current?.contains(event.target as HTMLElement)) {
                    return;
                  }
                  setOpen(false);
                }}
              >
                <MenuList
                  sx={{
                    padding: 0,
                  }}
                  autoFocusItem={open}
                  id="menu-list-grow"
                >
                  {carTypes.map((type) => {
                    return (
                      <MenuItem
                        key={type}
                        sx={{
                          padding: "10px 20px",
                        }}
                        onClick={handleTypeChange(type)}
                      >
                        {t(`carTypes.${type}`)}
                      </MenuItem>
                    );
                  })}
                  {tabName !== TabViews.INVENTORY && (
                    <MenuItem
                      sx={{
                        borderTop: "1px solid #E9F0F6",
                        color: "#1A06F9",
                        fontWeight: 700,
                        padding: "10px 20px",
                      }}
                      onClick={handleDeleteTabDialog}
                    >
                      {t("buttonNames.UnpinView")}
                    </MenuItem>
                  )}
                </MenuList>
              </ClickAwayListener>
            </Paper>
          </Grow>
        )}
      </Popper>
    </Box>
  );
};

export default DashboardTabsMenu;
