import React from "react";
import TextField, {TextFieldProps} from "@mui/material/TextField/TextField";
import {Theme} from "@mui/material/styles";
import createStyles from "@mui/styles/createStyles";
import makeStyles from "@mui/styles/makeStyles";

interface SettingsPhoneInputProps {
  margin?: "normal" | "dense" | "none" | undefined;
}

const SettingsPhoneInput: React.FC<
  TextFieldProps & SettingsPhoneInputProps
> = ({margin = "normal", ...rest}) => {
  const styles = useNotificationStyles();

  return (
    <TextField
      classes={{
        root: styles.root,
      }}
      {...rest}
      InputProps={{
        ...rest.InputProps,
        className: styles.input,
      }}
      fullWidth
      margin={margin}
      variant="outlined"
      size="small"
    />
  );
};

export const useNotificationStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      backgroundColor: "transparent",
    },
    input: {
      backgroundColor: "#ffffff",
      fontSize: "12px",
      borderRadius: "10px",
      "&.Mui-disabled": {
        backgroundColor: "#EBF1F6",
        color: "#C1C7D4",
      },
      "&.Mui-disabled .MuiOutlinedInput-notchedOutline": {
        borderWidth: 0,
      },

      "&:hover .MuiOutlinedInput-notchedOutline, & .MuiOutlinedInput-notchedOutline":
        {
          borderColor: "rgba(0, 0, 0, 0);",
        },
      "&.Mui-focused .MuiOutlinedInput-notchedOutline": {
        borderColor: theme.palette.primary.main,
        borderWidth: 2,
      },
      "&.Mui-error .MuiOutlinedInput-notchedOutline": {
        borderColor: theme.palette.error.main,
        borderWidth: 2,
      },
    },
  }),
);

export default SettingsPhoneInput;
