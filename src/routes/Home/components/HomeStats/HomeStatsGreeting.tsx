import Box from "@mui/material/Box";
import React from "react";
import {HomeStatsGreetingAbstract} from "./HomeStatsGreetingAbstract";
import {HomeStatsGreetingImage} from "./HomeStatsGreetingImage";

const HomeStatsGreeting = () => {
  return (
    <Box
      sx={{
        position: "relative",
        width: "300px",
        height: "174px",
      }}
    >
      <HomeStatsGreetingImage />
      <HomeStatsGreetingAbstract />
    </Box>
  );
};

export default HomeStatsGreeting;
