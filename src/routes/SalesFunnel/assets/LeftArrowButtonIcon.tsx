import React from "react";

export const LeftArrowButtonIcon = () => (
  <svg
    width="20"
    height="20"
    viewBox="0 0 20 20"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <rect width="20" height="20" rx="4" fill="#F6F8FA" />
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M11.6759 6.27314C11.3716 6.00333 10.8971 6.02139 10.6161 6.31349L7.56641 9.48297C7.12816 9.93844 7.15878 10.6491 7.63472 11.0682L10.66 13.7326C10.9652 14.0014 11.4396 13.9818 11.7197 13.6888C11.9997 13.3958 11.9792 12.9403 11.674 12.6715L8.89522 10.2242L11.7179 7.29058C11.999 6.99848 11.9802 6.54296 11.6759 6.27314Z"
      fill="#1A06F9"
    />
  </svg>
);
