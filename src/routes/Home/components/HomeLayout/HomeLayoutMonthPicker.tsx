import React from "react";
import Box from "@mui/material/Box";
import {useTheme} from "@mui/material/styles";
import {useHomeContext} from "../../state/HomeContextProvider";
import CartagMonthPicker from "../../../../components/CartagMonthPicker";

const HomeLayoutMonthPicker = () => {
  const {globalDate, setGlobalDate, rangeType, setRangeType} = useHomeContext();
  const theme = useTheme();
  return (
    <Box
      sx={{
        width: "250px",
        [theme.breakpoints.down("sm")]: {
          marginTop: 20,
        },
      }}
    >
      <CartagMonthPicker
        rangeType={rangeType}
        setRangeType={setRangeType}
        date={globalDate}
        onDateChanged={setGlobalDate}
      />
    </Box>
  );
};

export default HomeLayoutMonthPicker;
