import createStyles from "@mui/styles/createStyles";
import makeStyles from "@mui/styles/makeStyles";

export const useSalesFunnelCarItemSelectedStyles = makeStyles(() =>
  createStyles({
    carItemBody: {
      background: "#1A06F9 !important",
    },
    stockNumber: {
      color: "white !important",
    },
    carItemPercent: {
      color: "white !important",
    },
    carNameTitle: {
      color: "white !important",
    },
  }),
);

export const useSalesFunnelCarItemStyles = makeStyles(() =>
  createStyles({
    pointer: {
      cursor: "pointer",
    },
    carItemBody: {
      transition: "all 0.15s ease-in-out",
      background: "#FFFFFF",
      boxShadow: "0px 4px 20px rgba(203, 205, 217, 0.4)",
      borderRadius: "10px",
      paddingTop: 9,
      paddingRight: 12,
      paddingLeft: 12,
      // paddingBottom: 25,

      marginTop: 17,
      marginBottom: 17,

      width: 186,
      flexShrink: 0,
      marginLeft: 18,
      "&:first-item": {
        marginLeft: 30,
      },
    },

    carItemPercent: {
      fontStyle: "normal",
      fontWeight: 600,
      fontSize: "14px",
      lineHeight: "18.2px",
      display: "flex",
      alignItems: "center",
      textAlign: "center",
      color: "#222238",
    },

    carNameTitle: {
      fontStyle: "normal",
      fontWeight: 600,
      fontSize: "16px",
      lineHeight: "25.6px",
      color: "#222238",
    },
    confirmationDropDownText: {
      fontStyle: "normal",
      fontWeight: 600,
      fontSize: "14px",
      lineHeight: "130%",
      color: "#222238",
      paddingTop: "14px",
      paddingLeft: "16px",
    },
    stockNumber: {
      whiteSpace: "nowrap",
      overflow: "hidden",
      textOverflow: "ellipsis",

      fontStyle: "normal",
      fontWeight: "normal",
      fontSize: "14px",
      lineHeight: "19.25px",
      color: "#98A2B7",
      marginBottom: 16,
    },
  }),
);

export const useSalesFunnelCarItemSpecialStyles = makeStyles(() =>
  createStyles({
    carItemBody: {
      background: "rgb(237, 235, 255) !important",
      border: "1.5px dashed #1A06F9",
      boxShadow: "none !important",
      paddingTop: "12px !important",
      minHeight: 100,
    },
    text: {
      fontStyle: "normal",
      fontWeight: "normal",
      fontSize: "14px",
      lineHeight: "18.2px",
      textAlign: "center",
      color: "#1A06F9",
    },
  }),
);
