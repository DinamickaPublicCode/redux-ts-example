import React from 'react';
import Box from "@mui/material/Box";
import { ComponentStory, ComponentMeta } from '@storybook/react';
import HomePerformanceCarousel from './HomePerformanceCarousel';

export default {
  title: 'Bar chart',
  component: HomePerformanceCarousel,
} as ComponentMeta<typeof HomePerformanceCarousel>;


const Template: ComponentStory<typeof HomePerformanceCarousel> = (args) => <HomePerformanceCarousel {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  rawData: [[-1,-1],[-1,-1],[20,-1],[-1,50],[-1,-1],[-1,-1],[-1,-1],[-1,-1],[30,-1],[50,-1],[40,-1],[30,-1],[-1,-1],[-1,57],[96,-1],[-1,-1]],
}
