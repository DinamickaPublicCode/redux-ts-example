import React, {ChangeEvent, useMemo} from "react";
import ArrowDropDown from "@mui/icons-material/ArrowDropDown";
import PhotoCamera from "@mui/icons-material/PhotoCamera";
import {useTranslation} from "react-i18next";
import createStyles from "@mui/styles/createStyles";
import makeStyles from "@mui/styles/makeStyles";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import {FinanceTabType} from "src/types";
import StockCarInfoReadiness from "./StockCarInfoReadiness";
import {Theme} from "@mui/material/styles";

const StockCarInfoBlock = ({
  currentDeal,
}: {
  currentDeal: FinanceTabType | null;
}) => {
  const classes = useCarInfoStyles();
  const {t} = useTranslation();

  const {referenceStock, carName, carModel, carColor, stageType, icons} =
    useMemo(() => {
      return {
        referenceStock: currentDeal?.referenceStock,
        carName: currentDeal?.generalInfo?.make,
        carModel: currentDeal?.generalInfo?.model,
        carColor: currentDeal?.generalInfo?.color,
        stageType: currentDeal?.stageInfo?.stage,
        icons: currentDeal?.stageInfo?.icons,
      };
    }, [currentDeal]);

  const handleCarImgChange = (e: ChangeEvent<HTMLInputElement>) => {
    if (!e.target.files?.length) return;
    const {
      validity,
      files: [file],
    } = e.target;
    if (file.size > 8000000) {
      return; /*enqueueSnackbar("Maximum file size is 8MB", { variant: "error" });*/
    }
    const avatarPreview = URL.createObjectURL(file);
    console.log(avatarPreview);
    if (validity.valid) {
      // setAvatarEdited({ preview: avatarPreview, file });
    }
    // setOpenAvatarDialog(true);
  };

  return (
    <Grid container className={classes.infoWrapper}>
      <Grid item xs={3} sm={2}>
        <input
          accept="image/*"
          style={{
            display: "none",
          }}
          id="icon-button-file"
          type="file"
          onChange={handleCarImgChange}
        />
        <label htmlFor="icon-button-file">
          <IconButton
            color="default"
            component="span"
            className={classes.imageUploader}
            classes={{
              // eslint-disable-next-line @typescript-eslint/ban-ts-comment
              // @ts-ignore
              label: classes.imageUploaderLabel,
            }}
            size="large"
          >
            <PhotoCamera className={classes.imageUploaderIcon} />
            <Typography
              style={{
                fontSize: 10,
              }}
              variant="body2"
            >
              {t("Upload")}
            </Typography>
          </IconButton>
        </label>
      </Grid>
      <Grid item xs={12} sm={6}>
        <Typography
          variant="subtitle1"
          component="p"
          style={{
            display: "flex",
            alignItems: "center",
            fontWeight: 600,
            fontSize: 18,
          }}
        >
          {carName}{" "}
          <ArrowDropDown
            style={{
              marginLeft: 5,
              color: "#CBD1DD",
            }}
          />
        </Typography>
        <Box className={classes.infoTextBox} sx={{display: "flex"}}>
          <Typography
            className={classes.infoText}
            variant="body1"
            color="textSecondary"
          >
            {t("DashboardTableHead.referenceStock")}{" "}
            <span
              style={{
                textTransform: "capitalize",
                color: "#000",
              }}
            >
              {referenceStock && referenceStock}
            </span>
          </Typography>
          <Typography
            className={classes.infoText}
            variant="body1"
            color="textSecondary"
          >
            {t("DashboardTableHead.color")}{" "}
            <span
              style={{
                textTransform: "capitalize",
                color: "#000",
              }}
            >
              {carColor && carColor}
            </span>
          </Typography>
          <Typography
            className={classes.infoText}
            variant="body1"
            color="textSecondary"
          >
            {t("DashboardTableHead.model")}{" "}
            <span
              style={{
                textTransform: "capitalize",
                color: "#000",
              }}
            >
              {carModel && carModel}
            </span>
          </Typography>
        </Box>
      </Grid>
      <Grid
        item
        xs={4}
        style={{
          display: "flex",
          alignItems: "center",
        }}
      >
        <Typography className={classes.readinessText} variant="body2">
          {t("DashboardTableHead.itemReadiness")}
        </Typography>
        <StockCarInfoReadiness icons={icons} stageType={stageType} />
      </Grid>
    </Grid>
  );
};

export const useCarInfoStyles = makeStyles((theme: Theme) =>
  createStyles({
    infoWrapper: {
      backgroundColor: "#F6F8FA",
      padding: "15px 30px",
      alignItems: "center",
    },
    imageUploader: {
      padding: 0,
      backgroundColor: theme.palette.common.white,
      boxShadow: "0px 4px 12px rgba(203, 205, 217, 0.25)",
      "& $imageUploaderLabel": {
        width: 68,
        height: 68,
        display: "flex",
        flexDirection: "column",
      },
    },
    imageUploaderLabel: {},
    imageUploaderIcon: {
      width: 19,
      height: 19,
      color: theme.palette.action.disabled,
    },
    infoText: {
      marginRight: 10,
      textTransform: "uppercase",
      "&::after": {
        content: "'\\2022'",
        marginLeft: 10,
        color: theme.palette.primary.main,
      },
      "&:last-child": {
        "&::after": {
          content: "''",
        },
      },
    },
    readinessText: {
      fontSize: 13,
      fontWeight: 600,
      textTransform: "uppercase",
      color: theme.palette.action.disabled,
      marginRight: 15,
      // height: "max-content",
    },
    infoTextBox: {
      display: "flex",
      [theme.breakpoints.down("sm")]: {
        flexDirection: "column",
      },
    },
  }),
);

export default StockCarInfoBlock;
