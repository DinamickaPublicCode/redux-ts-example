import {DocumentInfo} from ".";

export type OnLoadingStateChangedCallback = (isLoading: boolean) => void;

export type DocumentBoxStyling = {
  hasTopPadding?: boolean;
  /** left and right padding added to the block. Defaults to 35 */
  edgeInsets?: number;
};

/**
 * A generic callback for handling document types actions
 * It relies on modifying the "documents" array which comes into
 * the DocumentBoxProvider
 */
export type DocumentBoxActionCallback = (
  documentInfo: DocumentInfo,
  onLoadingStateChanged: OnLoadingStateChangedCallback,
) => void;

export type DocumentInputFieldData = {
  validity: {valid: boolean};
  files: FileList;
};

export type DocumentBoxFileActionCallback = (
  documentId: DocumentInfo,
  file: DocumentInputFieldData,
  onLoadingStateChanged: OnLoadingStateChangedCallback,
  onComplete: () => void,
) => void;

export type DocumentBoxActionHandlers = {
  /**
   * This happens when user is clicked on the documents
   * Supports async behavior in-case the documents needs to be preloaded with
   * a specific authentication token.
   */
  onDownload: DocumentBoxActionCallback;

  /**
   * No-one knows for now how this could be handled, supports async behavior
   */
  onUploadFile: DocumentBoxFileActionCallback;

  /**
   * Async support, since supposedly the docs are cleaned up on server
   */
  onDeleteButtonClicked: DocumentBoxActionCallback;
};

export interface DocumentBoxContextData {
  documents: DocumentBoxEntries | null;
  actions: DocumentBoxActionHandlers;
}

// TODO: Remove this
export type DocumentsApiData = {
  files: {
    id: string;
    type: string;
    name: string;
  }[];
};

export enum DocumentFileTypes {
  PHOTO = "PHOTO",
  INSPECTION = "INSPECTION",
  CAR_PROOF = "CAR_PROOF",
  GARANTIE = "GARANTIE",
  HISTO = "HISTO",
}

export type DocumentBoxEntryFile = {
  id: string;
  name: string;
  url: string;
};

export type DocumentBoxEntryCategory = {
  type: DocumentFileTypes;
  file?: DocumentBoxEntryFile;
};
export type DocumentBoxEntries = {
  categories: DocumentBoxEntryCategory[];
};

export type DocumentEntryFile = {
  id: string;
  isImage?: boolean;
  name: string;
  url: string;
};

export type DocumentEntryData = {
  type: DocumentFileTypes;
  localeFriendlyTypeString: string;
  file?: DocumentEntryFile;
};

export type DocumentBoxCategory = {
  name: string;
  documents: DocumentEntryData[];
};
