import Box from "@mui/material/Box";
import Tabs from "@mui/material/Tabs";
import Typography from "@mui/material/Typography";
import React, {useCallback} from "react";
import {useSalesFunnelRowStyles} from "../../utils/salesFunnelRowStyles";
import {CarDealerSeller} from "src/types";
import {requestGeneratedAvatar} from "src/utils/avatarGenerator";
import SalesFunnelItem from "../SalesFunnelItem";
import {useTranslation} from "react-i18next";
import {SellerCarEntry} from "src/routes/SalesFunnel/types/apiDataProvider";
import {
  useSalesFunnelCarItemSpecialStyles,
  useSalesFunnelCarItemStyles,
} from "../../utils/salesFunnelCarItemStyles";
import clsx from "clsx";
import KingIconOutlined from "../../assets/KingIconOutlined";
import chunk from "lodash/chunk";

export const UserProfile = ({person}: {person: CarDealerSeller}) => {
  const styles = useSalesFunnelRowStyles();

  return (
    <Box
      display="flex"
      flexDirection="column"
      justifyContent="center"
      alignItems="center"
      padding="28px"
      height="138px"
      style={{
        backgroundColor: "#F6F8FA",
      }}
    >
      <Box
        className={styles.avatar}
        style={{
          backgroundImage: `url(${requestGeneratedAvatar(
            person.id,
            person.fullName,
          )})`,
          backgroundSize: "cover",
        }}
      ></Box>
      <Box className={styles.userName}>{person.fullName}</Box>
    </Box>
  );
};

export const UserStats = ({
  currentGoal,
  carsSold,
  totalYear,
}: {
  currentGoal: number;
  carsSold: number;
  totalYear: number;
}) => {
  const styles = useSalesFunnelRowStyles();
  const {t} = useTranslation();
  return (
    <Box className={styles.userStats}>
      <Box
        display="flex"
        width="100%"
        flexDirection="row"
        alignItems="center"
        justifyContent="space-around"
        className={styles.soldGoal}
      >
        <Box display="flex" flexDirection="column" alignItems="center">
          <Typography className={styles.statsFont}>
            {t("Sales_Funnel.Sold")}
          </Typography>
          <Typography className={styles.statsFontBold}>
            {formatNum(carsSold)}
          </Typography>
        </Box>
        <Box display="flex" flexDirection="column" alignItems="center">
          <Typography className={styles.statsFont}>
            {t("Sales_Funnel.Goal")}
          </Typography>
          <Typography
            style={{
              color: "#98A2B7",
            }}
            className={styles.statsFontBold}
          >
            {formatNum(currentGoal)}
          </Typography>
        </Box>
      </Box>
      <Box
        width="100%"
        display="flex"
        flexDirection="column"
        alignItems="center"
        justifyContent="center"
      >
        <Box
          display="flex"
          flexDirection="row"
          alignItems="center"
          className={styles.totalValue}
          justifyContent="space-around"
          width="100%"
        >
          <Box display="flex" flexDirection="column" alignItems="center">
            <Typography className={styles.statsFont}>
              {t("Sales_Funnel.Total_Value.Total")}
            </Typography>
            <Typography className={styles.statsFont}>
              {t("Sales_Funnel.Total_Value.Value")}
            </Typography>
          </Box>
          <Box display="flex" flexDirection="column" alignItems="center">
            <Typography className={styles.statsFontBold}>
              {formatNum(totalYear)}
            </Typography>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export const ScrollableItemsList = ({
  idUser,
  cars,
  carsSold,
  currentGoal,
}: {
  idUser: string;
  cars: SellerCarEntry[];
  carsSold: number;
  currentGoal: number;
}) => {
  const styles = useSalesFunnelRowStyles();
  const hasCompletedTheGoal = carsSold >= currentGoal;
  const numberOfEmptyBoxes = currentGoal - carsSold - 1;

  const renderEmptyBoxes = useCallback(() => {
    const elements = [];
    for (let x = 0; x < numberOfEmptyBoxes; x++) {
      elements.push(<LastSaleToComplete key={x} isTransparent={true} />);
    }
    return elements;
  }, [numberOfEmptyBoxes]);
  return (
    <Tabs
      scrollButtons={true}
      value={0}
      variant="scrollable"
      className={styles.scrollableItemsList}
    >
      {cars.map((car, index) => (
        <SalesFunnelItem
          /** Using complex key (which might be faulty in theory, but that should totally prevent
           * same-key errors).
           **/
          key={car.sale.id + idUser + index}
          idUser={idUser}
          car={car}
          isKing={index === currentGoal - 1 && hasCompletedTheGoal}
        />
      ))}
      {renderEmptyBoxes()}
      {!hasCompletedTheGoal && <LastSaleToComplete />}
      <LastSaleToComplete isTransparent={true} />
    </Tabs>
  );
};

export const LastSaleToComplete = ({
  isTransparent,
}: {
  isTransparent?: boolean;
}) => {
  const styles = useSalesFunnelCarItemStyles();
  const specialStyles = useSalesFunnelCarItemSpecialStyles();
  const {t} = useTranslation();
  return (
    <Box
      className={clsx(styles.carItemBody, specialStyles.carItemBody)}
      style={isTransparent ? {opacity: 0} : {}}
    >
      <Box
        display="flex"
        flexDirection="column"
        alignItems="center"
        justifyContent="center"
      >
        <Box marginBottom="8px">
          <KingIconOutlined />
        </Box>
        <Typography component="div" className={specialStyles.text}>
          {/**
           * This is ridiculously odd. But the line breaking appears to not work at all
           * TODO: needs more investigation.
           */}
          {chunk(t("The last sale to complete your goal").split(" "), 4).map(
            (x, idx) => (
              <React.Fragment key={idx}>
                <span>{x.join(" ")}</span>
                <br />
              </React.Fragment>
            ),
          )}
        </Typography>
      </Box>
    </Box>
  );
};

const formatNum = (num: number) =>
  ("" + num).length === 1 ? "0" + num.toString() : num.toString();
