import React, {useState} from "react";

import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Header from "src/components/Header";
import {CarTypes} from "src/types";
import SalesMonthCalendar from "./SalesMonthCalendar";
import StockDetailModal from "src/components/StockDetailModal/StockDetailModal";
import SalesCalendarTitle from "./SalesCalendarTitle";
import SalesCalendarFilter from "./SalesCalendarFilter";
import SalesCalendarPerDayView from "./SalesCalendarPerDayView";
import {useSalesCalendarHandlers} from "../utils/useSalesCalendarHandlers";
import DeliveryAddModal from "src/components/DeliveryAddModal";
import DeliveryUpdateModal from "src/components/DeliveryUpdateModal";

const SalesCalendarLayout = () => {
  const [currentDate, setCurrentDateChanged] = useState(() => new Date());
  const [typeOfCalendar, setTypeOfCalendar] = useState<"DAILY" | "MONTHLY">(
    "DAILY",
  );
  const [currentCarType, setCurrentCarType] = useState<CarTypes>("ALL");

  const {
    onGlobalSearch,
    //
    activeDay,
    setActiveDay,
    //
    deliveriesPerDay,
    changeActiveDate,
    currentSale,
    filteredCars,
    changeCurrentCarInfo,
    //
    //
    setCurrentSalesPerDay,
    setCurrentSale,
  } = useSalesCalendarHandlers(currentDate, typeOfCalendar, currentCarType);

  /** Modal states */
  const [addNewDeliveryOpen, setAddNewDeliveryOpen] = useState(false);
  const [openStockWithChat, setOpenStockWithChat] = useState<boolean>(false);
  const [updateNewDeliveryOpen, setUpdateNewDeliveryOpen] = useState(false);

  return (
    <>
      <Header />

      <Container
        maxWidth="xl"
        style={{
          height: "100%",
        }}
      >
        <SalesCalendarTitle
          onGlobalSearch={onGlobalSearch}
          onSetCurrentDateChanged={setCurrentDateChanged}
          currentDate={currentDate}
          typeOfCalendar={typeOfCalendar}
          onTypeOfCalendarChange={setTypeOfCalendar}
        />
        <Box display="flex">
          <SalesCalendarFilter
            currentCarType={currentCarType}
            onSetCurrentCarType={setCurrentCarType}
            onSetAddNewDeliveryOpen={setAddNewDeliveryOpen}
          />

          {typeOfCalendar === "DAILY" ? (
            <SalesCalendarPerDayView
              activeDay={activeDay}
              deliveriesPerDay={deliveriesPerDay}
              changeActiveDate={changeActiveDate}
              currentSale={currentSale}
              filteredCars={filteredCars}
              changeCurrentCarInfo={changeCurrentCarInfo}
              onOpenUpdateNewDelivery={setUpdateNewDeliveryOpen}
              setOpenStockWithChat={setOpenStockWithChat}
            />
          ) : (
            <SalesMonthCalendar
              deliveriesPerDay={deliveriesPerDay}
              onOpenUpdateDeliv={setUpdateNewDeliveryOpen}
              setActiveDay={setActiveDay}
              setCurrentSalesPerDay={setCurrentSalesPerDay}
              setTypeOfCalendar={setTypeOfCalendar}
              setCurrentSale={setCurrentSale}
            />
          )}
        </Box>
        <div>
          <SalesCalendarFilter
            currentCarType={currentCarType}
            onSetCurrentCarType={setCurrentCarType}
            onSetAddNewDeliveryOpen={setAddNewDeliveryOpen}
          />

          {typeOfCalendar === "DAILY" ? (
            <SalesCalendarPerDayView
              activeDay={activeDay}
              deliveriesPerDay={deliveriesPerDay}
              changeActiveDate={changeActiveDate}
              currentSale={currentSale}
              filteredCars={filteredCars}
              changeCurrentCarInfo={changeCurrentCarInfo}
              onOpenUpdateNewDelivery={setUpdateNewDeliveryOpen}
              setOpenStockWithChat={setOpenStockWithChat}
            />
          ) : (
            <SalesMonthCalendar
              deliveriesPerDay={deliveriesPerDay}
              onOpenUpdateDeliv={setUpdateNewDeliveryOpen}
              setActiveDay={setActiveDay}
              setCurrentSalesPerDay={setCurrentSalesPerDay}
              setTypeOfCalendar={setTypeOfCalendar}
              setCurrentSale={setCurrentSale}
            />
          )}
        </div>
        <DeliveryAddModal
          isOpen={addNewDeliveryOpen}
          onIsOpenChanged={setAddNewDeliveryOpen}
        />
        <DeliveryUpdateModal
          carDealerSale={currentSale}
          isOpen={updateNewDeliveryOpen}
          onIsOpenChanged={setUpdateNewDeliveryOpen}
        />
        <StockDetailModal
          open={openStockWithChat}
          handleClose={() => setOpenStockWithChat(false)}
          currentDealId={currentSale?.id}
        />
      </Container>
    </>
  );
};

export default SalesCalendarLayout;
