import {createContext} from "react";
import {FinanceTabType} from "../../types";

export const StockPreparationTabContext = createContext({
  deal: null as null | FinanceTabType,
});
