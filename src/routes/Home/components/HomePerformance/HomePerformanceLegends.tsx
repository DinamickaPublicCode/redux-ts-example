import Box from "@mui/material/Box";
import {LEGEND_COLORS} from "./HomePerformance";
import React from "react";
import HomePerformanceLegendsCircle from "./HomePerformanceLegendsCircle";

const HomePerformanceLegends = ({labels}: {labels: string[]}) => {
  return (
    <Box
      sx={{
        display: "flex",
        alignItems: "center",
      }}
    >
      {labels.map((label, index) => (
        <HomePerformanceLegendsCircle
          key={index}
          htmlColor={LEGEND_COLORS[index]}
        >
          {label}
        </HomePerformanceLegendsCircle>
      ))}
    </Box>
  );
};

export default HomePerformanceLegends;
