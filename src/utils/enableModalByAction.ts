export const enableModalByAction = (action: string | undefined) => {
  if (action === "WHOLESALE") {
    return true;
  }
  if (action === "OUTSOURCING") {
    return true;
  }
  if (action === "ARCHIVE") {
    return true;
  }
  if (action === "SELL_AS_IS") {
    return true;
  }
  if (action === "COUNT") {
    return true;
  }
  if (action === "BACK_FROM_OUTSC") {
    return true;
  }
  if (action === "CANCEL_SALE") {
    return true;
  }
  if (action === "MARK_AS_DONE") {
    return true;
  }
  return action === "PREPARE_ON_DELIV";
};
