import Box from "@mui/material/Box";
import React from "react";

export const SpaceX = ({size}: {size: number}) => <Box sx={{width: size}} />;
