import {SalesFunnelDataHandlers} from "./apiDataProvider";

export type CarSelection = {
  sellerId: string;
  inventoryId: string;
};

export type CarFilter = "USED" | "NEW";

export type SalesFunnelContextData = {
  chatInventoryId: string | null;
  onChatInventoryIdChanged: (value: string | null) => void;

  currentDate: Date;
  onCurrentDateChanged: (date: Date) => void;

  carFilter: CarFilter;
  onCarFilterChanged: (value: CarFilter) => void;

  searchText: string;
  onSearchTextChanged: (searchText: string) => void;

  dataHandlers: SalesFunnelDataHandlers;

  onShowSetGoalsModal: () => void;

  carSelection: CarSelection | null;
  setCarSelection: (selection: CarSelection | null) => void;
};
