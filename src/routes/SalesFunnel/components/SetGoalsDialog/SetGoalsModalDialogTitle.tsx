import {useCommonModalStyles} from "src/utils/useCommonModalStyles";
import {useSetGoalsModalTranslation} from "../../utils/useSetGoalsModalTranslation";
import GenericFormModalTitle from "../../../../components/GenericFormModalTitle";
import MonthPicker from "../../../../components/MonthPicker";
import React from "react";

const SetGoalsModalDialogTitle = ({
  date,
  setDate,
  handleClose,
}: {
  date: Date;
  setDate: (d: Date) => void;
  handleClose: () => void;
}) => {
  const styles = useCommonModalStyles();
  const {t} = useSetGoalsModalTranslation();
  return (
    <div className={styles.setGoalsMonthPickerBox}>
      <GenericFormModalTitle
        title={t("Set goals for your sales team")}
        handleClose={handleClose}
      >
        <div className={styles.monthPickerHead}>
          <MonthPicker date={date} onDateChanged={setDate} />
        </div>
      </GenericFormModalTitle>
    </div>
  );
};
export default SetGoalsModalDialogTitle;
