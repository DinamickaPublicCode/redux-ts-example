import React, {useEffect} from "react";
import {useAppSelector} from "src/state/hooks";
import {useDispatch} from "react-redux";

import {getNotifications} from "src/state/reducers/notifications";
import useConfigureInterceptors from "src/utils/useConfigureInterceptors";
import {useGetStatisticsQuery} from 'src/state/reducers/statisticsApi';
import {useGetNotificationsQuery} from 'src/state/reducers/notificationsApi';

const StateInitializer: React.FC<{children: React.ReactNode}> = ({
  children,
}) => {
  const token = useAppSelector((state) => state.token.value);
  useConfigureInterceptors();

  useGetStatisticsQuery('mechanic', { skip: !token });
  useGetStatisticsQuery('inventory', { skip: !token });
  useGetStatisticsQuery('esthetic', { skip: !token });
  useGetStatisticsQuery('delivery', { skip: !token });

  useGetNotificationsQuery(null, { skip: !token });

  return <>{children}</>;
};

export default StateInitializer;
