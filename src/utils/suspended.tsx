import CartagBackdrop from "src/components/CartagBackdrop";
import React from "react";
import {Suspense} from "react";

export interface SuspendedOptions {
  nosuspense?: boolean;
  fallback?: React.ReactNode;
}

/**
 * A helper function that creates a React component.
 * May wrap or not wrap a component with a default loader to prevent errors
 */
const suspended = function <T>(
  factory: () => Promise<{default: React.ComponentType<T>}>,
  options?: SuspendedOptions,
) {
  return () => {
    return !options?.nosuspense ? (
      <Suspense fallback={options?.fallback ?? <CartagBackdrop open={true} />}>
        {React.createElement(React.lazy(factory))}
      </Suspense>
    ) : (
      React.createElement(React.lazy(factory))
    );
  };
};
export default suspended;
