import React, {useEffect, useState} from "react";
import Box from "@mui/material/Box/Box";
import Fade from "@mui/material/Fade";
import HomeLayoutOptions from "./HomeLayoutOptions";
import HomeSplash from "../HomeSplash";
import HomePerformance from "../HomePerformance/HomePerformance";
import HomeBottom from "../HomeBottom";
import HomeStats from "../HomeStats";
import {useHomeContext} from "../../state/HomeContextProvider";
import HeaderLayout from "../../../../components/Header/HeaderLayout";

const SHOW_SPLASH_SCREEN = true;

const HomeLayout = () => {
  const [show, setShow] = useState(true);
  const {drawerOpen, setDrawerOpen} = useHomeContext();

  useEffect(() => {
    //setTimeout(() => setShow(true), 100);
  }, []);
  return (
    <>
      <HeaderLayout drawerOpen={drawerOpen} setDrawerOpen={setDrawerOpen} />
      {
        <>
          {SHOW_SPLASH_SCREEN ? (
            <HomeSplash />
          ) : (
            <Box
              style={{
                height: 18,
              }}
            />
          )}
          <Fade in={show}>
            <div>
              {show ? (
                <>
                  <HomeLayoutOptions />
                  <Box
                    sx={{
                      p: "20px",
                    }}
                  >
                    <HomeStats />
                    <HomePerformance />
                    <HomeBottom />
                  </Box>
                  {/** A vertical space for the footer */}
                  <Box
                    style={{
                      height: 24,
                    }}
                  />
                </>
              ) : null}
            </div>
          </Fade>
        </>
      }
    </>
  );
};
export default HomeLayout;
