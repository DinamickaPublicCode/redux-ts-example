export const queryArrayFromString = (query: string) => {
  return query.slice(query.indexOf("?") + 1).split("&");
};
