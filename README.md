# Redux ts example (update)

Here's the folder structure enfornced. Some fancy things like React.lazy/suspense code-splitting were tested

## Refactor notes:

- react is updated to v18. Changed props interfaces and initialization
- react router is changed to v6. useLocation/useNavigate and main routing is altered
- garbage dependencies were removed, some are replaced
- updated eslint rules to check for any types
- baseUrl was changed to . This helps with migrating the codebase to other monorepos that may not play well with baseUrls. Having src/ as a base path is much quicker to refactor and resolve
- moment was replaced with dayjs. Never use moment on frontend
- index.ts is only reserved for forwarding exports. Large codebases with index source files get unmanageable when it's necessary to quickly browse or find anything

## Package manager

`npm` is de-facto default package manager, but it has its problems. It's not the fastest at resolving dependencies, and it fails to resolve sometimes conflicting versions.

`yarn` is an improvement, but the new versions (>2) are not applicable to general use cases since it relies on additional tooling to get it working. Classic version (v1) is not often updated.

`pnpm` is an interesting option, it uses different approach in placing dependencies inside node_modules, that makes impossible to use implicitly installed dependencies. That only introduces problems on legacy codebases by requiring the developer to add missing libraries.

## Note on dependencies

### Maintenance

Installing any new library requires maintenance from developer team, so think twice if it's necessary to install a new set of dependencies or write the necessary code by yourself.

`react-` prefixed dependencies are often abandoned after a while (this is especially true for RN development).

### Bundle size optimization

Some libraries are quite unwanted like `lodash` or `moment`. Having a default import of lodash in the codebase adds 500kb to the bundle so be careful:

```
██████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
file:            /node_modules/.pnpm/lodash@4.17.21/node_modules/lodash/lodash.js
bundle space:    13.17 %
rendered size:   544.017 KB
original size:   544.096 KB
code reduction:  0.01 %
dependents:      8
  - /src/components/CartagMonthPicker/CartagMonthPickerView.tsx
  - /src/components/CartagMonthPicker/CartagMonthPickerBody.tsx
  - /src/routes/Home/components/HomePerformance/HomePerformanceVerticalAxis.tsx
  - /src/routes/Home/components/HomePerformance/HomePerformanceCarousel.tsx
  - /src/routes/Home/components/HomeStats/HomeStatsBlocks.tsx
  - /src/routes/SalesFunnel/utils/useCarDealerSalesStats.ts
  - /src/routes/SalesFunnel/components/SalesFunnelContent/SalesFunnelRow.tsx
  - /src/components/StockDetailModal/StockPreparationTabForm.tsx
```

The safest way is to use exact imports

```
import range from "lodash/range";
```

Same goes for _Material UI_:

**Prefer:**

```
import IconButton from "@mui/material/IconButton";
import Paper from "@mui/material/Paper";
import InputBase from "@mui/material/InputBase";
import SearchIcon from "@mui/icons-material/Search";
```

over

```
import {IconButton, Paper, InputBase, SearchIcon} from "@mui/material";
```

By using these approaches we get a uniform code splitting where each module takes 1% of the codebase or less. Except react-beautiful-dnd

## Docker image

A self-contained Dockerfile is added to quickly test and run the app on port 80. It uses nginx as a server to serve static files. This should be used as a reference for creating buildable image for CI/CD.

## Issues left to resolve

### Styling engine

We need to refactor the styling from old JSS (makeStyles) to emotion (styled). JSS in Material UI is considered legacy and should be removed, but that's tedious to do, so some help is needed. **Some components are already ported to styled (like DashboardTable)**

### Messy codebase

Finish **de-spaghettifying the code**. The codebase initally was riddled with cross-referenced modules, and it was hard to understand what comes from where. Mostly everything is moved to correct places and many helper functions are separated into separate files, however it's not perfect, we still need to inspect each file to see if it exports some random type imported in some other place from a completely unrelated feature.

The codebase is not very large but that's something for a complete example:

```
github.com/AlDanial/cloc v 1.92  T=0.35 s (1037.8 files/s, 94207.3 lines/s)
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
TypeScript                     359           1787            850          30184
JavaScript                       3             11             20             70
JSX                              1              5              0             24
-------------------------------------------------------------------------------
SUM:                           363           1803            870          30278
-------------------------------------------------------------------------------
```

### More state management examples

Migrate to **redux toolkit**. Thunk-based projects has proved to be harder to maintain while toolkit ones are well accepted by the team (I hope...)

Recoil would be also cool to try, but we would benefit from more complex/state server mutation example for that library.

### Color dictionary

Move all #hex color strings to the one object that contains the pallete. That's helpful for quickly theme the UI to a different color

### Fix broken styles after partially changing the styling engine

JSS is based on class names, and it's more difficult to translate to the `styled` way of customizing the UI components. Some pages/dialogs (particularly SalesCalendar) are broken because of that.
