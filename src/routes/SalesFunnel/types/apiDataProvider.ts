import {
  CarDealerLegend,
  CarDealerSale,
  CarDealerSaleEntry,
  CarDealerSeller,
  TopSeller,
} from "src/types";
import {CarFilter} from "./salesFunnelContext";

export interface SalesFunnelDataProviderOptions {
  carType: CarFilter;
  date: Date;
  searchText: string;
  idCarDealer?: string;
}

export interface SellerCarEntry {
  sale: CarDealerSale;
  reference: CarDealerSaleEntry;
}

export type SellersListEntry = {
  seller: CarDealerSeller;
  cars?: SellerCarEntry[];
  totalYear: number;
};

export interface SalesFunnelDataProviderInputs {
  legends: CarDealerLegend[] | undefined;
  idCarDealer: string | undefined;
  saleDateBegin: string;
  saleDateEnd: string;
  carType: CarFilter;
  searchText: string | undefined;
}

export enum SaleReferenceAction {
  CARRY_OVER = "CARRY_OVER",
  COUNT = "COUNT",
  CANCEL_SALE = "CANCEL_SALE",
  UPDATE_SALE = "UPDATE_SALE",
}

export interface SaleRefenceUpdateParams {
  action: SaleReferenceAction;
  legends?: string[];
  saleDate?: string;
}

export interface SalesFunnelStats {
  soldTotal: number;
  goalTotal: number;
  goalCompletion: number;
}

export interface SalesFunnelDataHandlers {
  legends: CarDealerLegend[] | undefined;
  topSellers: TopSeller[] | undefined;
  sellersList: SellersListEntry[] | undefined;
  stats: SalesFunnelStats;
  onSellersOrderChanged: (sellersOrderedIndices: {
    [index: string]: number;
  }) => Promise<void>;
  onSaleRefenceUpdated: (
    idInventory: string,
    idUser: string,
    saleReference: string,
    params: SaleRefenceUpdateParams,
  ) => Promise<void>;
}
