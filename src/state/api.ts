import axios from "axios";
import {
  CarDealer,
  UserType,
  CarDealerSeller,
  TopSeller,
  FinanceTabType,
  StatisticsData,
  PagedRowDataType,
  PagedNotifications,
  PagedCarDealerSale,
  CarDealerSale,
  DepartmentType,
  ApiResponse,
} from "src/types";
import {ActivityBoxApiMessage} from "src/types/activityBoxInterfaces";
import {queryArrayFromString} from "src/utils/queryArrFromString";
import dayjs from "dayjs";

export const baseUrl = process.env.NODE_ENV === "development" ? "" : "";

export const fetchActivityMessages = (): Promise<
  ApiResponse<ActivityBoxApiMessage[]>
> => {
  return axios.get("/mock/events/inventory/messages.json").then((res) => {
    return {
      response: res.data,
    };
  });
};

export const getActivityMessages = (
  idInventory: string,
  page: number,
  size: number,
) => {
  return axios.get("/mock/events/inventory/messages.json");
};

export const searchStockList = (
  idCarDealer: string,
  stockPattern: string | null,
) => {
  return axios
    .get("/mock/inventories/cardealer/carDealer1/inventories.json")
    .then((res) => {
      if (stockPattern) {
        res.data.content = res.data.filter((el: CarDealerSale) =>
          el.referenceStock.startsWith(stockPattern.toUpperCase()),
        );
      }
      return res;
    });
};
