export const defaultStatisticsData = {
  department: "",
  totalDep: 0,
  goalDep: null,
  details: [
    {
      typeAction: "INSPECTION",
      labelAction: "Inspection",
      iconAction: "ins",
      total: 0,
      goal: null,
      evolution: {
        percent: "0",
        data: [
          -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
          -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        ],
        highlights: [-1, -1, -1, -1, -1, -1, -1, -1],
      },
      avg: [
        -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0,
        -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0,
        -1.0, -1.0, -1.0, -1.0, -1.0, -1.0,
      ],
    },
    {
      typeAction: "REPAIR",
      labelAction: "Réparation",
      iconAction: "rep",
      total: 0,
      goal: null,
      evolution: {
        percent: "0",
        data: [
          -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
          -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        ],
        highlights: [-1, -1, -1, -1, -1, -1, -1, -1],
      },
      avg: [
        -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0,
        -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0,
        -1.0, -1.0, -1.0, -1.0, -1.0, -1.0,
      ],
    },
  ],
};
