import baseQuery from "src/state/baseQuery";
import {createApi} from '@reduxjs/toolkit/query/react';
import {setLoading} from './loading';
import {queryArrayFromString} from "src/utils/queryArrFromString";
import dayjs from "dayjs";
import {
  CarDealerSeller,
  FinanceTabType,
  TopSeller,
  UserType,
  CarDealer,
  PagedRowDataType,
  PagedCarDealerSale,
} from "src/types";


export const carDealerApi = createApi({
  reducerPath: 'carDealerApi',
  baseQuery,
  endpoints: (builder) => ({
    getCarDealerSellers: builder.query<CarDealerSeller[], void>({
      query: () => ({
        url: `/mock/users/cardealer/carDealer1/authorities.json`,
        method: 'GET',
      }),
      async onQueryStarted(_, {dispatch, queryFulfilled}) {
        try {
          dispatch(setLoading(true));
          await queryFulfilled;
          dispatch(setLoading(false));
        } catch (error) {
          dispatch(setLoading(false));
        }
      },
    }),
    getCarDealer: builder.query<CarDealer, void>({
      query: () => ({
        url: `/mock/users/cardealer/cardealer1.json`,
        method: 'GET',
      }),
      async onQueryStarted(_, {dispatch, queryFulfilled}) {
        try {
          dispatch(setLoading(true));
          await queryFulfilled;
          dispatch(setLoading(false));
        } catch (error) {
          dispatch(setLoading(false));
        }
      },
    }),
    getCarDealerSalesNew: builder.query<PagedCarDealerSale, void>({
      query: () => ({
        url: `/mock/inventories/cardealer/carDealer1/inventories.json`,
        method: 'GET',
      }),
      transformResponse: (res) => {
        let newData = res.map((el: CarDealerSale) => {
          const currentYear = dayjs().year();
          const currentMonth = dayjs().month();
          el.sales[0].saleDate = dayjs(el.sales[0].saleDate)
            .set("year", currentYear)
            .set("month", currentMonth)
            .toISOString();
          return el;
        });

        newData = res.filter((el: CarDealerSale) => el.generalInfo.newCar);

        return {
          content: newData,
          pageable: {
            sort: {
              sorted: false,
              unsorted: true,
              empty: true,
            },
            offset: 0,
            pageNumber: 0,
            pageSize: 20,
            unpaged: false,
            paged: true,
          },
          totalPages: 1,
          totalElements: newData.length,
          last: true,
          sort: {
            sorted: false,
            unsorted: true,
            empty: true,
          },
          size: 20,
          number: 0,
          numberOfElements: 20,
          first: true,
          empty: false,
        }
      },
      async onQueryStarted(_, {dispatch, queryFulfilled}) {
        try {
          dispatch(setLoading(true));
          await queryFulfilled;
          dispatch(setLoading(false));
        } catch (error) {
          dispatch(setLoading(false));
        }
      },
    }),
    getCarDealerSalesUsed: builder.query<PagedCarDealerSale, void>({
      query: () => ({
        url: `/mock/inventories/cardealer/carDealer1/inventories.json`,
        method: 'GET',
      }),
      transformResponse: (res) => {
        let newData = res.map((el: CarDealerSale) => {
          const currentYear = dayjs().year();
          const currentMonth = dayjs().month();
          el.sales[0].saleDate = dayjs(el.sales[0].saleDate)
            .set("year", currentYear)
            .set("month", currentMonth)
            .toISOString();
          return el;
        });

        newData = res.filter(
          (el: CarDealerSale) => !el.generalInfo.newCar,
        );

        return {
          content: newData,
          pageable: {
            sort: {
              sorted: false,
              unsorted: true,
              empty: true,
            },
            offset: 0,
            pageNumber: 0,
            pageSize: 20,
            unpaged: false,
            paged: true,
          },
          totalPages: 1,
          totalElements: newData.length,
          last: true,
          sort: {
            sorted: false,
            unsorted: true,
            empty: true,
          },
          size: 20,
          number: 0,
          numberOfElements: 20,
          first: true,
          empty: false,
        }
      },
      async onQueryStarted(_, {dispatch, queryFulfilled}) {
        try {
          dispatch(setLoading(true));
          await queryFulfilled;
          dispatch(setLoading(false));
        } catch (error) {
          dispatch(setLoading(false));
        }
      },
    }),
    getTopSellers: builder.query<TopSeller[], void>({
      query: () => ({
        url: `/mock/inventories/top/cardealer/topSellers.json`,
        method: 'GET',
      }),
      async onQueryStarted(_, {dispatch, queryFulfilled}) {
        try {
          dispatch(setLoading(true));
          await queryFulfilled;
          dispatch(setLoading(false));
        } catch (error) {
          dispatch(setLoading(false));
        }
      },
    }),
    getFinanceManagers: builder.query<UserType[], void>({
      query: () => ({
        url: `/mock/users/authoritiesManager.json`,
        method: 'GET',
      }),
      async onQueryStarted(_, {dispatch, queryFulfilled}) {
        try {
          dispatch(setLoading(true));
          await queryFulfilled;
          dispatch(setLoading(false));
        } catch (error) {
          dispatch(setLoading(false));
        }
      },
    }),
    getInventories: builder.query<PagedRowDataType, void>({
      query: () => ({
        url: `/mock/inventories/cardealer/carDealer1/tableAllInventories.json`,
        method: 'GET',
      }),
      transformResponse: (res) => {
        let query = ''; // temp
        let newData = res;
        const dep = queryArrayFromString(query).find((el) =>
          el.startsWith("dep"),
        );
        const stage = queryArrayFromString(query).find((el) =>
          el.startsWith("stage"),
        );
        const depStatus = queryArrayFromString(query).find((el) =>
          el.startsWith("depStatus"),
        );
        const cdtBg = queryArrayFromString(query).find((el) =>
          el.startsWith("cdtBg"),
        );
        const cdtEd = queryArrayFromString(query).find((el) =>
          el.startsWith("cdtEd"),
        );
        const sdtBg = queryArrayFromString(query).find((el) =>
          el.startsWith("sdtBg"),
        );
        const sdtEd = queryArrayFromString(query).find((el) =>
          el.startsWith("sdtEd"),
        );
        const ldtBg = queryArrayFromString(query).find((el) =>
          el.startsWith("ldtBg"),
        );
        const ldtEd = queryArrayFromString(query).find((el) =>
          el.startsWith("ldtEd"),
        );
        const ddtBg = queryArrayFromString(query).find((el) =>
          el.startsWith("ddtBg"),
        );
        const ddtEn = queryArrayFromString(query).find((el) =>
          el.startsWith("ddtEn"),
        );
        if (dep) {
          newData = newData.filter((el: FinanceTabType) =>
            el.departments
              ?.map((depart) => depart.departmentType)
              .includes(dep.split("=")[1]),
          );
        }
        if (stage) {
          newData = newData.filter((el: FinanceTabType) =>
            stage.split("=")[1].split(",").includes(el.stageInfo.stage),
          );
        }
        if (depStatus) {
          newData = newData.filter((el: FinanceTabType) =>
            depStatus
              .split("=")[1]
              .split(",")
              .includes(el?.departments?.[0]?.status!),
          );
        }
        if (cdtBg && cdtEd) {
          newData = newData.filter((el: FinanceTabType) =>
            dayjs(el.createdDate).isBetween(
              dayjs(cdtBg.split("=")[1]),
              dayjs(cdtEd.split("=")[1]),
            ),
          );
        }
        if (sdtBg && sdtEd) {
          newData = newData.filter((el: FinanceTabType) =>
            dayjs(el.sales[0].saleDate).isBetween(
              dayjs(sdtBg.split("=")[1]),
              dayjs(sdtEd.split("=")[1]),
            ),
          );
        }
        if (ldtBg && ldtEd) {
          newData = newData.filter((el: FinanceTabType) =>
            dayjs(el.limitPreparationDate).isBetween(
              dayjs(ldtBg.split("=")[1]),
              dayjs(ldtEd.split("=")[1]),
            ),
          );
        }
        if (ddtBg && ddtEn) {
          newData = newData.filter((el: FinanceTabType) =>
            dayjs(el.saleInfo?.deliveryInfo?.deliveryBeginDate).isBetween(
              dayjs(ddtBg.split("=")[1]),
              dayjs(ddtEn.split("=")[1]),
            ),
          );
        }
        return {
          content: newData,
          pageable: {
            sort: {
              sorted: false,
              unsorted: true,
              empty: true,
            },
            offset: 0,
            pageNumber: 0,
            pageSize: 100,
            unpaged: false,
            paged: true,
          },
          totalPages: 1,
          totalElements: newData.length,
          last: true,
          sort: {
            sorted: false,
            unsorted: true,
            empty: true,
          },
          size: 100,
          number: 0,
          numberOfElements: 100,
          first: true,
          empty: false,
        }
      },
      async onQueryStarted(_, {dispatch, queryFulfilled}) {
        try {
          dispatch(setLoading(true));
          await queryFulfilled;
          dispatch(setLoading(false));
        } catch (error) {
          dispatch(setLoading(false));
        }
      },
    }),
    getInventoryById: builder.query<FinanceTabType | undefined, string>({
      query: (id) => ({
        url: `/mock/inventories/cardealer/carDealer1/tableAllInventories.json`,
        method: 'GET',
      }),
      async onQueryStarted(_, {dispatch, queryFulfilled}) {
        try {
          dispatch(setLoading(true));
          await queryFulfilled;
          dispatch(setLoading(false));
        } catch (error) {
          dispatch(setLoading(false));
        }
      },
    }),
  }),
});

export const {
  useGetCarDealerSellersQuery,
  useGetCarDealerQuery,
  useGetCarDealerSalesNewQuery,
  useGetCarDealerSalesUsedQuery,
  useGetTopSellersQuery,
  useGetFinanceManagersQuery,
  useGetInventoriesQuery,
  useGetInventoryByIdQuery,
} = carDealerApi;
