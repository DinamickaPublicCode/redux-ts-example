import React, {useContext, useEffect, useState} from "react";
import {useLocation} from "react-router-dom";
import MuiTable from "@mui/material/Table";
import {useDispatch} from "react-redux";
import ConfirmationDialog from "src/components/ConfirmationDialog";
import {enableModalByAction} from "src/utils/enableModalByAction";
import TableContainer from "@mui/material/TableContainer";
import TableBody from "@mui/material/TableBody";
import {FinanceTabType} from "src/types";
import DashboardTableFilters from "./DashboardTableFilters";
import DashboardTableToolbar from "./DashboardTableToolbar";
import DashboardTableHead from "./DashboardTableHead";
import DashboardTableRow from "./DashboardTableRow";
import TablePagination from "src/components/TablePagination";
import {colsData, ColsDataType} from "src/utils/mockDataEditTable";
import StockDetailModal from "src/components/StockDetailModal/StockDetailModal";
import DashboardTabNameContext from "../DashboardTabNameContext";
import {useAppSelector} from "src/state/hooks";
import {useGetUserQuery} from 'src/state/reducers/userApi';
import {useGetInventoriesQuery} from 'src/state/reducers/carDealerApi';

type Order = "asc" | "desc";

const ROWS_PER_PAGE_OPTIONS = [10, 20, 50, 100];

const DashboardTable: React.FC = () => {
  const tabName = useContext(DashboardTabNameContext);
  const location = useLocation();
  const search = location.search;

  const dispatch = useDispatch();
  const {data: user} = useGetUserQuery();;
  const {data, refetch} = useGetInventoriesQuery('');

  useEffect(() => {
    refetch(search);
  }, [user?.defaultCarDealer?.id, search, dispatch]);

  const [order, setOrder] = useState<Order>("asc");
  const [orderBy, setOrderBy] = useState<string>("referenceStock");
  const [selected, setSelected] = useState<string[]>([]);
  const [page, setPage] = useState<number>(data?.number ?? 0);
  const [rowsPerPage, setRowsPerPage] = useState<number>(
    /* data?.size ?? */ ROWS_PER_PAGE_OPTIONS[0],
  );

  const [headCells, setHeadCells] = useState<string[]>(
    colsData[tabName as keyof ColsDataType].primaryCols,
  );

  const [currentlyOpenedDeal, setCurrentlyOpenedDeal] =
    useState<FinanceTabType | null>(null);
  const [stageAction, setStageAction] = useState<string | undefined>(undefined);
  const [confirmationOpen, setConfirmationOpen] = useState(false);

  const createDealOpen = (row: FinanceTabType, action?: string) => {
    setStageAction(action);
    if (action && enableModalByAction(action)) {
      setConfirmationOpen(true);
    } else {
      setCurrentlyOpenedDeal((currentOpenedDeal) =>
        currentOpenedDeal === null ? row : null,
      );
    }
  };

  const handleRequestSort = (
    _event: React.MouseEvent<unknown>,
    property: string,
  ) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleSelectAllClick = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.checked) {
      const newSelecteds = data?.content?.map((n) => n.referenceStock);
      if (newSelecteds) {
        setSelected(newSelecteds);
      }
      return;
    }
    setSelected([]);
  };

  const handleRowClick = (_event: React.MouseEvent<unknown>, name: string) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected: string[] = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }
    setSelected(newSelected);
  };

  const handleChangeRowsPerPage = (event: unknown) => {
    setRowsPerPage(
      (
        event as React.ChangeEvent<{
          name?: string;
          value: number;
        }>
      ).target.value,
    );
    setPage(0);
  };

  const isSelected = (name: string) => selected.indexOf(name) !== -1;
  return (
    <>
      <DashboardTableFilters
        tabName={tabName}
        headCells={headCells}
        setHeadCells={setHeadCells}
      />
      <DashboardTableToolbar selected={selected} />
      <TableContainer
        sx={{
          maxHeight: "calc(100vh - 74px - 280px)",
        }}>
        <MuiTable
          stickyHeader
          aria-labelledby="tableTitle"
          aria-label="enhanced table">
          <DashboardTableHead
            headCells={headCells}
            numSelected={selected.length}
            order={order}
            orderBy={orderBy}
            onSelectAllClick={handleSelectAllClick}
            onRequestSort={handleRequestSort}
            rowCount={data?.content?.length ?? 0}
          />
          <TableBody>
            {(
              (data?.content as unknown as React.ComponentProps<
                typeof DashboardTableRow
              >["row"][]) ?? []
            )
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row, index) => {
                const isItemSelected = isSelected(row.referenceStock);
                const labelId = `enhanced-table-checkbox-${index}`;

                return (
                  <DashboardTableRow
                    key={index}
                    tabName={tabName}
                    headCells={headCells}
                    row={row}
                    labelId={labelId}
                    isItemSelected={isItemSelected}
                    handleRowClick={handleRowClick}
                    createDealOpen={createDealOpen}
                  />
                );
              })}
          </TableBody>
        </MuiTable>
      </TableContainer>

      <TablePagination
        rowsPerPageOptions={ROWS_PER_PAGE_OPTIONS}
        count={
          data?.content?.length === undefined
            ? data?.totalPages ?? 0
            : Math.ceil(data?.content.length / rowsPerPage)
        } //{data?.totalPages ?? 0}
        page={page}
        rowsPerPage={rowsPerPage}
        onChangePage={(_event: unknown, newPage: number) => {
          setPage(newPage);
        }}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />

      <StockDetailModal
        open={currentlyOpenedDeal !== null}
        handleClose={() => {
          setCurrentlyOpenedDeal(null);
          setStageAction(undefined);
        }}
        currentDealId={currentlyOpenedDeal?.id}
        action={stageAction}
      />

      <ConfirmationDialog
        open={confirmationOpen}
        handleClose={() => setConfirmationOpen(false)}
        handleRequest={() => {}}
        action={stageAction}
      />
    </>
  );
};

export default DashboardTable;
