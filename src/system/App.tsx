import React, {Suspense} from "react";
import {Provider} from "react-redux";
import {ThemeProvider, StyledEngineProvider} from "@mui/material/styles";
import {SnackbarProvider} from "src/thirdparty/_notistack";
import CssBaseline from "@mui/material/CssBaseline";
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers'
import store from "src/state/store";
import ErrorBoundary from "./ErrorBoundary";
import StateInitializer from "./StateInitializer";
import {CartagSnackbarProvider} from "../components/CartagSnackbar";
import CartagBackdrop from "../components/CartagBackdrop";
import {theme, GlobalCss} from "../utils/theme";

const Router = React.lazy(() => import("./Router"));

/**
 * A crazy wrapper hell
 */
const App = () => (
  <StyledEngineProvider injectFirst>
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <GlobalCss />
      <LocalizationProvider dateAdapter={AdapterDayjs}>
        <SnackbarProvider maxSnack={8} autoHideDuration={6000}>
          <CartagSnackbarProvider>
            <ErrorBoundary>
              <Provider store={store}>
                <StateInitializer>
                  <Suspense fallback={<CartagBackdrop open={true} />}>
                    <Router />
                  </Suspense>
                </StateInitializer>
              </Provider>
            </ErrorBoundary>
          </CartagSnackbarProvider>
        </SnackbarProvider>
      </LocalizationProvider>
    </ThemeProvider>
  </StyledEngineProvider>
);

export default App;
