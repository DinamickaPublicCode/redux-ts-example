FROM node:16-alpine as build
RUN npm i -g pnpm

WORKDIR /usr/src/app
COPY package.json pnpm-lock.yaml ./
RUN pnpm install

COPY tsconfig.json vite.config.ts index.html ./
COPY src ./src
COPY public ./public

RUN npm run build

# Stage - Production 2
FROM nginx:1.17-alpine
COPY --from=build /usr/src/app/dist /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
WORKDIR /configs
RUN echo "Creating nginx config" \
  && echo "server {" >> nginx.conf \
  && echo "  listen 80;" >> nginx.conf \
  && echo "  location / {" >> nginx.conf \
  && echo "    root   /usr/share/nginx/html;" >> nginx.conf \
  && echo "    index  index.html index.htm;" >> nginx.conf \
  && echo "    try_files \$uri /index.html;" >> nginx.conf \
  && echo "  }" >> nginx.conf \
  && echo "}" >> nginx.conf

RUN cp ./nginx.conf /etc/nginx/conf.d
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
