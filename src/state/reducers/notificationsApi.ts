import baseQuery from "src/state/baseQuery";
import {createApi} from '@reduxjs/toolkit/query/react';
import {setLoading} from './loading';
import {setNotifications} from './notifications';
import {PagedNotifications} from "src/types";

export const notificationsApi = createApi({
  reducerPath: 'notificationsApi',
  baseQuery,
  endpoints: (builder) => ({
    getNotifications: builder.query<PagedNotifications>({
      query: () => ({
        url: `/mock/notifications/user/user1/notifications.json`,
        method: 'GET',
      }),
      async onQueryStarted(arg, {dispatch, queryFulfilled}) {
        try {
          dispatch(setLoading(true));
          const res = await queryFulfilled;
          dispatch(setNotifications(res.data));
          dispatch(setLoading(false));
        } catch (error) {
          dispatch(setLoading(false));
        }
      },
    }),
  })
});

export const {
  useGetNotificationsQuery
} = notificationsApi;
