import makeStyles from "@mui/styles/makeStyles";
import {Theme} from "@mui/material/styles";
import createStyles from "@mui/styles/createStyles";

export const useCommonModalStyles = makeStyles((theme: Theme) =>
  createStyles({
    dialogPaper: {
      width: "100%",
      maxWidth: 730,
      background: "white",
    },
    dialogTitle: {
      position: "relative",
      padding: 0,
      marginBottom: 20,
      marginTop: 20,
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
      paddingLeft: 30,
      paddingRight: 30,
    },
    titleText: {
      fontWeight: 600,
      fontSize: 22,
      color: "#222238",
    },
    dialogActions: {
      marginTop: 30,
      background: "#F6F8FA",
      padding: "28px 30px",
      [theme.breakpoints.down("sm")]: {
        display: "flex",
        flexDirection: "column",
      },
    },
    grayUppercaseText: {
      fontWeight: 600,
      fontSize: 13,
      textTransform: "uppercase",
      color: "#98A2B7",
    },
    mainBox: {},
    itemReadiness: {
      [theme.breakpoints.down("sm")]: {
        marginLeft: "100%",
      },
    },
    itemReadinessAddNewDeliveryAppointment: {},
    setGoalsMonthPickerBox: {},
    monthPickerHead: {
      [theme.breakpoints.down("sm")]: {
        display: "none",
      },
    },
    monthPickerDown: {
      display: "none",
      [theme.breakpoints.down("sm")]: {
        display: "block",
        marginBottom: "20px",
      },
    },
  }),
);
