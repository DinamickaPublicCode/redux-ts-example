import Paper from "@mui/material/Paper";
import dayjs from "dayjs";
import React, {useEffect} from "react";
import {CarDealerSale} from "src/types";
import SalesCalendarCarList from "./SalesCalendarCarList";
import SalesCalendarCarInfo from "./SalesCalendarCarInfo";
import SalesCalendarDayItem from "./SalesCalendarDayItem";

interface SalesCalendarPerDayViewProps {
  activeDay: dayjs.Dayjs | null;
  deliveriesPerDay: {
    day: dayjs.Dayjs;
    deliveries: CarDealerSale[];
  }[];
  changeActiveDate: (date: {
    day: dayjs.Dayjs;
    deliveries: CarDealerSale[] | undefined;
  }) => void;
  currentSale: CarDealerSale | null;
  filteredCars: CarDealerSale[] | null;
  changeCurrentCarInfo: (car: CarDealerSale) => void;
  onOpenUpdateNewDelivery: (open: boolean) => void;
  setOpenStockWithChat: (value: React.SetStateAction<boolean>) => void;
}

const SalesCalendarPerDayView: React.FC<SalesCalendarPerDayViewProps> = ({
  activeDay,
  deliveriesPerDay,
  changeActiveDate,
  currentSale,
  filteredCars,
  changeCurrentCarInfo,
  onOpenUpdateNewDelivery,
  setOpenStockWithChat,
}) => {
  useEffect(() => {
    if (activeDay && !currentSale) {
      const theDay = deliveriesPerDay
        .filter((value) => activeDay.isSame(value.day))
        .pop();
      if (theDay && !currentSale && theDay?.deliveries.length !== 0) {
        changeActiveDate(theDay);
      }
    }
  }, [activeDay, deliveriesPerDay, currentSale, changeActiveDate]);
  return (
    <>
      <div>
        <Paper
          style={{
            padding: 20,
            height: "100%",
            overflowY: "auto",
          }}
        >
          {deliveriesPerDay?.length &&
            activeDay &&
            deliveriesPerDay.map((value, index) => {
              return (
                <SalesCalendarDayItem
                  key={index}
                  day={value}
                  isActive={activeDay.isSame(value.day)}
                  onClick={changeActiveDate}
                />
              );
            })}
        </Paper>
      </div>
      {/** car list */}
      <SalesCalendarCarList
        currentSaleId={currentSale?.id ?? "the id"}
        currentSalesPerDay={filteredCars}
        changeCurrentCarInfo={changeCurrentCarInfo}
      />

      <div>
        {currentSale && (
          <SalesCalendarCarInfo
            currentCar={currentSale}
            openModal={onOpenUpdateNewDelivery}
            setOpenStockWithChat={setOpenStockWithChat}
          />
        )}
      </div>
    </>
  );
};

export default SalesCalendarPerDayView;
