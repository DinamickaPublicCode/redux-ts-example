import React, {createContext, useContext, useEffect, useState} from "react";
import {
  ActivityBoxApiMessage,
  ActivityBoxMessageType,
  ActivityBoxContextData,
  ActivityBoxProviderProps,
  ActivityBoxMessage,
  ActivityBoxType,
} from "src/types/activityBoxInterfaces";
import ActivityBoxLayout from "./ActivityBoxLayout";

export function convertApiMessageToDisplayMessage(
  apiMessage: ActivityBoxApiMessage,
) {
  return {
    id: apiMessage.id,
    type: apiMessage.type as ActivityBoxMessageType,
    date: new Date(apiMessage.date),
    dateDisplay: apiMessage.labelDate,
    user: {
      id: apiMessage.idUser,
      name: apiMessage.userName,
    },
    message: {
      title: apiMessage.title,
      text: apiMessage.message,
    },
    intentoryId: apiMessage.idInventory,
  };
}

const ActivityBox = createContext(null as null | ActivityBoxContextData);

export function useActivityBoxContext() {
  return useContext(ActivityBox) as ActivityBoxContextData;
}

const ActivityBoxProvider = (props: ActivityBoxProviderProps) => {
  const [parsedMessages, setParsedMessages] = useState(
    [] as Array<ActivityBoxMessage>,
  );
  const {apiMessages} = props;

  useEffect(() => {
    const parsedMessages = apiMessages
      .map(convertApiMessageToDisplayMessage)
      .sort((a, b) => b.date.getTime() - a.date.getTime());
    setParsedMessages(parsedMessages);
  }, [apiMessages]);

  return (
    <ActivityBox.Provider
      value={{
        messages: parsedMessages,
        tabs: props.tabs,
        onRequestUserAvatar: props.onRequestUserAvatar,
        onMessageSend: props.onMessageSend,
        onRequestMoreMessages: props.onRequestMoreMessages,
      }}
    >
      <ActivityBoxLayout
        type={props.type || ActivityBoxType.MODAL}
        shouldRender={props.shouldRender}
      />
    </ActivityBox.Provider>
  );
};

export default ActivityBoxProvider;
