import React, {useEffect, useRef, useState} from "react";
import dayjs from "dayjs";
import {CarDealerSale} from "src/types";
import Box from "@mui/material/Box/Box";
import Typography from "@mui/material/Typography/Typography";

interface SalesMonthCalendarProps {
  deliveriesPerDay: {
    day: dayjs.Dayjs;
    deliveries: CarDealerSale[];
  }[];
  onOpenUpdateDeliv: (open: boolean) => void;
  setCurrentSalesPerDay: React.Dispatch<
    React.SetStateAction<CarDealerSale[] | null>
  >;
  setActiveDay: React.Dispatch<React.SetStateAction<dayjs.Dayjs | null>>;
  setTypeOfCalendar: React.Dispatch<React.SetStateAction<"DAILY" | "MONTHLY">>;
  setCurrentSale: React.Dispatch<React.SetStateAction<CarDealerSale | null>>;
}

const SalesMonthCalendar: React.FC<SalesMonthCalendarProps> = ({
  deliveriesPerDay,
  onOpenUpdateDeliv,
  setCurrentSalesPerDay,
  setActiveDay,
  setTypeOfCalendar,
  setCurrentSale,
}) => {
  const calendarWrapperRef = useRef<HTMLDivElement | null>(null);

  const [width, setWidth] = useState(0);

  const currentMonthFirstDayOfWeek = deliveriesPerDay[0].day.weekday();

  useEffect(() => {
    if (calendarWrapperRef.current) {
      setWidth(calendarWrapperRef.current.clientWidth);
    }
  }, [calendarWrapperRef]);

  return (
    <Box sx={{width: "80%"}}>
      <Box sx={{display: "flex"}}>
        {dayjs.weekdays().map((el, idx) => {
          return (
            <Box
              key={idx}
              sx={{
                width: `calc(${width}px / 7 - 6px)`,
                padding: 2,
                margin: "2px",
              }}
            >
              <Typography
                style={{
                  textTransform: "uppercase",
                  color: "#98A2B7",
                }}
              >
                {el}
              </Typography>
            </Box>
          );
        })}
      </Box>
      <div
        ref={calendarWrapperRef}
        style={{
          display: "flex",
          flexWrap: "wrap",
        }}
      >
        {Array.from({
          length: currentMonthFirstDayOfWeek,
        }).map((_, index) => {
          return (
            <Box
              key={index}
              sx={{
                width: `calc(${width}px / 7 - 6px)`,
                height: `calc(${width}px / 7 - 6px)`,
                padding: 1,
                margin: "2px",
              }}
            />
          );
        })}

        {deliveriesPerDay?.length &&
          deliveriesPerDay.map((value, idx) => {
            return (
              <Box
                key={idx}
                sx={{
                  width: `calc(${width}px / 7 - 6px)`,
                  height: `calc(${width}px / 7 - 6px)`,
                  backgroundColor: "#fff",
                  padding: 1,
                  margin: "2px",
                  overflow: "hidden",
                  position: "relative",
                }}
              >
                <Typography
                  style={{
                    fontWeight: 600,
                    fontSize: 16,
                    marginBottom: 10,
                  }}
                >
                  {value.day.format("DD")}
                </Typography>
                <div>
                  {value.deliveries.map((deliv, index) => {
                    return (
                      <div
                        key={index}
                        style={{
                          display: "flex",
                        }}
                      >
                        <Typography
                          style={{
                            fontWeight: 400,
                            fontSize: 14,
                            textTransform: "capitalize",
                          }}
                          onClick={() => {
                            setActiveDay(value.day);
                            setCurrentSalesPerDay(value.deliveries);
                            setCurrentSale(deliv);
                            onOpenUpdateDeliv(true);
                          }}
                        >
                          <span
                            style={{
                              cursor: "pointer",
                              color: "#1A06F9",
                            }}
                          >
                            {deliv.generalInfo.make}
                          </span>{" "}
                          - {deliv.sales[0].saleReference}
                        </Typography>
                      </div>
                    );
                  })}
                  {value.deliveries.length > 1 && (
                    <Typography
                      style={{
                        display: "flex",
                        position: "absolute",
                        bottom: 0,
                        backgroundColor: "#fff",
                        color: "rgb(152, 162, 183)",
                        width: "100%",
                        fontWeight: 600,
                        cursor: "pointer",
                      }}
                      onClick={() => {
                        setActiveDay(value.day);
                        setCurrentSalesPerDay(value.deliveries);
                        setCurrentSale(value.deliveries[0]);
                        setTypeOfCalendar("DAILY");
                      }}
                    >
                      Show more
                    </Typography>
                  )}
                </div>
              </Box>
            );
          })}
      </div>
    </Box>
  );
};

export default SalesMonthCalendar;
