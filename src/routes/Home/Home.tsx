import React from "react";
import HomeLayout from "./components/HomeLayout";
import HomeContextProvider from "./state/HomeContextProvider";

const Home = () => (
  <HomeContextProvider>
    <HomeLayout />
  </HomeContextProvider>
);

export default Home;
