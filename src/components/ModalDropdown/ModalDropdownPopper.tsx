import Box from "@mui/material/Box";
import MenuItem from "@mui/material/MenuItem";
import MenuList from "@mui/material/MenuList";
import React from "react";
import {CustomDropdownPopperProps} from "./ModalDropdown";
import ModalDropdownPopperBody from "./ModalDropdownPopperBody";

/**
 * A wrapper around anything which is clickable
 * Spawns a dropdown list around the "children" prop
 * @param props
 */
const ModalDropdownPopper = (props: CustomDropdownPopperProps) => {
  const {options, handleMenuItemClick} = props;
  const {children, ...dropDownProps} = props;

  return (
    <div>
      <Box>
        <Box ref={props.anchorRef}>{children}</Box>
        <ModalDropdownPopperBody {...dropDownProps}>
          <MenuList id="split-button-menu">
            {options.map((label, index) => (
              <MenuItem
                key={index}
                onClick={(e) => handleMenuItemClick(e, index)}
              >
                {label.label}
              </MenuItem>
            ))}
          </MenuList>
        </ModalDropdownPopperBody>
      </Box>
    </div>
  );
};
export default ModalDropdownPopper;
