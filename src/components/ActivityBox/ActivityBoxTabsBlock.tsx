import Box from "@mui/material/Box";
import React, {useState} from "react";
import {useTranslation} from "react-i18next";
import {useActivityBoxContext} from "src/components/ActivityBox/ActivityBox";
import {useActivityBoxStyle} from "./ActivityBoxLayout";
import ActivityBoxTabItem from "./ActivityBoxTabItem";

const ActivityBoxTabsBlock = ({
  items,
  onItemClicked,
  currentIndex,
}: {
  items: string[];
  onItemClicked: (index: number) => void;
  currentIndex: number;
}) => {
  const styles = useActivityBoxStyle();
  return (
    <Box className={styles.tabsBlock}>
      {items.map((item, id) => (
        <ActivityBoxTabItem
          key={id}
          isSelected={currentIndex === id}
          title={item}
          onClick={() => onItemClicked(id)}
        />
      ))}
    </Box>
  );
};

export function useTabItems(defaultIndex: number) {
  const {tabs} = useActivityBoxContext();
  const items = tabs.map((x) => x.localeId);
  const {t} = useTranslation();
  const translated = items.map((x) => t(x));
  const [currentIndex, setCurrentIndex] = useState(defaultIndex);

  return {
    items: translated,
    currentIndex,
    onItemClicked: (index: number) => setCurrentIndex(index),
  };
}

export default ActivityBoxTabsBlock;
