import React, {useEffect, useState} from "react";
import ModalDropdownPopper from "./ModalDropdownPopper";
import ModalDropdownSelect from "./ModalDropdownSelect";
import useCustomDropdownHandlers from "src/utils/useCustomDropdownHandlers";

function ModalSelectDropdown<T>({
  value,
  onChange,
  labels,
  values,
}: {
  labels?: string[];
  values: T[];
  value: T;
  onChange: (value: T) => void;
}) {
  if (!labels) {
    labels = values.map((v) => "" + v);
  }

  const [currentIndex, setCurrentIndex] = useState(() => values.indexOf(value));
  const handlers = useCustomDropdownHandlers(
    labels.map((_, index) => index),
    setCurrentIndex,
    0,
    labels,
  );

  useEffect(() => {
    onChange(values[currentIndex]);
  }, [currentIndex, values, onChange]);

  return (
    <ModalDropdownPopper placement="bottom-start" {...handlers}>
      <ModalDropdownSelect
        title={labels[currentIndex]}
        onClick={
          handlers.handleToggle as unknown as React.ComponentProps<
            typeof ModalDropdownSelect
          >["onClick"]
        }
      />
    </ModalDropdownPopper>
  );
}

export default ModalSelectDropdown;
