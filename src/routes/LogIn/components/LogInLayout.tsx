import React, {ChangeEvent, useState} from "react";
import {Link, useNavigate} from "react-router-dom";
import MuiAppBar from "@mui/material/AppBar";
import Container from "@mui/material/Container";
import FormControl from "@mui/material/FormControl";
import Paper from "@mui/material/Paper";
import TextField from "@mui/material/TextField";
import MuiToolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import {useDispatch} from "react-redux";
import {LoadingButton} from "@mui/lab";
import HeaderLogo from "src/assets/icons/HeaderLogo";
import {setToken} from "src/state/reducers/token";
import {useAppSelector} from "src/state/hooks";
import {LogInUser} from "src/types";
import {styled} from "@mui/material/styles";
import Box from "@mui/material/Box/Box";

const LogInLayout = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const loading = useAppSelector((state) => state.loading.loading);

  const [logIn, setLogIn] = useState<LogInUser>({
    username: "example@email.com",
    password: "user123",
  });

  const handleSubmit = async (event: React.FormEvent) => {
    event.preventDefault();
    try {
      dispatch(setToken("mock token"));
      navigate("/");
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      <AppBar position="static">
        <Toolbar>
          <Link to="/">
            <HeaderLogo
              style={{
                margin: "10px 0 10px -20px",
              }}
            />
          </Link>
        </Toolbar>
      </AppBar>

      <ContentContainer maxWidth="xl">
        <FormWrapper variant="outlined">
          <FormControl
            sx={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              width: "100%",
            }}
            component="form"
            onSubmit={handleSubmit}
          >
            <Typography
              variant="h4"
              sx={{
                marginBottom: "40px",
                fontWeight: 700,
                fontSize: "28px",
              }}
            >
              Welcome Back!
            </Typography>
            <InputLabel>Work Email</InputLabel>
            <Input
              fullWidth
              type="email"
              name={"email"}
              variant="outlined"
              size="small"
              value={logIn.username}
              onChange={(e: ChangeEvent<HTMLInputElement>) =>
                setLogIn({
                  ...logIn,
                  username: e.target.value,
                })
              }
            />
            <InputLabel>Password</InputLabel>
            <Input
              fullWidth
              type="password"
              name={"password"}
              variant="outlined"
              size="small"
              value={logIn.password}
              onChange={(e: ChangeEvent<HTMLInputElement>) =>
                setLogIn({
                  ...logIn,
                  password: e.target.value,
                })
              }
            />
            <LoadingButton
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              disabled={loading}
              sx={{
                padding: "10px",
                marginBottom: "20px",
                textTransform: "uppercase",
              }}
            >
              Submit
            </LoadingButton>
            <Typography
              to="/"
              sx={{
                width: "100%",
                fontSize: 14,
                color: "#98A2B7",
                marginBottom: 12,
                textDecoration: "none",
              }}
              component={Link}
            >
              Forgot Password?
            </Typography>
          </FormControl>
        </FormWrapper>
        <Typography>
          Don’t have an cartag account?{" "}
          <Link
            to="/"
            style={{
              color: "#1A06F9",
              fontWeight: 700,
              textDecoration: "none",
            }}
          >
            Onboard your company
          </Link>
        </Typography>
      </ContentContainer>

      <Footer>
        <Toolbar
          sx={{
            justifyContent: "center",
          }}
        >
          <Box>
            <FooterLink to="/">Home</FooterLink>
            <FooterLink to="/">Terms</FooterLink>
            <FooterLink to="/">Privacy</FooterLink>
          </Box>
        </Toolbar>
      </Footer>
    </>
  );
};

const Input = styled(TextField)(({theme}) => ({
  width: "100%",
  backgroundColor: theme.palette.common.white,
  marginBottom: 30,
}));

const Footer = styled("footer")(({theme}) => ({
  backgroundColor: theme.palette.common.white,
}));

const FooterLink = styled(Link)({
  margin: "0 25px",
  color: "#222238",
  fontSize: 16,
  textDecoration: "none",
});

const AppBar = styled(MuiAppBar)(({theme}) => ({
  boxShadow: "none",
  [theme.breakpoints.up("sm")]: {
    boxShadow: "none",
  },
}));

const Toolbar = styled(MuiToolbar)({
  padding: "11px 20px",
  display: "flex",
  justifyContent: "center",
});

const FormWrapper = styled(Paper)({
  maxWidth: 500,
  width: "100%",
  padding: "50px 70px 40px",
  boxShadow: "0px 4px 20px rgba(20, 32, 100, 0.08)",
  marginBottom: "40px",
});

const ContentContainer = styled(Container)({
  height: "calc(100vh - 74px - 64px)",
  display: "flex",
  minHeight: "800px",
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",
});

const InputLabel = styled(Typography)({
  width: "100%",
  fontSize: "14px",
  color: "#98A2B7",
  marginBottom: "12px",
  textDecoration: "none",
});

export default LogInLayout;
