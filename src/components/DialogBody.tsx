import Box from "@mui/material/Box";
import React from "react";

/**
 * This component prevents having the ugly scrollbar in chrome which ignores the border radius.
 */
export const DialogBody = ({
  children,
}: {
  children: JSX.Element[] | JSX.Element;
}) => {
  return <Box overflow="auto">{children}</Box>;
};
