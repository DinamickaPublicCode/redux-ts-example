import React, {Dispatch, useEffect, useMemo, useState} from "react";
import clsx from "clsx";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import IconButton from "@mui/material/IconButton";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";
import CloseIcon from "@mui/icons-material/Close";
import {TabContext, TabPanel} from "@mui/lab";
import StockCarInfoBlock from "../StockCarInfoBlock";
import StockCreateDealBlock from "./StockCreateDealBlock";
import ActivitySidebar from "../ActivitySidebar/ActivitySidebar";
import {
  CreateDealTab,
  CreateDealTabs,
  createDealTabs,
} from "src/utils/createDealTabs";
import Transition from "../Transition";
import CartagBackdrop from "../CartagBackdrop";
import UploadFilesProvider from "src/components/UploadFilesProvider";
import {enableTabByStage} from "src/utils/enableTabByStage";
import {FinanceTabType} from "src/types";
import {enableTabByAction} from "src/utils/enableTabByAction";
import {useDispatch} from "react-redux";
import {lighten, Theme} from "@mui/material/styles";
import {useAppSelector} from "src/state/hooks";
import createStyles from "@mui/styles/createStyles";
import makeStyles from "@mui/styles/makeStyles";
import StockGeneralTab from "./StockGeneralTab";
import StockSalesTab from "./StockSalesTab";
import StockPreparationTab from "./StockPreparationTab";
import StockServiceTab from "./StockServiceTab";
import StockDeliveryTab from "./StockDeliveryTab";
import StockFinanceTab from "./StockFinanceTab";
import {useGetInventoryByIdQuery} from 'src/state/reducers/carDealerApi';

interface StockDetailModalProps {
  open: boolean;
  handleClose: Dispatch<boolean>;
  currentDealId: string | undefined;
  action?: string | undefined;
}

const StockDetailModal: React.FC<StockDetailModalProps> = ({
  open,
  handleClose,
  currentDealId,
  action,
}) => {
  const dispatch = useDispatch();
  const classes = useStockDetailModalStyles();

  const {data:currentDeal, refetch} = useGetInventoryByIdQuery('');
  const loading = useAppSelector((state) => state.loading.loading);

  const revalidateInventory = () => {
    refetch();
  };

  useEffect(() => {
    refetch();
  }, [currentDealId, dispatch]);

  const [tabId, setTabId] = useState<string>(CreateDealTabs.GENERAL);

  /**
   * Some tabs change the stage type on submit directly
   * It might happen that the currently opened tab gets blocked after the data is changed
   * This effect runs on every render to check if the currently opened tab is blocked.
   *
   * If it is, the modal redirects to the general tab.
   * This should NEVER lead to the infinite loop, considering that the "General" tab is available for every stage.
   */
  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => {
    //
    if (
      enableTabByStage(
        tabId as unknown as CreateDealTabs,
        (currentDeal as FinanceTabType | null)?.stageInfo?.stage,
      )
    ) {
      setTabId(CreateDealTabs.GENERAL);
    }
  });

  useEffect(() => {
    if (action) setTabId(enableTabByAction(action));
  }, [action]);

  const isTabExists = useMemo(
    () => Boolean(createDealTabs.find((i) => i === tabId)),
    [tabId],
  );
  useEffect(() => {
    if (!isTabExists) {
      setTabId("General");
    }
  }, [isTabExists]);

  const handleChangeTab =
    (newValue: string) => (_event: React.SyntheticEvent) => {
      if (tabId !== newValue) {
        setTabId(newValue);
      }
    };

  const currentTabOpened = (tab: CreateDealTab) => {
    if (tab === CreateDealTabs.GENERAL) {
      return (
        <StockGeneralTab
          currentDeal={currentDeal as FinanceTabType | null}
          revalidateInventory={revalidateInventory}
        />
      );
    } else if (tab === CreateDealTabs.SALES) {
      return (
        <StockSalesTab
          currentDeal={currentDeal as FinanceTabType | null}
          revalidateInventory={revalidateInventory}
        />
      );
    } else if (tab === CreateDealTabs.PREPARATION_REQUEST) {
      return (
        <StockPreparationTab
          currentDeal={currentDeal as FinanceTabType | null}
          revalidateInventory={revalidateInventory}
        />
      );
    } else if (tab === CreateDealTabs.SERVICE) {
      return (
        <StockServiceTab
          currentDeal={currentDeal as FinanceTabType | null}
          revalidateInventory={revalidateInventory}
        />
      );
    } else if (tab === CreateDealTabs.DELIVERY) {
      return (
        <StockDeliveryTab
          currentDeal={currentDeal as FinanceTabType | null}
          revalidateInventory={revalidateInventory}
        />
      );
    } else if (tab === CreateDealTabs.FINANCE) {
      return (
        <StockFinanceTab
          currentDeal={currentDeal as FinanceTabType | null}
          revalidateInventory={revalidateInventory}
        />
      );
    } else return tab;
  };

  const onCloseTab = () => {
    setTabId(CreateDealTabs.GENERAL);
    handleClose(false);
  };

  return (
    <Dialog
      scroll="body"
      open={open}
      TransitionComponent={Transition}
      keepMounted
      onClose={onCloseTab}
      classes={{
        paper: classes.dialogPaper,
      }}
    >
      <CartagBackdrop open={loading} />
      <DialogTitle
        classes={{
          root: classes.dialogTitle,
        }}
      >
        <Typography
          variant="h6"
          style={{
            fontSize: 22,
            fontWeight: 600,
          }}
        >
          Stock Detail
        </Typography>
        <IconButton size="small" onClick={onCloseTab}>
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <UploadFilesProvider deal={currentDeal as FinanceTabType | null}>
        <Box className={classes.mainBox}>
          <Box
            sx={{
              width: "100%",
            }}
          >
            <TabContext value={`${tabId}`}>
              <div className={classes.tabsWrapper}>
                {createDealTabs.map((tab) => {
                  return (
                    <Button
                      className={clsx(
                        classes.tabButton,
                        tabId === tab && classes.tabActiveButton,
                      )}
                      key={tab}
                      onClick={handleChangeTab(tab)}
                      disabled={enableTabByStage(
                        tab,
                        (currentDeal as FinanceTabType | null)?.stageInfo
                          ?.stage,
                      )}
                    >
                      {tab}
                    </Button>
                  );
                })}
              </div>

              <StockCarInfoBlock
                currentDeal={currentDeal as FinanceTabType | null}
              />
              <StockCreateDealBlock
                currentDeal={currentDeal as FinanceTabType | null}
              />

              {createDealTabs.map((tab) => {
                return (
                  <TabPanel
                    key={tab}
                    value={tab}
                    style={{
                      padding: 0,
                    }}
                  >
                    <Paper
                      variant="outlined"
                      square
                      className={classes.currentTabWrapper}
                    >
                      {currentTabOpened(tab)}
                    </Paper>
                  </TabPanel>
                );
              })}
            </TabContext>
          </Box>
          <Box className={classes.activitySidebar}>
            <ActivitySidebar
              currentDeal={currentDeal as FinanceTabType | null}
            />
          </Box>
        </Box>
      </UploadFilesProvider>
    </Dialog>
  );
};

export const useStockDetailModalStyles = makeStyles((theme: Theme) =>
  createStyles({
    dialogPaper: {
      width: "100%",
      [theme.breakpoints.up(1300)]: {
        maxWidth: 1200,
      },
    },
    dialogTitle: {
      display: "flex",
      justifyContent: "space-between",
    },
    tabsWrapper: {
      display: "flex",
      justifyContent: "space-between",
      [theme.breakpoints.down("sm")]: {
        display: "flex",
        flexDirection: "column",
      },
    },
    tabButton: {
      flex: 1,
      height: 76,
      borderRadius: 0,
      border: "2px solid #F6F8FA",
      borderRight: "none",
      lineHeight: 1.3,
      fontSize: 13,
      fontWeight: 600,
      color: "#98A2B7",
      transition: "0.3s all ease-in-out",
    },
    tabActiveButton: {
      color: theme.palette.primary.main,
      backgroundColor: lighten(theme.palette.primary.main, 0.9),
    },

    currentTabWrapper: {
      padding: "20px 16px 50px",
      border: "none",
      height: "100%",
    },
    mainBox: {
      display: "flex",
      justifyContent: "space-between",
      [theme.breakpoints.down("sm")]: {
        flexDirection: "column",
      },
    },
    activitySidebar: {
      [theme.breakpoints.up(1300)]: {
        maxWidth: 322,
      },
      width: "100%",
    },
  }),
);

export default StockDetailModal;
