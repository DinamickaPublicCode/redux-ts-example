import {useTheme} from "@mui/material/styles";
import Box from "@mui/material/Box";
import {GenericFormBurgerSubmitButton} from "./GenericFormBurgerSubmitButton";
import Button from "@mui/material/Button";
import React from "react";

export function GenericFormSubmitButton({
  children,
  addMargin,
  secondaryButtons,
  burgerButtons,
}: {
  burgerButtons?: {
    title: string;
    onClick: () => void;
  }[];
  secondaryButtons?: {
    title: string;
    onClick: () => void;
  }[];
  children: string;
  addMargin?: boolean;
}) {
  const theme = useTheme();
  return (
    <Box
      {...(addMargin ? {marginTop: "20px"} : {})}
      display="flex"
      flexDirection="row"
      justifyContent="flex-end"
      padding="23px 32px 25px 23px"
      sx={{
        backgroundColor: "#F6F8FA",
        borderBottomRightRadius: "14px",
        borderBottomLeftRadius: "14px",
      }}
    >
      {burgerButtons && (
        <GenericFormBurgerSubmitButton burgerButtons={burgerButtons} />
      )}
      {secondaryButtons?.map((secondaryButton, index) => (
        <Button
          key={index}
          type="button"
          style={{
            marginRight: 12,
          }}
          onClick={secondaryButton.onClick}
          sx={{
            color: theme.palette.primary.main,
            fontWeight: 600,
            fontSize: "14px",
            lineHeight: "17px",
            textAlign: "center",
            letterSpacing: "0.004em",
            boxShadow: "0px 3px 20px rgba(203, 205, 217, 0.4)",
            borderRadius: "10px",
            padding: "10px 35px",
          }}
        >
          {secondaryButton.title}
        </Button>
      ))}
      <Button
        type="submit"
        sx={{
          padding: "8px 30px",
          textTransform: "uppercase",
          fontSize: 14,
          fontWeight: 600,
        }}
        variant="contained"
      >
        {children}
      </Button>
    </Box>
  );
}
