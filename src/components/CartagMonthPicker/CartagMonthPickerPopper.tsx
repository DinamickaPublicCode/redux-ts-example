import React, {useRef} from "react";
import Box from "@mui/material/Box";
import ClickAwayListener from "@mui/material/ClickAwayListener";
import Grow from "@mui/material/Grow";
import Paper from "@mui/material/Paper";
import Popper from "@mui/material/Popper";

const CartagMonthPickerPopper = ({
  children,
  monthPickerBody,
  open,
  setOpen,
}: {
  children: JSX.Element | JSX.Element[];
  monthPickerBody: JSX.Element;
  open: boolean;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
}) => {
  const anchorRef = useRef<HTMLElement>(null);

  return (
    <div>
      <Box
        ref={anchorRef}
        onClick={(event) => {
          event.stopPropagation();
          setOpen((open) => !open);
        }}
      >
        {children}
      </Box>
      <Popper
        open={open}
        anchorEl={anchorRef.current}
        role={undefined}
        transition
        disablePortal
        placement={"bottom"}
        style={{zIndex: 100}}
      >
        {({TransitionProps, placement}) => (
          <Grow
            {...TransitionProps}
            style={{
              zIndex: 1000,
              transformOrigin:
                placement === "bottom" ? "center top" : "center bottom",
            }}
          >
            <Paper
              style={{
                marginTop: 16,
                boxShadow: "0px 4px 20px rgba(203, 205, 217, 0.3)",
                overflow: "hidden",
              }}
            >
              <ClickAwayListener
                onClickAway={(event) => {
                  if (
                    anchorRef.current &&
                    anchorRef.current.contains(event.target as HTMLElement)
                  ) {
                    return;
                  }
                  setOpen(false);
                }}
              >
                <Box>{monthPickerBody}</Box>
              </ClickAwayListener>
            </Paper>
          </Grow>
        )}
      </Popper>
    </div>
  );
};

export default CartagMonthPickerPopper;
