import React, {useMemo, useRef, useState} from "react";

export interface CustomDropdownHandlers {
  open: boolean;
  anchorRef: React.RefObject<HTMLButtonElement>;

  currentValue: string | number;
  options: {
    label: string;
    value: string | number;
  }[];
  handleMenuItemClick: (
    event: React.MouseEvent<HTMLLIElement, MouseEvent>,
    index: number,
  ) => void;
  handleToggle: (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>,
  ) => void;
  handleClose: (event: React.MouseEvent<Document, MouseEvent>) => void;
}

const useCustomDropdownHandlers = (
  options: (string | number)[],
  onChange?: (index: number) => void,
  initialValue?: string | number,
  labels?: string[],
) => {
  const theOptions: {
    label: string;
    value: string | number;
  }[] = useMemo(
    () =>
      options.map((x, idx) => ({
        value: x,
        label: labels ? labels[idx] : "" + x,
      })),
    [options, labels],
  );

  const [open, setOpen] = useState<boolean>(false);
  const anchorRef = useRef<HTMLButtonElement>(null);
  const [currentValue, setCurrentValue] = useState(
    initialValue || theOptions[0].label,
  );

  const handleMenuItemClick = (
    event: React.MouseEvent<HTMLLIElement, MouseEvent>,
    index: number,
  ) => {
    event.stopPropagation();
    setCurrentValue(theOptions[index].value);
    if (onChange) {
      onChange(index);
    }
    setOpen(false);
  };

  const handleToggle = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>,
  ) => {
    event.stopPropagation();
    setOpen((prevOpen) => !prevOpen);
  };

  const handleClose = (event: React.MouseEvent<Document, MouseEvent>) => {
    if (
      anchorRef.current &&
      anchorRef.current.contains(event.target as HTMLElement)
    ) {
      return;
    }
    setOpen(false);
  };

  return {
    currentValue,
    open,
    options: theOptions,
    anchorRef,
    handleMenuItemClick,
    handleToggle,
    handleClose,
  } as CustomDropdownHandlers;
};

export default useCustomDropdownHandlers;
