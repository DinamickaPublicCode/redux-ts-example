import {useTranslation} from "react-i18next";

export const useSetGoalsModalTranslation = () => {
  const {t} = useTranslation();
  return {
    t: (name: string) => t("SetGoalsModal." + name),
  };
};
