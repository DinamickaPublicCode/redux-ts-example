import React, {createContext, useContext, useState} from "react";
import {useAppSelector} from "src/state/hooks";
import {useSalesFunnelApiDataProvider} from "src/routes/SalesFunnel/utils/useSalesFunnelApiDataProvider";
import SetGoalsDialog from "../components/SetGoalsDialog";
import {
  CarFilter,
  CarSelection,
  SalesFunnelContextData,
} from "../types/salesFunnelContext";
import {useGetUserQuery} from 'src/state/reducers/userApi';

const SalesFunnelContext = createContext(null as null | SalesFunnelContextData);

export const useSalesFunnelContext = () => {
  return useContext(SalesFunnelContext) as SalesFunnelContextData;
};

const SalesFunnelProvider: React.FC<{children: React.ReactNode}> = ({
  children,
}) => {
  const [chatInventoryId, setChatInventoryId] = useState(null as string | null);
  const [carFilter, onCarFilterChanged] = useState("USED" as CarFilter);
  const [currentDate, onCurrentDateChanged] = useState(() => new Date());
  const [searchText, onSearchTextChanged] = useState("");
  const [setGoalsModalOpen, setSetGoalsModalOpen] = useState(false);
  const [carSelection, setCarSelection] = useState<null | CarSelection>(null);
  const {data: user} = useGetUserQuery();;

  const fetchedData = useSalesFunnelApiDataProvider({
    idCarDealer: user?.defaultCarDealer.id,
    searchText,
    date: currentDate,
    carType: carFilter,
  });

  return (
    <>
      <SalesFunnelContext.Provider
        value={{
          dataHandlers: fetchedData,
          chatInventoryId,
          onChatInventoryIdChanged: setChatInventoryId,
          currentDate,
          onCurrentDateChanged,
          carFilter,
          onCarFilterChanged,
          searchText,
          onSearchTextChanged,
          onShowSetGoalsModal: () => setSetGoalsModalOpen(true),
          carSelection,
          setCarSelection,
        }}
      >
        <SetGoalsDialog
          isOpen={setGoalsModalOpen}
          onIsOpenChanged={(value: boolean) => setSetGoalsModalOpen(value)}
          idCarDealer={user?.defaultCarDealer.id}
          initialDate={currentDate}
          newCar={carFilter === "NEW"}
        />
        {children}
      </SalesFunnelContext.Provider>
    </>
  );
};

export default SalesFunnelProvider;
