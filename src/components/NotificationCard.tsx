import React, {Dispatch, SetStateAction} from "react";
import dayjs from "dayjs";
import {useTranslation} from "react-i18next";
import Box from "@mui/material/Box";
import Divider from "@mui/material/Divider";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import {styled, useTheme} from "@mui/material/styles";
import {useNavigate} from "react-router-dom";
import {useDispatch} from "react-redux";
import {CTA, Notification} from "src/types";
import {setNotificationStatusRead} from "src/state/reducers/notifications";
import {notificationTypeIcons} from "../utils/notificationTypeIcons";

interface NotificationCardProps {
  data: Notification;
  onClose: Dispatch<SetStateAction<boolean>>;
  small?: boolean;
  openModal?: Dispatch<SetStateAction<string | undefined>>;
}

const NotificationCard: React.FC<NotificationCardProps> = ({
  onClose,
  small,
  data,
  openModal,
}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  const navigate = useNavigate();

  const handleCtaActionClick = (cta: CTA) => {
    dispatch(setNotificationStatusRead(data.id));
    if (cta.type === "GO_TO_SALE_PAGE") {
      return navigate("/sales-performance");
    } else if (cta.type === "GO_TO_DELIVERY_CALENDAR") {
      return navigate("/sales-calendar");
    } else if (cta.type === "GO_TO_DASHBOARD_FINANCE") {
      onClose(false);
      return navigate("/dashboard?dep=FINANCE&sort=referenceStock");
    } else if (cta.type === "GO_TO_DASHBOARD_ALL_INV") {
      onClose(false);
      return navigate("/dashboard?sort=referenceStock");
    } else if (cta.type === "GO_TO_DASHBOARD_DELIVERY") {
      onClose(false);
      return navigate("/dashboard?dep=DELIVERY&sort=referenceStock");
    } else if (cta.type === "GO_TO_DASHBOARD_MECANIC") {
      onClose(false);
      return navigate("/dashboard?dep=MECHANIC&sort=referenceStock");
    } else if (cta.type === "GO_TO_DASHBOARD_ESTHETIC") {
      onClose(false);
      return navigate("/dashboard?dep=ESTHETIC&sort=referenceStock");
    } else if (cta.type === "OPEN_TO_STOCK_DETAIL") {
      if (openModal) {
        openModal(
          cta.ctaParams.reduce((acc: undefined | string, el) => {
            if (el.name === "idInventory") return el.value;
            return acc;
          }, undefined),
        );
        return;
      }
    }
  };

  const theme = useTheme();

  return (
    <Paper
      sx={{
        backgroundColor: "#F6F8FA",
        margin: small ? "18px 34px" : "0 0 18px 0",
        overflow: "hidden",
        boxShadow: "none",
        flex: 1,
      }}
    >
      <Box sx={{p: 2.3}}>
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <Paper
            style={{
              width: "32px",
              height: "32px",
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            {notificationTypeIcons[data.type]}
          </Paper>
          <NotifCategory>{t(`NotificationTitles.${data.type}`)}</NotifCategory>
        </Box>
        <Box sx={{display: "flex"}}>
          <Box
            sx={{
              flex: 1,
              display: "flex",
              flexDirection: small ? "column" : "row",
              pt: 2,
              pb: 1.2,
            }}
          >
            <Typography
              style={{
                flex: 1,
                marginBottom: 8,
              }}
            >
              {data.message}
            </Typography>
            <Typography
              sx={{
                cursor: "pointer",
                color: theme.palette.primary.main,
                textTransform: "uppercase",
                fontSize: 14,
                fontWeight: 600,
                display: "flex",
                alignItems: "center",
              }}
              onClick={() => {
                try {
                  handleCtaActionClick(data.cta);
                } catch (e) {
                  console.log("Something unexpected happened, ", e);
                }
              }}
            >
              {t("SEE DETAILS")} <ChevronRightIcon color="primary" />
            </Typography>
          </Box>
          <Box
            sx={{
              display: "flex",
              flexDirection: small ? "column" : "row",
              alignItems: small ? "flex-end" : "center",
              justifyContent: small ? "center" : "flex-end",
              pt: 2,
              pb: 1.2,
            }}
          >
            <ColorDot
              style={{
                backgroundColor:
                  data.status === "DELIVERED" ? "red" : "transparent",
              }}
            />
            <span
              style={{
                fontWeight: 600,
                margin: "8px 0 8px 20px",
              }}
            >
              {dayjs(data.createdDate).fromNow()}
            </span>
          </Box>
        </Box>
      </Box>
      <Box
        sx={{
          p: 2.3,
          backgroundColor: "#F0F4F7",
        }}
      >
        {data.details.length ? (
          <Paper
            sx={{
              display: "flex",
              flexWrap: "wrap",
            }}
          >
            {data.details.map((el, index) => {
              return (
                <React.Fragment key={index}>
                  <Box
                    sx={{
                      display: "flex",
                      justifyContent: "space-between",
                      px: 2,
                      py: 1.3,
                      width: small ? "100%" : "49%",
                    }}
                  >
                    <Typography
                      style={{
                        fontSize: 12,
                        marginRight: 15,
                      }}
                    >
                      {el.key}
                    </Typography>
                    <Typography
                      style={{
                        fontSize: 12,
                        fontWeight: 600,
                      }}
                    >
                      {dayjs(el.value).isValid()
                        ? dayjs(el.value).format("DD-MM-YYYY - hh:mm")
                        : el.value}
                    </Typography>
                  </Box>
                  {(index + 1) % 2 ? (
                    <Divider
                      orientation={small ? "horizontal" : "vertical"}
                      flexItem
                    />
                  ) : null}
                </React.Fragment>
              );
            })}
          </Paper>
        ) : null}
      </Box>
    </Paper>
  );
};

const ColorDot = styled("span")({
  height: "10px",
  width: "10px",
  borderRadius: "50%",
  margin: "8px 0 8px 20px",
});

const NotifCategory = styled("span")(({theme}) => ({
  padding: "6px 18px",
  backgroundColor: theme.palette.common.white,
  borderRadius: "30px",
  color: theme.palette.primary.main,
  fontWeight: 600,
}));

export default NotificationCard;
