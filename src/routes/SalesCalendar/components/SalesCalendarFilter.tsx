import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import ControlPoint from "@mui/icons-material/ControlPoint";
import clsx from "clsx";
import React from "react";
import {useTranslation} from "react-i18next";
import {CarTypes} from "src/types";
import {CarIcon, NewCarIcon, RecyclingIcon} from "src/assets/icons";
import {useSalesCalendarLayoutStyles} from "../utils/salesCalendarLayoutStyles";

type SalesCalendarFilterProps = {
  currentCarType: CarTypes;
  onSetCurrentCarType: (type: CarTypes) => void;
  onSetAddNewDeliveryOpen: (open: boolean) => void;
};

const SalesCalendarFilter: React.FC<SalesCalendarFilterProps> = ({
  currentCarType,
  onSetCurrentCarType: setCurrentCarType,
  onSetAddNewDeliveryOpen: setAddNewDeliveryOpen,
}) => {
  const classes = useSalesCalendarLayoutStyles();
  const {t} = useTranslation();
  return (
    <div className={classes.box}>
      <Paper
        style={{
          padding: "20px 0",
          height: "100%",
          display: "flex",
          flexDirection: "column",
          justifyContent: "space-between",
        }}
      >
        <List component="nav">
          {["ALL", "NEW", "USED"].map((el) => {
            return (
              <ListItem
                key={el}
                button
                className={clsx(
                  classes.listItemGutters,
                  currentCarType === el && classes.activeCarType,
                )}
                onClick={() => setCurrentCarType(el as CarTypes)}
              >
                <ListItemIcon>
                  {el === "ALL" ? (
                    <CarIcon />
                  ) : el === "NEW" ? (
                    <NewCarIcon />
                  ) : (
                    <RecyclingIcon />
                  )}
                </ListItemIcon>
                <ListItemText primary={t(`carTypes.${el}`)} />
              </ListItem>
            );
          })}
        </List>

        <Box
          onClick={() => setAddNewDeliveryOpen(true)}
          display={"flex"}
          alignItems={"center"}
          pl={3}
          style={{
            cursor: "pointer",
            width: "fit-content",
          }}
        >
          <IconButton size="small" color="primary" component="span">
            <ControlPoint
              style={{
                width: 29,
                height: 29,
              }}
            />
          </IconButton>
          <Typography className={classes.openDeliveryText} variant="body2">
            Add new delivery
          </Typography>
        </Box>
      </Paper>
    </div>
  );
};

export default SalesCalendarFilter;
