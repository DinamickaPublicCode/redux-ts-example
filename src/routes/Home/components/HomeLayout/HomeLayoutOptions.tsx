import React from "react";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import HomeLayoutMonthPicker from "./HomeLayoutMonthPicker";
import HomeLayoutInventoryDropdown from "./HomeLayoutInventoryDropdown";
import {styled} from "@mui/material/styles";

const HomeLayoutOptions = () => {
  return (
    <HomePageMonthPicker>
      <HomeHeader as="div">Home</HomeHeader>
      <Box
        sx={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <Box sx={{mr: 2.5}}>
          <HomeLayoutInventoryDropdown />
        </Box>
        <HomeLayoutMonthPicker />
      </Box>
    </HomePageMonthPicker>
  );
};

const HomePageMonthPicker = styled(Box)(({theme}) => ({
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
  justifyContent: "space-between",
  pl: 4.8,
  pr: 2.5,
  width: "95%",
  marginLeft: "2%",
  [theme.breakpoints.down("sm")]: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
}));

const HomeHeader = styled(Typography)(({theme}) => ({
  fontStyle: "normal",
  fontWeight: 600,
  fontSize: "18px",
  lineHeight: "22.32px",
  color: "#222238",
}));

export default HomeLayoutOptions;
