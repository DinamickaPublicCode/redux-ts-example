import React from "react";
import Box from "@mui/material/Box";
import SalesFunnelMarker from "../SalesFunnelMarker/SalesFunnelMarker";
import {useSalesFunnelContext} from "../../state/SalesFunnelProvider";

export const SalesFunnelFooter = () => {
  const {
    dataHandlers: {legends},
  } = useSalesFunnelContext();

  return (
    <Box
      sx={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        paddingTop: "31px",
        paddingBottom: "29px",
        width: "100%",
        zIndex: 6,
        position: "fixed",
        bottom: "0",
        background: "white",
        boxShadow: "0px 4px 20px rgba(203, 205, 217, 0.3)",
      }}
    >
      {legends?.map((legend) => (
        <SalesFunnelMarker key={legend.idLegend} legend={legend} />
      ))}
    </Box>
  );
};
