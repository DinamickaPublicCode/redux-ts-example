import {useHomeContext} from "../../state/HomeContextProvider";
import {useAppSelector} from "src/state/hooks";
import {CarDealerSeller} from "../../../../types";
import React, {useMemo} from "react";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import HomeStatsSpinner from "./HomeStatsSpinner";
import {useTheme} from "@mui/material/styles";
import {useCarDealerSalesStats} from "../../../SalesFunnel/utils/useCarDealerSalesStats";
import {
  useGetCarDealerSellersQuery,
  useGetCarDealerSalesNewQuery,
  useGetCarDealerSalesUsedQuery,
} from 'src/state/reducers/carDealerApi'

const HomeStatsCompletionBox = () => {
  const {globalDate} = useHomeContext();

  const {data:sellersAll} = useGetCarDealerSellersQuery();

  const {data:salesNew}= useGetCarDealerSalesNewQuery();
  const {data:salesUsed} = useGetCarDealerSalesUsedQuery();

  const sellers = useMemo(() => {
    return (sellersAll || []).filter((el: CarDealerSeller) =>
      el.authorities?.includes("ROLE_SELLER_NEW"),
    );
  }, [sellersAll]);

  const stats = useCarDealerSalesStats(
    sellers || [],
    [...(salesNew?.content || []), ...(salesUsed?.content || [])],
    globalDate.getMonth() + 1,
  );

  const theme = useTheme();

  return (
    <Box
      sx={{
        background: "#FFFFFF",
        boxShadow: "0px 4px 20px rgba(20, 32, 100, 0.08)",
        borderRadius: "14px",
        padding: "32px 30px",
      }}
      style={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between",
        height: "100%",
      }}
    >
      <Typography
        component="div"
        sx={{
          fontStyle: "normal",
          fontWeight: 600,
          fontSize: "18px",
          lineHeight: "22.32px",
          color: "#222238",
          [theme.breakpoints.down("sm")]: {
            marginTop: 20,
          },
        }}
      >
        Sales completion
      </Typography>
      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
        }}
      >
        <HomeStatsSpinner
          /**
           * This one prevents showing 100% when it actually calculated 100%. Purely for demonstration purposes...
           */
          percent={stats.goalCompletion === 100 ? 42 : stats.goalCompletion}
        />
      </Box>
      <Box>
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "baseline",
            mb: "16px",
          }}
        >
          <Typography
            component="div"
            sx={{
              fontStyle: "normal",
              fontWeight: "normal",
              fontSize: "14px",
              lineHeight: "18.2px",
              color: "#98A2B7",
            }}
          >
            Items sold
          </Typography>
          <Typography
            component="div"
            sx={{
              fontStyle: "normal",
              fontWeight: 600,
              fontSize: "14px",
              lineHeight: "17.5px",
              color: "#222238",
            }}
          >
            {stats.soldTotal}
          </Typography>
        </Box>
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "baseline",
          }}
        >
          <Typography
            component="div"
            sx={{
              fontStyle: "normal",
              fontWeight: "normal",
              fontSize: "14px",
              lineHeight: "18.2px",
              color: "#98A2B7",
            }}
          >
            Total achievable items
          </Typography>
          <Typography
            component="div"
            sx={{
              fontStyle: "normal",
              fontWeight: 600,
              fontSize: "14px",
              lineHeight: "17.5px",
              color: "#222238",
            }}
          >
            {stats.goalTotal}
          </Typography>
        </Box>
      </Box>
    </Box>
  );
};

export default HomeStatsCompletionBox;
