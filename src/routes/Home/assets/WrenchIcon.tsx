import React from "react";

const WrenchIcon = (color: string) => (
  <svg width={86} height={86} viewBox="0 0 86 86" fill="none">
    <g filter="url(#prefix__filter0_d)">
      <rect x={20} y={18} width={46} height={46} rx={23} fill="#fff" />
      <rect x={20.5} y={18.5} width={45} height={45} rx={22.5} stroke={color} />
    </g>
    <path
      d="M53.93 39.779c-.034-.31-.395-.542-.706-.542a2.443 2.443 0 01-2.278-1.507 2.45 2.45 0 01.615-2.72.611.611 0 00.067-.83 10.886 10.886 0 00-1.742-1.76.613.613 0 00-.837.068c-.655.726-1.833.996-2.743.616a2.443 2.443 0 01-1.486-2.388.61.61 0 00-.54-.643 10.976 10.976 0 00-2.475-.006.613.613 0 00-.544.63 2.448 2.448 0 01-1.507 2.346c-.899.367-2.068.1-2.723-.62a.613.613 0 00-.83-.07c-.659.518-1.258 1.11-1.78 1.76a.612.612 0 00.066.836 2.438 2.438 0 01.615 2.744c-.38.905-1.318 1.487-2.393 1.487a.598.598 0 00-.636.54 11.028 11.028 0 00-.004 2.502c.035.31.406.54.721.54.958-.023 1.878.57 2.264 1.508.384.938.137 2.03-.616 2.72a.61.61 0 00-.066.829c.51.65 1.097 1.243 1.74 1.76a.612.612 0 00.837-.066c.658-.728 1.836-.997 2.742-.616a2.44 2.44 0 011.489 2.387.61.61 0 00.54.642 10.924 10.924 0 002.474.007.612.612 0 00.545-.63 2.445 2.445 0 011.505-2.345c.905-.37 2.07-.1 2.724.62a.614.614 0 00.83.069 10.95 10.95 0 001.78-1.76.611.611 0 00-.066-.836 2.437 2.437 0 01-.616-2.744 2.462 2.462 0 012.25-1.491l.136.004a.612.612 0 00.644-.54c.097-.827.098-1.668.005-2.501zM43 47.342a6.341 6.341 0 115.445-3.09l-2.777-2.777c.2-.474.307-.988.307-1.518a3.861 3.861 0 00-1.139-2.75 3.862 3.862 0 00-2.749-1.138c-.346 0-.691.046-1.025.137a.44.44 0 00-.304.307.43.43 0 00.122.42s1.37 1.382 1.83 1.84c.047.048.047.163.04.204l-.004.03c-.046.505-.135 1.11-.209 1.343l-.029.028-.03.03c-.236.076-.85.167-1.363.212v-.002l-.023.006a.293.293 0 01-.233-.088c-.477-.478-1.794-1.786-1.794-1.786a.448.448 0 00-.32-.142.435.435 0 00-.41.33 3.9 3.9 0 001.003 3.767 3.863 3.863 0 002.75 1.14 3.88 3.88 0 001.517-.308l2.808 2.807a6.31 6.31 0 01-3.413.998z"
      fill={color}
    />
    <defs>
      <filter
        id="prefix__filter0_d"
        x={0}
        y={0}
        width={86}
        height={86}
        filterUnits="userSpaceOnUse"
        colorInterpolationFilters="sRGB"
      >
        <feFlood floodOpacity={0} result="BackgroundImageFix" />
        <feColorMatrix
          in="SourceAlpha"
          values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
        />
        <feOffset dy={2} />
        <feGaussianBlur stdDeviation={10} />
        <feColorMatrix values="0 0 0 0 0.796875 0 0 0 0 0.804464 0 0 0 0 0.85 0 0 0 0.5 0" />
        <feBlend in2="BackgroundImageFix" result="effect1_dropShadow" />
        <feBlend in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
      </filter>
    </defs>
  </svg>
);

export default WrenchIcon;
