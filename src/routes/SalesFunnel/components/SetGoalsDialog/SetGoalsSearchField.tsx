import DeliveryModalSearch from "../../../../components/DeliveryModalSearch";
import React from "react";

export const SetGoalsSearchField = ({
  value,
  onChange,
  onSubmit: handleSubmit,
}: {
  value: string;
  onChange: (s: string) => void;
  onSubmit: (s: string) => void;
}) => {
  return (
    <form
      onSubmit={(e) => {
        e.preventDefault();
        handleSubmit(value);
      }}
    >
      <DeliveryModalSearch
        value={value}
        onChange={(e) => onChange(e.target.value)}
      />
    </form>
  );
};
