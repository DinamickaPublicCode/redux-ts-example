import {useState, useRef} from "react";
import dayjs from "dayjs";
import StaticDatePicker from "@mui/lab/StaticDatePicker";
import Box from "@mui/material/Box";
import Popper from "@mui/material/Popper";
import Grow from "@mui/material/Grow";
import Paper from "@mui/material/Paper";
import ClickAwayListener from "@mui/material/ClickAwayListener";
import TextField from "@mui/material/TextField";

const MonthPickerPopper = ({
  value,
  onChange,
  children,
  isYear,
}: {
  isYear?: boolean;
  value: Date;
  onChange: (date: Date) => void;
  children: JSX.Element | JSX.Element[] | string;
}) => {
  const anchorRef = useRef<HTMLElement>(null);
  const [open, setOpen] = useState(false);

  return (
    <Box>
      <Box
        ref={anchorRef}
        onClick={(event) => {
          event.stopPropagation();
          setOpen((prevOpen) => !prevOpen);
        }}
      >
        {children}
      </Box>
      <Popper
        open={open}
        anchorEl={anchorRef.current}
        role={undefined}
        transition
        disablePortal
        placement={"bottom"}
        style={{zIndex: 100}}
      >
        {({TransitionProps, placement}) => (
          <Grow
            {...TransitionProps}
            style={{
              zIndex: 1000,
              transformOrigin:
                placement === "bottom" ? "center top" : "center bottom",
            }}
          >
            <Paper
              style={{
                marginTop: 16,
                boxShadow: "0px 4px 20px rgba(203, 205, 217, 0.3)",
                overflow: "hidden",
              }}
            >
              <ClickAwayListener
                onClickAway={(event) => {
                  if (
                    anchorRef.current &&
                    anchorRef.current.contains(event.target as HTMLElement)
                  ) {
                    return;
                  }
                  setOpen(false);
                }}
              >
                <Box>
                  <StaticDatePicker
                    disableCloseOnSelect
                    // Commenting obsolete month props.
                    // leftArrowIcon={<NavigateBefore color="primary" />}
                    // rightArrowIcon={<NavigateNext color="primary" />}
                    // leftArrowButtonProps={{
                    //   style: {backgroundColor: "#F1F0FF"},
                    // }}
                    // rightArrowButtonProps={{
                    //   style: {backgroundColor: "#F1F0FF"},
                    // }}
                    displayStaticWrapperAs={"desktop"}
                    onChange={(d) => {
                      if (!d) {
                        return;
                      }
                      // It appears that for some reason the StaticDatePicker brings the
                      // moment date instead of the regular one...
                      // That's the fix which would resolve the date object
                      let incomingDate = d;
                      if (!("getFullYear" in incomingDate)) {
                        incomingDate = (d as unknown as dayjs.Dayjs).toDate();
                      }
                      onChange(incomingDate);
                    }}
                    value={value}
                    views={[!isYear ? "month" : "year"]}
                    renderInput={(props) => <TextField {...props} />}
                  />
                </Box>
              </ClickAwayListener>
            </Paper>
          </Grow>
        )}
      </Popper>
    </Box>
  );
};

export default MonthPickerPopper;
