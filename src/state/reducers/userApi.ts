import baseQuery from "src/state/baseQuery";
import {createApi} from '@reduxjs/toolkit/query/react';
import {setLoading} from './loading';
import {UserType} from 'src/types';

export const userApi = createApi({
  reducerPath: 'userApi',
  baseQuery,
  endpoints: (builder) => ({
    getUser: builder.query<UserType, void>({
      query: () => ({
        url: `/mock/account.json`,
        method: 'GET',
      }),
      async onQueryStarted(_, {dispatch, queryFulfilled}) {
        try {
          dispatch(setLoading(true));
          await queryFulfilled;
          dispatch(setLoading(false));
        } catch (error) {
          dispatch(setLoading(false));
        }
      },
    }),
  }),
});

export const {useGetUserQuery} = userApi;
