import React from "react";
import {VehicleIcon, TruckIcon, TimeIcon} from "src/assets/icons";

import SettingsOutlinedIcon from "@mui/icons-material/SettingsOutlined";

export const notificationTypeIcons = {
  SALE: (
    <VehicleIcon
      style={{
        height: 16,
        width: 16,
      }}
    />
  ),
  FINANCE: (
    <TruckIcon
      style={{
        height: 16,
        width: 16,
      }}
    />
  ),
  DELIVERY: (
    <TimeIcon
      style={{
        height: 16,
        width: 16,
      }}
    />
  ),
  MECHANIC: (
    <SettingsOutlinedIcon
      style={{
        height: 19,
        width: 19,
      }}
      color="primary"
    />
  ),
  ESTHETIC: (
    <SettingsOutlinedIcon
      style={{
        height: 19,
        width: 19,
      }}
      color="primary"
    />
  ),
};
