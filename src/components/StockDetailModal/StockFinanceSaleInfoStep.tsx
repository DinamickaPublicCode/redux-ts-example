import React, {useMemo} from "react";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import {Formik} from "formik";

import {StepsCheckedIcon, StepsUpIcon} from "src/assets/icons";
import StockFinanceAccordion from "./StockFinanceAccordion";
import CartagDatePicker from "../CartagDatePicker";
import CartagInputField from "../CartagInputField";
import * as yup from "yup";
import {SaleInformation} from "../../types/stockFinanceTab";
import {useTabsStyles} from "./StockDeliveryTab";
import {useTranslation} from "react-i18next";
import {STOCK_DETAIL_TRANSLATION_KEY} from "src/system/constants";

interface StockFinanceStepProps<T> {
  isEditing: boolean;
  isValidated: boolean;
  initialValues: T;
  onSetEditing: () => void;
  onSubmit: (values: T) => void;
}

export const StockFinanceSaleInfoStep = ({
  isEditing,
  isValidated,
  initialValues,
  onSubmit,
  onSetEditing,
}: StockFinanceStepProps<SaleInformation>) => {
  const {t: ft} = useFinanceTabTranslation();
  const classes = useTabsStyles();
  const shouldHideButton = !isValidated && !isEditing;

  const validationSchema: yup.ObjectSchema<
    yup.Shape<object | undefined, SaleInformation>
  > = useMemo(() => {
    return yup.object().shape({
      nameSeller: yup.string().required("Name Seller is required"),
      nameFinanceManager: yup
        .string()
        .required("Name Finance manager is required"),
      saleDate: yup.date().required("Sale date is required"),
      limitPreparationDate: yup
        .string()
        .required("Limit preparation date is required"),
      deliveryBeginDate: yup.string(),
    });
  }, []);

  return (
    <Formik
      initialValues={initialValues}
      enableReinitialize
      validationSchema={validationSchema}
      onSubmit={async (values) => {
        onSubmit(values);
      }}>
      {({
        handleSubmit,
        handleChange,
        handleBlur,
        values,
        touched,
        errors,
        setFieldValue,
      }) => (
        <StockFinanceAccordion
          onButtonClicked={
            shouldHideButton
              ? undefined
              : () => {
                  if (!isEditing) {
                    onSetEditing();
                    return;
                  }
                  handleSubmit();
                }
          }
          isEditButton={isValidated}
          stepIcon={isValidated ? <StepsCheckedIcon /> : <StepsUpIcon />}
          panelId={`${ft("Step")} 2`}
          panelName={ft("Sale_Reference")}
          open={isEditing}>
          <form
            style={{
              padding: 24,
              width: "100%",
            }}
            onSubmit={handleSubmit}>
            <Grid container spacing={1}>
              <Grid container item xs={12} spacing={6}>
                <Grid item xs={6}>
                  <CartagInputField
                    labelText={ft("Representant")}
                    name="nameSeller"
                    error={!!touched.nameSeller && !!errors.nameSeller}
                    helperText={errors.nameSeller}
                    value={values.nameSeller}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                </Grid>
                <Grid item xs={6}>
                  <CartagInputField
                    labelText={ft("Finance Manager")}
                    name="nameFinanceManager"
                    error={
                      !!touched.nameFinanceManager &&
                      !!errors.nameFinanceManager
                    }
                    helperText={errors.nameFinanceManager}
                    value={values.nameFinanceManager}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                </Grid>
              </Grid>
              <Grid container item xs={12} spacing={6}>
                <Grid item xs={6}>
                  <Typography className={classes.inputLabel}>
                    {ft("Sale_Date")}
                  </Typography>
                  <CartagDatePicker
                    error={!!touched.saleDate && !!errors.saleDate}
                    helperText={
                      errors.saleDate as unknown as string | undefined
                    }
                    valueDate={values.saleDate}
                    onChangeDate={(value) => setFieldValue("saleDate", value)}
                  />
                </Grid>
                <Grid item xs={6}>
                  <Typography className={classes.inputLabel}>
                    {ft("Limit_Date")}
                  </Typography>
                  <CartagDatePicker
                    error={
                      !!touched.limitPreparationDate &&
                      !!errors.limitPreparationDate
                    }
                    helperText={
                      errors.limitPreparationDate as unknown as
                        | string
                        | undefined
                    }
                    valueDate={new Date(values.limitPreparationDate)}
                    onChangeDate={(value) => {
                      try {
                        setFieldValue("limitPreparationDate", value);
                      } catch (e) {
                        setFieldValue("limitPreparationDate", "");
                      }
                    }}
                  />
                </Grid>
              </Grid>
              <Grid container item xs={12} spacing={6}>
                <Grid item xs={6}>
                  <Typography className={classes.inputLabel}>
                    {ft("Delivery_Date")}
                  </Typography>
                  <CartagDatePicker
                    isDateTimePicker
                    valueDate={new Date(values.deliveryBeginDate)}
                    onChangeDate={(value) => {
                      try {
                        setFieldValue("deliveryBeginDate", value);
                      } catch (e) {
                        setFieldValue("deliveryBeginDate", "");
                      }
                    }}
                  />
                </Grid>
              </Grid>
            </Grid>
          </form>
        </StockFinanceAccordion>
      )}
    </Formik>
  );
};

const useFinanceTabTranslation = () => {
  const {t} = useTranslation();
  return {
    t: (key: string) => t(`${STOCK_DETAIL_TRANSLATION_KEY}.FINANCE_TAB.${key}`),
  };
};
