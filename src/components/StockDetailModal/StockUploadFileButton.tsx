import React, {useRef, useState, useEffect} from "react";
import CircularProgress from "@mui/material/CircularProgress";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import ControlPoint from "@mui/icons-material/ControlPoint";
import {DocumentInputFieldData} from "src/types/documentBoxInterfaces";
import {useTabsStyles} from "./StockDeliveryTab";

interface StockUploadFileButtonProps {
  typeId?: string;
  title: string;
  multiple?: boolean;
  accept?: string;
  onClick?: () => void;
  onFileSelectChanged: (e: DocumentInputFieldData) => void;
  loading?: boolean;
}

/**
 * Please note, that file input elements don't work the same way
 * the other input elements are. React synthetic events don't
 * contain the file list information, thus it's necessary to query file list
 * from the html ref directly.
 *
 * Refer to the documentation:
 * https://reactjs.org/docs/uncontrolled-components.html#the-file-input-tag
 */
const StockUploadFileButton: React.FC<StockUploadFileButtonProps> = ({
  typeId,
  title,
  multiple = false,
  accept,
  onFileSelectChanged,
  loading,
}) => {
  const classes = useTabsStyles();
  const fileInput = useRef<HTMLInputElement | null>(null);
  const [inputRefreshState, setInputRefreshState] = useState(false);

  useEffect(() => {
    if (
      fileInput.current &&
      fileInput.current?.files &&
      fileInput.current?.validity &&
      fileInput.current?.files.length
    ) {
      onFileSelectChanged({
        files: fileInput.current?.files,
        validity: fileInput.current?.validity,
      });
    }
    // eslint-disable-next-line
  }, [inputRefreshState]);

  return (
    <div>
      <input
        ref={fileInput}
        multiple={multiple}
        accept={accept}
        style={{
          display: "none",
        }}
        id={`icon-button-file-${typeId}`}
        type="file"
        onChange={(_) => setInputRefreshState((prevState) => !prevState)}
      />
      <label
        htmlFor={`icon-button-file-${typeId}`}
        style={{
          display: "flex",
          alignItems: "center",
          cursor: "pointer",
          width: "fit-content",
        }}
      >
        <IconButton size="small" color="primary" component="span">
          {loading ? (
            <CircularProgress size={29} />
          ) : (
            <ControlPoint
              style={{
                width: 29,
                height: 29,
              }}
            />
          )}
        </IconButton>
        <Typography className={classes.plusIconText} variant="body2">
          {title}
        </Typography>
      </label>
    </div>
  );
};

export default StockUploadFileButton;
