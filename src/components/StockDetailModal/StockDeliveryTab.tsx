import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import React, {useEffect, useState} from "react";
import {useTranslation} from "react-i18next";
import StockInputFieldBody from "./StockInputFieldBody";
import CartagDatePicker from "../CartagDatePicker";
import {FinanceSaleInfo, FinanceTabType} from "src/types";
import CartagInputField from "../CartagInputField";
import * as yup from "yup";
import {Formik} from "formik";
import CartagBackdrop from "../CartagBackdrop";
import useCustomErrorSnackbar from "src/utils/useCustomErrorSnackbar";
import useCustomInfoSnackbar from "src/utils/useCustomInfoSnackbar";
import {Theme} from "@mui/material/styles";
import createStyles from "@mui/styles/createStyles";
import makeStyles from "@mui/styles/makeStyles";
import {DELIVERY_TAB_TRANSLATION_KEY} from "../../system/constants";
import {GenericFormBoxHeader} from "../GenericFormBoxHeader";
import {GenericFormBox} from "../GenericFormBox";
import {GenericFormSubmitButton} from "../GenericFormSubmitButton";
import {useAppSelector} from "src/state/hooks";
import {useGetUserQuery} from 'src/state/reducers/userApi';

const validationSchema = yup.object().shape({
  deliveryStartDate: yup.date().required("Start date is required"),
  deliveryEndDate: yup.date().required("End date is required"),
});

/**
 * TODO: This one also needs its own place
 * also used in add delivery modal
 *
 * @param param0
 */
export const FormSubtitle = ({children}: {children: string}) => (
  <Typography
    variant="h6"
    style={{
      fontSize: 18,
      fontWeight: 600,
      margin: "30px 0",
    }}
  >
    {children}
  </Typography>
);

export default function StockDeliveryTab({
  currentDeal,
  revalidateInventory,
}: {
  currentDeal: FinanceTabType | null;
  revalidateInventory: () => void;
}) {
  const {t} = useTranslation();
  const {data: user} = useGetUserQuery();;
  const baseKey = `${DELIVERY_TAB_TRANSLATION_KEY}.FORM.`;
  const enqueueErrorSnackbar = useCustomErrorSnackbar();
  const enqueueInfoSnackbar = useCustomInfoSnackbar();
  const [isLoading, setIsLoading] = useState(false);

  let sale: FinanceSaleInfo | undefined;
  try {
    sale = currentDeal?.sales[0];
  } catch (error) {
    sale = currentDeal?.saleInfo;
  }

  const extractDataFromCurrentDeal = (deal: FinanceTabType | null) => ({
    deliveryStartDate: deal?.saleInfo?.deliveryInfo?.deliveryBeginDate || null,
    deliveryEndDate: deal?.saleInfo?.deliveryInfo?.deliveryEndDate || null,
  });
  const [inputsData, setInputsData] = useState(
    extractDataFromCurrentDeal(currentDeal),
  );

  useEffect(() => {
    setInputsData(extractDataFromCurrentDeal(currentDeal));
  }, [currentDeal]);

  return (
    <>
      <CartagBackdrop open={isLoading} />
      <Formik
        initialValues={inputsData}
        enableReinitialize
        validationSchema={validationSchema}
        onSubmit={async (values, {setSubmitting, resetForm}) => {
          if (currentDeal && user) {
            setSubmitting(true);
            setIsLoading(true);
            try {
              // await updateInventoryData(
              //   {
              //     action: UpdateInventoryDataType.DELIVERY,
              //     idCarDealer: currentDeal.idCarDealer,
              //     idUser: user?.id,
              //     userName: user?.fullName,
              //     sales: [
              //       {
              //         deliveryInfo: {
              //           deliveryBeginDate: values?.deliveryStartDate?.toISOString(),
              //           getDeliveryEndDate: values?.deliveryEndDate?.toISOString(),
              //         },
              //       },
              //     ],
              //   },
              //   currentDeal.id
              // );
              enqueueInfoSnackbar("Success");
            } catch (e) {
            } finally {
              revalidateInventory();
              resetForm({
                values: {
                  deliveryStartDate: null,
                  deliveryEndDate: null,
                },
              });
              setIsLoading(false);
              setSubmitting(false);
            }
          } else {
            enqueueErrorSnackbar(
              "Invalid incoming data. Check currentDeal or user",
            );
          }
        }}
      >
        {({handleSubmit, values, touched, errors, setFieldValue}) => (
          <form onSubmit={handleSubmit}>
            <GenericFormBox
              header={
                <GenericFormBoxHeader>
                  <Typography
                    variant="h6"
                    style={{
                      fontSize: 18,
                      fontWeight: 600,
                    }}
                  >
                    {t(DELIVERY_TAB_TRANSLATION_KEY + ".MODIFY_DELIVERY")}
                  </Typography>
                </GenericFormBoxHeader>
              }
            >
              <Grid container spacing={5}>
                <Grid item xs>
                  <CartagInputField
                    disabled
                    value={sale?.saleReference || ""}
                    labelText={t(baseKey + "REFERENCE_OF_THE_SALE")}
                  />
                </Grid>
                <Grid item xs>
                  <CartagInputField
                    disabled
                    value={sale?.sellerInfo?.nameSeller || ""}
                    labelText={t(baseKey + "REPRESENTATIVE")}
                  />
                </Grid>
              </Grid>
              <Grid container spacing={5}>
                <Grid item xs={6}>
                  <CartagInputField
                    value={sale?.creditInfo?.nameFinanceManager || ""}
                    disabled
                    labelText={t(baseKey + "FINANCIAL_DIRECTOR")}
                  />
                </Grid>
              </Grid>
              <FormSubtitle>{t("Client Information")}</FormSubtitle>

              <Grid container spacing={5}>
                <Grid item xs>
                  <CartagInputField
                    disabled
                    value={
                      sale?.clientInfo?.name || sale?.clientInfo?.fullName || ""
                    }
                    labelText={t(baseKey + "CUSTOMER_NAME")}
                  />
                </Grid>
                <Grid item xs>
                  <CartagInputField
                    value={sale?.clientInfo?.telNumber1 || ""}
                    disabled
                    labelText={t(baseKey + "PHONE")}
                  />
                </Grid>
              </Grid>

              <Grid container spacing={5}>
                <Grid item xs={6}>
                  <CartagInputField
                    value={sale?.clientInfo?.email || ""}
                    disabled
                    labelText={t(baseKey + "EMAIL")}
                  />
                </Grid>
              </Grid>

              <FormSubtitle>{t("Delivery Information")}</FormSubtitle>

              <Grid container spacing={5}>
                <Grid item xs>
                  <StockInputFieldBody
                    label={t(baseKey + "DELIVERY_START_DATE")}
                  >
                    <CartagDatePicker
                      margin="none"
                      valueDate={values.deliveryStartDate}
                      error={
                        !!touched.deliveryStartDate &&
                        !!errors.deliveryStartDate
                      }
                      helperText={errors.deliveryStartDate}
                      onChangeDate={(value) =>
                        setFieldValue("deliveryStartDate", value)
                      }
                    />
                  </StockInputFieldBody>
                </Grid>
                <Grid item xs>
                  <StockInputFieldBody label={t(baseKey + "DELIVERY_END_DATE")}>
                    <CartagDatePicker
                      margin="none"
                      error={
                        !!touched.deliveryEndDate && !!errors.deliveryEndDate
                      }
                      helperText={errors.deliveryEndDate}
                      valueDate={values.deliveryEndDate}
                      onChangeDate={(value) =>
                        setFieldValue("deliveryEndDate", value)
                      }
                    />
                  </StockInputFieldBody>
                </Grid>
              </Grid>
            </GenericFormBox>

            <GenericFormSubmitButton>
              {t(`${DELIVERY_TAB_TRANSLATION_KEY}.DELIVERY`)}
            </GenericFormSubmitButton>
          </form>
        )}
      </Formik>
    </>
  );
}
export const useTabsStyles = makeStyles((theme: Theme) =>
  createStyles({
    plusIconText: {
      marginLeft: 5,
      fontSize: 16,
      fontWeight: 600,
      color: theme.palette.primary.main,
    },
    tabPaper: {
      border: "2px solid #F6F8FA",
      overflow: "hidden",
    },
    inputLabel: {
      fontSize: 14,
      color: "#98A2B7",
    },
    input: {
      backgroundColor: "#F6F8FA",
      "&:hover .MuiOutlinedInput-notchedOutline, & .MuiOutlinedInput-notchedOutline":
        {
          borderColor: "rgba(0, 0, 0, 0);",
        },
      "&.Mui-focused .MuiOutlinedInput-notchedOutline": {
        borderColor: theme.palette.primary.main,
        borderWidth: 2,
      },
      "&.Mui-error .MuiOutlinedInput-notchedOutline": {
        borderColor: theme.palette.error.main,
        borderWidth: 2,
      },
    },
    salesSubTitle: {
      fontSize: 18,
      fontWeight: 600,
      margin: "30px 0",
      width: "100%",
    },
    submitButtonWrapper: {
      display: "flex",
      justifyContent: "flex-end",
      backgroundColor: "#F6F8FA",
      padding: "25px 32px",
    },
    submitButton: {
      padding: "8px 30px",
      textTransform: "uppercase",
      fontSize: 14,
      fontWeight: 600,
    },
    gridSpacing: {
      padding: "0 20px 30px",
    },
  }),
);
