import React from "react";
import CartagMonthPicker, {
  CustomMonthPickerRangeType,
} from "src/components/CartagMonthPicker";
import {styled, useTheme} from "@mui/material/styles";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import HomePerformanceFilter from "./HomePerformanceFilter";
import HomePerformanceLegends from "./HomePerformanceLegends";

/**
 * Heading for preparation performance block
 *
 * Contains title, legends, dropdown and the month picker inside.
 */
const HomePerformanceHeader = ({
  legendLabels,
  date,
  setDate,
  rangeType,
  setRangeType,
  departmentType,
  setDepartmentType,
}: {
  legendLabels: string[];
  date: Date;
  rangeType: CustomMonthPickerRangeType;
  setDate: (date: Date) => void;
  setRangeType: React.Dispatch<
    React.SetStateAction<CustomMonthPickerRangeType>
  >;
  departmentType: "mechanic" | "esthetic";
  setDepartmentType: React.Dispatch<
    React.SetStateAction<"mechanic" | "esthetic">
  >;
}) => {
  const theme = useTheme();
  return (
    <>
      <Box
        sx={{
          width: "100%",
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
          mb: "30px",
        }}
      >
        <BlockTitle as="div">Preparation Performance</BlockTitle>
        <BlueRedButtonsHidden>
          {legendLabels.length && (
            <HomePerformanceLegends labels={legendLabels} />
          )}
          <Box sx={{width: "25px"}} />
          <HomePerformanceFilter
            departmentType={departmentType}
            setDepartmentType={setDepartmentType}
          />
          <Box sx={{width: "25px"}} />
          <Box
            sx={{
              width: "255px",
            }}
          >
            <Box
              sx={{
                [theme.breakpoints.down("sm")]: {
                  display: "none",
                },
              }}
            >
              <CartagMonthPicker
                rangeType={rangeType}
                setRangeType={setRangeType}
                date={date}
                onDateChanged={setDate}
              />
            </Box>
          </Box>
        </BlueRedButtonsHidden>
      </Box>
      <Grid
        item
        xs={12}
        sx={{
          display: "none",
          [theme.breakpoints.down("sm")]: {
            display: "block",
          },
        }}
      >
        <CartagMonthPicker
          rangeType={rangeType}
          setRangeType={setRangeType}
          date={date}
          onDateChanged={setDate}
        />
      </Grid>
    </>
  );
};

const BlockTitle = styled(Typography)(({theme}) => ({
  fontStyle: "normal",
  fontWeight: 600,
  fontSize: "18px",
  lineHeight: "22.32px",
  color: "#222238",
  [theme.breakpoints.down("sm")]: {
    marginTop: 20,
  },
}));

const BlueRedButtonsHidden = styled(Box)(({theme}) => ({
  display: "flex",
  alignItems: "center",
  width: "100%",

  [theme.breakpoints.down("sm")]: {
    display: "none",
  },
}));

export default HomePerformanceHeader;
