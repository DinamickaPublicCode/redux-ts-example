import React from "react";
import NotificationsListLayout from "./components/NotificationsListLayout";

const NotificationsList = () => {
  return <NotificationsListLayout />;
};

export default NotificationsList;
