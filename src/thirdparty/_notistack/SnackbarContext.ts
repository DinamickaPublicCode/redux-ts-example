/* eslint-disable @typescript-eslint/ban-ts-comment */
import React from "react";
import {ProviderContext} from ".";

// @ts-ignore
export default React.createContext<ProviderContext>();
