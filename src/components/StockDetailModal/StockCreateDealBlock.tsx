import React from "react";
import clsx from "clsx";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import IconButton from "@mui/material/IconButton";
import Tooltip from "@mui/material/Tooltip";
import Typography from "@mui/material/Typography";
import {useTranslation} from "react-i18next";
import { ReactComponent as CheckboxDoneIcon } from "src/assets/icons/CheckboxDoneIcon.svg";
import {FinanceTabType} from "src/types";
import dayjs from "dayjs";
import {Theme} from "@mui/material/styles";

import createStyles from "@mui/styles/createStyles";
import makeStyles from "@mui/styles/makeStyles";

const StockCreateDealBlock = ({
  currentDeal,
}: {
  currentDeal: FinanceTabType | null;
}) => {
  const classes = useCreateDealStyles();
  const {t} = useTranslation();

  const stage = currentDeal?.stageInfo?.stage;
  const pipelineStages = currentDeal?.stageInfo?.pipelineStages;
  const sortedPipeline =
    pipelineStages &&
    pipelineStages.sort((a, b) => {
      if (a.order < b.order) return -1;
      if (a.order > b.order) return 1;
      return 0;
    });
  const lastPipelineDone =
    sortedPipeline &&
    sortedPipeline.filter((el) => el.status === "DONE").reverse();

  return (
    <Grid container className={classes.infoWrapper}>
      <Grid item xs={2}>
        <Typography
          style={{fontSize: 14}}
          variant="body1"
          color="textSecondary"
        >
          {t("DashboardTableHead.stage")}
        </Typography>
        <Typography
          variant="body1"
          color="textPrimary"
          style={{
            fontWeight: 600,
            fontSize: 14,
          }}
        >
          {stage && t(`stages.${stage}`)}
        </Typography>
      </Grid>
      <Grid item xs={12} sm={6}>
        <Box>
          {sortedPipeline &&
            sortedPipeline.map((item, id) => {
              return (
                <Tooltip key={id} title={item.description} arrow>
                  {item.status === "DONE" ? (
                    <IconButton
                      size="small"
                      className={clsx(
                        classes.iconStage,
                        classes.iconStageChecked,
                      )}
                    >
                      <CheckboxDoneIcon />
                    </IconButton>
                  ) : (
                    <IconButton
                      size="small"
                      className={clsx(
                        classes.iconStage,
                        classes.iconStageNotChecked,
                      )}
                    >
                      {item.abv.toUpperCase()}
                    </IconButton>
                  )}
                </Tooltip>
              );
            })}
        </Box>
      </Grid>
      <Grid
        item
        xs={4}
        sm={4}
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        {lastPipelineDone?.length && lastPipelineDone[0].stageDate ? (
          <Box>
            <Typography
              style={{
                fontSize: 14,
              }}
              variant="body1"
              color="textSecondary"
            >
              {t("Deal created")}
            </Typography>
            <Typography
              variant="body1"
              color="textPrimary"
              style={{
                fontWeight: 600,
                fontSize: 14,
              }}
            >
              {dayjs(lastPipelineDone[0].stageDate).format("DD/MM/YYYY hh:mm")}
            </Typography>
          </Box>
        ) : (
          ""
        )}
      </Grid>
    </Grid>
  );
};

const useCreateDealStyles = makeStyles((theme: Theme) =>
  createStyles({
    infoWrapper: {
      backgroundColor: "#F6F8FA",
      padding: "15px 30px",
      alignItems: "center",
    },
    iconStage: {
      borderRadius: 6,
      height: 26,
      width: 26,
      padding: 0,
      margin: "0 6px",
      fontWeight: 600,
      fontSize: 10,
    },
    iconStageChecked: {
      backgroundColor: "#1A06F9",
      boxShadow: "0px 3px 10px rgba(2, 53, 236, 0.3)",
      "&:hover": {
        backgroundColor: "#1A06F9",
      },
    },
    iconStageNotChecked: {
      backgroundColor: theme.palette.common.white,
      boxShadow: "0px 4px 12px rgba(203, 205, 217, 0.25)",
    },
  }),
);

export default StockCreateDealBlock;
