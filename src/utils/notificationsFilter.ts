import {Notification, NotificationType} from "src/types";

export const filterNotifications = (
  notificationsInitial: Notification[] | undefined,
  switchCheck: "DELIVERED" | "ALL",
  switchCheckArchived: boolean,
  activeFilters: NotificationType[],
): Notification[] => {
  const notifications = notificationsInitial || [];
  return (
    switchCheck !== "DELIVERED"
      ? notifications
      : notifications?.filter((x) => x.status === "DELIVERED")
  )
    .filter((x) =>
      activeFilters.length ? activeFilters.includes(x.type) : true,
    )
    .filter((x) => (switchCheckArchived ? !x.active : x.active));
};
