import DialogTitle from "@mui/material/DialogTitle";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import Divider from "@mui/material/Divider";
import React from "react";
import {useCommonModalStyles} from "../utils/useCommonModalStyles";

export const GenericFormModalTitle = ({
  title,
  children,
  handleClose,
}: {
  title: string;
  children?: JSX.Element;
  handleClose: () => void;
}) => {
  const styles = useCommonModalStyles();
  return (
    <>
      <DialogTitle
        classes={{
          root: styles.dialogTitle,
        }}
        id="alert-dialog-archive-title"
      >
        <Grid container alignItems="center">
          <Grid item xs={6}>
            <Typography variant="h6" className={styles.titleText}>
              {title}
            </Typography>
          </Grid>
          <Grid item xs={1} />
          <Grid item xs={4}>
            <Box
              flex="1"
              display="flex"
              alignItems="flex-end"
              flexDirection="column"
            >
              {children}
            </Box>
          </Grid>
          <Grid item xs={1} />
        </Grid>
        <IconButton
          size="small"
          style={{
            position: "absolute",
            right: 30,
          }}
          onClick={handleClose}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <Divider />
    </>
  );
};

export default GenericFormModalTitle;
