import Box from "@mui/material/Box";
import React from "react";
import HomeStatsSpinnerSvg from "./HomeStatsSpinnerSvg";
import HomeStatsSpinnerImpl from "./HomeStatsSpinnerImpl";

const HomeStatsSpinner = ({percent}: {percent: number}) => {
  return (
    <Box
      sx={{
        position: "relative",
      }}
    >
      <HomeStatsSpinnerImpl percent={percent} />
      <HomeStatsSpinnerSvg />
    </Box>
  );
};

export default HomeStatsSpinner;
