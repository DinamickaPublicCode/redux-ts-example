import React, {useMemo, useState} from "react";
import {useTranslation} from "react-i18next";

import {
  FinanceSaleInfo,
  FinanceTabType,
  UpdateFinanceRequest,
  UpdateInventoryDataType,
} from "src/types";
import {StockFinanceClientInfoStep} from "./StockFinanceClientInfoStep";
import {StockFinanceSaleInfoStep} from "./StockFinanceSaleInfoStep";
import {StockFinanceAccessoriesStep} from "./StockFinanceAccessoriesStep";
import {useUploadFilesState} from "src/components/UploadFilesProvider";
import CartagBackdrop from "../CartagBackdrop";
import useCustomErrorSnackbar from "src/utils/useCustomErrorSnackbar";
import useCustomInfoSnackbar from "src/utils/useCustomInfoSnackbar";
import {GenericFormSubmitButton} from "../GenericFormSubmitButton";

const StockFinanceTab = ({
  currentDeal,
  revalidateInventory,
}: {
  currentDeal: FinanceTabType | null;
  revalidateInventory: () => void;
}) => {
  const state = useUploadFilesState();
  const [currentEditingIndex, setCurrentEditingIndex] = useState(0);
  const [validatedStates, setValidatedStates] = useState([false, false, false]);
  const {t} = useTranslation();
  const [isLoading, setIsLoading] = useState(false);
  // That's a really nice idea, yeah... sigh
  const outgoingRequest = useMemo(() => {
    return {
      action: UpdateInventoryDataType.APV_CREDIT,
      id: currentDeal?.id || "",
      idCarDealer: "carDealer1",
      idUser: "idUser1",
      userName: "user Name",
      limitPreparationDate: "2020-09-09T08:43:37.914Z",
      sales: [
        {
          clientInfo: {
            name: "Client Name",
            telNumber1: "333-333-3333",
            email: "test@test.com",
          },
          creditInfo: {
            idFinanceManager: "finManager1",
            nameFinanceManager: "Finance Name",
          },
          saleDate: "2020-09-09T08:43:37.914Z",
          saleReference: "saleReference1",
          sellerInfo: {
            idSeller: "seller1",
            nameSeller: "Seller Name",
          },
          deliveryInfo: {
            deliveryBeginDate: "2020-09-09T08:43:37.914Z",
          },
        },
      ],
      files: [],
    } as UpdateFinanceRequest;
  }, [currentDeal]);

  // Who knows if it's necessary...
  const accessoryFiles = useMemo(() => {
    const filtered = state.files.filter((file) => file.type === "ACCESSORIES");
    if (filtered.length) {
      return filtered;
    }
    return [];
  }, [state.files]);

  let sale: FinanceSaleInfo | undefined;
  try {
    sale = currentDeal?.sales[0];
  } catch (error) {
    sale = currentDeal?.saleInfo;
  }

  const subKey = "StockDetail.FINANCE_TAB";
  const enqueueErrorSnackbar = useCustomErrorSnackbar();
  const enqueueInfoSnackbar = useCustomInfoSnackbar();

  const onSubmitHandler = async (actionType: UpdateInventoryDataType) => {
    outgoingRequest.action = actionType;
    // check if stuff is validated
    let validated = true;
    for (const val of validatedStates) {
      validated = validated && val;
    }

    if (!validated) {
      enqueueErrorSnackbar("All steps should be validated.");
      return;
    }
    if (currentDeal === null) {
      enqueueErrorSnackbar("Invalid deal ID.");
      return;
    }

    setIsLoading(true);
    try {
      enqueueInfoSnackbar("Success");
    } catch (e) {
      // Show the snackbar of snackbars
    } finally {
      revalidateInventory();
      setIsLoading(false);
    }
  };

  return (
    <>
      <CartagBackdrop open={isLoading} />
      <StockFinanceClientInfoStep
        onSetEditing={() => {
          setCurrentEditingIndex(0);
        }}
        isEditing={currentEditingIndex === 0}
        isValidated={validatedStates[0]}
        initialValues={{
          saleReference: sale?.saleReference || "",
          name: sale?.clientInfo?.name || sale?.clientInfo?.fullName || "",
          telephone: sale?.clientInfo?.telNumber1 || "",
          email: sale?.clientInfo?.email || "",
        }}
        onSubmit={(values: {
          saleReference: string;
          name: string;
          telephone: string;
          email: string;
        }) => {
          outgoingRequest.sales[0].saleReference = values.saleReference;
          outgoingRequest.sales[0].clientInfo.name = values.name;
          outgoingRequest.sales[0].clientInfo.telNumber1 = values.telephone;
          outgoingRequest.sales[0].clientInfo.email = values.email;
          // change accordions states
          setValidatedStates([true, validatedStates[1], validatedStates[2]]);
          setCurrentEditingIndex(1);
        }}
      />
      <StockFinanceSaleInfoStep
        onSetEditing={() => {
          setCurrentEditingIndex(1);
        }}
        isEditing={currentEditingIndex === 1}
        isValidated={validatedStates[1]}
        initialValues={{
          nameSeller: sale?.sellerInfo?.nameSeller || "",
          nameFinanceManager: sale?.creditInfo?.nameFinanceManager || "",
          saleDate: new Date(sale?.saleDate || new Date()),
          limitPreparationDate: "",
          deliveryBeginDate: "",
        }}
        onSubmit={(values) => {
          outgoingRequest.sales[0].sellerInfo.nameSeller = values.nameSeller;
          outgoingRequest.sales[0].creditInfo.nameFinanceManager =
            values.nameFinanceManager;
          outgoingRequest.sales[0].saleDate = values.saleDate.toISOString();
          outgoingRequest.limitPreparationDate = values.limitPreparationDate;
          outgoingRequest.sales[0].deliveryInfo.deliveryBeginDate =
            values.deliveryBeginDate;
          // change accordions states
          setValidatedStates([validatedStates[0], true, validatedStates[2]]);
          setCurrentEditingIndex(2);
        }}
      />
      <StockFinanceAccessoriesStep
        onSetEditing={() => {
          setCurrentEditingIndex(2);
        }}
        isEditing={currentEditingIndex === 2}
        isValidated={validatedStates[2]}
        initialValues={{
          accessoryFiles,
        }}
        onSubmit={(values) => {
          outgoingRequest.files = values.accessoryFiles;
          // change accordions states
          setValidatedStates([validatedStates[0], validatedStates[1], true]);
        }}
      />
      <form
        onSubmit={(e) => {
          e.preventDefault();
          onSubmitHandler(UpdateInventoryDataType.APV_CREDIT);
        }}
      >
        <GenericFormSubmitButton
          secondaryButtons={[
            {
              title: t(`${subKey}.REFUSE_CREDIT`),
              onClick: () =>
                onSubmitHandler(UpdateInventoryDataType.PREPARE_ON_DELIV),
            },
          ]}
        >
          {t(`${subKey}.APV_CREDIT`)}
        </GenericFormSubmitButton>
      </form>
    </>
  );
};

export default StockFinanceTab;
