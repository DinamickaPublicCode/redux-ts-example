import {CarFilter} from "../types/salesFunnelContext";
import {SellersRoleType} from "../../../types";

export const getSellerRoleFromCarFilter = (carFilter: CarFilter) =>
  (carFilter === "USED"
    ? "ROLE_SELLER_USED"
    : "ROLE_SELLER_NEW") as SellersRoleType;
