import Typography from "@mui/material/Typography";
import React from "react";
import {CarDealerLegend} from "src/types";
import {useSalesFunnelStyles} from "../../utils/salesFunnelStyles";
import SalesFunnelMarkerCircle from "./SalesFunnelMarkerCircle";

const SalesFunnelMarker = ({legend}: {legend: CarDealerLegend}) => {
  const styles = useSalesFunnelStyles();

  return (
    <div className={styles.markerText}>
      <SalesFunnelMarkerCircle htmlColor={legend.colorLegend} />
      <Typography>{legend.labelLegend}</Typography>
    </div>
  );
};

export default SalesFunnelMarker;
