import React, {useEffect, useMemo} from "react";
import chunk from "lodash/chunk";
import {useDispatch} from "react-redux";
import HomeStatsBlock, {StatsBlockData} from "./HomeStatsBlock";
import Grid from "@mui/material/Grid";
import {useHomeContext} from "../../state/HomeContextProvider";
import getDetail from "../../utils/getDetail";
import smallStatsIcons from "../../utils/smallStatsIcons";
import {useAppSelector} from "src/state/hooks";
import {useGetUserQuery} from 'src/state/reducers/userApi';

const HomeStatsBlocks = () => {
  const {globalDate, inventoryType, rangeType} = useHomeContext();
  const dispatch = useDispatch();
  const {inventory, esthetic, mechanic, delivery} = useAppSelector(
    (state) => state.statistics,
  );

  const {data: user} = useGetUserQuery();;
  const idCarDealer = user?.defaultCarDealer.id;

  const monthRangeValue =
    rangeType === "year" ? undefined : globalDate.getMonth() + 1;
  const fullYear = globalDate.getFullYear();
  
  /**
   * This looks ugly and verbose, but quite easy to edit (for now).
   * TODO: move to the other place into hook
   */
  const processedStats = useMemo(() => {
    return [
      {
        icon: smallStatsIcons[getDetail(inventory)?.iconAction || "inv"],
        graph: getDetail(inventory)?.evolution.highlights || [],
        percentString: (getDetail(inventory)?.evolution.percent || "0") + "%",
        textA: "Total",
        textB: getDetail(inventory)?.labelAction || "",
        number: +(getDetail(inventory)?.total || "0"),
        color: [26, 6, 249],
        direction:
          (getDetail(inventory)?.evolution.percent || "0")[0] === "-"
            ? "down"
            : "up",
      },
      {
        icon: smallStatsIcons[
          getDetail(esthetic, "PHOTO")?.iconAction || "inv"
        ],
        graph: getDetail(esthetic, "PHOTO")?.evolution.highlights || [],
        percentString:
          (getDetail(esthetic, "PHOTO")?.evolution.percent || "0") + "%",
        textA: "Total",
        textB: getDetail(esthetic, "PHOTO")?.labelAction || "",
        number: +(getDetail(esthetic, "PHOTO")?.total || "0"),
        color: [255, 19, 76],
        direction:
          (getDetail(esthetic, "PHOTO")?.evolution.percent || "0")[0] === "-"
            ? "down"
            : "up",
      },
      {
        icon: smallStatsIcons[
          getDetail(esthetic, "WASHING")?.iconAction || "inv"
        ],
        graph: getDetail(esthetic, "WASHING")?.evolution.highlights || [],
        percentString:
          (getDetail(esthetic, "WASHING")?.evolution.percent || "0") + "%",
        textA: "Total",
        textB: getDetail(esthetic, "WASHING")?.labelAction || "",
        number: +(getDetail(esthetic, "WASHING")?.total || "0"),
        color: [255, 19, 76],
        direction:
          (getDetail(esthetic, "WASHING")?.evolution.percent || "0")[0] === "-"
            ? "down"
            : "up",
      },
      {
        icon: smallStatsIcons[
          getDetail(mechanic, "REPAIR")?.iconAction || "inv"
        ],
        graph: getDetail(mechanic, "REPAIR")?.evolution.highlights || [],
        percentString:
          (getDetail(mechanic, "REPAIR")?.evolution.percent || "0") + "%",
        textA: "Total",
        textB: getDetail(mechanic, "REPAIR")?.labelAction || "",
        number: +(getDetail(mechanic, "REPAIR")?.total || "0"),
        color: [255, 201, 63],
        direction:
          (getDetail(mechanic, "REPAIR")?.evolution.percent || "0")[0] === "-"
            ? "down"
            : "up",
      },
      {
        icon: smallStatsIcons[
          getDetail(mechanic, "INSPECTION")?.iconAction || "inv"
        ],
        graph: getDetail(mechanic, "INSPECTION")?.evolution.highlights || [],
        percentString:
          (getDetail(mechanic, "INSPECTION")?.evolution.percent || "0") + "%",
        textA: "Total",
        textB: getDetail(mechanic, "INSPECTION")?.labelAction || "",
        number: +(getDetail(mechanic, "INSPECTION")?.total || "0"),
        color: [255, 201, 63],
        direction:
          (getDetail(mechanic, "INSPECTION")?.evolution.percent || "0")[0] ===
          "-"
            ? "down"
            : "up",
      },
      {
        icon: smallStatsIcons[
          getDetail(delivery, "CREATE_DELIVERY")?.iconAction || "inv"
        ],
        graph:
          getDetail(delivery, "CREATE_DELIVERY")?.evolution.highlights || [],
        percentString:
          (getDetail(delivery, "CREATE_DELIVERY")?.evolution.percent || "0") +
          "%",
        textA: "Total",
        textB: getDetail(delivery, "CREATE_DELIVERY")?.labelAction || "",
        number: +(getDetail(delivery, "CREATE_DELIVERY")?.total || "0"),
        color: [26, 6, 249],
        direction:
          (getDetail(delivery, "CREATE_DELIVERY")?.evolution.percent ||
            "0")[0] === "-"
            ? "down"
            : "up",
      },
    ] as StatsBlockData[];
  }, [inventory, esthetic, mechanic, delivery]);

  return (
    <>
      {chunk(processedStats, 2).map((row, index) => (
        <Grid
          sx={{
            "&:last-child": {
              marginBottom: 0,
            },
            paddingBottom: 0,
            marginBottom: "20px",
          }}
          key={index}
          container
          spacing={3}
        >
          <Grid
            item
            xs={6}
            style={{
              paddingBottom: 0,
            }}
          >
            <HomeStatsBlock data={row[0]} />
          </Grid>
          <Grid
            item
            xs={6}
            style={{
              paddingBottom: 0,
            }}
          >
            <HomeStatsBlock data={row[1]} />
          </Grid>
        </Grid>
      ))}
    </>
  );
};

export default HomeStatsBlocks;
