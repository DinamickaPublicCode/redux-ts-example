import React from 'react';

import { ComponentStory, ComponentMeta } from '@storybook/react';
import { PercentBarWidthText } from './YourStats';

export default {
  title: 'Progress bar',
  component: PercentBarWidthText,
} as ComponentMeta<typeof PercentBarWidthText>;


const Template: ComponentStory<typeof PercentBarWidthText> = (args) => <PercentBarWidthText {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  percent: 80
}
