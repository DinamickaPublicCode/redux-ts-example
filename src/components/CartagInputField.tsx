import React from "react";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import Typography from "@mui/material/Typography";
import {useCommonStyles} from "../utils/commonStyles";
import {TextFieldProps} from "@mui/material/TextField/TextField";

interface CartagInputFieldProps {
  labelText?: string;
  disableLabel?: boolean;
  margin?: "normal" | "dense" | "none" | undefined;
}

const CartagInputField: React.FC<TextFieldProps & CartagInputFieldProps> = ({
  labelText,
  disableLabel,
  margin = "normal",
  ...rest
}) => {
  const classes = useCommonStyles();

  return (
    <Box sx={{width: "100%"}}>
      {!disableLabel && (
        <Typography className={classes.inputLabel}>{labelText}</Typography>
      )}
      <TextField
        {...rest}
        InputProps={{
          ...rest.InputProps,
          className: classes.input,
        }}
        fullWidth
        margin={margin}
        variant="outlined"
        size="small"
      />
    </Box>
  );
};

export const CustomInputFieldForDatePicker: React.FC<
  TextFieldProps & CartagInputFieldProps
> = ({labelText, disableLabel, margin = "normal", ...rest}) => {
  const classes = useCommonStyles();

  return (
    <Box sx={{width: "100%"}}>
      {!disableLabel && (
        <Typography className={classes.inputLabel}>{labelText}</Typography>
      )}
      <TextField
        {...rest}
        InputProps={{
          ...rest.InputProps,
          className: classes.inputForDatePicker,
        }}
        fullWidth
        margin={margin}
        variant="outlined"
        size="small"
      />
    </Box>
  );
};

export default CartagInputField;
