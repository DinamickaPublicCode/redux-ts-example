import {ItemReadinessIcons, StageType} from "../types";
import CircularProgressWithLabel from "./CircularProgressWithLabel";
import {getColor, getPercentage} from "../utils/stageData";
import {stageInfoIcons} from "../utils/stageInfoIcons";
import React from "react";

/**
 * A separate component since it's also used in the add new delivery modal.
 * TODO: Find its own place somewhere...
 *
 * @param param0
 */
const StockCarInfoReadiness = ({
  icons,
  stageType,
}: {
  icons?: ItemReadinessIcons[];
  stageType?: StageType;
}) => (
  <div
    style={{
      display: "flex",
      alignItems: "center",
    }}
  >
    {stageType && (
      <CircularProgressWithLabel
        size={42}
        thickness={2}
        style={{
          color: getColor(stageType as StageType),
          marginRight: 5,
        }}
        value={getPercentage(stageType as StageType) as number}
      />
    )}
    {icons &&
      icons.map((el) => {
        return (
          <div
            style={{
              width: "max-content",
              margin: "0 2px",
            }}
            key={el}
          >
            {stageInfoIcons[el as ItemReadinessIcons]}
          </div>
        );
      })}
  </div>
);

export default StockCarInfoReadiness;
