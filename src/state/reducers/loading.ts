import {LoadingState} from "src/types";

const initialState: LoadingState = {
  loading: false,
};

type LoadingActionType = {type: 'SET_LOADING'; payload: boolean};

const loadingReducer = (
  state: LoadingState = initialState,
  action: LoadingActionType,
): TokenState => {
  switch (action.type) {
    case 'SET_LOADING': {
      return {
        ...state,
        loading: action.payload as LoadingState["loading"],
      };
    }
    default:
      return state;
  }
};

export const setLoading = (v: boolean): LoadingActionType => ({
  type: 'SET_LOADING',
  payload: v,
});

export default loadingReducer;
