import React from 'react';

import { ComponentStory, ComponentMeta } from '@storybook/react';
import CountUp from './CountUp';

export default {
  title: 'CountUp',
  component: CountUp,
} as ComponentMeta<typeof CountUp>;



const Template: ComponentStory<typeof CountUp> = (args) => <CountUp {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  loaded: true,
  end: 100,
  duration: 1
}
