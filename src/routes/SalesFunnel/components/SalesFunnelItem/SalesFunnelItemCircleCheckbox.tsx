import {CarDealerLegend} from "../../../../types";
import Box from "@mui/material/Box";
import SalesFunnelItemCircle from "./SalesFunnelItemCircle";
import React from "react";

const SalesFunnelItemCircleCheckbox = ({
  checked,
  legend,
  title,
  onChange,
  index,
}: {
  legend: CarDealerLegend;
  index: number;
  checked: boolean;
  title: string;
  onChange: (checked: boolean, index: number) => void;
}) => {
  return (
    <Box
      display="flex"
      flexDirection="row"
      alignItems="center"
      style={{
        cursor: "pointer",
      }}
      paddingBottom="17px"
    >
      <Box marginRight="20px">
        <SalesFunnelItemCircle
          color={legend.colorLegend}
          onClick={() => onChange(!checked, index)}
          checked={checked}
        />
      </Box>
      <Box
        onClick={() => onChange(!checked, index)}
        style={{
          fontStyle: "normal",
          fontWeight: "normal",
          fontSize: "14px",
          lineHeight: "18.2px",
          color: checked ? "#000000" : "#98A2B7",
        }}
      >
        {title}
      </Box>
    </Box>
  );
};

export default SalesFunnelItemCircleCheckbox;
