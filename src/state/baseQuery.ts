import {fetchBaseQuery} from '@reduxjs/toolkit/dist/query';
import {RootState} from '../state/store';
import {baseUrl} from 'src/system/constants';

/**
 * A configuration object for RTK query, we get the token from state and
 * set the bearer token
 */
const baseQuery = fetchBaseQuery({
  baseUrl,
  // credentials: 'include',
  // prepareHeaders: (headers, {getState}) => {
  //   const token = (getState() as RootState).token.value;
  //   if (token) {
  //     headers.set('Authorization', `Token ${token}`);
  //   }
  //   return headers;
  // },
});

export default baseQuery;
