import React from "react";
import CloseIcon from "@mui/icons-material/Close";
import {useTranslation} from "react-i18next";
import {TabViewsType} from "src/types";
import {useAppSelector} from "src/state/hooks";
import useCustomInfoSnackbar from "src/utils/useCustomInfoSnackbar";
import IconButton from "@mui/material/IconButton";
import DialogTitle from "@mui/material/DialogTitle";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import Typography from "@mui/material/Typography";
import Transition from "src/components/Transition";
import {useGetUserQuery} from 'src/state/reducers/userApi';

interface DashboardTabsDialogUnpinViewProps {
  tabName: TabViewsType;
  open: boolean;
  handleClose: () => void;
}

const DashboardTabsDialogUnpinView: React.FC<
  DashboardTabsDialogUnpinViewProps
> = ({tabName, open, handleClose}) => {
  const {data: user} = useGetUserQuery();;
  const {t} = useTranslation();

  const enqueueInfoSnackbar = useCustomInfoSnackbar();

  const handleUnpin = async () => {
    if (!user?.id) {
      return;
    }
    try {
      enqueueInfoSnackbar("View successfully unpinned");
    } catch (err) {
      console.log(err);
    } finally {
      handleClose();
    }
  };

  return (
    <Dialog
      open={open}
      TransitionComponent={Transition}
      keepMounted
      maxWidth={"xs"}
      onClose={handleClose}
      /* classes={{paper: classes.dialogPaper}} */
      aria-labelledby="alert-dialog-archive-title"
      aria-describedby="alert-dialog-archive-description"
    >
      <DialogTitle
        /* classes={{root: classes.dialogTitle}} */
        id="alert-dialog-archive-title"
      >
        <Typography variant="h6">{t("UnpinView")}</Typography>
        <IconButton size="small" onClick={handleClose}>
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent
        style={{
          padding: 0,
          marginBottom: 25,
        }}
      >
        <DialogContentText id="alert-dialog-archive-description">
          {t("DoYouReally")}{" "}
          <span
            style={{
              fontWeight: 700,
            }}
          >
            {t(tabName)} {t("View")}
          </span>{" "}
          {t("FromYourDashboard")}?
        </DialogContentText>
      </DialogContent>
      <DialogActions style={{padding: 0}}>
        <Button
          style={{
            textTransform: "uppercase",
            padding: 15,
            boxShadow: "0px 4px 20px rgba(203, 205, 217, 0.3)",
          }}
          fullWidth
          onClick={handleClose}
        >
          {t("buttonNames.NoKeepIt")}
        </Button>
        <Button
          style={{
            textTransform: "uppercase",
            padding: 15,
            boxShadow: "0px 4px 20px rgba(203, 205, 217, 0.3)",
          }}
          fullWidth
          variant="contained"
          onClick={handleUnpin}
        >
          {t("buttonNames.YesUnpinIt")}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default DashboardTabsDialogUnpinView;
