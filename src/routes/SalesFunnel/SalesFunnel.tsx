import React from "react";
import SalesFunnelProvider from "./state/SalesFunnelProvider";
import SalesFunnelLayout from "./components/SalesFunnelLayout";

const SalesFunnel = () => {
  return (
    <SalesFunnelProvider>
      <SalesFunnelLayout />
    </SalesFunnelProvider>
  );
};

export default SalesFunnel;
