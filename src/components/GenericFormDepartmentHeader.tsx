import {useTranslation} from "react-i18next";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import {STOCK_DETAIL_TRANSLATION_KEY} from "../system/constants";
import React from "react";

export function GenericFormDepartmentHeader({children}: {children: string}) {
  const {t} = useTranslation();
  return (
    <Box
      minWidth="293px"
      display="flex"
      flexDirection="row"
      alignItems="baseline"
    >
      <Typography
        component="div"
        sx={{
          fontWeight: 600,
          fontSize: "13px",
          lineHeight: "16.9px",
          letterSpacing: "0.5px",
          textTransform: "uppercase",
          color: "#98A2B7",
          marginRight: "12px",
        }}
      >
        {t(STOCK_DETAIL_TRANSLATION_KEY + ".DEPARTMENT")}
      </Typography>
      <Typography
        component="div"
        sx={{
          fontWeight: 600,
          fontSize: "18px",
          lineHeight: "22.32px",
          color: "#222238",
        }}
      >
        {children}
      </Typography>
    </Box>
  );
}
