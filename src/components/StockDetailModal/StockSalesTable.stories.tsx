import React from 'react';

import { ComponentStory, ComponentMeta } from '@storybook/react';
import StockSalesTabTable from './StockSalesTabTable';

export default {
  title: 'Checkbox table',
  component: StockSalesTabTable,
} as ComponentMeta<typeof StockSalesTabTable>;

const Template: ComponentStory<typeof StockSalesTabTable> = (args) => <StockSalesTabTable {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  groupAccessories: [
     {
        "id":"5f8c74c3edbb6270e37d3e57",
        "name":"mecanique",
        "accessories":[
           {
              "idAcc":"e1MeRBMQ",
              "name":"acc11",
              "price":10
           },
           {
              "idAcc":"z4hM2UIU",
              "name":"acc12",
              "price":20
           }
        ],
        "idCarDealer":"95ad9687-9b35-4676-a968-a69a902bbd23"
     },
     {
        "id":"5f8c74ceedbb6270e37d3e59",
        "name":"Esthetique",
        "accessories":[
           {
              "idAcc":"hCr48GY0",
              "name":"acc21",
              "price":10
           },
           {
              "idAcc":"CdtfFafp",
              "name":"acc22",
              "price":20
           }
        ],
        "idCarDealer":"95ad9687-9b35-4676-a968-a69a902bbd23"
     }
  ],
  selected: [],
  setSelected: () => false
}
