import React, {useState} from "react";
import Grid from "@mui/material/Grid";
import {useTranslation} from "react-i18next";

import {StepsCheckedIcon, StepsUpIcon} from "src/assets/icons";
import StockFinanceAccordion from "./StockFinanceAccordion";
import StockUploadFileButton from "./StockUploadFileButton";
import {AccessoriesInformation} from "../../types/stockFinanceTab";
import {useUploadFilesDispatch} from "src/components/UploadFilesProvider";
import {DocumentInfo} from "src/types";
import useCustomErrorSnackbar from "src/utils/useCustomErrorSnackbar";
import DocumentDisplay from "../DocumentDisplay";
import {STOCK_DETAIL_TRANSLATION_KEY} from "../../system/constants";

interface StockFinanceStepProps<T> {
  isEditing: boolean;
  isValidated: boolean;
  initialValues: T;
  onSetEditing: () => void;
  onSubmit: (values: T) => void;
}

const StockFinanceAccessoriesDocumentDisplay = ({
  file,
}: {
  file: DocumentInfo;
}) => {
  const [loading, setLoading] = useState(false);
  const {onDownload, onDeleteButtonClicked} = useUploadFilesDispatch();
  return (
    <DocumentDisplay
      onDocumentAction={() => {
        onDeleteButtonClicked(file, setLoading);
      }}
      onDownload={() => {
        onDownload(file, setLoading);
      }}
      isLoading={loading}
      filename={file.name}
    />
  );
};

export const StockFinanceAccessoriesStep = ({
  isEditing,
  isValidated,
  onSubmit,
  initialValues,
  onSetEditing,
}: StockFinanceStepProps<AccessoriesInformation>) => {
  const {t} = useTranslation();
  const {t: ft} = useFinanceTabTranslation();
  const {onUploadFile} = useUploadFilesDispatch();
  const enqueueErrorSnackbar = useCustomErrorSnackbar();
  const [loading, setIsLoading] = useState(false);

  const shouldHideButton = !isValidated && !isEditing;

  return (
    <>
      <StockFinanceAccordion
        isEditButton={isValidated}
        onButtonClicked={
          shouldHideButton
            ? undefined
            : () => {
                if (!isEditing) {
                  onSetEditing();
                  return;
                }
                if (initialValues.accessoryFiles.length === 0) {
                  // TODO: Show snackbar
                  enqueueErrorSnackbar("No files were uploaded.");
                  return;
                }
                onSubmit(initialValues);
              }
        }
        stepIcon={isValidated ? <StepsCheckedIcon /> : <StepsUpIcon />}
        panelId={`${ft("Step")} 3`}
        panelName={ft("Select_Accessories")}
        open={isEditing}
      >
        <form
          style={{
            padding: 24,
            width: "100%",
          }}
        >
          <Grid container spacing={1}>
            <Grid container item xs={12} spacing={6}>
              <Grid item xs={12}>
                <StockUploadFileButton
                  loading={loading}
                  typeId="accessories"
                  accept="image/png,image/jpg,application/pdf"
                  title={t("Accessories")}
                  onFileSelectChanged={(fileData) => {
                    for (let i = 0; i < fileData.files.length; i++) {
                      // TODO: I would definitely benefit from async implementation
                      onUploadFile(
                        {
                          id: "",
                          name: fileData.files[i].name,
                          type: "ACCESSORIES",
                        },
                        fileData,
                        setIsLoading,
                        () => {},
                      );
                    }
                  }}
                />
              </Grid>
            </Grid>
            {initialValues.accessoryFiles.map((file, idx) => (
              <StockFinanceAccessoriesDocumentDisplay key={idx} file={file} />
            ))}
          </Grid>
        </form>
      </StockFinanceAccordion>
    </>
  );
};

const useFinanceTabTranslation = () => {
  const {t} = useTranslation();
  return {
    t: (key: string) => t(`${STOCK_DETAIL_TRANSLATION_KEY}.FINANCE_TAB.${key}`),
  };
};
