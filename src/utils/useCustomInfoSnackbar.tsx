import React from "react";
import makeStyles from "@mui/styles/makeStyles";
import CheckRounded from "@mui/icons-material/CheckRounded";
import Typography from "@mui/material/Typography";
import {Theme} from "@mui/material/styles";
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
import {useSnackbar} from "src/thirdparty/_notistack";

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    flexGrow: 1,
    minWidth: "500px !important",
  },
  snackWrapper: {
    display: "flex",
    alignItems: "center",
    borderRadius: 10,
    padding: "9px 25px",
    backgroundColor: theme.palette.primary.main,
    boxShadow: "0px 3px 25px rgba(116, 173, 109, 0.3)",
    width: "100%",
  },
  typography: {
    flex: 1,
    fontWeight: 600,
    color: "#FFFFFF",
  },
}));

const useCustomInfoSnackbar = () => {
  const classes = useStyles();
  const {enqueueSnackbar, closeSnackbar} = useSnackbar();

  return (msg: string) =>
    enqueueSnackbar(msg, {
      preventDuplicate: true,
      variant: "info",
      anchorOrigin: {
        vertical: "bottom",
        horizontal: "center",
      },
      content: (key, message) => {
        return (
          <div className={classes.root}>
            <div className={classes.snackWrapper}>
              <CheckRounded
                htmlColor={"#fff"}
                style={{
                  marginRight: 20,
                }}
              />
              <Typography className={classes.typography}>{message}</Typography>
              <Box>
                <Button
                  style={{
                    fontWeight: 600,
                    fontSize: 14,
                    textTransform: "uppercase",
                    color: "#222238",
                    backgroundColor: "#FFFFFF",
                    borderRadius: 10,
                    padding: "7px 22px",
                  }}
                  onClick={() => closeSnackbar(key)}
                >
                  Dismiss
                </Button>
              </Box>
            </div>
          </div>
        );
      },
    });
};

export default useCustomInfoSnackbar;
