import {CarDealerSale, CarTypes, PagedCarDealerSale} from "../../../types";
import {useCallback, useEffect, useState} from "react";
import {useAppSelector} from "src/state/hooks";
import {Dayjs} from "dayjs";
import {useActiveDay} from "./useActiveDay";
import {useDeliveriesPerDay} from "./useDeliveriesPerDay";
import {useFilteredCars} from "./useFilteredCars";
import {useGetCarDealerSalesNewQuery, useGetCarDealerSalesUsedQuery} from 'src/state/reducers/carDealerApi';

export const useSalesCalendarHandlers = (
  currentDate: Date,
  typeOfCalendar: "DAILY" | "MONTHLY",
  currentCarType: CarTypes,
) => {
  const [deliveries, setDeliveries] = useState<PagedCarDealerSale | undefined>(
    undefined,
  );
  const [globalSearchValue, setGlobalSearchValue] = useState<string>("");

  const {data:deliveriesNew} = useGetCarDealerSalesNewQuery();
  const {data:deliveriesUsed} = useGetCarDealerSalesUsedQuery();

  useEffect(() => {
    const newValues = currentCarType === "NEW" ? deliveriesNew : deliveriesUsed;
    setDeliveries(newValues);
  }, [deliveriesNew, deliveriesUsed, currentCarType]);

  const deliveriesPerDay = useDeliveriesPerDay(currentDate, deliveries);

  const {
    activeDay,
    setActiveDay,
    currentSale,
    currentSalesPerDay,
    setCurrentSalesPerDay,
    setCurrentSale,
  } = useActiveDay(
    currentDate,
    currentCarType,
    deliveriesPerDay,
    typeOfCalendar,
  );

  const changeActiveDate = useCallback(
    (date: {day: Dayjs; deliveries: CarDealerSale[] | undefined}) => {
      setActiveDay(date.day);
      setCurrentSalesPerDay(date.deliveries ?? null);
      setCurrentSale((date.deliveries && date?.deliveries[0]) ?? null);
    },
    [setActiveDay, setCurrentSale, setCurrentSalesPerDay],
  );

  const filteredCars = useFilteredCars(
    currentSalesPerDay,
    currentCarType,
    globalSearchValue,
  );

  return {
    filteredCars,

    onGlobalSearch: setGlobalSearchValue,
    //
    activeDay,
    setActiveDay,
    //
    deliveriesPerDay,
    changeActiveDate,
    currentSale,
    changeCurrentCarInfo: setCurrentSale,
    //
    //
    setCurrentSalesPerDay,
    setCurrentSale,
  };
};
