import React from "react";
import {
  BrushIcon,
  CameraIcon,
  DollarIcon,
  KeyIcon,
  TickIcon,
  WrenchIcon,
} from "src/assets/icons";

export const stageInfoIcons = {
  MEC_INS: <TickIcon />,
  MEC_REP: <WrenchIcon />,
  EST_WASH: <BrushIcon />,
  EST_PHOTO: <CameraIcon />,
  CREDIT: <DollarIcon />,
  DELIV: <KeyIcon />,
};
