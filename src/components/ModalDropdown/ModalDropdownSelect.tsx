import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import ArrowDropDown from "@mui/icons-material/ArrowDropDown";
import React from "react";

const ModalDropdownSelect = ({
  title,
  onClick,
}: {
  title: string;
  onClick: (event: unknown) => void;
}) => {
  return (
    <Box
      style={{
        cursor: "pointer",
      }}
      display="flex"
      flexDirection="row"
      alignItems="center"
      onClick={onClick}
    >
      <Typography
        style={{
          fontStyle: "normal",
          fontWeight: "normal",
          fontSize: "14px",
          lineHeight: "17px",
          color: "#98A2B7",
          marginRight: 9,
        }}
      >
        {title}
      </Typography>
      <ArrowDropDown htmlColor="#CBD1DD" />
    </Box>
  );
};
export default ModalDropdownSelect;
