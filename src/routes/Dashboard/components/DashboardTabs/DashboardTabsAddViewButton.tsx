import React, {useEffect, useState} from "react";
import AddIcon from "@mui/icons-material/Add";
import {tabsViews} from "src/utils/tabsViews";
import { ReactComponent as CheckboxIcon } from "src/assets/icons/CheckboxIcon.svg";
import { ReactComponent as CheckboxCheckedIcon } from "src/assets/icons/CheckboxCheckedIcon.svg";
import {TabViewsType} from "src/types";
import {useAppSelector} from "src/state/hooks";
import {useTranslation} from "react-i18next";
import useCustomInfoSnackbar from "src/utils/useCustomInfoSnackbar";
import Box from "@mui/material/Box";
import ButtonBase from "@mui/material/ButtonBase";
import Checkbox from "@mui/material/Checkbox";
import FormControl from "@mui/material/FormControl";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormGroup from "@mui/material/FormGroup";
import IconButton from "@mui/material/IconButton";
import Menu from "@mui/material/Menu";
import {useTheme} from "@mui/material/styles";
import Typography from "@mui/material/Typography";
import {useGetUserQuery} from 'src/state/reducers/userApi';

interface DashboardTabsAddViewButtonProps {
  handleSaveTabs: (tabs: TabViewsType[]) => void;
}

const DashboardTabsAddViewButton: React.FC<DashboardTabsAddViewButtonProps> = ({
  handleSaveTabs,
}) => {
  const {data: user} = useGetUserQuery();;
  const {t} = useTranslation();

  const enqueueInfoSnackbar = useCustomInfoSnackbar();

  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const [tabTemp, setTabTemp] = useState<TabViewsType[]>([]);

  useEffect(() => {
    if (user?.views) {
      setTabTemp(user.views);
    }
  }, [user]);

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleTabItemClick =
    (id: TabViewsType) => (_event: React.ChangeEvent<HTMLInputElement>) => {
      if (tabTemp.some((el) => el.toLowerCase() === id.toLowerCase())) {
        return setTabTemp((prevState) => [
          ...prevState.filter((el) => el.toLowerCase() !== id.toLowerCase()),
        ]);
      }
      const currentTab = tabsViews.find(
        (tab) => tab.toLowerCase() === id.toLowerCase(),
      );
      if (currentTab) {
        setTabTemp((prevState) => [...prevState, currentTab]);
      }
    };

  const onSaveTabs = async () => {
    if (!user?.id) return;
    try {
      enqueueInfoSnackbar("View successfully updated");
    } catch (err) {
      console.log(err);
    } finally {
      handleSaveTabs(tabTemp);
      setAnchorEl(null);
    }
  };
  const checkIsSelected = (name: TabViewsType) => {
    return tabTemp.some((el) => el.toLowerCase() === name.toLowerCase());
  };
  const theme = useTheme();
  return (
    <div>
      <Box
        sx={{
          position: "relative",
          cursor: "pointer",
          display: "flex",
          alignItems: "center",
          padding: "0 32px",
          color: "#98A2B7",
          zIndex: 10,
          [theme.breakpoints.down("sm")]: {
            width: "100%",
          },
          "&::before": {
            content: '""',
            position: "absolute",
            display: "block",
            top: 0,
            right: 0,
            bottom: -3,
            left: 0,
            borderRadius: "10px 10px 0 0",
            transform: "perspective(5px) rotateX(1deg) translateZ(-1px)",
            transformOrigin: "0 0",
            backfaceVisibility: "hidden",
            zIndex: -1,
            transition: "0.3s background-color ease-in-out",
          },
        }}
        onClick={handleClick}
      >
        <IconButton
          size="small"
          style={{
            marginRight: 5,
          }}
        >
          <AddIcon
            style={{
              height: 18,
              width: 18,
              fill: "#98A2B7",
            }}
          />
        </IconButton>
        <Typography
          sx={{
            lineHeight: 2.5,
            fontWeight: 500,
            textTransform: "capitalize",
            [theme.breakpoints.down("sm")]: {
              fontSize: "12px",
            },
            [theme.breakpoints.down("sm")]: {
              fontSize: "10px",
            },
            [theme.breakpoints.down("sm")]: {
              fontSize: "15px",
            },
          }}
        >
          {t("buttonNames.AddView")}
        </Typography>
      </Box>
      <Menu
        anchorEl={anchorEl}
        variant={"selectedMenu"}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
        /*
        Styling...

        menuFilterPaper: {
          boxShadow: "0px 4px 20px rgba(203, 205, 217, 0.3)",
          "& $menuWrapper": {
            padding: 0,
          },
        },
      */
      >
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            minWidth: 270,
            outline: "none",
          }}
        >
          <FormControl
            component="fieldset"
            style={{
              margin: "10px 0",
              maxHeight: 300,
              overflowY: "auto",
            }}
          >
            <FormGroup>
              {tabsViews?.map((tab) => (
                <FormControlLabel
                  style={{
                    padding: "0 20px",
                    marginRight: 0,
                    backgroundColor: checkIsSelected(tab) ? "#F6F8FA" : "#fff",
                  }}
                  key={tab}
                  checked={checkIsSelected(tab)}
                  disabled={checkIsSelected(tab)}
                  control={
                    <Checkbox
                      color="default"
                      icon={<CheckboxIcon />}
                      checkedIcon={<CheckboxCheckedIcon />}
                      onChange={handleTabItemClick(tab)}
                      name={tab.toLowerCase()}
                    />
                  }
                  label={
                    <Box
                      sx={{
                        display: "flex",
                        ml: 1,
                      }}
                    >
                      <Typography
                        style={{
                          textTransform: "capitalize",
                        }}
                      >
                        {t(tab).toLowerCase()} {t("View")}
                      </Typography>
                    </Box>
                  }
                />
              ))}
            </FormGroup>
          </FormControl>
          <ButtonBase
            sx={{
              padding: 13,
              backgroundColor: theme.palette.primary.main,
              color: theme.palette.common.white,
              fontWeight: 600,
              textTransform: "uppercase",
            }}
            onClick={onSaveTabs}
          >
            {t("buttonNames.Save")}
          </ButtonBase>
        </Box>
      </Menu>
    </div>
  );
};

export default DashboardTabsAddViewButton;
