import Box from "@mui/material/Box";
import React from "react";

export const GenericFormBox = ({
  header,
  children,
}: {
  header: JSX.Element[] | JSX.Element;
  children: JSX.Element | JSX.Element[];
}) => (
  <Box
    sx={{
      paddingTop: "20px",
      paddingLeft: "16px",
      paddingRight: "16px",
      paddingBottom: "0px",
      backgroundColor: "white",
    }}
  >
    <Box
      sx={{
        backgroundColor: "white",
        paddingBottom: "10px",
      }}
    >
      {header}
      <Box
        sx={{
          padding: "30px 63px 24px 35px",
          backgroundColor: "white",
          border: "2px solid #F6F8FA",
          borderBottomRightRadius: "14px",
          borderBottomLeftRadius: "14px",
        }}
      >
        {children}
      </Box>
    </Box>
  </Box>
);
