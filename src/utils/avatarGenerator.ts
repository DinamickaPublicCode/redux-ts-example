const avatars: {
  [index: string]: string;
} = {};
export const requestGeneratedAvatar = (userId: string, userName: string) => {
  let avatar = avatars[userId];
  if (!avatar) {
    const fl = userName
      .split(" ")
      .reduce(
        (a, b) =>
          a.substring(0, 1).toUpperCase() + b.substring(0, 1).toUpperCase(),
      );
    const svg = `<svg width="50" height="50" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
  <mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="50" height="50">
  <circle cx="24.75" cy="24.75" r="24.75" fill="#C4C4C4"/>
  </mask>
  <g mask="url(#mask0)">
  <text fill="#1A06F9" xml:space="preserve" style="white-space: pre" font-family="Nunito Sans, Arial" text-anchor="middle" font-size="16" letter-spacing="1.5px"><tspan x="25.5" y="31.39">${fl}</tspan></text>
  </g>
  </svg>
  `;
    avatar = "data:image/svg+xml;base64," + window.btoa(svg);
    avatars[userId] = avatar;
  }
  return avatar;
};
