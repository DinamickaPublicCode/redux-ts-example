import {DocumentBoxActionCallback} from "../types/documentBoxInterfaces";
import mime from "mime";

export const downloadFileFromServer: DocumentBoxActionCallback = async (
  id,
  onLoadingStateChanged,
) => {
  if (!id.id || !id.name) {
    // TODO: Show snackbar
    console.error("Invalid document info.");
    return;
  }
  onLoadingStateChanged(true);
  try {
    let type = mime.getType(id.name);
    if (type === null) {
      console.error("invalid mime type");
    }
  } catch (e) {
  } finally {
    onLoadingStateChanged(false);
  }
};
