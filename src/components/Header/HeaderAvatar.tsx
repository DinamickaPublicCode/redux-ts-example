import Avatar from "@mui/material/Avatar";
import Box from "@mui/material/Box";
import MenuItem from "@mui/material/MenuItem";
import MenuList from "@mui/material/MenuList";
import {useRef, useState} from "react";
import {useNavigate} from "react-router-dom";
import {UserType} from "src/types";
import {requestGeneratedAvatar} from "../../utils/avatarGenerator";
import ModalDropdownPopperBody from "../ModalDropdown/ModalDropdownPopperBody";

const HeaderAvatar = ({
  user,
  onLogOut,
}: {
  user?: UserType | null;
  onLogOut: () => void;
}) => {
  const anchor = useRef(null);
  const [open, setOpen] = useState(false);

  const navigate = useNavigate();
  return (
    <>
      <Box
        style={{
          cursor: "pointer",
        }}
        onClick={() => {
          if (open) {
            return;
          }
          setOpen((e) => !e);
        }}
      >
        <Avatar
          ref={anchor}
          src={requestGeneratedAvatar(
            user?.id || "id",
            user?.fullName || "User name",
          )}
          sx={{
            width: "49px",
            height: "49px",
            marginLeft: "30px",
            backgroundColor: "white",
            border: "2px solid #fff",
          }}
        >
          SM
        </Avatar>
      </Box>
      <ModalDropdownPopperBody
        handleClose={() => setOpen((e) => !e)}
        open={open}
        anchorRef={anchor}
      >
        <MenuList
          id="split-button-menu"
          style={{
            marginTop: 15,
            minWidth: 165,
          }}
        >
          <MenuItem key="settings" onClick={() => navigate("/settings")}>
            Settings
          </MenuItem>
          <MenuItem key="title" onClick={onLogOut}>
            Log Out
          </MenuItem>
        </MenuList>
      </ModalDropdownPopperBody>
    </>
  );
};

export default HeaderAvatar;
