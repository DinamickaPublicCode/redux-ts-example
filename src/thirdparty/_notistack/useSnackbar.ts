import {useContext} from "react";
import SnackbarContext from "./SnackbarContext";
import {ProviderContext} from ".";

const SnackbarHandler = (): ProviderContext => useContext(SnackbarContext);
export default SnackbarHandler;
