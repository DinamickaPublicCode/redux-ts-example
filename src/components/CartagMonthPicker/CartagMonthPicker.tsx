import React, {useState, useMemo} from "react";
import dayjs from "dayjs";
import Box from "@mui/material/Box";
import CartagMonthPickerBody from "./CartagMonthPickerBody";
import CartagMonthPickerPopper from "./CartagMonthPickerPopper";
import MonthPickerButton from "../MonthPickerButton";

export type CustomMonthPickerRangeType = "year" | "month";

interface CartagMonthPickerProps {
  date: Date;
  onDateChanged: (date: Date) => void;
  /**
   * This option bypasses the confirmation button and changes the incoming date types
   * immediately
   */
  applyImmediately?: boolean;
  noswitch?: boolean;
  rangeType: CustomMonthPickerRangeType;
  setRangeType: React.Dispatch<
    React.SetStateAction<CustomMonthPickerRangeType>
  >;
}

const CartagMonthPicker = ({
  date,
  onDateChanged,
  rangeType,
  setRangeType,
  applyImmediately,
  noswitch,
}: CartagMonthPickerProps) => {
  const dateMoment = useMemo(() => dayjs(date), [date]);
  const [open, setOpen] = useState(false);

  return (
    <MonthPickerButton
      date={date}
      onClickRight={() => onDateChanged(dateMoment.add(1, "month").toDate())}
      onClickLeft={() =>
        onDateChanged(dateMoment.subtract(1, "month").toDate())
      }
    >
      <CartagMonthPickerPopper
        open={open}
        setOpen={setOpen}
        monthPickerBody={
          <CartagMonthPickerBody
            noswitch={noswitch}
            immediate={applyImmediately}
            rangeType={rangeType}
            setRangeType={setRangeType}
            date={date}
            onChange={(date) => onDateChanged(date)}
            /**
             * Use onApplyButtonPressed callback to trigger the date picker closing
             *
             * e.g.
             * onApplyButtonPressed={() => setOpen(false)}
             */
            onApplyButtonPressed={() => setOpen(false)}
          />
        }
      >
        <Box
          sx={{
            cursor: "pointer",
            mr: 1,
          }}
        >
          {rangeType === "year"
            ? dateMoment.year()
            : dateMoment.format("MMMM") + " " + dateMoment.year()}
        </Box>
      </CartagMonthPickerPopper>
    </MonthPickerButton>
  );
};

export default CartagMonthPicker;
